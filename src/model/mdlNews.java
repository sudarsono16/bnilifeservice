package model;

public class mdlNews {
    public String news_id;
    public String news_type;
    public String news_title_id;
    public String news_content_id;
    public String news_title_en;
    public String news_content_en;
    public String news_title;
    public String news_content;
    public String news_image;
    public String news_link;
}
