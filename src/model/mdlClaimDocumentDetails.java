package model;

public class mdlClaimDocumentDetails {
    public String claim_document_id;
    public String username;
    public String claim_no;
    public String member_id;
    public String member_child_id;
    public String document_filename;
    public String document_directory;
    public String document_link;
    public Integer uploaded;
}
