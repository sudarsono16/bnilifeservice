package model;

public class mdlMenuMember {
    public String menu_member_id;
    public String member_type;
    public String menu_name;
    public String menu_name_english;
    public String menu_image;
    public String menu_order;
    public Integer show;
    public Integer enabled;
}
