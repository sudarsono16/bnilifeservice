package model;

import java.util.List;

import com.google.gson.annotations.SerializedName;

public class mdlMember {
    @SerializedName(value = "MEMBER_TYPE", alternate = "Member_type")
    public String member_type;
    @SerializedName(value = "MEMBER_DATA", alternate = "Member_data")
    public List<model.mdlMemberDetails> member_data;
}