package model;

import com.google.gson.annotations.SerializedName;

public class mdlTransaction {
    @SerializedName(value = "Invest_period")
    public String invest_period; // "Invest_period": "2019-09-01"
    @SerializedName(value = "Invest_last_period")
    public String invest_last_period; // "Invest_last_period": "212341452.0319"
    @SerializedName(value = "Contribution_premi")
    public String contribution_premi; // ""Contribution_premi": "0.0000"
    @SerializedName(value = "Net_invest_amount")
    public String net_invest_amount; // "Net_invest_amount": "896549.05506"
    @SerializedName(value = "Invest_total")
    public String invest_total; // "Invest_total": "213238001.0869"

    // field khusus OGS Link
    @SerializedName(value = "Fund_name")
    public String fund_name;// "Fund_name": "BNI LIFE DKMP-SKK PENSIUN"
    @SerializedName(value = "Withdrawal")
    public String withdrawal;// "Withdrawal": "-2426.8271"
    @SerializedName(value = "Insurance_fee")
    public String insurance_fee;// "Insurance_fee": "-88.8207"
    @SerializedName(value = "Invest_total_unit")
    public String invest_total_unit;// "Invest_total_unit": "1172187.0454"
    @SerializedName(value = "Invest_total_nab")
    public String invest_total_nab;// "Invest_total_nab": "1039.3327"
    @SerializedName(value = "Invest_total_rp")
    public String invest_total_rp;// "Invest_total_rp": "1218292326.8006"

    // field khusus OGS DHT dan DHT Link
    @SerializedName(value = "Contribution")
    public String contribution;
    @SerializedName(value = "Ujroh")
    public String ujroh;
    @SerializedName(value = "Tabarru")
    public String tabarru;
    @SerializedName(value = "Adjust_amt")
    public String adjust_amt;
    @SerializedName(value = "Gross_inv_amt")
    public String gross_inv_amt;
}