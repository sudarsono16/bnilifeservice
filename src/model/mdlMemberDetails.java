package model;

import java.util.List;

import com.google.gson.annotations.SerializedName;

public class mdlMemberDetails {
    @SerializedName(value = "Company_code")
    public String company_code; // "company_code": "0893"
    @SerializedName(value = "Company_name")
    public String company_name; // "company_name": "LEMBAGA PENGELOLA DANA BERGULIR KOPERASI DAN USAHA MIKRO, KECIL, DAN MENENGAH(LPDB - KUMKM)"
    @SerializedName(value = "Member_employee_no")
    public String member_employee_no; // "member_employee_no": "LPD - KES00077"
    @SerializedName(value = "Member_no")
    public String member_no; // "member_no": "LPD - KES00077"
    @SerializedName(value = "Member_name")
    public String member_name; // "member_name": "IRMA FATIMAH"
    @SerializedName(value = "Status")
    public String status; // "status": "AKTIF",
    @SerializedName(value = "Class")
    public String class_type; // "class": "IP150"
    @SerializedName(value = "Class_benefit_table")
    public String class_benefit_table; // "class_benefit_table": "PLANF"
    @SerializedName(value = "Provider")
    public String provider; // "provider": "BNI Life",
    @SerializedName(value = "Gender")
    public String gender; // "gender": "P",
    @SerializedName(value = "DOB")
    public String dob; // "dob": "8/17/1983 12:00:00 AM"
    @SerializedName(value = "Mobile_phone_number")
    public String mobile_phone_number; // "mobile_phone_number": ""
    @SerializedName(value = "Member_part")
    public String member_part; // "member_part": "Perempuan"
    @SerializedName(value = "Relation")
    public String relation; // "relation": "Karyawan"
    @SerializedName(value = "Policy_period")
    public String policy_period; // "policy_period": "0613"
    @SerializedName(value = "Policy_start_date")
    public String policy_start_date; // "policy_start_date": "6/1/2013 12:00:00 AM"
    @SerializedName(value = "Policy_end_date")
    public String policy_end_date; // "policy_end_date": "5/31/2014 12:00:00 AM"
    @SerializedName(value = "KodeBank")
    public String bank_code; // "bank_code": ""
    @SerializedName(value = "NamaBank")
    public String bank_name; // "bank_name": "BNI"
    @SerializedName(value = "BankCabang")
    public String bank_branch; // "bank_name": "BNI"
    @SerializedName(value = "NomorRekening")
    public String account_number; // "account_number": "0077989027"
    @SerializedName(value = "AtasNama")
    public String account_name; // "account_name": "IRMA FATIMAH"
    @SerializedName(value = "TglKeluar")
    public String exit_date; // "exit_date": "2018-10-31 00:00:00.0"
    @SerializedName(value = "TglKeluarPt")
    public String company_exit_date; // "company_exit_date": "2018-10-31 00:00:00.0"
    @SerializedName(value = "Benefit")
    public List<model.mdlBenefit> benefit;

    // model khusus OGS
    @SerializedName(value = "Policy_no")
    public String policy_no; // "Policy_no": "1908/PK-SP/0616 "
    @SerializedName(value = "Sum_insured")
    public String sum_insured; // "Sum_insured": "12500000.0000"
    @SerializedName(value = "Join")
    public String join;// "Join": "2017-04-01"
    @SerializedName(value = "Pension")
    public String pension;// "Pension": "2050-06-19"
    @SerializedName(value = "Currency")
    public String currency;// "Currency": "Rp"
    @SerializedName(value = "Transaction")
    public List<model.mdlTransaction> transaction;
    @SerializedName(value = "NAB_access")
    public String nab_access;// "NAB_access": "0"
    @SerializedName(value = "FFS_access")
    public String ffs_access;// "FFS_access": "0"
}
