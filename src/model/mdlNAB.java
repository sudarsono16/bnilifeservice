package model;

import java.util.List;

import com.google.gson.annotations.SerializedName;

public class mdlNAB {
    @SerializedName(value = "MEMBER_TYPE", alternate = "Member_type")
    public String member_type;
    @SerializedName(value = "COMPANY_DATA", alternate = "Company_data")
    public List<model.mdlNABDetails> company_data;
}
