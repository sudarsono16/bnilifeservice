package model;

public class mdlProvider {
    public String provider_id;
    public String tipe_provider;
    public String tipe_rekanan;
    public String nama_provider;
    public String alamat;
    public String no_telp1;
    public String no_telp2;
    public String no_telp3;
    public String no_telp4;
    public String kota;
    public String provinsi;
    public String latitude;
    public String longitude;
    public model.mdlProviderPerawatan perawatan;
    public model.mdlProviderPersalinan persalinan;
    public model.mdlProviderPersalinanSectio persalinan_sectio;
    public model.mdlProviderRawatJalan rawat_jalan;
    public model.mdlProviderRawatInap rawat_inap;
    public String distance;
}