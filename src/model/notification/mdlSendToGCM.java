package model.notification;

public class mdlSendToGCM {
	public String FirebaseAPIKey;
	public String FirebaseTitle;
	public String FirebaseMessage;
	public String FirebaseNotificationID;
	public String CustomID;
	public String ClaimNo;
	public String ClaimStatus;
	public String ClaimMemberName;
	public String ClaimDate;
	public java.util.List<String> FirebaseUserTokenList;
}
