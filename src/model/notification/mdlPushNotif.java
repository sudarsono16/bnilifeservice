package model.notification;

import java.util.List;

public class mdlPushNotif {
	
	public String notification;
	public List<String> registration_ids;
	public String priority;
	public Boolean delay_while_idle;
	public Boolean content_available;
	public Object data;
	
	public mdlPushNotif(){
		notification = null;
		priority = "high";
		delay_while_idle = false;
		content_available = true;	
	}
}
