package model;

import java.util.List;

public class mdlProductCategory {
    public String product_category_id;
    public String product_category_name;
    public String product_category_name_english;
    public String product_category_icon;
    public String product_category_order;
    public List<model.mdlProduct> product_list;
}
