package model;

public class mdlClaimDocumentType {
    public String claim_type_code;
    public String claim_type_name;
    public String claim_document_type_id;
    public String claim_document_type_name;
    public String claim_document_type_details;
    public String claim_document_type_information1;
    public String claim_document_type_information2;
    public String mandatory;
    public Integer claim_document_type_count;
}