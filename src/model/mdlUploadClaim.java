package model;

import java.util.List;

public class mdlUploadClaim {
    public String username;
    public String member_id;
    public String member_child_id;
    public String member_type;
    public String claim_no;
    public List<model.mdlFilenameList> filename_list;
    public String language;
}
