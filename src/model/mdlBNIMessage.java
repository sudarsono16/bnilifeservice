package model;

import com.google.gson.annotations.SerializedName;

public class mdlBNIMessage {
    @SerializedName(value="CODE")
    public String code;
    @SerializedName(value="MESSAGE")
    public String message;
}
