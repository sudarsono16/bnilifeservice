package model;

import com.google.gson.annotations.SerializedName;

public class mdlBenefitType {
    @SerializedName(value="Type_code")
    public String type_code;
    @SerializedName(value="Type")
    public String type;
}
