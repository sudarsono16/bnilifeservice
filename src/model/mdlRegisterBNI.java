package model;

import com.google.gson.annotations.SerializedName;

public class mdlRegisterBNI {
    @SerializedName(value="Member_employee_no")
    public String member_employee_no;
    @SerializedName(value="Mobile_phone_number")
    public String mobile_phone_number;
    @SerializedName(value="Email")
    public String email;
    @SerializedName(value="DOB")
    public String dob;
    @SerializedName(value="Member_type")
    public String member_type;
}
