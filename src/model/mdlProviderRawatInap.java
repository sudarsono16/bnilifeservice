package model;

public class mdlProviderRawatInap {
    public Double rawat_inap_presiden;
    public Double rawat_inap_svip;
    public Double rawat_inap_vvip;
    public Double rawat_inap_vip;
    public Double rawat_inap_utama;
    public Double rawat_inap_kelas1;
    public Double rawat_inap_kelas2;
    public Double rawat_inap_kelas3;
}
