package model;

import com.google.gson.annotations.SerializedName;

public class mdlMemberList {
    @SerializedName(value = "Member_employee_no")
    public String member_id; // "member_employee_no": "LPD - KES00077"
    public model.mdlCardLink links;
    public model.mdlMember member; 
}
