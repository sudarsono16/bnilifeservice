package model;

import com.google.gson.annotations.SerializedName;

public class mdlUpdateClaimData {
    @SerializedName(value="KodeStatus")
    public String claim_status_code;
    @SerializedName(value="NamaStatus")
    public String claim_status_name;
    @SerializedName(value="NomorSurat")
    public String letter_number;
    @SerializedName(value="TglApprove")
    public String approve_date;
    @SerializedName(value="NomorPesertaInti")
    public String member_id;
    @SerializedName(value="NamaPesertaInti")
    public String member_name;
    @SerializedName(value="NomorSuratMasuk")
    public String claim_no;
    @SerializedName(value="TanggalKlaim")
    public String claim_date;
    @SerializedName(value="TitleJK")
    public String title;
    @SerializedName(value="KetStatus")
    public String status_info;
    @SerializedName(value="NomorPeserta")
    public String member_child_id;
    @SerializedName(value="NamaPeserta")
    public String member_child_name;
    @SerializedName(value="Amount")
    public String amount;
    @SerializedName(value="KetStatusDetail")
    public String status_info_detail;
    @SerializedName(value="KetTambahan")
    public String additional_info;
    @SerializedName(value="BnilNamaManagerClaim")
    public String bnil_claim_manager_name;
    @SerializedName(value="BnilJabatan")
    public String bnil_position;
}
