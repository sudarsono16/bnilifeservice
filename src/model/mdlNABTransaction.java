package model;

import com.google.gson.annotations.SerializedName;

public class mdlNABTransaction {
    @SerializedName(value = "NAB_date")
    public String nab_date; // "NAB_date": "2019-10-22 00:00:00.0"
    @SerializedName(value = "NAB_value")
    public String nab_value; // "NAB_value": "1180.5051"
}
