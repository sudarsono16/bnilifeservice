package model;

import com.google.gson.annotations.SerializedName;

public class mdlEmail {
    @SerializedName(value="EmailTo")
    public String email_to;
    @SerializedName(value="EmailSubject")
    public String email_subject;
    @SerializedName(value="EmailMessage")
    public String email_message;
}
