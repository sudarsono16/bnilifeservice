package model;

public class mdlImage {
    public String claim_image_id;
    public String claim_no;
    public String claim_document_type_id;
    public String username;
    public String image_original_name;
    public String image_uploaded_name;
    public String image_directory;
    public String image_link;
    public Integer uploaded;
}
