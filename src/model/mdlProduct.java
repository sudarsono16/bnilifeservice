package model;

public class mdlProduct {
    public String product_id;
    public String product_category_id;
    public String product_category_name;
    public String product_category_name_english;
    public String product_title;
    public String product_title_english;
    public String product_desc;
    public String product_desc_english;
    public String product_content;
    public String product_content_english;
    public String product_image;
    public String product_link;
}
