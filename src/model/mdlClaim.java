package model;

import java.util.List;

import com.google.gson.annotations.SerializedName;

public class mdlClaim {
    @SerializedName("Username")
    public String username;
    @SerializedName("Member_type")
    public String member_type;
    @SerializedName("Company_code")
    public String company_code;
    @SerializedName("Member_Employee_no")
    public String member_id;
    @SerializedName("Member_no")
    public String member_child_id;
    @SerializedName("Member_name")
    public String member_name;
    @SerializedName("Claim_no")
    public String claim_no;
    @SerializedName("Claim_status")
    public String claim_status;
    @SerializedName("Type_code")
    public String claim_type;
    @SerializedName("Type_name")
    public String claim_type_name;
    @SerializedName("Claim_date")
    public String claim_date;
    @SerializedName("Startdate")
    public String start_date;
    @SerializedName("Enddate")
    public String end_date;
    @SerializedName("Amount")
    public String amount;
    @SerializedName("Policy_period")
    public String policy_period;
    @SerializedName("Mobile_phone_number")
    public String phone_number;
    @SerializedName("Bank")
    public String bank_name;
    @SerializedName("Account_no")
    public String account_no;
    @SerializedName("Account_name")
    public String account_name;
    @SerializedName("Language")
    public String language;
    @SerializedName("Document_type_list")
    public List<model.mdlClaimDocumentType> document_type_list;
    @SerializedName("Image_list")
    public List<model.mdlImage> image_list;
    @SerializedName("Pdf_link")
    public String pdf_link;
    @SerializedName("Claim_history")
    public List<model.mdlClaimHistory> claim_history;
    @SerializedName ("Update_document_link")
    public String update_document_link;
}
