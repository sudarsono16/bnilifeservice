package model;

import com.google.gson.annotations.SerializedName;

public class mdlBenefit {
    @SerializedName(value="Type_code")
    public String type_code; //"type_code": "RI"
    @SerializedName(value="Type")
    public String type; //"type": "Rawat Inap"
    @SerializedName(value="Type_sub_code")
    public String type_sub_code; //"type_sub_code": "IS"
    @SerializedName(value="Type_sub")
    public String type_sub; //"type_sub": "Operasi tanpa RI"
    @SerializedName(value="Platform")
    public String platform; //"platform": "1000000"
    @SerializedName(value="Trans_amount")
    public String trans_amount; //"trans_amount": "0"
    @SerializedName(value="Paid_amount")
    public String paid_amount; //"paid_amount": "0"
    @SerializedName(value="Reject_amount")
    public String reject_amount; //"reject_amount": "0"
    @SerializedName(value="Platform_less")
    public String platform_less; //"platform_less": "1000000"
}
