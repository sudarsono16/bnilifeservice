package model;

import java.util.List;

public class mdlUser {
    public String username;
    public String password;
    public String member_id; //nomor polis
    public String member_type;
    public List<model.mdlMemberList> member_list;
    public String email; // email uer
    public String phone_number; // nomor handphone user
    public String date_of_birth; // tanggal lahir pemegang polis
    public String is_active;
    public String device_id;
    public String firebase_token;
    public String os_type;
    public String language;
}
