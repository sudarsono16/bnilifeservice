package model;

import com.google.gson.annotations.SerializedName;

public class mdlOTP {
    @SerializedName(value="Mobile_phone_number")
    public String phone_number;
    @SerializedName(value="OTP")
    public String otp;
    @SerializedName(value="Message")
    public String message;
    @SerializedName(value="Username")
    public String username;
    @SerializedName(value="Member_ID")
    public String member_id;
}
