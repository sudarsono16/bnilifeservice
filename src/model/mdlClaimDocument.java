package model;

import java.util.List;

public class mdlClaimDocument {
    public String username;
    public String member_id;
    public String member_child_id;
    public String claim_no;
    public List<model.mdlImage> image_link;
    public String pdf_link;
}