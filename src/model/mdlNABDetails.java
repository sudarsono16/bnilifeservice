package model;

import java.util.List;

import com.google.gson.annotations.SerializedName;

public class mdlNABDetails {
    @SerializedName(value = "Policy_no")
    public String policy_no; // "Policy_no": "2103/PK-SP/1016"
    @SerializedName(value = "Company_code")
    public String company_code; // "Company_code": "052"
    @SerializedName(value = "Company_code2")
    public String company_code2; // "Company_code2": "001"
    @SerializedName(value = "Company_name")
    public String company_name; // "Company_name": "YAYASAN KESEJAHTERAAN PEGAWAI BNI - PENSIUNAN"
    @SerializedName(value = "Member_employee_no")
    public String member_employee_no; // "Member_employee_no": "P012998"
    @SerializedName(value = "Fund_name")
    public String fund_name; // "Fund_name": "BNI DKMP PENSION FUND"
    @SerializedName(value = "Transaction")
    public List<model.mdlNABTransaction> transaction;
}
