package model;

public class mdlBroadcastMessage {
    public String broadcast_id;
    public String broadcast_type; // ALL, OGH, OGS, PERSONAL
    public String username;
    public String broadcast_title;
    public String broadcast_content;
}
