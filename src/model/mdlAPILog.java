package model;

public class mdlAPILog {
    public String api_log_id;
    public String api_source;
    public String api_name;
    public String api_status;
    public String function_name;
    public String api_input;
    public String api_output;
    public String exception;
}
