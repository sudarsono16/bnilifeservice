package adapter;
import java.util.ArrayList;
import java.util.List;
import javax.sql.rowset.CachedRowSet;

import org.apache.log4j.Logger;

import adapter.Base64Adapter;
import database.QueryExecuteAdapter;
import model.Globals;
import model.mdlQueryExecute;

public class ClientIdAdapter {
    final static Logger logger = Logger.getLogger(ClientIdAdapter.class);

    public static List<String> CheckClientID(String clientID, String clientSecret) {
	List<String> appData = new ArrayList<String>();
	List<mdlQueryExecute> listParam = new ArrayList<mdlQueryExecute>();
	CachedRowSet jrs = null;
	String sql = "";
	String appName = "";
	String apiKey = "";
	appData.add(appName);
	appData.add(apiKey);
	try {
	    sql = "SELECT TOP 1 AppName, APIKey FROM clientid WHERE ClientID = ? AND ClientSecret = ? AND IsActive = 1";
	    listParam.add(QueryExecuteAdapter.QueryParam("string", clientID));
	    listParam.add(QueryExecuteAdapter.QueryParam("string", clientSecret));
	    jrs = QueryExecuteAdapter.QueryExecute(sql, listParam, "sp_GetClientID");
	    while (jrs.next()) {
		appName = jrs.getString(1);
		apiKey = jrs.getString(2);
		appData.set(0, appName);
		appData.set(1, apiKey);
	    }
	} catch (Exception ex) {
	    LogAdapter.InsertLog(ex.toString(), "checkClientID", sql, appName);
	}

	return appData;
    }
    
    public static String GetClientID(String UserName, String AppName) {
	String ClientID = "";
	List<mdlQueryExecute> listParam = new ArrayList<mdlQueryExecute>();
	CachedRowSet jrs = null;
	String sql = "";

	try {
	    sql = "{call sp_GetClientID(?,?)}";
	    listParam.add(QueryExecuteAdapter.QueryParam("string", UserName));
	    listParam.add(QueryExecuteAdapter.QueryParam("string", AppName));
	    jrs = QueryExecuteAdapter.QueryExecute(sql, listParam, "sp_GetClientID");
	    while (jrs.next()) {
		String originalClientID = jrs.getString("UserName") + "&&" + jrs.getString("AppName");
		ClientID = EncryptAdapter.encrypt(originalClientID, Globals.keyAPI);
	    }
	} catch (Exception ex) {
	    LogAdapter.InsertLog(ex.toString(), "GetClientID", sql, UserName);
	}
	return ClientID;
    }
    
    public static model.mdlKey CreateClientKeys(String appName) {
	model.mdlKey mdlKey = new model.mdlKey();
	String clientID, clientSecret, apiKey, clientID64, clientSecret64, apiKey64 = "";
	try {
	    clientID = EncryptAdapter.encrypt(appName, "inventlogistic");
	    clientSecret = EncryptAdapter.encrypt(clientID, appName);
	    apiKey = EncryptAdapter.encrypt(clientSecret, clientID);

	    clientID64 = Base64Adapter.EncryptBase64(clientID);
	    clientSecret64 = Base64Adapter.EncryptBase64(clientSecret);
	    apiKey64 = Base64Adapter.EncryptBase64(apiKey);

	    mdlKey.AppName = appName;
	    mdlKey.ClientID = clientID64;
	    mdlKey.ClientSecret = clientSecret64;
	    mdlKey.APIKey = apiKey64;
	} catch (Exception ex) {
	    logger.info("FAILED. API : create-keys, method : CreateClientKeys, Exception : " + ex.toString());
	}
	return mdlKey;
    }

    public static model.mdlKey DecryptClientKeys(model.mdlKey mdlKey) {
	model.mdlKey mdlKeyDecrypted = new model.mdlKey();
	String appName, clientID, clientSecret, apiKey, clientIDDecrypted, clientSecretDecrypted = "";
	try {
	    clientID = Base64Adapter.DecryptBase64(mdlKey.ClientID);
	    clientSecret = Base64Adapter.DecryptBase64(mdlKey.ClientSecret);
	    apiKey = Base64Adapter.DecryptBase64(mdlKey.APIKey);

	    appName = EncryptAdapter.decrypt(clientID, "inventlogistic");
	    clientIDDecrypted = EncryptAdapter.decrypt(clientSecret, appName);
	    clientSecretDecrypted = EncryptAdapter.decrypt(apiKey, clientID);

	    mdlKeyDecrypted.AppName = appName;
	    mdlKeyDecrypted.ClientID = clientIDDecrypted;
	    mdlKeyDecrypted.ClientSecret = clientSecretDecrypted;
	    mdlKeyDecrypted.APIKey = "";
	} catch (Exception ex) {
	    logger.info("FAILED. API : decrypt-keys, method : DecryptClientKeys, Exception : " + ex.toString());
	}
	return mdlKeyDecrypted;
    }
}
