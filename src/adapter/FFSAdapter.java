package adapter;

import java.util.ArrayList;
import java.util.List;

import javax.sql.rowset.CachedRowSet;

import org.apache.log4j.Logger;

import com.google.gson.Gson;

import database.QueryExecuteAdapter;
import model.mdlQueryExecute;

public class FFSAdapter {
    final static Logger logger = Logger.getLogger(FFSAdapter.class);
    static Gson gson = new Gson();
    
    public static model.mdlFFS GetFFS(String companyNo) {
	String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();
	model.mdlFFS mdlFFS = new model.mdlFFS();
	List<mdlQueryExecute> listParam = new ArrayList<mdlQueryExecute>();
	CachedRowSet jrs = null;
	String sql = "";
	try {
	    sql = "SELECT TOP 1 * FROM ms_ffs WHERE ffs_company_code = ? AND ffs_status = 1 ORDER BY ffs_date DESC";
	    listParam.add(QueryExecuteAdapter.QueryParam("string", companyNo));
	    jrs = QueryExecuteAdapter.QueryExecute(sql, listParam, functionName);
	    while (jrs.next()) {
		mdlFFS.ffs_id = jrs.getString("ffs_id");
		mdlFFS.ffs_company_code = jrs.getString("ffs_company_code");
		mdlFFS.ffs_company_name = jrs.getString("ffs_company_name");
		mdlFFS.ffs_date= jrs.getString("ffs_date");
		mdlFFS.ffs_pdf = jrs.getString("ffs_pdf");
		mdlFFS.ffs_link = jrs.getString("ffs_link");
		mdlFFS.ffs_status = jrs.getString("ffs_status");
	    }
	} catch (Exception ex) {
	    logger.error("FAILED. Function : " + functionName + ", companyNo : " + gson.toJson(companyNo) + ", Exception:" + ex.toString(), ex);
	}
	return mdlFFS;
    }
}
