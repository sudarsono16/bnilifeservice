package adapter;

import java.util.ArrayList;
import java.util.List;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.sql.rowset.CachedRowSet;

import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.WebResource.Builder;

import database.QueryExecuteAdapter;
import model.mdlQueryExecute;
import model.notification.*;

public class NotificationAdapter {
    final static Logger logger = Logger.getLogger(NotificationAdapter.class);
    static Gson gson = new Gson();
    static Client client = Client.create();
    
    public static boolean PushBroadcastNotificationToDevice(model.mdlBroadcastMessage mdlBroadcastMessage, List<String> tokenList) {
	boolean success = false;
	try {
	    Context web_context = (Context) new InitialContext().lookup("java:comp/env");
	    String apiKey = (String) web_context.lookup("firebase_api_key");

	    mdlSendToGCM mdlSendToGCM = new mdlSendToGCM();
	    mdlSendToGCM.FirebaseAPIKey = apiKey;
	    mdlSendToGCM.FirebaseTitle = mdlBroadcastMessage.broadcast_title == null ? "" : mdlBroadcastMessage.broadcast_title;
	    mdlSendToGCM.FirebaseMessage = mdlBroadcastMessage.broadcast_content;
	    mdlSendToGCM.FirebaseNotificationID = "";
	    mdlSendToGCM.ClaimNo = "";
	    mdlSendToGCM.ClaimStatus = "";
	    mdlSendToGCM.ClaimMemberName = "";
	    mdlSendToGCM.ClaimDate = "";

	    mdlSendToGCM.FirebaseUserTokenList = tokenList;

	    success = SendToGCMWrapper(mdlSendToGCM);
	} catch (Exception ex) {
	    String jsonIn = gson.toJson(mdlBroadcastMessage);
	    logger.error("FAILED = method : PushNotificationToDevice, mdlBroadcastMessage : " + jsonIn + ", Exception : " + ex.toString(), ex);
	}
	return success;
    }

    public static boolean PushNotificationToDevice(model.mdlUserMessage mdlUserMessage, String language) {
	boolean success = false;
	try {
	    Context web_context = (Context) new InitialContext().lookup("java:comp/env");
	    String apiKey = (String) web_context.lookup("firebase_api_key");
	    List<String> usernameTokenList = GetDeviceTokenByUsername(mdlUserMessage.username);

	    mdlSendToGCM mdlSendToGCM = new mdlSendToGCM();
	    mdlSendToGCM.FirebaseAPIKey = apiKey;
	    mdlSendToGCM.FirebaseTitle = mdlUserMessage.message_title == null ? "" : mdlUserMessage.message_title;
	    mdlSendToGCM.FirebaseMessage = mdlUserMessage.message_content_indonesian;
	    if (language.equalsIgnoreCase("EN")) {
		mdlSendToGCM.FirebaseMessage = mdlUserMessage.message_content_english;
	    }
	    mdlSendToGCM.FirebaseNotificationID = mdlUserMessage.message_value;
	    mdlSendToGCM.ClaimNo = mdlUserMessage.message_value;
	    mdlSendToGCM.ClaimStatus = mdlUserMessage.message_value2;
	    mdlSendToGCM.ClaimMemberName = mdlUserMessage.message_value3 == null ? "" : mdlUserMessage.message_value3;
	    mdlSendToGCM.ClaimDate = mdlUserMessage.message_value4 == null ? "" : mdlUserMessage.message_value4;

	    mdlSendToGCM.FirebaseUserTokenList = usernameTokenList;

	    success = SendToGCMWrapper(mdlSendToGCM);
	} catch (Exception ex) {
	    String jsonIn = gson.toJson(mdlUserMessage);
	    logger.error("FAILED = method : PushNotificationToDevice, mdlUserMessage : " + jsonIn + ", Exception : " + ex.toString(), ex);
	}
	return success;
    }

    public static boolean SendToGCMWrapper(mdlSendToGCM mdlSendToGCM) {
	boolean success = false;
	try {
	    mdlPush mdlPush = new mdlPush();
	    mdlPush.registration_ids = mdlSendToGCM.FirebaseUserTokenList;
	    mdlPush.priority = "high";
	    mdlPush.delay_while_idle = true;
	    mdlPush.content_available = true;

	    mdlData mdlData = new mdlData();
	    mdlData.title = mdlSendToGCM.FirebaseTitle;
	    mdlData.msg = mdlSendToGCM.FirebaseMessage;
	    mdlData.notificationid = mdlSendToGCM.FirebaseNotificationID;
	    mdlData.claimno = mdlSendToGCM.ClaimNo;
	    mdlData.claimstatus = mdlSendToGCM.ClaimStatus;
	    mdlData.claimmembername = mdlSendToGCM.ClaimMemberName;
	    mdlData.claimdate = mdlSendToGCM.ClaimDate;
	    mdlPush.data = mdlData;

	    mdlNotification mdlNotification = new mdlNotification();
	    mdlNotification.title = mdlSendToGCM.FirebaseTitle;
	    mdlNotification.body = mdlSendToGCM.FirebaseMessage;
	    mdlPush.notification = mdlNotification;

	    success = PostToGCMGeneral(mdlSendToGCM.FirebaseAPIKey, mdlPush);
	} catch (Exception ex) {
	    logger.error("FAILED. Function : SendToGCMWrapper, Exception:" + ex.toString(), ex);
	}
	return success;
    }

    public static boolean PostToGCMGeneral(String apiKey, mdlPush content) {
	long startTime = System.currentTimeMillis();
	long stopTime = 0;
	long elapsedTime = 0;
	boolean success = false;
	String jsonIn = "";
	String jsonOut = "";
	String urlString = "https://fcm.googleapis.com/fcm/send";
	try {
	    jsonIn = gson.toJson(content);
	    WebResource webResource = client.resource(urlString);
	    ClientResponse response = null;
	    Builder builder = webResource.type("application/json").header("Content-Type", "application/json").header("Authorization", "key=" + apiKey);
	    response = builder.post(ClientResponse.class, jsonIn);
	    jsonOut = response.getEntity(String.class);
	    stopTime = System.currentTimeMillis();
	    elapsedTime = stopTime - startTime;
	    String responseStatus = Integer.toString(response.getStatus());
	    success = true;
	    logger.info("SUCCESS = Response Time : " + elapsedTime + ", Response Code : " + responseStatus + ", method : PostToGCMGeneral, url : " + urlString + ", Key :" + apiKey + ", jsonIn : " + jsonIn + ", jsonOut : " + jsonOut);
	} catch (Exception ex) {
	    logger.error("FAILED = method : PostToGCMGeneral, url : " + urlString + ", APIKey :" + apiKey + ", jsonIn : " + jsonIn + ", Exception : " + ex.toString(), ex);
	}
	return success;
    }

    public static List<String> GetDeviceTokenByUsername(String username) {
	List<mdlQueryExecute> listParam = new ArrayList<mdlQueryExecute>();
	CachedRowSet jrs = null;
	List<String> tokenList = new ArrayList<String>();
	String sql = "";
	try {
	    sql = "SELECT firebase_token FROM ms_user_device WHERE username = ? ";
	    listParam.add(QueryExecuteAdapter.QueryParam("string", username));
	    jrs = QueryExecuteAdapter.QueryExecute(sql, listParam, "GetDeviceTokenByUsername");
	    while (jrs.next()) {
		tokenList.add(jrs.getString("firebase_token"));
	    }
	} catch (Exception ex) {
	    logger.error("FAILED = method : GetDeviceTokenByUsername, username : " + username + ", Exception : " + ex.toString(), ex);
	}
	return tokenList;
    }
}
