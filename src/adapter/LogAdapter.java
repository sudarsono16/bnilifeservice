package adapter;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import database.QueryExecuteAdapter;
import model.mdlQueryExecute;

/**
 * Documentation
 * 
 */
public class LogAdapter {
    final static Logger logger = Logger.getLogger(LogAdapter.class);

    public static void InsertLog(String log_exception, String log_function, String log_query, String log_created_by) {
	String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();
	List<mdlQueryExecute> listParam = new ArrayList<mdlQueryExecute>();
	String sql = "";
	try {
	    log_created_by = log_created_by == null ? "" : log_created_by;
	    sql = "INSERT INTO log_exception (log_exception, log_function, log_query, create_by) VALUES " + "(?, ?, ?, ?) ";
	    listParam.add(QueryExecuteAdapter.QueryParam("string", log_exception));
	    listParam.add(QueryExecuteAdapter.QueryParam("string", log_function));
	    listParam.add(QueryExecuteAdapter.QueryParam("string", log_query));
	    listParam.add(QueryExecuteAdapter.QueryParam("string", log_created_by));
	    QueryExecuteAdapter.QueryManipulate(sql, listParam, functionName);
	} catch (Exception ex) {
	    logger.error("FAILED. Function : " + functionName + ", Exception : " + ex.toString(), ex);
	}
    }

    public static void InsertAPILog(String apiSource, String apiStatus, String apiName, String apiFunction, String apiInput, String apiOutput, String exception) {
	String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();
	List<mdlQueryExecute> listParam = new ArrayList<mdlQueryExecute>();
	String sql = "";
	try {
	    sql = "INSERT INTO api_log (api_source, api_status, api_name, function_name, api_input, api_output, exception) VALUES "
		    + "(?, ?, ?, ?, ?, ?, ?) ";
	    listParam.add(QueryExecuteAdapter.QueryParam("string", apiSource));
	    listParam.add(QueryExecuteAdapter.QueryParam("string", apiStatus));
	    listParam.add(QueryExecuteAdapter.QueryParam("string", apiName));
	    listParam.add(QueryExecuteAdapter.QueryParam("string", apiFunction));
	    listParam.add(QueryExecuteAdapter.QueryParam("string", apiInput));
	    listParam.add(QueryExecuteAdapter.QueryParam("string", apiOutput));
	    listParam.add(QueryExecuteAdapter.QueryParam("string", exception));
	    QueryExecuteAdapter.QueryManipulate(sql, listParam, functionName);
	} catch (Exception ex) {
	    logger.error("FAILED. Function : " + functionName + ", Exception : " + ex.toString(), ex);
	}
    }

    public static void InsertAPILog(String apiSource, String apiStatus, String apiName, String apiFunction, String apiInput, String apiOutput) {
	String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();
	List<mdlQueryExecute> listParam = new ArrayList<mdlQueryExecute>();
	String sql = "";
	try {
	    sql = "INSERT INTO api_log (api_source, api_status, api_name, function_name, api_input, api_output) VALUES "
		    + "(?, ?, ?, ?, ?, ?) ";
	    listParam.add(QueryExecuteAdapter.QueryParam("string", apiSource));
	    listParam.add(QueryExecuteAdapter.QueryParam("string", apiStatus));
	    listParam.add(QueryExecuteAdapter.QueryParam("string", apiName));
	    listParam.add(QueryExecuteAdapter.QueryParam("string", apiFunction));
	    listParam.add(QueryExecuteAdapter.QueryParam("string", apiInput));
	    listParam.add(QueryExecuteAdapter.QueryParam("string", apiOutput));
	    QueryExecuteAdapter.QueryManipulate(sql, listParam, functionName);
	} catch (Exception ex) {
	    logger.error("FAILED. Function : " + functionName + ", Exception : " + ex.toString(), ex);
	}
    }

}
