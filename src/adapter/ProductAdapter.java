package adapter;

import java.util.ArrayList;
import java.util.List;

import javax.sql.rowset.CachedRowSet;

import org.apache.log4j.Logger;

import database.QueryExecuteAdapter;
import model.mdlQueryExecute;

public class ProductAdapter {
    final static Logger logger = Logger.getLogger(ProductAdapter.class);
    static String source = "Web Server";
    
    public static List<model.mdlProductCategory> GetProductCategoryWithProductList() {
	String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();
	List<model.mdlProductCategory> productCategoryList = new ArrayList<model.mdlProductCategory>();
	CachedRowSet jrs = null;

	try {	    
	    String sql = "SELECT product_category_id, product_category_name, product_category_name_english, product_category_icon, product_category_order FROM ms_product_category "
	    		+ "ORDER BY product_category_order";

	    jrs = QueryExecuteAdapter.QueryExecute(sql, functionName);
	    while (jrs.next()) {
		model.mdlProductCategory mdlProductCategory = new model.mdlProductCategory();
		mdlProductCategory.product_category_id = jrs.getString("product_category_id");
		mdlProductCategory.product_category_name = jrs.getString("product_category_name");
		mdlProductCategory.product_category_name_english = jrs.getString("product_category_name_english");
		mdlProductCategory.product_category_icon = jrs.getString("product_category_icon");
		mdlProductCategory.product_category_order = jrs.getString("product_category_order");
		mdlProductCategory.product_list = GetProductByCategoryList(mdlProductCategory.product_category_id);
		productCategoryList.add(mdlProductCategory);
	    }
	} catch (Exception ex) {
	    logger.error("FAILED. Function : " + functionName + ", Exception:" + ex.toString(), ex);
	}
	return productCategoryList;
    }
    
    public static List<model.mdlProduct> GetProductByCategoryList(String product_category_id) {
	String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();
	List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
	List<model.mdlProduct> productList = new ArrayList<model.mdlProduct>();
	CachedRowSet jrs = null;
	String sql = "";
	try {	    
	    sql = "SELECT prod.product_id, cat.product_category_name, cat.product_category_name_english, prod.product_title, prod.product_title_english, prod.product_desc, prod.product_desc_english, "
	    		+ "prod.product_content , prod.product_content_english , prod.product_image, prod.product_link FROM ms_product prod "
	    		+ "LEFT JOIN ms_product_category cat on prod.product_category = cat.product_category_id "
	    		+ "WHERE cat.product_category_id = ? ORDER BY cat.product_category_order, prod.product_title";
	    listParam.add(database.QueryExecuteAdapter.QueryParam("string", product_category_id));
	   jrs = QueryExecuteAdapter.QueryExecute(sql, listParam, functionName);
	    while (jrs.next()) {
		model.mdlProduct mdlProduct = new model.mdlProduct();
		mdlProduct.product_id = jrs.getString("product_id");
		mdlProduct.product_category_name = jrs.getString("product_category_name");
		mdlProduct.product_category_name_english = jrs.getString("product_category_name_english");
		mdlProduct.product_title = jrs.getString("product_title");
		mdlProduct.product_title_english = jrs.getString("product_title_english");
		mdlProduct.product_desc = jrs.getString("product_desc");
		mdlProduct.product_desc_english = jrs.getString("product_desc_english");
		mdlProduct.product_content = jrs.getString("product_content");
		mdlProduct.product_content_english = jrs.getString("product_content_english");
		mdlProduct.product_image = jrs.getString("product_image");
		mdlProduct.product_link = jrs.getString("product_link");
		productList.add(mdlProduct);
	    }
	} catch (Exception ex) {
	    LogAdapter.InsertLog(ex.toString(), functionName, sql, source);
	    logger.error("FAILED. Function : " + functionName + ", Exception:" + ex.toString(), ex);
	}
	return productList;
    }
    
    public static List<model.mdlProductCategory> GetProductCategoryList(String id, String pageNumber, String pageSize, String search) {
	String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();
	List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
	List<model.mdlProductCategory> productCategoryList = new ArrayList<model.mdlProductCategory>();
	CachedRowSet jrs = null;
	String sql = "";
	String search_part = "";
	
	try {
	    if (search != null) {
		search_part = "AND product_category_name LIKE ? ";
	    }
	    
	    sql = "SELECT product_category_id, product_category_name, product_category_name_english, product_category_icon, product_category_order FROM ms_product_category "
		    + "WHERE product_category_id = COALESCE(?, product_category_id) " + search_part + "ORDER BY product_category_order ASC "
		    + "OFFSET ? ROWS FETCH NEXT ? ROWS ONLY;";
	    listParam.add(id != null ? QueryExecuteAdapter.QueryParam("int", Integer.parseInt(id)) : QueryExecuteAdapter.QueryParam("null", java.sql.Types.NULL));
	    
	    if (search != null) {
		String searchString = "%" + search.trim().replace(" ", "%") + "%";
		listParam.add(QueryExecuteAdapter.QueryParam("string", searchString));
	    }
	    
	    listParam.add(QueryExecuteAdapter.QueryParam("int", ((Integer.parseInt(pageNumber) - 1) * Integer.parseInt(pageSize))));
	    listParam.add(QueryExecuteAdapter.QueryParam("int", Integer.parseInt(pageSize)));
	    
	    jrs = QueryExecuteAdapter.QueryExecute(sql, listParam, functionName);
	    while (jrs.next()) {
		model.mdlProductCategory mdlProductCategory = new model.mdlProductCategory();
		mdlProductCategory.product_category_id = jrs.getString("product_category_id");
		mdlProductCategory.product_category_name = jrs.getString("product_category_name");
		mdlProductCategory.product_category_name_english = jrs.getString("product_category_name_english");
		mdlProductCategory.product_category_icon = jrs.getString("product_category_icon");
		mdlProductCategory.product_category_order = jrs.getString("product_category_order");
		productCategoryList.add(mdlProductCategory);
	    }
	} catch (Exception ex) {
	    LogAdapter.InsertLog(ex.toString(), functionName, sql, source);
	    logger.error("FAILED. Function : " + functionName + ", Exception:" + ex.toString(), ex);
	}
	return productCategoryList;
    }
    
    public static int GetProductCategoryTotalList(String id, String search) {
	String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();
	List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
	CachedRowSet crs = null;
	String sql = "";
	int return_value = 0;
	String search_part = "";
	try {
	    if (search != null) {
		search_part = "AND product_category_name LIKE ? ";
	    }
	    sql = "SELECT COUNT(1) AS total FROM ms_product_category WHERE product_category_id = COALESCE(?, product_category_id) " + search_part;
	    listParam.add(id != null ? QueryExecuteAdapter.QueryParam("string", id) : QueryExecuteAdapter.QueryParam("null", java.sql.Types.NULL));
	    if (search != null) {
		String searchString = "%" + search.trim().replace(" ", "%") + "%";
		listParam.add(QueryExecuteAdapter.QueryParam("string", searchString));
	    }
	    crs = QueryExecuteAdapter.QueryExecute(sql, listParam, functionName);

	    while (crs.next()) {
		return_value = crs.getInt("total");
	    }
	} catch (Exception ex) {
	    LogAdapter.InsertLog(ex.toString(), functionName, sql, source);
	    logger.error("FAILED. Function : " + functionName + ", service_id : " + id + ", Exception:" + ex.toString(), ex);
	}
	return return_value;
    }
    
    public static List<model.mdlProduct> GetProductList(String id, String pageNumber, String pageSize, String search) {
	String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();
	List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
	List<model.mdlProduct> productList = new ArrayList<model.mdlProduct>();
	CachedRowSet jrs = null;
	String search_part = search != null ? search_part = "AND (prod.product_title LIKE ? OR prod.product_title LIKE ? "
		+ "OR prod.product_desc LIKE ? OR prod.product_desc_english LIKE ? ) " : "";
	String sql = "";

	try {	    
	    sql = "SELECT prod.product_id, cat.product_category_id, cat.product_category_name, cat.product_category_name_english, prod.product_title, prod.product_title_english, prod.product_desc, prod.product_desc_english, "
	    		+ "prod.product_content , prod.product_content_english , prod.product_image, prod.product_link FROM ms_product prod "
	    		+ "LEFT JOIN ms_product_category cat on prod.product_category = cat.product_category_id "
	    		+ "WHERE prod.product_id = COALESCE(?, prod.product_id) " + search_part 
	    		+ " ORDER BY cat.product_category_order, prod.product_title OFFSET ? ROWS FETCH NEXT ? ROWS ONLY;";
	    listParam.add(id != null ? QueryExecuteAdapter.QueryParam("int", Integer.parseInt(id)) : QueryExecuteAdapter.QueryParam("null", java.sql.Types.NULL));
	    
	    if (search != null) {
		String searchString = "%" + search.trim().replace(" ", "%") + "%";
		listParam.add(QueryExecuteAdapter.QueryParam("string", searchString));
		listParam.add(QueryExecuteAdapter.QueryParam("string", searchString));
		listParam.add(QueryExecuteAdapter.QueryParam("string", searchString));
		listParam.add(QueryExecuteAdapter.QueryParam("string", searchString));
	    }
	    
	    listParam.add(QueryExecuteAdapter.QueryParam("int", ((Integer.parseInt(pageNumber) - 1) * Integer.parseInt(pageSize))));
	    listParam.add(QueryExecuteAdapter.QueryParam("int", Integer.parseInt(pageSize)));
	    
	    jrs = QueryExecuteAdapter.QueryExecute(sql, listParam, functionName);
	    while (jrs.next()) {
		model.mdlProduct mdlProduct = new model.mdlProduct();
		mdlProduct.product_id = jrs.getString("product_id");
		mdlProduct.product_category_id = jrs.getString("product_category_id");
		mdlProduct.product_category_name = jrs.getString("product_category_name");
		mdlProduct.product_category_name_english = jrs.getString("product_category_name");
		mdlProduct.product_title = jrs.getString("product_title");
		mdlProduct.product_title_english = jrs.getString("product_title_english");
		mdlProduct.product_desc = jrs.getString("product_desc");
		mdlProduct.product_desc_english = jrs.getString("product_desc_english");
		mdlProduct.product_content = jrs.getString("product_content");
		mdlProduct.product_content_english = jrs.getString("product_content_english");
		mdlProduct.product_image = jrs.getString("product_image") == null ? "" : jrs.getString("product_image");
		mdlProduct.product_link = jrs.getString("product_link") == null ? "" : jrs.getString("product_link");
		productList.add(mdlProduct);
	    }
	} catch (Exception ex) {
	    LogAdapter.InsertLog(ex.toString(), functionName, sql, source);
	    logger.error("FAILED. Function : " + functionName + ", Exception:" + ex.toString(), ex);
	}
	return productList;
    }
    
    public static int GetProductTotalList(String id, String search) {
	String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();
	List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
	CachedRowSet crs = null;
	String sql = "";
	int return_value = 0;
	String search_part = search != null ? search_part = "AND (prod.product_title LIKE ? OR prod.product_title LIKE ? " + "OR prod.product_desc LIKE ? OR prod.product_desc_english LIKE ? ) " : "";
	try {
	    sql = "SELECT COUNT(1) AS total FROM ms_product prod " 
		    + "LEFT JOIN ms_product_category cat on prod.product_category = cat.product_category_id "
		    + "WHERE prod.product_id = COALESCE(?, prod.product_id) " + search_part;
	    listParam.add(id != null ? QueryExecuteAdapter.QueryParam("string", id) : QueryExecuteAdapter.QueryParam("null", java.sql.Types.NULL));
	    
	    if (search != null) {
		String searchString = "%" + search.trim().replace(" ", "%") + "%";
		listParam.add(QueryExecuteAdapter.QueryParam("string", searchString));
		listParam.add(QueryExecuteAdapter.QueryParam("string", searchString));
		listParam.add(QueryExecuteAdapter.QueryParam("string", searchString));
		listParam.add(QueryExecuteAdapter.QueryParam("string", searchString));
	    }
	    crs = QueryExecuteAdapter.QueryExecute(sql, listParam, functionName);

	    while (crs.next()) {
		return_value = crs.getInt("total");
	    }
	} catch (Exception ex) {
	    LogAdapter.InsertLog(ex.toString(), functionName, sql, source);
	    logger.error("FAILED. Function : " + functionName + ", service_id : " + id + ", Exception:" + ex.toString(), ex);
	}
	return return_value;
    }
    
    public static boolean InsertUpdateProduct(model.mdlProduct param){
	String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();
	List<mdlQueryExecute> listParam = new ArrayList<mdlQueryExecute>();
	String sql = "";
	boolean success = false;

	String productImageQuery = param.product_image != null ? "product_image = ? , " : "";

	try {
	    sql = "BEGIN TRAN UPDATE ms_product SET product_category = ?, product_title = ?, product_title_english = ?, product_desc = ?, product_desc_english = ?, "
		    + " product_content = ?, product_content_english = ?, " + productImageQuery 
		    + " product_link = ? WHERE product_id = ? IF @@rowcount = 0 " 
		    + " BEGIN INSERT INTO ms_product (product_category, product_title, product_title_english, product_desc, product_desc_english, "
		    + " product_content, product_content_english, product_image, product_link) " 
		    + " VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?) END COMMIT TRAN";
	    listParam.add(QueryExecuteAdapter.QueryParam("string", param.product_category_id));
	    listParam.add(QueryExecuteAdapter.QueryParam("string", param.product_title));
	    listParam.add(QueryExecuteAdapter.QueryParam("string", param.product_title_english));
	    listParam.add(QueryExecuteAdapter.QueryParam("string", param.product_desc));
	    listParam.add(QueryExecuteAdapter.QueryParam("string", param.product_desc_english));
	    listParam.add(QueryExecuteAdapter.QueryParam("string", param.product_content));
	    listParam.add(QueryExecuteAdapter.QueryParam("string", param.product_content_english));
	    if (param.product_image != null) {
		listParam.add(database.QueryExecuteAdapter.QueryParam("string", param.product_image));
	    }
	    listParam.add(QueryExecuteAdapter.QueryParam("string", param.product_link));
	    listParam.add(QueryExecuteAdapter.QueryParam("string", param.product_id == null ? "0" : param.product_id));
	    
	    listParam.add(QueryExecuteAdapter.QueryParam("string", param.product_category_id));
	    listParam.add(QueryExecuteAdapter.QueryParam("string", param.product_title));
	    listParam.add(QueryExecuteAdapter.QueryParam("string", param.product_title_english));
	    listParam.add(QueryExecuteAdapter.QueryParam("string", param.product_desc == null ? "" : param.product_desc));
	    listParam.add(QueryExecuteAdapter.QueryParam("string", param.product_desc_english == null ? "" : param.product_desc_english));
	    listParam.add(QueryExecuteAdapter.QueryParam("string", param.product_content));
	    listParam.add(QueryExecuteAdapter.QueryParam("string", param.product_content_english));
	    listParam.add(database.QueryExecuteAdapter.QueryParam("string", param.product_image == null ? "" : param.product_image));
	    listParam.add(QueryExecuteAdapter.QueryParam("string", param.product_link));
	    
	    success = QueryExecuteAdapter.QueryManipulate(sql, listParam, functionName);
	} catch (Exception ex) {
	    logger.error("FAILED. Function : " + functionName + ", product_id : " + param.product_id + ", Exception:" + ex.toString(), ex);
	}
	return success;
    }

    public static boolean DeleteProduct(String ID) {
	String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();
	List<mdlQueryExecute> listParam = new ArrayList<mdlQueryExecute>();
	String sql = "";
	boolean success = false;

	try {
	    sql = "DELETE FROM ms_product WHERE product_id = ?";
	    listParam.add(QueryExecuteAdapter.QueryParam("string", ID));
	    success = QueryExecuteAdapter.QueryManipulate(sql, listParam, functionName);
	} catch (Exception ex) {
	    logger.error("FAILED. Function : " + functionName + ", news_id : " + ID + ", Exception:" + ex.toString(), ex);
	}
	return success;
    }
    
    public static boolean InsertUpdateProductCategory(model.mdlProductCategory param){
	String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();
	List<mdlQueryExecute> listParam = new ArrayList<mdlQueryExecute>();
	String sql = "";
	boolean success = false;

	String productCategoryIconQuery = param.product_category_icon != null ? "product_category_icon = ? , " : "";

	try {
	    sql = "BEGIN TRAN UPDATE ms_product_category SET product_category_name = ?, product_category_name_english = ?, product_category_order = ?, " 
		    + productCategoryIconQuery 
		    + " update_date = CURRENT_TIMESTAMP WHERE product_category_id = ? IF @@rowcount = 0 " 
		    + " BEGIN INSERT INTO ms_product_category (product_category_name, product_category_name_english, product_category_icon, product_category_order) " 
		    + "VALUES (?, ?, ?, ?) END COMMIT TRAN";
	    listParam.add(QueryExecuteAdapter.QueryParam("string", param.product_category_name));
	    listParam.add(QueryExecuteAdapter.QueryParam("string", param.product_category_name_english));
	    listParam.add(QueryExecuteAdapter.QueryParam("string", param.product_category_order));
	    if (param.product_category_icon != null) {
		listParam.add(database.QueryExecuteAdapter.QueryParam("string", param.product_category_icon));
	    }
	    listParam.add(QueryExecuteAdapter.QueryParam("string", param.product_category_id == null ? "0" : param.product_category_id));

	    listParam.add(QueryExecuteAdapter.QueryParam("string", param.product_category_name));
	    listParam.add(QueryExecuteAdapter.QueryParam("string", param.product_category_name_english));
	    listParam.add(QueryExecuteAdapter.QueryParam("string", param.product_category_icon == null ? "" : param.product_category_icon));
	    listParam.add(QueryExecuteAdapter.QueryParam("string", param.product_category_order));
	    success = QueryExecuteAdapter.QueryManipulate(sql, listParam, functionName);
	} catch (Exception ex) {
	    logger.error("FAILED. Function : " + functionName + ", product_category_id : " + param.product_category_id + ", Exception:" + ex.toString(), ex);
	}
	return success;
    }
    
    public static boolean DeleteProductCategory(String ID) {
	String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();
	List<mdlQueryExecute> listParam = new ArrayList<mdlQueryExecute>();
	String sql = "";
	boolean success = false;

	try {
	    sql = "DELETE FROM ms_product_category WHERE product_category_id = ?";
	    listParam.add(QueryExecuteAdapter.QueryParam("string", ID));
	    success = QueryExecuteAdapter.QueryManipulate(sql, listParam, functionName);
	} catch (Exception ex) {
	    logger.error("FAILED. Function : " + functionName + ", product_category_id : " + ID + ", Exception:" + ex.toString(), ex);
	}
	return success;
    }
}
