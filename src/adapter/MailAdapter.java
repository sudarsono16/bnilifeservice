package adapter;

import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.naming.Context;
import javax.naming.InitialContext;

import org.apache.log4j.Logger;
import com.google.gson.Gson;

public class MailAdapter {
    final static Logger logger = Logger.getLogger(MailAdapter.class);

    public static boolean SendForgotPasswordEmail(model.mdlUser mdlUser, String queryString, String url) {
	Gson gson = new Gson();
	String jsonIn = gson.toJson(mdlUser);
	boolean success = false;
	try {
	    // Get the base naming context from web.xml
	    Context web_context = (Context) new InitialContext().lookup("java:comp/env");
	    // Recipient's email ID needs to be mentioned.
	    String to = mdlUser.email;

	    String host = (String) web_context.lookup("bnilife_email_host");
	    String from = (String) web_context.lookup("bnilife_email");
	    String port = (String) web_context.lookup("bnilife_email_port");
	    String username = from;
	    String password = (String) web_context.lookup("bnilife_email_password");

	    // String host = "smtp.gmail.com";
	    // String from = "bnilifemobiledevelop@gmail.com";

	    // String password = "invent2016";
	    // String port = "465";

	    // web domain
	    // String domain = (String) web_context.lookup("domain");
	    // admin contact
	    // String contact = (String) web_context.lookup("contact");

	    // Get system properties
	    String SSL_FACTORY = "javax.net.ssl.SSLSocketFactory";
	    Properties properties = System.getProperties();
	    // Setup mail server
	    properties.setProperty("mail.smtp.host", host);
	    properties.setProperty("mail.smtp.port", port);
//	    properties.setProperty("mail.smtp.ssl.enable", "false");
//	    properties.setProperty("mail.smtp.starttls.enable", "true");
	    // properties.setProperty("mail.smtp.socketFactory.class", SSL_FACTORY);
	    // properties.setProperty("mail.smtp.socketFactory.fallback", "false");
	    // properties.setProperty("mail.smtp.socketFactory.port", port);
	    properties.put("mail.smtp.auth", "true");
	    properties.put("mail.debug", "true");
	    properties.put("mail.store.protocol", "pop3");
	    properties.put("mail.transport.protocol", "smtp");

	    // Get the default Session object.
	    Session session = Session.getDefaultInstance(properties, new Authenticator() {
		protected PasswordAuthentication getPasswordAuthentication() {
		    return new PasswordAuthentication(username, password);
		}
	    });
	    // Create a default MimeMessage object.
	    MimeMessage message = new MimeMessage(session);
	    // Set From: header field of the header.
	    message.setFrom(new InternetAddress(from));
	    // Set To: header field of the header.
	    message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));
	    // Set Subject: header field
	    message.setSubject("BNILife Mobile Account Reset Password Information");
	    // Send the actual HTML message, as big as you like <a href=\"http://www.example.com/login.aspx\">login</a>
	    message.setContent("<h3>Dear, " + mdlUser.username + ",<br><br>Your BNILife Mobile Account has requested to reset your password. <br><br>Please click this URL to reset your password : <br><br><a href=\"" + url + "\">" + url + "</a></h3>", "text/html");
	    // Send message
	    Transport.send(message);
	    success = true;
	    logger.info("SUCCESS. method : SendForgotPasswordEmail, jsonIn : " + jsonIn + ", queryString : " + queryString + ", URL : " + url);
	} catch (Exception ex) {
	    logger.info("FAILED. method : SendForgotPasswordEmail, jsonIn : " + jsonIn + ", queryString : " + queryString + ", URL : " + url + ", Exception : " + ex.toString());
	}
	return success;
    }
}
