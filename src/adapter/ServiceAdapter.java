package adapter;

import java.util.ArrayList;
import java.util.List;

import javax.sql.rowset.CachedRowSet;

import org.apache.log4j.Logger;

import com.google.gson.Gson;

import database.QueryExecuteAdapter;
import model.mdlQueryExecute;

public class ServiceAdapter {
    final static Logger logger = Logger.getLogger(ServiceAdapter.class);
    static Gson gson = new Gson();
    static String source = "Web Server";

    public static List<model.mdlService> GetServiceList(String id, String pageNumber, String pageSize, String search) {
	String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();
	List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
	List<model.mdlService> serviceList = new ArrayList<model.mdlService>();
	CachedRowSet jrs = null;
	String sql = "";
	String search_part = "";
	try {
	    if (search != null) {
		search_part = "AND (service_title LIKE ? OR service_title_english LIKE ? OR service_content LIKE ? OR service_content_english LIKE ? ) ";
	    }
	    sql = "SELECT service_id, service_title, service_title_english, service_content, service_content_english, service_order FROM ms_service " 
		    + "WHERE service_id = COALESCE(?, service_id) " + search_part + "ORDER BY service_order ASC OFFSET ? ROWS FETCH NEXT ? ROWS ONLY;";
	    listParam.add(id != null ? QueryExecuteAdapter.QueryParam("int", Integer.parseInt(id)) : QueryExecuteAdapter.QueryParam("null", java.sql.Types.NULL));

	    if (search != null) {
		String searchString = "%" + search.trim().replace(" ", "%") + "%";
		listParam.add(QueryExecuteAdapter.QueryParam("string", searchString));
		listParam.add(QueryExecuteAdapter.QueryParam("string", searchString));
		listParam.add(QueryExecuteAdapter.QueryParam("string", searchString));
		listParam.add(QueryExecuteAdapter.QueryParam("string", searchString));
	    }
	    
	    listParam.add(QueryExecuteAdapter.QueryParam("int", ((Integer.parseInt(pageNumber) - 1) * Integer.parseInt(pageSize))));
	    listParam.add(QueryExecuteAdapter.QueryParam("int", Integer.parseInt(pageSize)));

	    jrs = QueryExecuteAdapter.QueryExecute(sql, listParam, functionName);
	    while (jrs.next()) {
		model.mdlService mdlService = new model.mdlService();
		mdlService.service_id = jrs.getString("service_id");
		mdlService.title = jrs.getString("service_title");
		mdlService.title_english = jrs.getString("service_title_english");
		mdlService.content = jrs.getString("service_content");
		mdlService.content_english = jrs.getString("service_content_english");
		mdlService.order = jrs.getString("service_order");
		serviceList.add(mdlService);
	    }
	} catch (Exception ex) {
	    LogAdapter.InsertLog(ex.toString(), functionName, sql, source);
	    logger.error("FAILED. Function : " + functionName + ", service_id : " + id + ", Exception:" + ex.toString(), ex);
	}
	return serviceList;
    }

    public static int GetServiceTotalList(String id, String search) {
	String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();
	List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
	CachedRowSet crs = null;
	String sql = "";
	int return_value = 0;
	String search_part = search != null ? "AND (service_title LIKE ? OR service_title_english LIKE ? OR service_content LIKE ? OR service_content_english LIKE ? ) " : "";
	try {
	    sql = "SELECT COUNT(1) AS total FROM ms_service WHERE service_id = COALESCE(?, service_id) " + search_part;
	    listParam.add(id != null ? QueryExecuteAdapter.QueryParam("string", id) : QueryExecuteAdapter.QueryParam("null", java.sql.Types.NULL));

	    if (search != null) {
		String searchString = "%" + search.trim().replace(" ", "%") + "%";
		listParam.add(QueryExecuteAdapter.QueryParam("string", searchString));
		listParam.add(QueryExecuteAdapter.QueryParam("string", searchString));
		listParam.add(QueryExecuteAdapter.QueryParam("string", searchString));
		listParam.add(QueryExecuteAdapter.QueryParam("string", searchString));
	    }
	    crs = QueryExecuteAdapter.QueryExecute(sql, listParam, functionName);

	    while (crs.next()) {
		return_value = crs.getInt("total");
	    }
	} catch (Exception ex) {
	    LogAdapter.InsertLog(ex.toString(), functionName, sql, source);
	    logger.error("FAILED. Function : " + functionName + ", service_id : " + id + ", Exception:" + ex.toString(), ex);
	}
	return return_value;
    }

    public static boolean InsertUpdateService(model.mdlService mdlService) {
	boolean success = false;
	String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();
	List<mdlQueryExecute> listParam = new ArrayList<mdlQueryExecute>();
	String sql = "";
	try {
	    sql = "BEGIN TRAN UPDATE ms_service SET " 
		    + "service_title = ?, service_title_english = ?, service_content = ?, service_content_english = ?, service_order = ? WHERE service_id = ? " 
		    + "IF @@rowcount = 0 BEGIN INSERT INTO ms_service " 
		    + "(service_title, service_title_english, service_content, service_content_english, service_order) VALUES " 
		    + "(?, ?, ?, ?, ?) END COMMIT TRAN";
	    listParam.add(QueryExecuteAdapter.QueryParam("string", mdlService.title));
	    listParam.add(QueryExecuteAdapter.QueryParam("string", mdlService.title_english));
	    listParam.add(QueryExecuteAdapter.QueryParam("string", mdlService.content));
	    listParam.add(QueryExecuteAdapter.QueryParam("string", mdlService.content_english));
	    listParam.add(QueryExecuteAdapter.QueryParam("string", mdlService.order));
	    listParam.add(QueryExecuteAdapter.QueryParam("string", mdlService.service_id == null ? "0" : mdlService.service_id));

	    listParam.add(QueryExecuteAdapter.QueryParam("string", mdlService.title));
	    listParam.add(QueryExecuteAdapter.QueryParam("string", mdlService.title_english));
	    listParam.add(QueryExecuteAdapter.QueryParam("string", mdlService.content));
	    listParam.add(QueryExecuteAdapter.QueryParam("string", mdlService.content_english));
	    listParam.add(QueryExecuteAdapter.QueryParam("string", mdlService.order));
	    success = QueryExecuteAdapter.QueryManipulate(sql, listParam, functionName);
	} catch (Exception ex) {
	    LogAdapter.InsertLog(ex.toString(), functionName, sql, source);
	    logger.error("FAILED. Function : " + functionName + ", mdlService : " + gson.toJson(mdlService) + ", Exception:" + ex.toString(), ex);
	}
	return success;
    }

    public static boolean DeleteService(String id) {
	boolean success = false;
	String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();
	List<mdlQueryExecute> listParam = new ArrayList<mdlQueryExecute>();
	String sql = "";
	try {
	    sql = "DELETE FROM ms_service WHERE service_id = ? ";
	    listParam.add(QueryExecuteAdapter.QueryParam("string", id));
	    success = QueryExecuteAdapter.QueryManipulate(sql, listParam, functionName);
	} catch (Exception ex) {
	    LogAdapter.InsertLog(ex.toString(), functionName, sql, source);
	    logger.error("FAILED. Function : " + functionName + ", service_id : " + id + ", Exception:" + ex.toString(), ex);
	}
	return success;
    }

}
