package adapter;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.UUID;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.sql.rowset.CachedRowSet;
import org.apache.log4j.Logger;

import com.google.gson.Gson;

import database.QueryExecuteAdapter;
import model.ErrorStatus;
import model.mdlQueryExecute;

public class UserAdapter {
    final static Logger logger = Logger.getLogger(UserAdapter.class);
    static Gson gson = new Gson();

    public static boolean CheckUserPassword(String username, String password) {
	String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();
	boolean isExists = false;
	List<mdlQueryExecute> listParam = new ArrayList<mdlQueryExecute>();
	CachedRowSet jrs = null;
	String sql = "";
	try {
	    sql = "SELECT 1 FROM ms_user WHERE username = ? AND password = ? ";
	    listParam.add(QueryExecuteAdapter.QueryParam("string", username));
	    listParam.add(QueryExecuteAdapter.QueryParam("string", password));
	    jrs = QueryExecuteAdapter.QueryExecute(sql, listParam, functionName);
	    while (jrs.next()) {
		isExists = true;
		break;
	    }
	} catch (Exception ex) {
	    logger.error("FAILED. Function : " + functionName + ", username : " + username + ", password : " + password + ", Exception:" + ex.toString(), ex);
	}
	return isExists;
    }

    public static boolean CheckUserActiveAndPassword(String username) {
	String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();
	List<mdlQueryExecute> listParam = new ArrayList<mdlQueryExecute>();
	CachedRowSet jrs = null;
	String sql = "";
	boolean allowSetPassword = false;
	String password = "";
	Integer is_active = 0;
	try {
	    sql = "SELECT password, is_active from ms_user where username = ? ";
	    listParam.add(QueryExecuteAdapter.QueryParam("string", username));
	    jrs = QueryExecuteAdapter.QueryExecute(sql, listParam, functionName);
	    while (jrs.next()) {
		password = jrs.getString("password");
		is_active = jrs.getInt("is_active");
	    }
	    if (is_active == 1 && (password == null || password.equals(""))) {
		allowSetPassword = true;
	    }
	} catch (Exception ex) {
	    logger.error("FAILED. Function : " + functionName + ", username : " + username + ", Exception:" + ex.toString(), ex);
	}
	return allowSetPassword;
    }

    public static boolean CheckUsernameAndEmail(String username, String email) {
	String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();
	boolean isExists = false;
	List<mdlQueryExecute> listParam = new ArrayList<mdlQueryExecute>();
	CachedRowSet jrs = null;
	String sql = "";
	try {
	    sql = "SELECT 1 FROM ms_user WHERE username = ? AND email = ? ";
	    listParam.add(QueryExecuteAdapter.QueryParam("string", username));
	    listParam.add(QueryExecuteAdapter.QueryParam("string", email));
	    jrs = QueryExecuteAdapter.QueryExecute(sql, listParam, functionName);
	    while (jrs.next()) {
		isExists = true;
		break;
	    }
	} catch (Exception ex) {
	    logger.error("FAILED. Function : " + functionName + ", username : " + username + ", email : " + email + ", Exception:" + ex.toString(), ex);
	}
	return isExists;
    }

    public static boolean CheckUserEmail(String email) {
	String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();
	boolean isExists = false;
	List<mdlQueryExecute> listParam = new ArrayList<mdlQueryExecute>();
	CachedRowSet jrs = null;
	String sql = "";
	try {
	    sql = "SELECT 1 FROM ms_user WHERE email = ? ";
	    listParam.add(QueryExecuteAdapter.QueryParam("string", email));
	    jrs = QueryExecuteAdapter.QueryExecute(sql, listParam, functionName);
	    while (jrs.next()) {
		isExists = true;
		break;
	    }
	} catch (Exception ex) {
	    logger.error("FAILED. Function : " + functionName + ", email : " + email + ", Exception:" + ex.toString(), ex);
	}
	return isExists;
    }

    public static boolean CheckUserPhoneNumber(String phone_number) {
	String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();
	boolean isExists = false;
	List<mdlQueryExecute> listParam = new ArrayList<mdlQueryExecute>();
	CachedRowSet jrs = null;
	String sql = "";
	try {
	    sql = "SELECT 1 FROM ms_user WHERE phone_number = ? ";
	    listParam.add(QueryExecuteAdapter.QueryParam("string", phone_number));
	    jrs = QueryExecuteAdapter.QueryExecute(sql, listParam, functionName);
	    while (jrs.next()) {
		isExists = true;
		break;
	    }
	} catch (Exception ex) {
	    logger.error("FAILED. Function : " + functionName + ", phone_number : " + phone_number + ", Exception:" + ex.toString(), ex);
	}
	return isExists;
    }

    public static model.mdlUser CheckUser(String username) {
	String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();
	model.mdlUser mdlUserExists = new model.mdlUser();
	List<mdlQueryExecute> listParam = new ArrayList<mdlQueryExecute>();
	CachedRowSet jrs = null;
	String sql = "";
	try {
	    sql = "SELECT usr.username, usr.password, member.member_id, usr.email, usr.phone_number, usr.date_of_birth, usr.is_active FROM ms_user usr " + "LEFT JOIN ms_user_member member ON usr.username = member.username " + "WHERE usr.username = ? ";
	    listParam.add(QueryExecuteAdapter.QueryParam("string", username));
	    jrs = QueryExecuteAdapter.QueryExecute(sql, listParam, functionName);
	    while (jrs.next()) {
		mdlUserExists.username = jrs.getString(1);
		mdlUserExists.password = jrs.getString(2);
		mdlUserExists.member_id = jrs.getString(3);
		mdlUserExists.email = jrs.getString(4);
		mdlUserExists.phone_number = jrs.getString(5);
		mdlUserExists.date_of_birth = jrs.getString(6);
		mdlUserExists.is_active = String.valueOf(jrs.getInt(7));
	    }
	} catch (Exception ex) {
	    logger.error("FAILED. Function : " + functionName + ", username : " + username + ", Exception:" + ex.toString(), ex);
	}
	return mdlUserExists;
    }

    public static model.mdlUser GetUser(String username, String email, String phone_number, String password) {
	String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();
	model.mdlUser mdlUserExists = new model.mdlUser();
	List<mdlQueryExecute> listParam = new ArrayList<mdlQueryExecute>();
	CachedRowSet jrs = null;
	String sql = "";
	try {
	    sql = "SELECT username, email, phone_number, date_of_birth, is_active FROM ms_user " 
		    + "WHERE password = ? AND (username = ? OR email = ? OR REPLACE(phone_number, '+62', '0') = ?) AND is_active = 1";
	    listParam.add(QueryExecuteAdapter.QueryParam("string", password));
	    listParam.add(QueryExecuteAdapter.QueryParam("string", username));
	    listParam.add(QueryExecuteAdapter.QueryParam("string", username));
	    listParam.add(QueryExecuteAdapter.QueryParam("string", username));
	    jrs = QueryExecuteAdapter.QueryExecute(sql, listParam, functionName);
	    while (jrs.next()) {
		mdlUserExists.username = jrs.getString(1);
		mdlUserExists.email = jrs.getString(2);
		mdlUserExists.phone_number = jrs.getString(3);
		mdlUserExists.date_of_birth = jrs.getString(4);
		mdlUserExists.is_active = String.valueOf(jrs.getInt(5));
	    }
	} catch (Exception ex) {
	    String jsonIn = "username : " + username + ", email : " + email + ", phone_number : " + phone_number + ", password :" + password;
	    logger.error("FAILED. Function : " + functionName + ", UserModel : " + jsonIn + ", Exception:" + ex.toString(), ex);
	}
	return mdlUserExists;
    }

    public static List<model.mdlUserMember> GetUserMemberList(String username) {
	String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();
	List<model.mdlUserMember> userMemberList = new ArrayList<model.mdlUserMember>();
	List<mdlQueryExecute> listParam = new ArrayList<mdlQueryExecute>();
	CachedRowSet jrs = null;
	String sql = "";
	try {
	    sql = "SELECT member_id, member_type, phone_number, email, date_of_birth FROM ms_user_member WHERE username = ? ORDER BY member_type ASC";
	    listParam.add(QueryExecuteAdapter.QueryParam("string", username));
	    jrs = QueryExecuteAdapter.QueryExecute(sql, listParam, functionName);
	    while (jrs.next()) {
		model.mdlUserMember userMember = new model.mdlUserMember();
		userMember.member_id = jrs.getString("member_id");
		userMember.member_type = jrs.getString("member_type");
		userMember.phone_number = jrs.getString("phone_number");
		userMember.email = jrs.getString("email");
		userMember.date_of_birth = jrs.getString("date_of_birth");
		userMemberList.add(userMember);
	    }
	} catch (Exception ex) {
	    logger.error("FAILED. Function : " + functionName + ", username : " + username + ", Exception:" + ex.toString(), ex);
	}
	return userMemberList;
    }

    public static model.mdlUser CheckUserFromEmail(String email) {
	String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();
	model.mdlUser mdlUser = new model.mdlUser();
	List<mdlQueryExecute> listParam = new ArrayList<mdlQueryExecute>();
	CachedRowSet jrs = null;
	String sql = "";
	try {
	    sql = "SELECT usr.username, usr.password, member.member_id, usr.email, usr.phone_number, usr.date_of_birth, usr.is_active FROM ms_user usr " 
		    + "LEFT JOIN ms_user_member member ON usr.username = member.username WHERE usr.email = ? ";
	    listParam.add(QueryExecuteAdapter.QueryParam("string", email));
	    jrs = QueryExecuteAdapter.QueryExecute(sql, listParam, functionName);
	    while (jrs.next()) {
		mdlUser.username = jrs.getString(1);
		mdlUser.password = jrs.getString(2);
		mdlUser.member_id = jrs.getString(3);
		mdlUser.email = jrs.getString(4);
		mdlUser.phone_number = jrs.getString(5);
		mdlUser.date_of_birth = jrs.getString(6);
		mdlUser.is_active = String.valueOf(jrs.getInt(7));
	    }
	} catch (Exception ex) {
	    logger.error("FAILED. Function : " + functionName + ", email : " + email + ", Exception:" + ex.toString(), ex);
	}
	return mdlUser;
    }

    public static boolean CheckUserFromUUID(String username, String uuid) {
	String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();
	boolean success = false;
	List<mdlQueryExecute> listParam = new ArrayList<mdlQueryExecute>();
	CachedRowSet jrs = null;
	String sql = "";
	try {
	    sql = "SELECT username, uuid FROM ms_user_reset_password WHERE username = ? AND uuid = ? ";
	    listParam.add(QueryExecuteAdapter.QueryParam("string", username));
	    listParam.add(QueryExecuteAdapter.QueryParam("string", uuid));
	    jrs = QueryExecuteAdapter.QueryExecute(sql, listParam, functionName);
	    while (jrs.next()) {
		success = true;
	    }
	} catch (Exception ex) {
	    logger.error("FAILED. Function : " + functionName + ", username : " + username + ", uuid : " + uuid + ", Exception:" + ex.toString(), ex);
	}
	return success;
    }

    public static boolean CheckMember(String member_id) {
	String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();
	boolean isExists = false;
	List<mdlQueryExecute> listParam = new ArrayList<mdlQueryExecute>();

	CachedRowSet jrs = null;

	String sql = "";
	try {
	    sql = "SELECT member_id FROM ms_user_member WHERE member_id = ? ";
	    listParam.add(QueryExecuteAdapter.QueryParam("string", member_id));
	    jrs = QueryExecuteAdapter.QueryExecute(sql, listParam, functionName);

	    while (jrs.next()) {
		isExists = true;
	    }
	} catch (Exception ex) {
	    logger.error("FAILED. Function : " + functionName + ", member_id : " + member_id + ", Exception:" + ex.toString(), ex);
	}

	return isExists;
    }

    public static boolean RegisterNewUser(model.mdlUser mdlUser) {
	String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();
	List<model.mdlQueryTransaction> listmdlQueryTransaction = new ArrayList<model.mdlQueryTransaction>();
	model.mdlQueryTransaction mdlQueryTransaction;
	boolean success = false;
	try {
	    // insert into ms_user and ms_user_member when register
	    mdlQueryTransaction = new model.mdlQueryTransaction();
	    mdlQueryTransaction.sql = "INSERT INTO ms_user(username, email, phone_number, date_of_birth, is_active) VALUES (?,?,?,?,0)";
	    mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", mdlUser.username));
	    mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", mdlUser.email));
	    mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", mdlUser.phone_number));
	    mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", mdlUser.date_of_birth));
	    listmdlQueryTransaction.add(mdlQueryTransaction);

	    mdlQueryTransaction = new model.mdlQueryTransaction();
	    mdlQueryTransaction.sql = "INSERT INTO ms_user_member(username, member_id, member_type, email, phone_number, date_of_birth) VALUES (?,?,?,?,?,?)";
	    mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", mdlUser.username));
	    mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", mdlUser.member_id));
	    mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", mdlUser.member_type));
	    mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", mdlUser.email));
	    mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", mdlUser.phone_number));
	    mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", mdlUser.date_of_birth));
	    listmdlQueryTransaction.add(mdlQueryTransaction);

	    success = QueryExecuteAdapter.QueryTransaction(listmdlQueryTransaction, functionName);

	} catch (Exception ex) {
	    String jsonIn = gson.toJson(mdlUser);
	    logger.error("FAILED. Function : " + functionName + ", UserModel : " + jsonIn + ", Exception:" + ex.toString(), ex);
	}
	return success;
    }

    public static model.mdlOTPAttempt CheckUsernameOTPAttempt(String username, String member_id) {
	String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();
	List<mdlQueryExecute> listParam = new ArrayList<mdlQueryExecute>();
	CachedRowSet jrs = null;
	String sql = "";
	Integer otpAttempt = null;
	String last_otp_date = "";
	boolean newUser = false;
	model.mdlOTPAttempt mdlOTPAttempt = new model.mdlOTPAttempt();

	try {
	    sql = "SELECT otp_attempt, last_otp_date FROM ms_otp WHERE username = ? AND member_id = ? ";
	    listParam.add(QueryExecuteAdapter.QueryParam("string", username));
	    listParam.add(QueryExecuteAdapter.QueryParam("string", member_id));
	    jrs = QueryExecuteAdapter.QueryExecute(sql, listParam, functionName);
	    while (jrs.next()) {
		otpAttempt = jrs.getInt(1);
		last_otp_date = jrs.getString(2);
	    }

	    // if username never use OTP or no data OTP of the designated username
	    if (otpAttempt == null || last_otp_date.equals("")) {
		otpAttempt = 0;
		newUser = true;
	    } else {
		// check if last otp date is same with today or not. if last otp date is not today, reset otp attempt
		String dateNow = LocalDateTime.now().toString().substring(0, 10);
		if (!dateNow.equals(last_otp_date)) {
		    otpAttempt = 0;
		}
	    }
	    mdlOTPAttempt.newUser = newUser;
	    mdlOTPAttempt.otpAttempt = otpAttempt;

	} catch (Exception ex) {
	    logger.error("FAILED. Function : " + functionName + ", username : " + username + ", member_id : " + member_id + ", Exception:" + ex.toString(), ex);
	}
	return mdlOTPAttempt;

    }

    public static Integer CheckUsernameVerifyOTPAttempt(String username, String member_id) {
	String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();
	List<mdlQueryExecute> listParam = new ArrayList<mdlQueryExecute>();
	CachedRowSet jrs = null;
	String sql = "";
	Integer verifyOtpAttempt = 0;
	String last_otp_date = "";

	try {
	    sql = "SELECT verify_attempt, last_otp_date FROM ms_otp WHERE username = ? AND member_id = ? ";
	    listParam.add(QueryExecuteAdapter.QueryParam("string", username));
	    listParam.add(QueryExecuteAdapter.QueryParam("string", member_id));
	    jrs = QueryExecuteAdapter.QueryExecute(sql, listParam, functionName);
	    while (jrs.next()) {
		verifyOtpAttempt = jrs.getInt(1);
		last_otp_date = jrs.getString(2);
	    }

	    // if username never use OTP or no data OTP of the designated username
	    if (verifyOtpAttempt == null || last_otp_date.equals("")) {
		verifyOtpAttempt = 0;
	    } else {
		// check if last otp date is same with today or not. if last otp date is not today, reset otp attempt
		String dateNow = LocalDateTime.now().toString().substring(0, 10);
		if (!dateNow.equals(last_otp_date)) {
		    verifyOtpAttempt = 0;
		}
	    }
	} catch (Exception ex) {
	    logger.error("FAILED. Function : " + functionName + ", username : " + username + ", member_id : " + member_id + ", Exception:" + ex.toString(), ex);
	}
	return verifyOtpAttempt;

    }

    public static boolean SendOTP(model.mdlUser mdlUser, model.mdlOTPAttempt mdlOTPAttempt) {
	String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();
	boolean success = false;
	boolean successHitAPI = false;
	List<mdlQueryExecute> listParam = new ArrayList<mdlQueryExecute>();
	String sql = "";

	try {
	    // Generate OTP
	    Random random = new Random();
	    int otp = 100000 + random.nextInt(900000);
	    String otpString = String.valueOf(otp);

	    // only for testing, please comment when API send OTP BNI can be hit
	    // otpString = "123456";
	    String otpMessage = "";
	    otpMessage = "Jangan memberikan kode OTP kepada orang lain! Silahkan masukan kode OTP. Kode OTP anda adalah : " + otpString;
	    if (mdlUser.language.equalsIgnoreCase("en")) {
		otpMessage = "Do not give OTP code to someone else! Please input OTP Code. Your OTP code is : " + otpString;
	    }
	    successHitAPI = APIAdapter.HitAPISendOTP(mdlUser, otpString, otpMessage);
	    // if success hit API send OTP, save data in table ms_otp
	    if (successHitAPI) {
		String otpCount = String.valueOf(mdlOTPAttempt.otpAttempt + 1);
		// check if there is otp data for the username and member_id. if exists, update, if not exists, insert new otp data
		if (mdlOTPAttempt.newUser) {
		    String dateNow = LocalDateTime.now().toString().substring(0, 10).toString();
		    sql = "INSERT INTO ms_otp (username, phone_number, otp, last_otp_date, otp_attempt, member_id) VALUES (?, ?, ?, ?, ?, ?)";
		    listParam.add(QueryExecuteAdapter.QueryParam("string", mdlUser.username));
		    listParam.add(QueryExecuteAdapter.QueryParam("string", mdlUser.phone_number));
		    listParam.add(QueryExecuteAdapter.QueryParam("string", otpString));
		    listParam.add(QueryExecuteAdapter.QueryParam("string", dateNow));
		    listParam.add(QueryExecuteAdapter.QueryParam("string", otpCount));
		    listParam.add(QueryExecuteAdapter.QueryParam("string", mdlUser.member_id));
		    success = QueryExecuteAdapter.QueryManipulate(sql, listParam, functionName);
		} else {
		    String dateNow = LocalDateTime.now().toString().substring(0, 10).toString();
		    sql = "UPDATE ms_otp SET phone_number = ?, otp = ?, last_otp_date = ?, otp_attempt = ? WHERE username = ? AND member_id = ? ";
		    listParam.add(QueryExecuteAdapter.QueryParam("string", mdlUser.phone_number));
		    listParam.add(QueryExecuteAdapter.QueryParam("string", otpString));
		    listParam.add(QueryExecuteAdapter.QueryParam("string", dateNow));
		    listParam.add(QueryExecuteAdapter.QueryParam("string", otpCount));
		    listParam.add(QueryExecuteAdapter.QueryParam("string", mdlUser.username));
		    listParam.add(QueryExecuteAdapter.QueryParam("string", mdlUser.member_id));
		    success = QueryExecuteAdapter.QueryManipulate(sql, listParam, functionName);
		}

		success = true;
	    }
	} catch (Exception ex) {
	    String jsonInUserModel = gson.toJson(mdlUser);
	    String jsonInOTPModel = gson.toJson(mdlOTPAttempt);
	    logger.error("FAILED. Function : " + functionName + ", UserModel : " + jsonInUserModel + ", OTPModel : " + jsonInOTPModel + ", Exception:" + ex.toString(), ex);
	}
	return success;
    }

    public static boolean VerifyOTPCode(model.mdlOTP mdlOTP, Integer verifyOtpAttempt) {
	String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();
	List<mdlQueryExecute> listParam = new ArrayList<mdlQueryExecute>();
	CachedRowSet jrs = null;
	String sql = "";
	boolean success = false;
	Integer otpFromDatabase = null;
	String createDateString = "";
	String updateDateString = "";
	String otpDateString = "";
	try {
	    sql = "SELECT otp, create_date, update_date FROM ms_otp WHERE username = ? AND member_id = ? ";
	    listParam.add(QueryExecuteAdapter.QueryParam("string", mdlOTP.username));
	    listParam.add(QueryExecuteAdapter.QueryParam("string", mdlOTP.member_id));
	    jrs = QueryExecuteAdapter.QueryExecute(sql, listParam, functionName);
	    while (jrs.next()) {
		otpFromDatabase = jrs.getInt(1);
		createDateString = jrs.getString(2);
		updateDateString = jrs.getString(3);
	    }

	    otpDateString = updateDateString == null || updateDateString.equals("") ? createDateString : updateDateString;
	    int expiresIn = 300;

	    // check if token still valid from it's date time
	    Date dateNow = new Date();
	    DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
	    Date otpDate = df.parse(otpDateString.replace("T", " "));
	    long toSecond = 1000;
	    int dateDiff = (int) (dateNow.getTime() - otpDate.getTime());
	    int diffSecond = (int) (dateDiff / toSecond);
	    if (diffSecond < expiresIn) {
		if (otpFromDatabase != null) {
		    String otpFromDatabaseString = String.valueOf(otpFromDatabase);
		    if (otpFromDatabaseString.equals(mdlOTP.otp)) {
			listParam = new ArrayList<mdlQueryExecute>();
			sql = "UPDATE ms_user SET is_active = 1 WHERE username = ? ";
			listParam.add(QueryExecuteAdapter.QueryParam("string", mdlOTP.username));
			success = QueryExecuteAdapter.QueryManipulate(sql, listParam, functionName);
		    } else {
			verifyOtpAttempt = verifyOtpAttempt + 1;
			listParam = new ArrayList<mdlQueryExecute>();
			String dateNowString = LocalDateTime.now().toString().substring(0, 10).toString();
			sql = "UPDATE ms_otp SET verify_attempt = ?, last_otp_date = ? WHERE username = ? AND member_id = ? ";
			listParam.add(QueryExecuteAdapter.QueryParam("integer", verifyOtpAttempt));
			listParam.add(QueryExecuteAdapter.QueryParam("string", dateNowString));
			listParam.add(QueryExecuteAdapter.QueryParam("string", mdlOTP.username));
			listParam.add(QueryExecuteAdapter.QueryParam("string", mdlOTP.member_id));
			QueryExecuteAdapter.QueryManipulate(sql, listParam, functionName);
		    }
		}
	    }
	} catch (Exception ex) {
	    String jsonIn = gson.toJson(mdlOTP);
	    logger.error("FAILED. Function : " + functionName + ", OTPModel : " + jsonIn + ", Exception:" + ex.toString(), ex);
	}
	return success;
    }

    public static boolean UpdateUserActive(String username, Integer active) {
	String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();
	List<mdlQueryExecute> listParam = new ArrayList<mdlQueryExecute>();
	String sql = "";
	boolean success = false;

	try {
	    sql = "UPDATE ms_user SET is_active = ? WHERE username = ? ";
	    listParam.add(QueryExecuteAdapter.QueryParam("integer", active));
	    listParam.add(QueryExecuteAdapter.QueryParam("string", username));
	    success = QueryExecuteAdapter.QueryManipulate(sql, listParam, functionName);
	} catch (Exception ex) {
	    logger.error("FAILED. Function : " + functionName + ", Username : " + username + ", Exception:" + ex.toString(), ex);
	}
	return success;
    }

    public static boolean SetUserPassword(model.mdlUser mdlUser) {
	String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();
	List<mdlQueryExecute> listParam = new ArrayList<mdlQueryExecute>();
	String sql = "";
	boolean success = false;
	try {
	    sql = "UPDATE ms_user SET password = ? WHERE username = ? ";
	    listParam.add(QueryExecuteAdapter.QueryParam("string", mdlUser.password));
	    listParam.add(QueryExecuteAdapter.QueryParam("string", mdlUser.username));
	    success = QueryExecuteAdapter.QueryManipulate(sql, listParam, functionName);
	} catch (Exception ex) {
	    logger.error("FAILED. Function : " + functionName + ", UserModel : " + gson.toJson(mdlUser) + ", Exception:" + ex.toString(), ex);
	}
	return success;
    }

    public static model.mdlUser RegisterUserToBNILife(model.mdlUser mdlUser) {
	String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();
	model.mdlUser mdlUserRegistered = new model.mdlUser();
	List<model.mdlMemberList> allMemberList = new ArrayList<model.mdlMemberList>();
	List<model.mdlMember> memberList = new ArrayList<model.mdlMember>();
	model.mdlMemberList mdlMemberList = new model.mdlMemberList();
	try {
	    memberList = APIAdapter.HitAPIRegister(mdlUser);

	    if (memberList != null && memberList.size() > 0) {
		boolean success = SetUserPassword(mdlUser);
		if (success) {
		    mdlUserRegistered.username = mdlUser.username;
		    mdlUserRegistered.email = mdlUser.email;
		    mdlUserRegistered.phone_number = mdlUser.phone_number;
		    mdlUserRegistered.date_of_birth = mdlUser.date_of_birth;
		    String memberListString = gson.toJson(memberList.get(0));
		    mdlMemberList.member_id = mdlUser.member_id;
		    mdlMemberList.member = gson.fromJson(memberListString, model.mdlMember.class);
		    allMemberList.add(mdlMemberList);
		    mdlUserRegistered.member_list = allMemberList;
		}
	    }
	} catch (Exception ex) {
	    logger.error("FAILED. Function : " + functionName + ", UserModel : " + gson.toJson(mdlUser) + ", Exception:" + ex.toString(), ex);
	}

	return mdlUserRegistered;
    }

    public static model.mdlUser UserLogin(model.mdlUser mdlUser) {
	String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();
	model.mdlUser mdlUserLogin = new model.mdlUser();
	try {
	    mdlUserLogin = GetUser(mdlUser.username, mdlUser.email, mdlUser.phone_number, mdlUser.password);
	    if (mdlUserLogin != null && mdlUserLogin.username != null && !mdlUserLogin.username.equals("")) {
		List<model.mdlMemberList> allMemberList = new ArrayList<model.mdlMemberList>();
		allMemberList = APIAdapter.HitAPILogin(mdlUserLogin);
		if (allMemberList != null && allMemberList.size() > 0) {
		    mdlUserLogin.member_list = allMemberList;
		}
	    }
	} catch (Exception ex) {
	    logger.error("FAILED. Function : " + functionName + ", mdlUser : " + gson.toJson(mdlUser) + ", Exception:" + ex.toString(), ex);
	}
	return mdlUserLogin;
    }

    public static model.mdlUser GetAccountList(String username) {
	String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();
	model.mdlUser mdlUserLogin = new model.mdlUser();
	try {
	    mdlUserLogin = GetAccountList(username);
	    if (mdlUserLogin != null && mdlUserLogin.username != null && !mdlUserLogin.username.equals("")) {
		List<model.mdlMemberList> allMemberList = new ArrayList<model.mdlMemberList>();
		allMemberList = APIAdapter.HitAPILogin(mdlUserLogin);
		if (allMemberList != null && allMemberList.size() > 0) {
		    mdlUserLogin.member_list = allMemberList;
		}
	    }
	} catch (Exception ex) {
	    logger.error("FAILED. Function : " + functionName + ", username : " + username + ", Exception:" + ex.toString(), ex);
	}
	return mdlUserLogin;
    }

    public static boolean SetUserDeviceLogin(model.mdlUser mdlUser) {
	String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();
	List<mdlQueryExecute> listParam = new ArrayList<mdlQueryExecute>();
	String sql = "";
	CachedRowSet jrs = null;
	boolean isExists = false;
	boolean success = false;
	try {
	    sql = "SELECT 1 FROM ms_user_device WHERE device_id = ? ";
	    listParam.add(QueryExecuteAdapter.QueryParam("string", mdlUser.device_id));
	    jrs = QueryExecuteAdapter.QueryExecute(sql, listParam, "SetUserDeviceLogin");
	    while (jrs.next()) {
		isExists = true;
	    }

	    listParam = new ArrayList<mdlQueryExecute>();
	    if (isExists) {
		sql = "UPDATE ms_user_device SET firebase_token = ?, is_login = 1 WHERE username = ? AND device_id = ? AND os_type = ? ;";
		listParam.add(QueryExecuteAdapter.QueryParam("string", mdlUser.firebase_token));
		listParam.add(QueryExecuteAdapter.QueryParam("string", mdlUser.username));
		listParam.add(QueryExecuteAdapter.QueryParam("string", mdlUser.device_id));
		listParam.add(QueryExecuteAdapter.QueryParam("string", mdlUser.os_type));
		success = QueryExecuteAdapter.QueryManipulate(sql, listParam, functionName);
	    } else {
		sql = "INSERT INTO ms_user_device (username, device_id, firebase_token, os_type, is_login) VALUES (?, ?, ?, ?, 1);";
		listParam.add(QueryExecuteAdapter.QueryParam("string", mdlUser.username));
		listParam.add(QueryExecuteAdapter.QueryParam("string", mdlUser.device_id));
		listParam.add(QueryExecuteAdapter.QueryParam("string", mdlUser.firebase_token));
		listParam.add(QueryExecuteAdapter.QueryParam("string", mdlUser.os_type));
		success = QueryExecuteAdapter.QueryManipulate(sql, listParam, functionName);
	    }
	} catch (Exception ex) {
	    String jsonIn = gson.toJson(mdlUser);
	    logger.error("FAILED. Function : " + functionName + ", OTPModel : " + jsonIn + ", Exception:" + ex.toString(), ex);
	}
	return success;
    }

    public static boolean SendForgotPasswordEmail(model.mdlUser mdlUser, String language) {
	String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();
	List<mdlQueryExecute> listParam = new ArrayList<mdlQueryExecute>();
	String sql = "";
	boolean success = false;
	try {
	    // generate query forgot password
	    Date now = new Date();
	    DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
	    String startDate = df.format(now);
	    String uuid = UUID.randomUUID().toString().replace("-", "");

	    sql = "DELETE FROM ms_user_reset_password WHERE username = ? ";
	    listParam.add(QueryExecuteAdapter.QueryParam("string", mdlUser.username));
	    success = QueryExecuteAdapter.QueryManipulate(sql, listParam, functionName);
	    if (success) {
		success = false;
		sql = "INSERT INTO ms_user_reset_password (username, uuid) VALUES (?, ?);";
		listParam.add(QueryExecuteAdapter.QueryParam("string", uuid));
		success = QueryExecuteAdapter.QueryManipulate(sql, listParam, functionName);
		if (success) {
		    success = false;
		    String queryString = mdlUser.username + ";" + "resetpassword" + ";" + uuid + ";" + startDate;
		    String hashedQuery = EncryptAdapter.encrypt(queryString, "bnilife");
		    String encodedQuery = Base64Adapter.EncryptBase64(hashedQuery);
		    Context context = (Context) new InitialContext().lookup("java:comp/env");
		    String baseURLForgotPassword = (String) context.lookup("url_forgot_password");
		    String url = baseURLForgotPassword + "/reset?q=" + encodedQuery;

		    String mailTo = mdlUser.email;
		    String mailTitle, mailContent = "";
		    if (language == null || language.equals("")) {
			mailTitle = "Informasi Reset Kata Sandi Akun BNI Life Mobile Anda";
			mailContent = "<h3>Yth. " + mdlUser.username + ",<br><br>Akun BNI Life Mobile Anda telah meminta untuk me-reset kata sandi. <br><br>Silahkan klik link dibawah ini untuk me-reset kata sandi anda : <br><br><a href=\"" + url + "\">" + url + "</a></h3>";
		    } else {
			if (language.equalsIgnoreCase("in")) {
			    mailTitle = "Informasi Reset Kata Sandi Akun BNI Life Mobile Anda";
			    mailContent = "<h3>Yth. " + mdlUser.username + ",<br><br>Akun BNI Life Mobile Anda telah meminta untuk me-reset kata sandi. <br><br>Silahkan klik link dibawah ini untuk me-reset kata sandi anda : <br><br><a href=\"" + url + "\">" + url + "</a></h3>";
			} else {
			    mailTitle = "BNI Life Mobile Account Reset Password Information";
			    mailContent = "<h3>Dear " + mdlUser.username + ",<br><br>Your BNI Life Mobile Account has requested to reset your password. <br><br>Please click this URL to reset your password : <br><br><a href=\"" + url + "\">" + url + "</a></h3>";
			}
		    }
		    success = APIAdapter.HitAPISendEmail(mailTo, mailTitle, mailContent);
		}
	    }
	} catch (Exception ex) {
	    String jsonIn = gson.toJson(mdlUser);
	    logger.error("FAILED. Function : " + functionName + ", UserModel : " + jsonIn + ", Exception:" + ex.toString(), ex);
	}
	return success;
    }

    public static boolean ResetUserPassword(String username, String password) {
	String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();
	List<mdlQueryExecute> listParam = new ArrayList<mdlQueryExecute>();
	String sql = "";
	boolean success = false;
	try {
	    sql = "UPDATE ms_user SET password = ? WHERE username = ? ";
	    listParam.add(QueryExecuteAdapter.QueryParam("string", password));
	    listParam.add(QueryExecuteAdapter.QueryParam("string", username));
	    success = QueryExecuteAdapter.QueryManipulate(sql, listParam, functionName);
	} catch (Exception ex) {
	    logger.error("FAILED. Function : " + functionName + ", username : " + username + ", password : " + password + ", Exception:" + ex.toString(), ex);
	}
	return success;
    }

    public static boolean ChangeUserEmail(String username, String email) {
	String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();
	List<mdlQueryExecute> listParam = new ArrayList<mdlQueryExecute>();
	String sql = "";
	boolean success = false;
	try {
	    sql = "UPDATE ms_user SET email = ? WHERE username = ? ";
	    listParam.add(QueryExecuteAdapter.QueryParam("string", email));
	    listParam.add(QueryExecuteAdapter.QueryParam("string", username));
	    success = QueryExecuteAdapter.QueryManipulate(sql, listParam, functionName);
	} catch (Exception ex) {
	    logger.error("FAILED. Function : " + functionName + ", username : " + username + ", email : " + email + ", Exception:" + ex.toString(), ex);
	}
	return success;
    }

    public static boolean DeleteUserDevice(String device_id) {
	String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();
	List<mdlQueryExecute> listParam = new ArrayList<mdlQueryExecute>();
	String sql = "";
	boolean success = false;
	try {
	    sql = "DELETE ms_user_device WHERE device_id = ? ";
	    listParam.add(QueryExecuteAdapter.QueryParam("string", device_id));
	    success = QueryExecuteAdapter.QueryManipulate(sql, listParam, functionName);
	} catch (Exception ex) {
	    logger.error("FAILED. Function : " + functionName + ", device_id : " + device_id + ", Exception:" + ex.toString(), ex);
	}
	return success;
    }

    public static boolean AddNewAccount(model.mdlUser mdlUser) {
	String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();
	boolean success = false;
	List<mdlQueryExecute> listParam = new ArrayList<mdlQueryExecute>();
	String sql = "";
	try {
	    sql = "INSERT INTO ms_user_member(username, member_id, member_type, email, phone_number, date_of_birth) VALUES (?,?,?,?,?,?)";
	    listParam.add(QueryExecuteAdapter.QueryParam("string", mdlUser.username));
	    listParam.add(QueryExecuteAdapter.QueryParam("string", mdlUser.member_id));
	    listParam.add(QueryExecuteAdapter.QueryParam("string", mdlUser.member_type));
	    listParam.add(QueryExecuteAdapter.QueryParam("string", mdlUser.email));
	    listParam.add(QueryExecuteAdapter.QueryParam("string", mdlUser.phone_number));
	    listParam.add(QueryExecuteAdapter.QueryParam("string", mdlUser.date_of_birth));
	    success = QueryExecuteAdapter.QueryManipulate(sql, listParam, functionName);
	} catch (Exception ex) {
	    logger.error("FAILED. Function : " + functionName + ", mdlUser : " + gson.toJson(mdlUser) + ", Exception:" + ex.toString(), ex);
	}
	return success;
    }

    public static List<model.mdlUser> GetMobileUserList(String pageNumber, String pageSize, String search) {
	String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();
	List<model.mdlUser> userList = new ArrayList<model.mdlUser>();
	CachedRowSet jrs = null;
	String sql = "";
	String search_part = "";
	try {
	    if (search != null) {
		search_part = "WHERE username LIKE ? ";
	    }
	    sql = "SELECT username, email, phone_number, date_of_birth, is_active FROM ms_user " 
	    + search_part + "ORDER BY create_date DESC OFFSET ? ROWS FETCH NEXT ? ROWS ONLY;";
	    List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();

	    if (search != null) {
		String searchString = "%" + search.trim().replace(" ", "%") + "%";
		listParam.add(QueryExecuteAdapter.QueryParam("string", searchString));
	    }

	    listParam.add(QueryExecuteAdapter.QueryParam("int", ((Integer.parseInt(pageNumber) - 1) * Integer.parseInt(pageSize))));
	    listParam.add(QueryExecuteAdapter.QueryParam("int", Integer.parseInt(pageSize)));
	    jrs = QueryExecuteAdapter.QueryExecute(sql, listParam, functionName);
	    while (jrs.next()) {
		model.mdlUser mdlUser = new model.mdlUser();
		mdlUser.username = jrs.getString("username");
		mdlUser.email = jrs.getString("email");
		mdlUser.phone_number = jrs.getString("phone_number");
		mdlUser.date_of_birth = jrs.getString("date_of_birth");
		mdlUser.is_active = jrs.getString("is_active");
		userList.add(mdlUser);
	    }
	} catch (Exception ex) {
	    logger.error("FAILED. Function : " + functionName + ", Exception:" + ex.toString(), ex);
	}
	return userList;
    }

    public static int GetMobileUserTotalList(String search) {
	String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();
	CachedRowSet crs = null;
	String sql = "";
	int return_value = 0;
	String search_part = "";
	try {
	    if (search != null) {
		search_part = "WHERE username LIKE ? ";
	    }
	    sql = "SELECT COUNT(*) total " + "FROM ms_user " + search_part;
	    List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
	    if (search != null) {
		String searchString = "%" + search.trim().replace(" ", "%") + "%";
		listParam.add(QueryExecuteAdapter.QueryParam("string", searchString));
	    }
	    crs = QueryExecuteAdapter.QueryExecute(sql, listParam, functionName);

	    while (crs.next()) {
		return_value = crs.getInt("total");
	    }
	} catch (Exception ex) {
	    logger.error("FAILED. Function : " + functionName + ", Exception:" + ex.toString(), ex);
	}
	return return_value;
    }
    
    public static List<String> GetUsernameList(){
	String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();
	List<String> usernameList = new ArrayList<String>();
	CachedRowSet jrs = null;
	String sql = "";
	try{
	    sql = "SELECT username FROM ms_user where is_active = 1 AND [password] IS NOT NULL";
	    jrs = QueryExecuteAdapter.QueryExecute(sql, functionName);
	    while(jrs.next()){
		usernameList.add(jrs.getString("username"));
	    }
	}catch(Exception ex){
	    logger.error("FAILED. Function : " + functionName + ", Exception:" + ex.toString(), ex);
	}
	return usernameList;
    }
}
