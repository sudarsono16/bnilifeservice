package adapter;

import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.sql.rowset.CachedRowSet;
import org.apache.log4j.Logger;
import com.google.gson.Gson;
import database.QueryExecuteAdapter;
import model.mdlQueryExecute;

public class CMSAdapter {
    final static Logger logger = Logger.getLogger(ClaimAdapter.class);
    static Gson gson = new Gson();

    public static List<model.mdlCMSUser> GetCMSUserList(String pageNumber, String pageSize, String search) {
	String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();
	List<model.mdlCMSUser> cmsUserList = new ArrayList<model.mdlCMSUser>();
	CachedRowSet jrs = null;
	String sql = "";
	String search_part = "";
	try {
	    if (search != null) {
		search_part = "WHERE username LIKE ? ";
	    }
	    sql = "SELECT username, password, is_active, RoleID FROM ms_user_cms " + search_part + "ORDER BY create_date DESC OFFSET ? ROWS FETCH NEXT ? ROWS ONLY;";
	    List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
	    // if (username != null) {
	    // listParam.add(QueryExecuteAdapter.QueryParam("string", username));
	    // } else {
	    // listParam.add(QueryExecuteAdapter.QueryParam("null", java.sql.Types.NULL));
	    // }

	    if (search != null) {
		String searchString = "%" + search.trim().replace(" ", "%") + "%";
		listParam.add(QueryExecuteAdapter.QueryParam("string", searchString));
	    }

	    listParam.add(QueryExecuteAdapter.QueryParam("int", ((Integer.parseInt(pageNumber) - 1) * Integer.parseInt(pageSize))));
	    listParam.add(QueryExecuteAdapter.QueryParam("int", Integer.parseInt(pageSize)));
	    jrs = QueryExecuteAdapter.QueryExecute(sql, listParam, functionName);
	    while (jrs.next()) {
		model.mdlCMSUser mdlCMSUser = new model.mdlCMSUser();
		mdlCMSUser.username = jrs.getString("username");
		mdlCMSUser.password = jrs.getString("password");
		mdlCMSUser.is_active = jrs.getString("is_active");
		mdlCMSUser.RoleID = jrs.getString("RoleID");
		cmsUserList.add(mdlCMSUser);
	    }
	} catch (Exception ex) {
	    logger.error("FAILED. Function : " + functionName + ", Exception:" + ex.toString(), ex);
	}
	return cmsUserList;
    }

    public static int GetCMSUserTotalList(String search) {
	String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();
	CachedRowSet crs = null;
	String sql = "";
	int return_value = 0;
	String search_part = "";
	try {
	    if (search != null) {
		search_part = "WHERE username LIKE ? ";
	    }
	    sql = "SELECT COUNT(1) total " + "FROM ms_user_cms " + search_part;
	    List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
	    if (search != null) {
		String searchString = "%" + search.trim().replace(" ", "%") + "%";
		listParam.add(QueryExecuteAdapter.QueryParam("string", searchString));
	    }

	    crs = QueryExecuteAdapter.QueryExecute(sql, listParam, functionName);

	    while (crs.next()) {
		return_value = crs.getInt("total");
	    }
	} catch (Exception ex) {
	    logger.error("FAILED. Function : " + functionName + ", Exception:" + ex.toString(), ex);
	}
	return return_value;
    }

    public static boolean InsertUpdateCMSUser(model.mdlCMSUser mdlCMSUser) throws InvalidKeyException, UnsupportedEncodingException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException {
	boolean success = false;
	String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();
	List<mdlQueryExecute> listParam = new ArrayList<mdlQueryExecute>();
	String sql = "";
	String encryptedPassword = EncryptAdapter.encrypt(mdlCMSUser.password, "bnilifemobileclaim");
	try {
	    sql = "BEGIN TRAN UPDATE ms_user_cms SET password = COALESCE(?, password), is_active = COALESCE(?, is_active), " 
		    + "RoleID = COALESCE(?, RoleID) " + "WHERE username = ? " + "IF @@rowcount = 0 BEGIN INSERT INTO ms_user_cms " 
		    + "(username, password, is_active, is_login, RoleID) VALUES " + "(?, ?, ?, 0, ?) END COMMIT TRAN";
	    if (mdlCMSUser.password != null) {
		listParam.add(database.QueryExecuteAdapter.QueryParam("string", encryptedPassword));
	    } else {
		listParam.add(database.QueryExecuteAdapter.QueryParam("null", java.sql.Types.NULL));
	    }

	    listParam.add(QueryExecuteAdapter.QueryParam("string", mdlCMSUser.is_active == null || mdlCMSUser.is_active.equals("") ? 0 : mdlCMSUser.is_active));

	    if (mdlCMSUser.RoleID != null) {
		listParam.add(database.QueryExecuteAdapter.QueryParam("string", mdlCMSUser.RoleID));
	    } else {
		listParam.add(database.QueryExecuteAdapter.QueryParam("null", java.sql.Types.NULL));
	    }

	    listParam.add(QueryExecuteAdapter.QueryParam("string", mdlCMSUser.username));

	    listParam.add(QueryExecuteAdapter.QueryParam("string", mdlCMSUser.username));
	    listParam.add(QueryExecuteAdapter.QueryParam("string", encryptedPassword));
	    listParam.add(QueryExecuteAdapter.QueryParam("string", mdlCMSUser.is_active == null || mdlCMSUser.is_active.equals("") ? 0 : mdlCMSUser.is_active));
	    listParam.add(QueryExecuteAdapter.QueryParam("string", mdlCMSUser.RoleID));

	    // sql = "BEGIN TRAN UPDATE ms_user_cms SET " + "is_active = ?, RoleID = ? WHERE username = ? "
	    // + "IF @@rowcount = 0 BEGIN INSERT INTO ms_user_cms "
	    // + "(username, password, is_active, is_login, RoleID) VALUES " + "(?, ?, ?, 0, ?) END COMMIT TRAN";
	    // listParam.add(QueryExecuteAdapter.QueryParam("string", mdlCMSUser.is_active == null || mdlCMSUser.is_active.equals("") ? 0 :
	    // mdlCMSUser.is_active));
	    // listParam.add(QueryExecuteAdapter.QueryParam("string", mdlCMSUser.RoleID));
	    // listParam.add(QueryExecuteAdapter.QueryParam("string", mdlCMSUser.username));
	    //
	    // listParam.add(QueryExecuteAdapter.QueryParam("string", mdlCMSUser.username));
	    // listParam.add(QueryExecuteAdapter.QueryParam("string", mdlCMSUser.password));
	    // listParam.add(QueryExecuteAdapter.QueryParam("string", mdlCMSUser.is_active == null || mdlCMSUser.is_active.equals("") ? 0 :
	    // mdlCMSUser.is_active));
	    // listParam.add(QueryExecuteAdapter.QueryParam("string", mdlCMSUser.RoleID));

	    success = QueryExecuteAdapter.QueryManipulate(sql, listParam, functionName);
	} catch (Exception ex) {
	    logger.error("FAILED. Function : " + functionName + ", mdlCMSUser : " + gson.toJson(mdlCMSUser) + ", Exception:" + ex.toString(), ex);
	}
	return success;
    }

    public static boolean DeleteCMSUser(String username) {
	boolean success = false;
	String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();
	List<mdlQueryExecute> listParam = new ArrayList<mdlQueryExecute>();
	String sql = "";
	try {
	    sql = "DELETE FROM ms_user_cms WHERE username = ? ";
	    listParam.add(QueryExecuteAdapter.QueryParam("string", username));
	    success = QueryExecuteAdapter.QueryManipulate(sql, listParam, functionName);
	} catch (Exception ex) {
	    logger.error("FAILED. Function : " + functionName + ", username : " + username + ", Exception:" + ex.toString(), ex);
	}
	return success;
    }

    public static List<model.mdlAPILog> GetAPILogList(String pageNumber, String pageSize, String search) {
	String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();
	List<model.mdlAPILog> apiLogList = new ArrayList<model.mdlAPILog>();
	CachedRowSet jrs = null;
	String sql = "";
	String search_part = "";
	try {
	    if (search != null) {
		search_part = "WHERE api_source LIKE ? OR api_name LIKE ? OR function_name LIKE ? OR api_status LIKE ? ";
	    }
	    sql = "SELECT api_log_id, api_source, api_name, api_status, function_name, SUBSTRING(api_input,1,250) as api_input, " + "SUBSTRING(api_output,1,250) as api_output FROM api_log " + search_part + "ORDER BY create_date DESC OFFSET ? ROWS FETCH NEXT ? ROWS ONLY;";
	    List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();

	    if (search != null) {
		String searchString = "%" + search.trim().replace(" ", "%") + "%";
		listParam.add(QueryExecuteAdapter.QueryParam("string", searchString));
		listParam.add(QueryExecuteAdapter.QueryParam("string", searchString));
		listParam.add(QueryExecuteAdapter.QueryParam("string", searchString));
		listParam.add(QueryExecuteAdapter.QueryParam("string", searchString));
	    }

	    listParam.add(QueryExecuteAdapter.QueryParam("int", ((Integer.parseInt(pageNumber) - 1) * Integer.parseInt(pageSize))));
	    listParam.add(QueryExecuteAdapter.QueryParam("int", Integer.parseInt(pageSize)));
	    jrs = QueryExecuteAdapter.QueryExecute(sql, listParam, functionName);
	    while (jrs.next()) {
		model.mdlAPILog mdlAPILog = new model.mdlAPILog();
		mdlAPILog.api_log_id = jrs.getString("api_log_id");
		mdlAPILog.api_source = jrs.getString("api_source");
		mdlAPILog.api_name = jrs.getString("api_name");
		mdlAPILog.api_status = jrs.getString("api_status");
		mdlAPILog.function_name = jrs.getString("function_name");
		mdlAPILog.api_input = jrs.getString("api_input");
		mdlAPILog.api_output = jrs.getString("api_output");
		apiLogList.add(mdlAPILog);
	    }
	} catch (Exception ex) {
	    logger.error("FAILED. Function : " + functionName + ", Exception:" + ex.toString(), ex);
	}
	return apiLogList;
    }

    public static int GetAPILogTotalList(String search) {
	String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();
	CachedRowSet crs = null;
	String sql = "";
	int return_value = 0;
	String search_part = "";
	try {
	    if (search != null) {
		search_part = "WHERE api_source LIKE ? OR api_name LIKE ? OR function_name LIKE ? OR api_status LIKE ? ";
	    }
	    sql = "SELECT COUNT(1) total " + "FROM api_log " + search_part;
	    List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
	    if (search != null) {
		String searchString = "%" + search.trim().replace(" ", "%") + "%";
		listParam.add(QueryExecuteAdapter.QueryParam("string", searchString));
		listParam.add(QueryExecuteAdapter.QueryParam("string", searchString));
		listParam.add(QueryExecuteAdapter.QueryParam("string", searchString));
		listParam.add(QueryExecuteAdapter.QueryParam("string", searchString));
	    }

	    crs = QueryExecuteAdapter.QueryExecute(sql, listParam, functionName);

	    while (crs.next()) {
		return_value = crs.getInt("total");
	    }
	} catch (Exception ex) {
	    logger.error("FAILED. Function : " + functionName + ", Exception:" + ex.toString(), ex);
	}
	return return_value;
    }
}
