package adapter;

import java.util.ArrayList;
import java.util.List;

import javax.sql.rowset.CachedRowSet;

import org.apache.log4j.Logger;

import database.QueryExecuteAdapter;
import model.mdlQueryExecute;

public class NewsAdapter {
    final static Logger logger = Logger.getLogger(NewsAdapter.class);

    public static List<model.mdlNews> GetNewsList(String language, String id, String news_type) {
	String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();
	List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
	List<model.mdlNews> newsList = new ArrayList<model.mdlNews>();
	CachedRowSet jrs = null;
	String typeString = "";
	String type = "OGH";
	if (news_type.contains("ALL")) {
	    typeString = "";
	} else if (news_type.contains("OGH")) {
	    typeString = "AND news_type = ? ";
	    type = "OGH";
	} else if (news_type.contains("OGS")) {
	    typeString = "AND news_type = ? ";
	    type = "OGS";
	}

	try {
	    String sql = "SELECT news_id, news_type, news_title_id, news_title_en, news_image, news_content_en, news_content_id, news_link FROM ms_news " 
		    + "WHERE news_id = COALESCE(?, news_id) " + typeString + " ORDER BY create_date DESC";
	    if (id != null) {
		listParam.add(database.QueryExecuteAdapter.QueryParam("int", Integer.parseInt(id)));
	    } else {
		listParam.add(database.QueryExecuteAdapter.QueryParam("null", java.sql.Types.NULL));
	    }
	    if (!news_type.equalsIgnoreCase("ALL")) {
		listParam.add(database.QueryExecuteAdapter.QueryParam("string", type));
	    }

	    jrs = QueryExecuteAdapter.QueryExecute(sql, listParam, functionName);
	    while (jrs.next()) {
		model.mdlNews mdlNews = new model.mdlNews();
		mdlNews.news_id = jrs.getString("news_id");
		mdlNews.news_type = jrs.getString("news_type");
		mdlNews.news_title_id = jrs.getString("news_title_id");
		mdlNews.news_title_en = jrs.getString("news_title_en");
		if (language.equalsIgnoreCase("id")) {
		    mdlNews.news_title = jrs.getString("news_title_id");
		} else {
		    mdlNews.news_title = jrs.getString("news_title_en");
		}

		mdlNews.news_image = jrs.getString("news_image");
		mdlNews.news_content_en = jrs.getString("news_content_en");
		mdlNews.news_content_id = jrs.getString("news_content_id");

		if (language.equalsIgnoreCase("id")) {
		    mdlNews.news_content = jrs.getString("news_content_id");
		} else {
		    mdlNews.news_content = jrs.getString("news_content_en");
		}

		mdlNews.news_link = jrs.getString("news_link");
		newsList.add(mdlNews);
	    }
	} catch (Exception ex) {
	    logger.error("FAILED. Function : " + functionName + ", Exception:" + ex.toString(), ex);
	}
	return newsList;
    }

    public static boolean InsertUpdateNews(model.mdlNews param) {
	String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();
	List<mdlQueryExecute> listParam = new ArrayList<mdlQueryExecute>();
	String sql = "";
	boolean success = false;

	String newsImageQuery = param.news_image != null ? "news_image = ? , " : "";

	try {
	    sql = "BEGIN TRAN UPDATE ms_news SET news_type = ?, " + "news_title_id = ?, " + "news_content_id = ?, " + "news_title_en = ?, " + "news_content_en = ?, " + newsImageQuery + "update_date = CURRENT_TIMESTAMP, " + "news_link = ? " + "WHERE news_id = ? " + "IF @@rowcount = 0 " + "BEGIN INSERT INTO ms_news (news_type, news_title_id, news_content_id, news_title_en, news_content_en, news_image, create_date, news_link) " + "VALUES (?, ?, ?, ?, ?, ?, CURRENT_TIMESTAMP, ?) " + "END " + "COMMIT TRAN";
	    listParam.add(QueryExecuteAdapter.QueryParam("string", param.news_type));
	    listParam.add(QueryExecuteAdapter.QueryParam("string", param.news_title_id));
	    listParam.add(QueryExecuteAdapter.QueryParam("string", param.news_content_id == null ? "" : param.news_content_id));
	    listParam.add(QueryExecuteAdapter.QueryParam("string", param.news_title_en));
	    listParam.add(QueryExecuteAdapter.QueryParam("string", param.news_content_en == null ? "" : param.news_content_en));
	    if (param.news_image != null) {
		listParam.add(database.QueryExecuteAdapter.QueryParam("string", param.news_image));
	    }
	    listParam.add(QueryExecuteAdapter.QueryParam("string", param.news_link));

	    listParam.add(QueryExecuteAdapter.QueryParam("string", param.news_id == null ? "0" : param.news_id));

	    listParam.add(QueryExecuteAdapter.QueryParam("string", param.news_type));
	    listParam.add(QueryExecuteAdapter.QueryParam("string", param.news_title_id));
	    listParam.add(QueryExecuteAdapter.QueryParam("string", param.news_content_id == null ? "" : param.news_content_id));
	    listParam.add(QueryExecuteAdapter.QueryParam("string", param.news_title_en));
	    listParam.add(QueryExecuteAdapter.QueryParam("string", param.news_content_en == null ? "" : param.news_content_en));
	    listParam.add(QueryExecuteAdapter.QueryParam("string", param.news_image == null ? "" : param.news_image));
	    listParam.add(QueryExecuteAdapter.QueryParam("string", param.news_link));
	    success = QueryExecuteAdapter.QueryManipulate(sql, listParam, functionName);
	} catch (Exception ex) {
	    logger.error("FAILED. Function : " + functionName + ", news_id : " + param.news_id + ", Exception:" + ex.toString(), ex);
	}
	return success;
    }

    public static boolean DeleteNews(String ID) {
	String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();
	List<mdlQueryExecute> listParam = new ArrayList<mdlQueryExecute>();
	String sql = "";
	boolean success = false;

	try {
	    sql = "DELETE FROM ms_news WHERE news_id = ?";
	    listParam.add(QueryExecuteAdapter.QueryParam("string", ID));
	    success = QueryExecuteAdapter.QueryManipulate(sql, listParam, functionName);
	} catch (Exception ex) {
	    logger.error("FAILED. Function : " + functionName + ", news_id : " + ID + ", Exception:" + ex.toString(), ex);
	}
	return success;
    }
}
