package adapter;

import java.util.ArrayList;
import java.util.List;

import javax.sql.rowset.CachedRowSet;

import database.QueryExecuteAdapter;

public class HomeAdapter {
    public static List<model.mdlSlideshow> GetSlideshowData() {
	List<model.mdlSlideshow> listSlideshow = new ArrayList<model.mdlSlideshow>();
	CachedRowSet jrs = null;
	String sql = "";
	try {
	    sql = "{call ws_GetSlideshowData(?,?)}";
	    jrs = QueryExecuteAdapter.QueryExecute(sql, "ws_GetSlideshowData");

	    while (jrs.next()) {
		model.mdlSlideshow mdlSlideshow = new model.mdlSlideshow();
		mdlSlideshow.id = jrs.getString("id");
		mdlSlideshow.title = jrs.getString("title");
		mdlSlideshow.link_image = jrs.getString("link_image");
		mdlSlideshow.link_url = jrs.getString("link_url");
		listSlideshow.add(mdlSlideshow);
	    }
	} catch (Exception ex) {
	    LogAdapter.InsertLog(ex.toString(), "ws_GetSlideshowData", sql, "");
	}
	return listSlideshow;
    }
}
