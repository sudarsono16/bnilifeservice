package adapter;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.google.gson.Gson;

import model.ErrorStatus;
import adapter.ClientIdAdapter;

public class AuthorizationAdapter {
    final static Logger logger = Logger.getLogger(AuthorizationAdapter.class);
    static Gson gson = new Gson();
    
    public static model.mdlErrorSchema CheckAuthorization(String Authorization, String Key, String Timestamp, String Signature, HttpServletResponse response, String jsonIn, String apiMethod, String url, model.mdlAPIObjectResult mdlAPIResult) {
	String jsonOut = "";
	// check token, key and signature
	model.mdlErrorSchema mdlAPIStatus = CheckAuthorization(Authorization, Key, url, Timestamp, Signature, apiMethod);
	if (mdlAPIStatus == null) {
	    mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_99_999);
	    response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
	} else if (!mdlAPIStatus.error_code.equalsIgnoreCase("ERR-00-000")) {
	    mdlAPIResult.error_schema = mdlAPIStatus;
	    if (mdlAPIStatus.error_code.equalsIgnoreCase("ERR-99-005") || mdlAPIStatus.error_code.equalsIgnoreCase("ERR-99-006")) {
		// if token invalid or token expired
		response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
	    } else {
		response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
	    }
	    jsonOut = gson.toJson(mdlAPIResult);
	    logger.error("FAILED = API : " + url + ", method : " + apiMethod + ", jsonIn :" + jsonIn + ", jsonOut : " + jsonOut);
	}
	return mdlAPIStatus;
    }
    
    public static model.mdlErrorSchema CheckAuthorizationArray(String Authorization, String Key, String Timestamp, String Signature, HttpServletResponse response, String jsonIn, String apiMethod, String url, model.mdlAPIArrayResult mdlAPIResult) {
	String jsonOut = "";
	// check token, key and signature
	model.mdlErrorSchema mdlAPIStatus = AuthorizationAdapter.CheckAuthorization(Authorization, Key, url, Timestamp, Signature, apiMethod);
	if (mdlAPIStatus == null) {
	    mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_99_999);
	    response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
	} else if (!mdlAPIStatus.error_code.equalsIgnoreCase("ERR-00-000")) {
	    mdlAPIResult.error_schema = mdlAPIStatus;
	    if (mdlAPIStatus.error_code.equalsIgnoreCase("ERR-99-005") || mdlAPIStatus.error_code.equalsIgnoreCase("ERR-99-006")) {
		// if token invalid or token expired
		response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
	    } else {
		response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
	    }
	    jsonOut = gson.toJson(mdlAPIResult);
	    logger.error("FAILED = API : " + url + ", method : " + apiMethod + ", jsonIn :" + jsonIn + ", jsonOut : " + jsonOut);
	}
	return mdlAPIStatus;
    }

    public static List<String> decryptAuthorizationKey(String authorization) {
	// decrypt authorization key to get clientID and clientSecret
	String clientIDclientSecret = Base64Adapter.DecryptBase64(authorization);
	String[] clientData = clientIDclientSecret.split(";");
	List<String> result = Arrays.stream(clientData).collect(Collectors.toList());
	return result;
    }

    public static String getAuthorizationKey(String ClientID, String ClientSecret) {
	String stringToSign = ClientID + ";" + ClientSecret;
	// encrypt clientID and clientSecret to base64 string
	String authorizationKey = Base64Adapter.EncryptBase64(stringToSign);
	return authorizationKey;
    }

    public static model.mdlErrorSchema CheckAuthorization(String authorization, String key, String url, String timestamp, String signature, String method) {
	model.mdlErrorSchema mdlErrorSchema = new model.mdlErrorSchema();

	try {
	    String[] authorizationList = authorization.split(" ");
	    String base64token = authorizationList[1];
	    // convert token which from Base64 into basic string
	    String encrytedToken = Base64Adapter.DecryptBase64(base64token);
	    // decrypt token using APIKey
	    String token = EncryptAdapter.decrypt(encrytedToken, key);
	    String[] tokenData = token.split(";");
	    List<String> tokenDataList = Arrays.stream(tokenData).collect(Collectors.toList());
	    if (tokenDataList.size() != 3) {
		// invalid token
		mdlErrorSchema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_99_005);
	    } else {
		// get all data from token
		String authString = tokenDataList.get(0);
		String startDate = tokenDataList.get(1);
		int expiresIn = Integer.parseInt(tokenDataList.get(2));

		// check if token still valid from it's date time
		Date dateNow = new Date();
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
		Date dateToken = df.parse(startDate.replace("T", " "));
		long toSecond = 1000;
		long dateDiff = (long) (dateNow.getTime() - dateToken.getTime());
		long diffSecond = (long) (dateDiff / toSecond);
		if (diffSecond >= expiresIn) {
		    // token expired
		    mdlErrorSchema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_99_006);
		} else {
		    // get client data from authorization string
		    List<String> clientData = AuthorizationAdapter.decryptAuthorizationKey(authString);
		    String clientID = clientData.get(0);
		    String clientSecret = clientData.get(1);
		    // check if client data is exist
		    if (clientData.size() != 2 || clientID.equals("") || clientSecret.equals("")) {
			// invalid token
			mdlErrorSchema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_99_005);
		    } else {
			// get app data in database from clientID and clientSecret
			List<String> appData = ClientIdAdapter.CheckClientID(clientID, clientSecret);
			String appName = appData.get(0);
			String apiKey = appData.get(1);
			// check if app data is exist in database
			if (appData.size() != 2 || appName.trim().equals("") || apiKey.equals("")) {
			    // invalid token
			    mdlErrorSchema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_99_005);
			} else {
			    // check signature
			    // remove hashedBody in signature to make mobile generate signature easier
			    // String bodyToHash = jsonIn.replaceAll("\\s+", "");
			    // String hashedBody = SHA256Adapter.sha256(bodyToHash).toLowerCase();
			    // String stringToSign = method.toUpperCase() + ";" + url + ";" + base64token + ";" + hashedBody + ";" + timestamp;
			    // use signature without body
			    String stringToSign = method.toUpperCase() + ";" + url + ";" + base64token + ";" + timestamp;
			    String apiSignature = SHA256Adapter.generateHmacSHA256Signature(stringToSign, apiKey);
			    if (!signature.equals(apiSignature)) {
				// error HMAC tidak cocok
				mdlErrorSchema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_99_007);
			    } else {
				mdlErrorSchema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_00_000);
			    }
			}
		    }
		}
	    }

	    // TODO : ONLY FOR TESTING! PLEASE REMOVE WHEN DEPLOY
	     mdlErrorSchema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_00_000);
	} catch (Exception ex) {
	    logger.info("FAILED. API : CheckAuthorization, method : CheckAuthorization, Authorization : " + authorization + ", key : " + key + ", Exception : " + ex.toString(), ex);
	    mdlErrorSchema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_99_999);
	}
	return mdlErrorSchema;
    }
}
