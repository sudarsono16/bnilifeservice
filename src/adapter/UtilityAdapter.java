package adapter;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.sql.rowset.CachedRowSet;
import org.apache.log4j.Logger;

import com.google.gson.Gson;

import database.QueryExecuteAdapter;
import model.mdlQueryExecute;

public class UtilityAdapter {
    final static Logger logger = Logger.getLogger(UtilityAdapter.class);
    static Gson gson = new Gson();

    public static model.mdlAPKVersion GetAPKVersion() {
	model.mdlAPKVersion mdlAPKVersion = new model.mdlAPKVersion();
	CachedRowSet jrs = null;
	String sql = "";
	try {
	    sql = "SELECT version, version_code, action, link FROM ms_apk_version ORDER BY version desc";
	    jrs = QueryExecuteAdapter.QueryExecute(sql, "GetAPKVersion");
	    while (jrs.next()) {
		mdlAPKVersion.version = jrs.getString("version");
		mdlAPKVersion.version_code = jrs.getString("version_code");
		mdlAPKVersion.action = jrs.getString("action");
		mdlAPKVersion.link = jrs.getString("link");
	    }
	} catch (Exception ex) {
	    logger.error("FAILED. Function : GetAPKVersion, Exception:" + ex.toString(), ex);
	}
	return mdlAPKVersion;
    }

    public static List<model.mdlMenuMember> GetMenuMemberList(String member_type) {
	String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();
	List<model.mdlMenuMember> menuMemberList = new ArrayList<model.mdlMenuMember>();
	List<mdlQueryExecute> listParam = new ArrayList<mdlQueryExecute>();
	CachedRowSet jrs = null;
	String sql = "";
	try {
	    sql = "SELECT menu_member_id, member_type, menu_name, menu_name_english, menu_image, menu_order FROM ms_menu_member WHERE member_type = ? AND show = 1 ORDER BY menu_order ASC";
	    listParam.add(QueryExecuteAdapter.QueryParam("string", member_type));
	    jrs = QueryExecuteAdapter.QueryExecute(sql, listParam, functionName);
	    while (jrs.next()) {
		model.mdlMenuMember mdlMenuMember = new model.mdlMenuMember();
		mdlMenuMember.menu_member_id = jrs.getString("menu_member_id");
		mdlMenuMember.member_type = jrs.getString("member_type");
		mdlMenuMember.menu_name = jrs.getString("menu_name");
		mdlMenuMember.menu_name_english = jrs.getString("menu_name_english");
		mdlMenuMember.menu_image = jrs.getString("menu_image");
		mdlMenuMember.menu_order = jrs.getString("menu_order");
		menuMemberList.add(mdlMenuMember);
	    }
	} catch (Exception ex) {
	    logger.error("FAILED. Function : " + functionName + ", member_type : " + member_type + ", Exception:" + ex.toString(), ex);
	}
	return menuMemberList;
    }

    public static List<model.mdlMenuMember> GetMenuMember(String pageNumber, String pageSize, String search) {
	String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();
	List<model.mdlMenuMember> menuMemberList = new ArrayList<model.mdlMenuMember>();
	CachedRowSet jrs = null;
	String sql = "";
	String search_part = "";
	try {
	    if (search != null) {
		search_part = "WHERE member_type LIKE ? OR menu_name LIKE ? ";
	    }
	    sql = "SELECT menu_member_id, member_type, menu_name, menu_name_english, menu_image, menu_order, show FROM ms_menu_member " 
	    + search_part + "ORDER BY member_type, menu_order ASC OFFSET ? ROWS FETCH NEXT ? ROWS ONLY;";
	    List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();

	    if (search != null) {
		String searchString = "%" + search.trim().replace(" ", "%") + "%";
		listParam.add(QueryExecuteAdapter.QueryParam("string", searchString));
		listParam.add(QueryExecuteAdapter.QueryParam("string", searchString));
	    }
	    listParam.add(QueryExecuteAdapter.QueryParam("int", ((Integer.parseInt(pageNumber) - 1) * Integer.parseInt(pageSize))));
	    listParam.add(QueryExecuteAdapter.QueryParam("int", Integer.parseInt(pageSize)));

	    jrs = QueryExecuteAdapter.QueryExecute(sql, listParam, functionName);
	    while (jrs.next()) {
		model.mdlMenuMember mdlMenuMember = new model.mdlMenuMember();
		mdlMenuMember.menu_member_id = jrs.getString("menu_member_id");
		mdlMenuMember.member_type = jrs.getString("member_type");
		mdlMenuMember.menu_name = jrs.getString("menu_name");
		mdlMenuMember.menu_name_english = jrs.getString("menu_name_english");
		mdlMenuMember.menu_image = jrs.getString("menu_image");
		mdlMenuMember.menu_order = jrs.getString("menu_order");
		mdlMenuMember.show = jrs.getInt("show");
		menuMemberList.add(mdlMenuMember);
	    }
	} catch (Exception ex) {
	    logger.error("FAILED. Function : " + functionName + ", Exception:" + ex.toString(), ex);
	}
	return menuMemberList;
    }

    public static int GetMenuMemberTotalList(String search) {
	String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();
	CachedRowSet crs = null;
	String sql = "";
	int return_value = 0;
	String search_part = "";
	try {
	    if (search != null) {
		search_part = "WHERE member_type LIKE ? OR menu_name LIKE ? ";
	    }
	    sql = "SELECT COUNT(*) total " + "FROM ms_menu_member " + search_part;
	    List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
	    if (search != null) {
		String searchString = "%" + search.trim().replace(" ", "%") + "%";
		listParam.add(QueryExecuteAdapter.QueryParam("string", searchString));
		listParam.add(QueryExecuteAdapter.QueryParam("string", searchString));
	    }

	    crs = QueryExecuteAdapter.QueryExecute(sql, listParam, functionName);

	    while (crs.next()) {
		return_value = crs.getInt("total");
	    }
	} catch (Exception ex) {
	    logger.error("FAILED. Function : " + functionName + ", Exception:" + ex.toString(), ex);
	}
	return return_value;
    }

    public static boolean UpdateMenuMember(model.mdlMenuMember mdlMenuMember) {
	boolean success = false;
	String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();
	List<mdlQueryExecute> listParam = new ArrayList<mdlQueryExecute>();
	String sql = "";
	try {
	    sql = "UPDATE ms_menu_member SET show = ? WHERE menu_member_id = ? ";
	    listParam.add(QueryExecuteAdapter.QueryParam("integer", mdlMenuMember.show));
	    listParam.add(QueryExecuteAdapter.QueryParam("string", mdlMenuMember.menu_member_id));
	    success = QueryExecuteAdapter.QueryManipulate(sql, listParam, functionName);
	} catch (Exception ex) {
	    logger.error("FAILED. Function : " + functionName + ", mdlMenuMember : " + gson.toJson(mdlMenuMember) + ", Exception:" + ex.toString(), ex);
	}
	return success;
    }

    public static List<model.mdlHelp> GetHelpContent(String id) {
	String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();
	List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
	List<model.mdlHelp> helpList = new ArrayList<model.mdlHelp>();
	CachedRowSet jrs = null;
	String sql = "";
	try {
	    sql = "SELECT help_id, help_title, help_title_english, help_content, help_content_english, help_order FROM ms_help " 
	+ "WHERE help_id = COALESCE(?, help_id) " + "ORDER BY help_order ASC";
	    if (id != null) {
		listParam.add(database.QueryExecuteAdapter.QueryParam("int", Integer.parseInt(id)));
	    } else {
		listParam.add(database.QueryExecuteAdapter.QueryParam("null", java.sql.Types.NULL));
	    }

	    jrs = QueryExecuteAdapter.QueryExecute(sql, listParam, functionName);
	    while (jrs.next()) {
		model.mdlHelp mdlHelp = new model.mdlHelp();
		mdlHelp.help_id = jrs.getString("help_id");
		mdlHelp.title = jrs.getString("help_title");
		mdlHelp.title_english = jrs.getString("help_title_english");
		mdlHelp.content = jrs.getString("help_content");
		mdlHelp.content_english = jrs.getString("help_content_english");
		mdlHelp.order = jrs.getString("help_order");
		helpList.add(mdlHelp);
	    }
	} catch (Exception ex) {
	    logger.error("FAILED. Function : " + functionName + ", Exception:" + ex.toString(), ex);
	}
	return helpList;
    }

    public static boolean InsertUpdateHelp(model.mdlHelp mdlHelp) {
	boolean success = false;
	String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();
	List<mdlQueryExecute> listParam = new ArrayList<mdlQueryExecute>();
	String sql = "";
	try {
	    sql = "BEGIN TRAN UPDATE ms_help SET " + "help_title = ?, help_title_english = ?, help_content = ?, help_content_english = ?, help_order = ? WHERE help_id = ? " + "IF @@rowcount = 0 BEGIN INSERT INTO ms_help " + "(help_title, help_title_english, help_content, help_content_english, help_order) VALUES " + "(?, ?, ?, ?, ?) END COMMIT TRAN";
	    listParam.add(QueryExecuteAdapter.QueryParam("string", mdlHelp.title));
	    listParam.add(QueryExecuteAdapter.QueryParam("string", mdlHelp.title_english));
	    listParam.add(QueryExecuteAdapter.QueryParam("string", mdlHelp.content));
	    listParam.add(QueryExecuteAdapter.QueryParam("string", mdlHelp.content_english));
	    listParam.add(QueryExecuteAdapter.QueryParam("string", mdlHelp.order));
	    listParam.add(QueryExecuteAdapter.QueryParam("string", mdlHelp.help_id == null ? "0" : mdlHelp.help_id));

	    listParam.add(QueryExecuteAdapter.QueryParam("string", mdlHelp.title));
	    listParam.add(QueryExecuteAdapter.QueryParam("string", mdlHelp.title_english));
	    listParam.add(QueryExecuteAdapter.QueryParam("string", mdlHelp.content));
	    listParam.add(QueryExecuteAdapter.QueryParam("string", mdlHelp.content_english));
	    listParam.add(QueryExecuteAdapter.QueryParam("string", mdlHelp.order));
	    success = QueryExecuteAdapter.QueryManipulate(sql, listParam, functionName);
	} catch (Exception ex) {
	    logger.error("FAILED. Function : " + functionName + ", mdlHelp : " + gson.toJson(mdlHelp) + ", Exception:" + ex.toString(), ex);
	}
	return success;
    }

    public static boolean DeleteHelp(String id) {
	boolean success = false;
	String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();
	List<mdlQueryExecute> listParam = new ArrayList<mdlQueryExecute>();
	String sql = "";
	try {
	    sql = "DELETE FROM ms_help WHERE help_id = ? ";
	    listParam.add(QueryExecuteAdapter.QueryParam("string", id));
	    success = QueryExecuteAdapter.QueryManipulate(sql, listParam, functionName);
	} catch (Exception ex) {
	    logger.error("FAILED. Function : " + functionName + ", help_id : " + id + ", Exception:" + ex.toString(), ex);
	}
	return success;
    }

    public static model.mdlCardLink GetAccountImageLink(String memberType) {
	model.mdlCardLink mdlCardLink = new model.mdlCardLink();
	String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();
	List<mdlQueryExecute> listParam = new ArrayList<mdlQueryExecute>();
	CachedRowSet jrs = null;
	String sql = "";
	try {
	    sql = "SELECT card_image_link, account_image_link FROM ms_image_link WHERE member_type = ? ";
	    listParam.add(QueryExecuteAdapter.QueryParam("string", memberType));
	    jrs = QueryExecuteAdapter.QueryExecute(sql, listParam, functionName);
	    while (jrs.next()) {
		mdlCardLink.card_image_link = jrs.getString("card_image_link");
		mdlCardLink.account_image_link = jrs.getString("account_image_link");
	    }
	} catch (Exception ex) {
	    logger.error("FAILED. Function : " + functionName + ", memberType : " + memberType + ", Exception:" + ex.toString(), ex);
	}
	return mdlCardLink;
    }

    public static List<model.mdlMobileConfig> GetMobileConfigList() {
	String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();
	List<model.mdlMobileConfig> mobileConfigList = new ArrayList<model.mdlMobileConfig>();
	CachedRowSet jrs = null;
	String sql = "";
	try {
	    sql = "SELECT mobile_config_id, config_type, config_value FROM ms_mobile_config";
	    jrs = QueryExecuteAdapter.QueryExecute(sql, functionName);
	    while (jrs.next()) {
		model.mdlMobileConfig mdlMobileConfig = new model.mdlMobileConfig();
		mdlMobileConfig.mobile_config_id = jrs.getString("mobile_config_id");
		mdlMobileConfig.config_type = jrs.getString("config_type");
		mdlMobileConfig.config_value = jrs.getString("config_value");
		mobileConfigList.add(mdlMobileConfig);
	    }
	} catch (Exception ex) {
	    logger.error("FAILED. Function : " + functionName + ", Exception:" + ex.toString(), ex);
	}
	return mobileConfigList;
    }

    public static boolean InsertUpdateMobileConfig(model.mdlMobileConfig mdlMobileConfig) {
	boolean success = false;
	String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();
	List<mdlQueryExecute> listParam = new ArrayList<mdlQueryExecute>();
	String sql = "";
	try {
	    sql = "BEGIN TRAN UPDATE ms_mobile_config SET " + "config_value = ? WHERE config_type = ? " + "IF @@rowcount = 0 BEGIN INSERT INTO ms_mobile_config " + "(config_type, config_value) VALUES " + "(?, ?) END COMMIT TRAN";
	    listParam.add(QueryExecuteAdapter.QueryParam("string", mdlMobileConfig.config_value));
	    listParam.add(QueryExecuteAdapter.QueryParam("string", mdlMobileConfig.config_type));

	    listParam.add(QueryExecuteAdapter.QueryParam("string", mdlMobileConfig.config_type));
	    listParam.add(QueryExecuteAdapter.QueryParam("string", mdlMobileConfig.config_value));
	    success = QueryExecuteAdapter.QueryManipulate(sql, listParam, functionName);
	} catch (Exception ex) {
	    logger.error("FAILED. Function : " + functionName + ", mdlMobileConfig : " + gson.toJson(mdlMobileConfig) + ", Exception:" + ex.toString(), ex);
	}
	return success;
    }

    public static boolean DeleteMobileConfig(String mobile_config_id) {
	boolean success = false;
	String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();
	List<mdlQueryExecute> listParam = new ArrayList<mdlQueryExecute>();
	String sql = "";
	try {
	    sql = "DELETE FROM ms_mobile_config WHERE mobile_config_id = ? ";
	    listParam.add(QueryExecuteAdapter.QueryParam("string", mobile_config_id));
	    success = QueryExecuteAdapter.QueryManipulate(sql, listParam, functionName);
	} catch (Exception ex) {
	    logger.error("FAILED. Function : " + functionName + ", mobile_config_id : " + mobile_config_id + ", Exception:" + ex.toString(), ex);
	}
	return success;
    }

    public static boolean InsertRating(model.mdlRating mdlRating) {
	boolean success = false;
	String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();
	List<mdlQueryExecute> listParam = new ArrayList<mdlQueryExecute>();
	String sql = "";
	try {
	    sql = "INSERT INTO ms_rating (username, rating, comment) VALUES (?, ?, ? ) ";
	    listParam.add(QueryExecuteAdapter.QueryParam("string", mdlRating.username));
	    listParam.add(QueryExecuteAdapter.QueryParam("integer", mdlRating.rating));
	    listParam.add(QueryExecuteAdapter.QueryParam("string", mdlRating.comment == null ? "" : mdlRating.comment));
	    success = QueryExecuteAdapter.QueryManipulate(sql, listParam, functionName);
	} catch (Exception ex) {
	    logger.error("FAILED. Function : " + functionName + ", mdlRating : " + gson.toJson(mdlRating) + ", Exception:" + ex.toString(), ex);
	}

	return success;
    }

    public static List<model.mdlRating> GetRating(String pageNumber, String pageSize, String search) {
	String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();
	List<model.mdlRating> ratingList = new ArrayList<model.mdlRating>();
	CachedRowSet jrs = null;
	String sql = "";
	String search_part = "";
	try {
	    if (search != null) {
		search_part = "WHERE username LIKE ? OR rating LIKE ? OR comment LIKE ? ";
	    }
	    sql = "SELECT username, rating, comment FROM ms_rating " + search_part + "ORDER BY create_date DESC OFFSET ? ROWS FETCH NEXT ? ROWS ONLY;";
	    List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();

	    if (search != null) {
		String searchString = "%" + search.trim().replace(" ", "%") + "%";
		listParam.add(QueryExecuteAdapter.QueryParam("string", searchString));
		listParam.add(QueryExecuteAdapter.QueryParam("string", searchString));
		listParam.add(QueryExecuteAdapter.QueryParam("string", searchString));
	    }
	    listParam.add(QueryExecuteAdapter.QueryParam("int", ((Integer.parseInt(pageNumber) - 1) * Integer.parseInt(pageSize))));
	    listParam.add(QueryExecuteAdapter.QueryParam("int", Integer.parseInt(pageSize)));

	    jrs = QueryExecuteAdapter.QueryExecute(sql, listParam, functionName);
	    while (jrs.next()) {
		model.mdlRating mdlRating = new model.mdlRating();
		mdlRating.username = jrs.getString("username");
		mdlRating.rating = jrs.getInt("rating");
		mdlRating.comment = jrs.getString("comment");
		ratingList.add(mdlRating);
	    }
	} catch (Exception ex) {
	    logger.error("FAILED. Function : " + functionName + ", Exception:" + ex.toString(), ex);
	}
	return ratingList;
    }

    public static int GetRatingTotalList(String search) {
	String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();
	CachedRowSet crs = null;
	String sql = "";
	int return_value = 0;
	String search_part = "";
	try {
	    if (search != null) {
		search_part = "WHERE username LIKE ? OR rating LIKE ? OR comment LIKE ? ";
	    }
	    sql = "SELECT COUNT(*) total " + "FROM ms_rating " + search_part;
	    List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
	    if (search != null) {
		String searchString = "%" + search.trim().replace(" ", "%") + "%";
		listParam.add(QueryExecuteAdapter.QueryParam("string", searchString));
		listParam.add(QueryExecuteAdapter.QueryParam("string", searchString));
		listParam.add(QueryExecuteAdapter.QueryParam("string", searchString));
	    }

	    crs = QueryExecuteAdapter.QueryExecute(sql, listParam, functionName);

	    while (crs.next()) {
		return_value = crs.getInt("total");
	    }
	} catch (Exception ex) {
	    logger.error("FAILED. Function : " + functionName + ", Exception:" + ex.toString(), ex);
	}
	return return_value;
    }

    public static boolean SendEmail(model.mdlEmail mdlEmail) {
	String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();
	boolean success = false;
	try {
	    String to = mdlEmail.email_to;
	    String from = "inventlogistic2016@gmail.com";
	    String host = "smtp.gmail.com";
	    String SSL_FACTORY = "javax.net.ssl.SSLSocketFactory";
	    Properties properties = System.getProperties();
	    properties.setProperty("mail.smtp.host", host);
	    properties.setProperty("mail.smtp.socketFactory.class", SSL_FACTORY);
	    properties.setProperty("mail.smtp.socketFactory.fallback", "false");
	    properties.setProperty("mail.smtp.port", "465");
	    properties.setProperty("mail.smtp.socketFactory.port", "465");
	    properties.put("mail.smtp.auth", "true");
	    properties.put("mail.debug", "true");
	    properties.put("mail.store.protocol", "pop3");
	    properties.put("mail.transport.protocol", "smtp");

	    String username = from;
	    String password = "invent2016";
	    Session session = Session.getDefaultInstance(properties, new Authenticator() {
		@Override
		protected PasswordAuthentication getPasswordAuthentication() {
		    return new PasswordAuthentication(username, password);
		}
	    });

	    MimeMessage message = new MimeMessage(session);
	    message.setFrom(new InternetAddress(from));
	    message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));
	    message.setSubject(mdlEmail.email_subject);

	    String HtmlString = mdlEmail.email_message;

	    message.setContent(HtmlString, "text/html");

	    Transport.send(message);
	    success = true;
	} catch (Exception ex) {
	    logger.error("FAILED. Function : " + functionName + ", Exception:" + ex.toString(), ex);
	}
	return success;
    }
}
