package adapter;

import java.util.ArrayList;
import java.util.List;

import javax.sql.rowset.CachedRowSet;

import org.apache.log4j.Logger;

import com.google.gson.Gson;

import database.QueryExecuteAdapter;
import model.mdlQueryExecute;

public class TermConditionAdapter {
    final static Logger logger = Logger.getLogger(TermConditionAdapter.class);
    static Gson gson = new Gson();

    public static model.mdlTermCondition GetTermAndCondition(String type) {
	String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();
	model.mdlTermCondition mdlTermCondition = new model.mdlTermCondition();
	List<mdlQueryExecute> listParam = new ArrayList<mdlQueryExecute>();
	CachedRowSet jrs = null;
	String sql = "";
	try {
	    sql = "SELECT term_id, type, english_text, indonesian_text FROM ms_term_condition WHERE type = ? ";
	    listParam.add(QueryExecuteAdapter.QueryParam("string", type));
	    jrs = QueryExecuteAdapter.QueryExecute(sql, listParam, functionName);
	    while (jrs.next()) {
		mdlTermCondition.term_id = jrs.getString(1);
		mdlTermCondition.type = jrs.getString(2);
		mdlTermCondition.english_text = jrs.getString(3);
		mdlTermCondition.indonesian_text = jrs.getString(4);
	    }
	} catch (Exception ex) {
	    LogAdapter.InsertLog(ex.toString(), functionName, sql, "");
	    logger.error("FAILED. Function : " + functionName + ", Exception:" + ex.toString(), ex);
	}
	return mdlTermCondition;
    }

    public static List<model.mdlTermCondition> GetTermConditionList() {
	String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();
	List<model.mdlTermCondition> termConditionList = new ArrayList<model.mdlTermCondition>();
	CachedRowSet jrs = null;
	String sql = "";
	try {
	    sql = "SELECT term_id, type, english_text, indonesian_text FROM ms_term_condition " 
		    + "ORDER BY term_id ASC";

	    jrs = QueryExecuteAdapter.QueryExecute(sql, functionName);
	    while (jrs.next()) {
		model.mdlTermCondition mdlTermCondition = new model.mdlTermCondition();
		mdlTermCondition.term_id = jrs.getString("term_id");
		mdlTermCondition.type = jrs.getString("type");
		mdlTermCondition.indonesian_text = jrs.getString("indonesian_text");
		mdlTermCondition.english_text = jrs.getString("english_text");
		termConditionList.add(mdlTermCondition);
	    }
	} catch (Exception ex) {
	    LogAdapter.InsertLog(ex.toString(), functionName, sql, "");
	    logger.error("FAILED. Function : " + functionName + ", Exception:" + ex.toString(), ex);
	}
	return termConditionList;
    }

    public static boolean InsertUpdateTermCondition(model.mdlTermCondition mdlTermCondition) {
	String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();
	List<mdlQueryExecute> listParam = new ArrayList<mdlQueryExecute>();
	String sql = "";
	boolean success = false;

	try {
	    sql = "BEGIN TRAN " + "UPDATE ms_term_condition SET indonesian_text = ?, english_text = ? " 
		    + "WHERE term_id = ? " + "IF @@rowcount = 0 " 
		    + "BEGIN INSERT INTO ms_term_condition (type, indonesian_text, english_text) " 
		    + "VALUES (?, ?, ?) END COMMIT TRAN";
	    listParam.add(QueryExecuteAdapter.QueryParam("string", mdlTermCondition.indonesian_text == null ? "" : mdlTermCondition.indonesian_text));
	    listParam.add(QueryExecuteAdapter.QueryParam("string", mdlTermCondition.english_text == null ? "" : mdlTermCondition.english_text));
	    listParam.add(QueryExecuteAdapter.QueryParam("string", mdlTermCondition.term_id));
	    
	    listParam.add(QueryExecuteAdapter.QueryParam("string", mdlTermCondition.type));
	    listParam.add(QueryExecuteAdapter.QueryParam("string", mdlTermCondition.indonesian_text == null ? "" : mdlTermCondition.indonesian_text));
	    listParam.add(QueryExecuteAdapter.QueryParam("string", mdlTermCondition.english_text == null ? "" : mdlTermCondition.english_text));
	    success = QueryExecuteAdapter.QueryManipulate(sql, listParam, functionName);
	} catch (Exception ex) {
	    LogAdapter.InsertLog(ex.toString(), functionName, sql, "");
	    logger.error("FAILED. Function : " + functionName + ", mdlTermCondition : " + gson.toJson(mdlTermCondition) + ", Exception:" + ex.toString(), ex);
	}
	return success;
    }

    public static boolean DeleteTermCondition(String term_id) {
	String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();
	List<mdlQueryExecute> listParam = new ArrayList<mdlQueryExecute>();
	String sql = "";
	boolean success = false;

	try {
	    sql = "DELETE FROM ms_term_condition WHERE term_id = ?";
	    listParam.add(QueryExecuteAdapter.QueryParam("string", term_id));
	    success = QueryExecuteAdapter.QueryManipulate(sql, listParam, functionName);
	} catch (Exception ex) {
	    logger.error("FAILED. Function : " + functionName + ", term_id : " + term_id + ", Exception:" + ex.toString(), ex);
	}
	return success;
    }
}
