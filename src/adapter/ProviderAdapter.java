package adapter;

import java.util.ArrayList;
import java.util.List;

import javax.sql.rowset.CachedRowSet;

import org.apache.log4j.Logger;

import database.QueryExecuteAdapter;
import model.mdlQueryExecute;

public class ProviderAdapter {
    final static Logger logger = Logger.getLogger(ProviderAdapter.class);
    static String source = "Web Server";

    public static List<model.mdlProvider> GetNearestProvider(model.mdlLatitudeLongitude mdlLatitudeLongitude) {
	String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();
	List<model.mdlProvider> providerList = new ArrayList<model.mdlProvider>();
	List<mdlQueryExecute> listParam = new ArrayList<mdlQueryExecute>();
	CachedRowSet jrs = null;
	String sql = "";
	try {
	    sql = "DECLARE @Latitude as varchar(max) = ? DECLARE @Longitude as varchar(max) = ? DECLARE @orig as geography = geography::Point(@Latitude,@Longitude,4326) " 
	+ "select * from ( select a.*, FLOOR(@orig.STDistance(geography::Point(Convert(float,a.Latitude),Convert(float,a.Longitude), 4326))) as distance " 
		    + "from ms_provider a ) tbl where tbl.distance < 3000 " + "ORDER BY tbl.nama_provider ASC";
	    // + "ORDER BY tbl.distance ASC";

	    listParam.add(QueryExecuteAdapter.QueryParam("string", mdlLatitudeLongitude.latitude));
	    listParam.add(QueryExecuteAdapter.QueryParam("string", mdlLatitudeLongitude.longitude));
	    jrs = QueryExecuteAdapter.QueryExecute(sql, listParam, functionName);
	    while (jrs.next()) {
		model.mdlProvider provider = new model.mdlProvider();
		provider.provider_id = jrs.getString("provider_id");
		provider.tipe_provider = jrs.getString("tipe_provider");
		provider.tipe_rekanan = jrs.getString("tipe_rekanan");
		provider.nama_provider = jrs.getString("nama_provider");
		provider.alamat = jrs.getString("alamat");
		provider.no_telp1 = jrs.getString("no_telp1");
		provider.no_telp2 = jrs.getString("no_telp2");
		provider.no_telp3 = jrs.getString("no_telp3");
		provider.no_telp4 = jrs.getString("no_telp4");
		provider.kota = jrs.getString("kota");
		provider.provinsi = jrs.getString("provinsi");
		provider.latitude = jrs.getString("latitude");
		provider.longitude = jrs.getString("longitude");
		model.mdlProviderPerawatan mdlPerawatan = new model.mdlProviderPerawatan();
		mdlPerawatan.perawatan_presiden = jrs.getDouble("perawatan_presiden");
		mdlPerawatan.perawatan_svip = jrs.getDouble("perawatan_svip");
		mdlPerawatan.perawatan_vvip = jrs.getDouble("perawatan_vvip");
		mdlPerawatan.perawatan_vip = jrs.getDouble("perawatan_vip");
		mdlPerawatan.perawatan_utama = jrs.getDouble("perawatan_utama");
		mdlPerawatan.perawatan_kelas1 = jrs.getDouble("perawatan_kelas1");
		mdlPerawatan.perawatan_kelas2 = jrs.getDouble("perawatan_kelas2");
		mdlPerawatan.perawatan_kelas3 = jrs.getDouble("perawatan_kelas3");
		provider.perawatan = mdlPerawatan;
		model.mdlProviderPersalinan mdlPersalinan = new model.mdlProviderPersalinan();
		mdlPersalinan.persalinan_presiden = jrs.getDouble("persalinan_presiden");
		mdlPersalinan.persalinan_svip = jrs.getDouble("persalinan_svip");
		mdlPersalinan.persalinan_vvip = jrs.getDouble("persalinan_vvip");
		mdlPersalinan.persalinan_vip = jrs.getDouble("persalinan_vip");
		mdlPersalinan.persalinan_utama = jrs.getDouble("persalinan_utama");
		mdlPersalinan.persalinan_kelas1 = jrs.getDouble("persalinan_kelas1");
		mdlPersalinan.persalinan_kelas2 = jrs.getDouble("persalinan_kelas2");
		mdlPersalinan.persalinan_kelas3 = jrs.getDouble("persalinan_kelas3");
		provider.persalinan = mdlPersalinan;
		model.mdlProviderPersalinanSectio mdlPersalinanSectio = new model.mdlProviderPersalinanSectio();
		mdlPersalinanSectio.persalinan_sectio_presiden = jrs.getDouble("persalinan_sectio_presiden");
		mdlPersalinanSectio.persalinan_sectio_svip = jrs.getDouble("persalinan_sectio_svip");
		mdlPersalinanSectio.persalinan_sectio_vvip = jrs.getDouble("persalinan_sectio_vvip");
		mdlPersalinanSectio.persalinan_sectio_vip = jrs.getDouble("persalinan_sectio_vip");
		mdlPersalinanSectio.persalinan_sectio_utama = jrs.getDouble("persalinan_sectio_utama");
		mdlPersalinanSectio.persalinan_sectio_kelas1 = jrs.getDouble("persalinan_sectio_kelas1");
		mdlPersalinanSectio.persalinan_sectio_kelas2 = jrs.getDouble("persalinan_sectio_kelas2");
		mdlPersalinanSectio.persalinan_sectio_kelas3 = jrs.getDouble("persalinan_sectio_kelas3");
		provider.persalinan_sectio = mdlPersalinanSectio;
		model.mdlProviderRawatJalan mdlRawatJalan = new model.mdlProviderRawatJalan();
		mdlRawatJalan.rawat_jalan_dokter_umum = jrs.getDouble("rawat_jalan_dokter_umum");
		mdlRawatJalan.rawat_jalan_dokter_spesialis = jrs.getDouble("rawat_jalan_dokter_spesialis");
		provider.rawat_jalan = mdlRawatJalan;
		model.mdlProviderRawatInap mdlRawatInap = new model.mdlProviderRawatInap();
		mdlRawatInap.rawat_inap_presiden = jrs.getDouble("rawat_inap_presiden");
		mdlRawatInap.rawat_inap_svip = jrs.getDouble("rawat_inap_svip");
		mdlRawatInap.rawat_inap_vvip = jrs.getDouble("rawat_inap_vvip");
		mdlRawatInap.rawat_inap_vip = jrs.getDouble("rawat_inap_vip");
		mdlRawatInap.rawat_inap_utama = jrs.getDouble("rawat_inap_utama");
		mdlRawatInap.rawat_inap_kelas1 = jrs.getDouble("rawat_inap_kelas1");
		mdlRawatInap.rawat_inap_kelas2 = jrs.getDouble("rawat_inap_kelas2");
		mdlRawatInap.rawat_inap_kelas3 = jrs.getDouble("rawat_inap_kelas3");
		provider.rawat_inap = mdlRawatInap;
		Double distanceValue = jrs.getDouble(50);
		provider.distance = String.valueOf(distanceValue.intValue());
		providerList.add(provider);
	    }
	} catch (Exception ex) {
	    LogAdapter.InsertLog(ex.toString(), functionName, sql, source);
	    logger.error("FAILED. Function : " + functionName + ", Exception:" + ex.toString(), ex);
	}
	return providerList;
    }

    public static List<model.mdlProvider> GetProviderByName(String providerName, String pageString) {
	String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();
	int page = Integer.valueOf(pageString);
	int pagesize = 20;
	int page_start = page * pagesize - (pagesize - 1);
	int page_end = page * pagesize;

	List<model.mdlProvider> providerList = new ArrayList<model.mdlProvider>();
	List<mdlQueryExecute> listParam = new ArrayList<mdlQueryExecute>();
	CachedRowSet jrs = null;
	String sql = "";
	String providerNameString = "%" + providerName.trim().replace(" ", "%") + "%";
	try {
	    sql = "select * from (select ROW_NUMBER() OVER(ORDER BY a.nama_provider) AS row_number,a.* from ms_provider a " 
	+ "where a.nama_provider like ? OR a.alamat like ? OR a.kota like ? OR a.provinsi like ? ) tbl " + "WHERE tbl.row_number between ? and ? ";
	    listParam.add(QueryExecuteAdapter.QueryParam("string", providerNameString));
	    listParam.add(QueryExecuteAdapter.QueryParam("string", providerNameString));
	    listParam.add(QueryExecuteAdapter.QueryParam("string", providerNameString));
	    listParam.add(QueryExecuteAdapter.QueryParam("string", providerNameString));
	    listParam.add(QueryExecuteAdapter.QueryParam("int", page_start));
	    listParam.add(QueryExecuteAdapter.QueryParam("int", page_end));
	    jrs = QueryExecuteAdapter.QueryExecute(sql, listParam, functionName);
	    while (jrs.next()) {
		model.mdlProvider provider = new model.mdlProvider();
		provider.provider_id = jrs.getString("provider_id");
		provider.tipe_provider = jrs.getString("tipe_provider");
		provider.tipe_rekanan = jrs.getString("tipe_rekanan");
		provider.nama_provider = jrs.getString("nama_provider");
		provider.alamat = jrs.getString("alamat");
		provider.no_telp1 = jrs.getString("no_telp1");
		provider.no_telp2 = jrs.getString("no_telp2");
		provider.no_telp3 = jrs.getString("no_telp3");
		provider.no_telp4 = jrs.getString("no_telp4");
		provider.kota = jrs.getString("kota");
		provider.provinsi = jrs.getString("provinsi");
		provider.latitude = jrs.getString("latitude");
		provider.longitude = jrs.getString("longitude");
		model.mdlProviderPerawatan mdlPerawatan = new model.mdlProviderPerawatan();
		mdlPerawatan.perawatan_presiden = jrs.getDouble("perawatan_presiden");
		mdlPerawatan.perawatan_svip = jrs.getDouble("perawatan_svip");
		mdlPerawatan.perawatan_vvip = jrs.getDouble("perawatan_vvip");
		mdlPerawatan.perawatan_vip = jrs.getDouble("perawatan_vip");
		mdlPerawatan.perawatan_utama = jrs.getDouble("perawatan_utama");
		mdlPerawatan.perawatan_kelas1 = jrs.getDouble("perawatan_kelas1");
		mdlPerawatan.perawatan_kelas2 = jrs.getDouble("perawatan_kelas2");
		mdlPerawatan.perawatan_kelas3 = jrs.getDouble("perawatan_kelas3");
		provider.perawatan = mdlPerawatan;
		model.mdlProviderPersalinan mdlPersalinan = new model.mdlProviderPersalinan();
		mdlPersalinan.persalinan_presiden = jrs.getDouble("persalinan_presiden");
		mdlPersalinan.persalinan_svip = jrs.getDouble("persalinan_svip");
		mdlPersalinan.persalinan_vvip = jrs.getDouble("persalinan_vvip");
		mdlPersalinan.persalinan_vip = jrs.getDouble("persalinan_vip");
		mdlPersalinan.persalinan_utama = jrs.getDouble("persalinan_utama");
		mdlPersalinan.persalinan_kelas1 = jrs.getDouble("persalinan_kelas1");
		mdlPersalinan.persalinan_kelas2 = jrs.getDouble("persalinan_kelas2");
		mdlPersalinan.persalinan_kelas3 = jrs.getDouble("persalinan_kelas3");
		provider.persalinan = mdlPersalinan;
		model.mdlProviderPersalinanSectio mdlPersalinanSectio = new model.mdlProviderPersalinanSectio();
		mdlPersalinanSectio.persalinan_sectio_presiden = jrs.getDouble("persalinan_sectio_presiden");
		mdlPersalinanSectio.persalinan_sectio_svip = jrs.getDouble("persalinan_sectio_svip");
		mdlPersalinanSectio.persalinan_sectio_vvip = jrs.getDouble("persalinan_sectio_vvip");
		mdlPersalinanSectio.persalinan_sectio_vip = jrs.getDouble("persalinan_sectio_vip");
		mdlPersalinanSectio.persalinan_sectio_utama = jrs.getDouble("persalinan_sectio_utama");
		mdlPersalinanSectio.persalinan_sectio_kelas1 = jrs.getDouble("persalinan_sectio_kelas1");
		mdlPersalinanSectio.persalinan_sectio_kelas2 = jrs.getDouble("persalinan_sectio_kelas2");
		mdlPersalinanSectio.persalinan_sectio_kelas3 = jrs.getDouble("persalinan_sectio_kelas3");
		provider.persalinan_sectio = mdlPersalinanSectio;
		model.mdlProviderRawatJalan mdlRawatJalan = new model.mdlProviderRawatJalan();
		mdlRawatJalan.rawat_jalan_dokter_umum = jrs.getDouble("rawat_jalan_dokter_umum");
		mdlRawatJalan.rawat_jalan_dokter_spesialis = jrs.getDouble("rawat_jalan_dokter_spesialis");
		provider.rawat_jalan = mdlRawatJalan;
		model.mdlProviderRawatInap mdlRawatInap = new model.mdlProviderRawatInap();
		mdlRawatInap.rawat_inap_presiden = jrs.getDouble("rawat_inap_presiden");
		mdlRawatInap.rawat_inap_svip = jrs.getDouble("rawat_inap_svip");
		mdlRawatInap.rawat_inap_vvip = jrs.getDouble("rawat_inap_vvip");
		mdlRawatInap.rawat_inap_vip = jrs.getDouble("rawat_inap_vip");
		mdlRawatInap.rawat_inap_utama = jrs.getDouble("rawat_inap_utama");
		mdlRawatInap.rawat_inap_kelas1 = jrs.getDouble("rawat_inap_kelas1");
		mdlRawatInap.rawat_inap_kelas2 = jrs.getDouble("rawat_inap_kelas2");
		mdlRawatInap.rawat_inap_kelas3 = jrs.getDouble("rawat_inap_kelas3");
		provider.rawat_inap = mdlRawatInap;
		providerList.add(provider);
	    }
	} catch (Exception ex) {
	    LogAdapter.InsertLog(ex.toString(), functionName, sql, source);
	    logger.error("FAILED. Function : " + functionName + ", Exception:" + ex.toString(), ex);
	}
	return providerList;
    }

    public static int GetProviderTotalList(String id, String search) {
	String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();
	CachedRowSet crs = null;
	String sql = "";
	int return_value = 0;
	String search_part = "";
	try {
		if(search != null) {
			search_part = "AND nama_provider LIKE ? OR kota LIKE ? OR provinsi LIKE ? ";
		}
	    sql = "SELECT COUNT(*) total " + "FROM ms_provider " + "WHERE provider_id = COALESCE(?, provider_id) "+search_part;
	    List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
	    if (id != null) {
		listParam.add(QueryExecuteAdapter.QueryParam("string", id));
	    } else {
		listParam.add(QueryExecuteAdapter.QueryParam("null", java.sql.Types.NULL));
	    }
	    
	    if (search != null) {
	    	String searchString = "%" + search.trim().replace(" ", "%") + "%";
	    	listParam.add(QueryExecuteAdapter.QueryParam("string", searchString));
	    	listParam.add(QueryExecuteAdapter.QueryParam("string", searchString));
	    	listParam.add(QueryExecuteAdapter.QueryParam("string", searchString));
	    }

	    crs = QueryExecuteAdapter.QueryExecute(sql, listParam, functionName);

	    while (crs.next()) {
		return_value = crs.getInt("total");
	    }
	} catch (Exception ex) {
	    LogAdapter.InsertLog(ex.toString(), functionName, sql, source);
	    logger.error("FAILED. Function : GetProviderTotalList, Exception:" + ex.toString(), ex);
	}
	return return_value;
    }

    public static List<model.mdlProvider> GetProviderList(String id, String pageNumber, String pageSize, String search) {
	String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();
	List<model.mdlProvider> mdlProviderList = new ArrayList<model.mdlProvider>();
	CachedRowSet crs = null;
	String sql = "";
	String search_part = "";
	try {
		if(search != null) {
			search_part = "AND nama_provider LIKE ? OR kota LIKE ? OR provinsi LIKE ? ";
		}
	    sql = "SELECT provider_id, tipe_provider, tipe_rekanan, nama_provider, alamat, " 
			+ "no_telp1, no_telp2, no_telp3, no_telp4, " 
			+ "kota, provinsi, latitude, longitude, " 
			+ "perawatan_presiden, perawatan_svip, perawatan_vvip, perawatan_vip, perawatan_utama, perawatan_kelas1, perawatan_kelas2, perawatan_kelas3, " 
			+ "persalinan_presiden, persalinan_svip, persalinan_vvip, persalinan_vip, persalinan_utama, persalinan_kelas1, persalinan_kelas2, persalinan_kelas3, " 
			+ "persalinan_sectio_presiden, persalinan_sectio_svip, persalinan_sectio_vvip, persalinan_sectio_vip, persalinan_sectio_utama, persalinan_sectio_kelas1, persalinan_sectio_kelas2, persalinan_sectio_kelas3, " 
			+ "rawat_jalan_dokter_umum, rawat_jalan_dokter_spesialis, " 
			+ "rawat_inap_presiden, rawat_inap_svip, rawat_inap_vvip, rawat_inap_vip, rawat_inap_utama, rawat_inap_kelas1, rawat_inap_kelas2, rawat_inap_kelas3, " 
			+ "create_date, update_date " 
			+ "FROM ms_provider " 
			+ "WHERE provider_id = COALESCE(?, provider_id) " 
			+ search_part
			+ "ORDER BY provider_id " 
			+ "OFFSET ? ROWS FETCH NEXT ? ROWS ONLY;";
	    List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
	    if (id != null) {
		listParam.add(QueryExecuteAdapter.QueryParam("string", id));
	    } else {
		listParam.add(QueryExecuteAdapter.QueryParam("null", java.sql.Types.NULL));
	    }
	    
	    if (search != null) {
	    	String searchString = "%" + search.trim().replace(" ", "%") + "%";
	    	listParam.add(QueryExecuteAdapter.QueryParam("string", searchString));
	    	listParam.add(QueryExecuteAdapter.QueryParam("string", searchString));
	    	listParam.add(QueryExecuteAdapter.QueryParam("string", searchString));
	    }

	    listParam.add(QueryExecuteAdapter.QueryParam("int", ((Integer.parseInt(pageNumber) - 1) * Integer.parseInt(pageSize))));
	    listParam.add(QueryExecuteAdapter.QueryParam("int", Integer.parseInt(pageSize)));

	    crs = QueryExecuteAdapter.QueryExecute(sql, listParam, functionName);

	    while (crs.next()) {
		model.mdlProvider mdlProvider = new model.mdlProvider();

		mdlProvider.provider_id = crs.getString("provider_id");
		mdlProvider.tipe_provider = crs.getString("tipe_provider");
		mdlProvider.tipe_rekanan = crs.getString("tipe_rekanan");
		mdlProvider.nama_provider = crs.getString("nama_provider");
		mdlProvider.alamat = crs.getString("alamat");
		mdlProvider.no_telp1 = crs.getString("no_telp1");
		mdlProvider.no_telp2 = crs.getString("no_telp2");
		mdlProvider.no_telp3 = crs.getString("no_telp3");
		mdlProvider.no_telp4 = crs.getString("no_telp4");
		mdlProvider.kota = crs.getString("kota");
		mdlProvider.provinsi = crs.getString("provinsi");
		mdlProvider.latitude = crs.getString("latitude");
		mdlProvider.longitude = crs.getString("longitude");

		model.mdlProviderPerawatan mdlProviderPerawatan = new model.mdlProviderPerawatan();
		mdlProviderPerawatan.perawatan_presiden = crs.getDouble("perawatan_presiden");
		mdlProviderPerawatan.perawatan_svip = crs.getDouble("perawatan_svip");
		mdlProviderPerawatan.perawatan_vvip = crs.getDouble("perawatan_vvip");
		mdlProviderPerawatan.perawatan_vip = crs.getDouble("perawatan_vip");
		mdlProviderPerawatan.perawatan_utama = crs.getDouble("perawatan_utama");
		mdlProviderPerawatan.perawatan_kelas1 = crs.getDouble("perawatan_kelas1");
		mdlProviderPerawatan.perawatan_kelas2 = crs.getDouble("perawatan_kelas2");
		mdlProviderPerawatan.perawatan_kelas3 = crs.getDouble("perawatan_kelas3");
		mdlProvider.perawatan = mdlProviderPerawatan;

		model.mdlProviderPersalinan mdlProviderPersalinan = new model.mdlProviderPersalinan();
		mdlProviderPersalinan.persalinan_presiden = crs.getDouble("persalinan_presiden");
		mdlProviderPersalinan.persalinan_svip = crs.getDouble("persalinan_svip");
		mdlProviderPersalinan.persalinan_vvip = crs.getDouble("persalinan_vvip");
		mdlProviderPersalinan.persalinan_vip = crs.getDouble("persalinan_vip");
		mdlProviderPersalinan.persalinan_utama = crs.getDouble("persalinan_utama");
		mdlProviderPersalinan.persalinan_kelas1 = crs.getDouble("persalinan_kelas1");
		mdlProviderPersalinan.persalinan_kelas2 = crs.getDouble("persalinan_kelas2");
		mdlProviderPersalinan.persalinan_kelas3 = crs.getDouble("persalinan_kelas3");
		mdlProvider.persalinan = mdlProviderPersalinan;

		model.mdlProviderPersalinanSectio mdlProviderPersalinanSectio = new model.mdlProviderPersalinanSectio();
		mdlProviderPersalinanSectio.persalinan_sectio_presiden = crs.getDouble("persalinan_sectio_presiden");
		mdlProviderPersalinanSectio.persalinan_sectio_svip = crs.getDouble("persalinan_sectio_svip");
		mdlProviderPersalinanSectio.persalinan_sectio_vvip = crs.getDouble("persalinan_sectio_vvip");
		mdlProviderPersalinanSectio.persalinan_sectio_vip = crs.getDouble("persalinan_sectio_vip");
		mdlProviderPersalinanSectio.persalinan_sectio_utama = crs.getDouble("persalinan_sectio_vip");
		mdlProviderPersalinanSectio.persalinan_sectio_kelas1 = crs.getDouble("persalinan_sectio_kelas1");
		mdlProviderPersalinanSectio.persalinan_sectio_kelas2 = crs.getDouble("persalinan_sectio_kelas2");
		mdlProviderPersalinanSectio.persalinan_sectio_kelas3 = crs.getDouble("persalinan_sectio_kelas3");
		mdlProvider.persalinan_sectio = mdlProviderPersalinanSectio;

		model.mdlProviderRawatJalan mdlProviderRawatJalan = new model.mdlProviderRawatJalan();
		mdlProviderRawatJalan.rawat_jalan_dokter_umum = crs.getDouble("rawat_jalan_dokter_umum");
		mdlProviderRawatJalan.rawat_jalan_dokter_spesialis = crs.getDouble("rawat_jalan_dokter_spesialis");
		mdlProvider.rawat_jalan = mdlProviderRawatJalan;

		model.mdlProviderRawatInap mdlProviderRawatInap = new model.mdlProviderRawatInap();
		mdlProviderRawatInap.rawat_inap_presiden = crs.getDouble("rawat_inap_presiden");
		mdlProviderRawatInap.rawat_inap_svip = crs.getDouble("rawat_inap_svip");
		mdlProviderRawatInap.rawat_inap_vvip = crs.getDouble("rawat_inap_vvip");
		mdlProviderRawatInap.rawat_inap_vip = crs.getDouble("rawat_inap_vip");
		mdlProviderRawatInap.rawat_inap_utama = crs.getDouble("rawat_inap_utama");
		mdlProviderRawatInap.rawat_inap_kelas1 = crs.getDouble("rawat_inap_kelas1");
		mdlProviderRawatInap.rawat_inap_kelas2 = crs.getDouble("rawat_inap_kelas2");
		mdlProviderRawatInap.rawat_inap_kelas3 = crs.getDouble("rawat_inap_kelas3");
		mdlProvider.rawat_inap = mdlProviderRawatInap;
		mdlProviderList.add(mdlProvider);
	    }
	} catch (Exception ex) {
	    LogAdapter.InsertLog(ex.toString(), functionName, sql, source);
	    logger.error("FAILED. Function : GetProviderList, Exception:" + ex.toString(), ex);
	}
	return mdlProviderList;
    }

    public static boolean InsertUpdateProvider(model.mdlProvider param) {
	String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();
	List<mdlQueryExecute> listParam = new ArrayList<mdlQueryExecute>();
	String sql = "";
	boolean success = false;

	try {
	    sql = "BEGIN TRAN UPDATE ms_provider SET " 
		    + "tipe_provider = ?, nama_provider = ?, alamat = ?, no_telp1 = ?, no_telp2 = ?, " 
		    + "no_telp3 = ?, no_telp4 = ?, kota = ?, provinsi = ?, latitude = ?, " 
		    + "longitude = ?, update_date = CURRENT_TIMESTAMP, " 
		    + "perawatan_presiden = ?, perawatan_svip = ?, perawatan_vvip = ?, perawatan_vip = ?, perawatan_utama = ?, " 
		    + "perawatan_kelas1 = ?, perawatan_kelas2 = ?, perawatan_kelas3 = ?, " 
		    + "persalinan_presiden = ?, persalinan_svip = ?, persalinan_vvip = ?, persalinan_vip = ?, persalinan_utama = ?, " 
		    + "persalinan_kelas1 = ?, persalinan_kelas2 = ?, persalinan_kelas3 = ?, " 
		    + "persalinan_sectio_presiden = ?, persalinan_sectio_svip = ?, persalinan_sectio_vvip = ?, persalinan_sectio_vip = ?, persalinan_sectio_utama = ?, " 
		    + "persalinan_sectio_kelas1 = ?, persalinan_sectio_kelas2 = ?, persalinan_sectio_kelas3 = ?, " 
		    + "rawat_jalan_dokter_umum = ?, rawat_jalan_dokter_spesialis = ?, " 
		    + "rawat_inap_presiden = ?, rawat_inap_svip = ?, rawat_inap_vvip = ?, rawat_inap_vip = ?, rawat_inap_utama = ?, " 
		    + "rawat_inap_kelas1 = ?, rawat_inap_kelas2 = ?, rawat_inap_kelas3 = ?, tipe_rekanan = ?  " 
		    + "WHERE provider_id = ? " 
		    + "IF @@rowcount = 0 BEGIN INSERT INTO ms_provider " 
		    + "(tipe_provider, nama_provider, alamat, no_telp1, no_telp2, " 
		    + "no_telp3, no_telp4, kota, provinsi, latitude, " 
		    + "longitude, create_date, " 
		    + "perawatan_presiden, perawatan_svip, perawatan_vvip, perawatan_vip, perawatan_utama, perawatan_kelas1, perawatan_kelas2, perawatan_kelas3, " 
		    + "persalinan_presiden, persalinan_svip, persalinan_vvip, persalinan_vip, persalinan_utama, " 
		    + "persalinan_kelas1, persalinan_kelas2, persalinan_kelas3, " 
		    + "persalinan_sectio_presiden, persalinan_sectio_svip, persalinan_sectio_vvip, persalinan_sectio_vip, persalinan_sectio_utama, " 
		    + "persalinan_sectio_kelas1, persalinan_sectio_kelas2, persalinan_sectio_kelas3, " 
		    + "rawat_jalan_dokter_umum, rawat_jalan_dokter_spesialis, " 
		    + "rawat_inap_presiden, rawat_inap_svip, rawat_inap_vvip, rawat_inap_vip, rawat_inap_utama, " 
		    + "rawat_inap_kelas1, rawat_inap_kelas2, rawat_inap_kelas3,"
		    + "tipe_rekanan ) " 
		    + "VALUES " 
		    + "(?, ?, ?, ?, ?, " 
		    + "?, ?, ?, ?, ?, " 
		    + "?, CURRENT_TIMESTAMP, " 
		    + "?, ?, ?, ?, ?, ?, ?, ?, " 
		    + "?, ?, ?, ?, ?, ?, ?, ?, " 
		    + "?, ?, ?, ?, ?, ?, ?, ?, " 
		    + "?, ?, " 
		    + "?, ?, ?, ?, ?, ?, ?, ?, "
		    + "? ) END COMMIT TRAN";
	    listParam.add(QueryExecuteAdapter.QueryParam("string", param.tipe_provider));
	    listParam.add(QueryExecuteAdapter.QueryParam("string", param.nama_provider));
	    listParam.add(QueryExecuteAdapter.QueryParam("string", param.alamat));
	    listParam.add(QueryExecuteAdapter.QueryParam("string", param.no_telp1));
	    listParam.add(QueryExecuteAdapter.QueryParam("string", param.no_telp2));

	    listParam.add(QueryExecuteAdapter.QueryParam("string", param.no_telp3));
	    listParam.add(QueryExecuteAdapter.QueryParam("string", param.no_telp4));
	    listParam.add(QueryExecuteAdapter.QueryParam("string", param.kota));
	    listParam.add(QueryExecuteAdapter.QueryParam("string", param.provinsi));
	    listParam.add(QueryExecuteAdapter.QueryParam("string", param.latitude));

	    listParam.add(QueryExecuteAdapter.QueryParam("string", param.longitude));

	    listParam.add(QueryExecuteAdapter.QueryParam("double", param.perawatan.perawatan_presiden));
	    listParam.add(QueryExecuteAdapter.QueryParam("double", param.perawatan.perawatan_svip));
	    listParam.add(QueryExecuteAdapter.QueryParam("double", param.perawatan.perawatan_vvip));
	    listParam.add(QueryExecuteAdapter.QueryParam("double", param.perawatan.perawatan_vip));
	    listParam.add(QueryExecuteAdapter.QueryParam("double", param.perawatan.perawatan_utama));

	    listParam.add(QueryExecuteAdapter.QueryParam("double", param.perawatan.perawatan_kelas1));
	    listParam.add(QueryExecuteAdapter.QueryParam("double", param.perawatan.perawatan_kelas2));
	    listParam.add(QueryExecuteAdapter.QueryParam("double", param.perawatan.perawatan_kelas3));

	    listParam.add(QueryExecuteAdapter.QueryParam("double", param.persalinan.persalinan_presiden));
	    listParam.add(QueryExecuteAdapter.QueryParam("double", param.persalinan.persalinan_svip));
	    listParam.add(QueryExecuteAdapter.QueryParam("double", param.persalinan.persalinan_vvip));
	    listParam.add(QueryExecuteAdapter.QueryParam("double", param.persalinan.persalinan_vip));
	    listParam.add(QueryExecuteAdapter.QueryParam("double", param.persalinan.persalinan_utama));

	    listParam.add(QueryExecuteAdapter.QueryParam("double", param.persalinan.persalinan_kelas1));
	    listParam.add(QueryExecuteAdapter.QueryParam("double", param.persalinan.persalinan_kelas2));
	    listParam.add(QueryExecuteAdapter.QueryParam("double", param.persalinan.persalinan_kelas3));

	    listParam.add(QueryExecuteAdapter.QueryParam("double", param.persalinan_sectio.persalinan_sectio_presiden));
	    listParam.add(QueryExecuteAdapter.QueryParam("double", param.persalinan_sectio.persalinan_sectio_svip));
	    listParam.add(QueryExecuteAdapter.QueryParam("double", param.persalinan_sectio.persalinan_sectio_vvip));
	    listParam.add(QueryExecuteAdapter.QueryParam("double", param.persalinan_sectio.persalinan_sectio_vip));
	    listParam.add(QueryExecuteAdapter.QueryParam("double", param.persalinan_sectio.persalinan_sectio_utama));

	    listParam.add(QueryExecuteAdapter.QueryParam("double", param.persalinan_sectio.persalinan_sectio_kelas1));
	    listParam.add(QueryExecuteAdapter.QueryParam("double", param.persalinan_sectio.persalinan_sectio_kelas2));
	    listParam.add(QueryExecuteAdapter.QueryParam("double", param.persalinan_sectio.persalinan_sectio_kelas3));

	    listParam.add(QueryExecuteAdapter.QueryParam("double", param.rawat_jalan.rawat_jalan_dokter_umum));
	    listParam.add(QueryExecuteAdapter.QueryParam("double", param.rawat_jalan.rawat_jalan_dokter_spesialis));

	    listParam.add(QueryExecuteAdapter.QueryParam("double", param.rawat_inap.rawat_inap_presiden));
	    listParam.add(QueryExecuteAdapter.QueryParam("double", param.rawat_inap.rawat_inap_svip));
	    listParam.add(QueryExecuteAdapter.QueryParam("double", param.rawat_inap.rawat_inap_vvip));
	    listParam.add(QueryExecuteAdapter.QueryParam("double", param.rawat_inap.rawat_inap_vip));
	    listParam.add(QueryExecuteAdapter.QueryParam("double", param.rawat_inap.rawat_inap_utama));

	    listParam.add(QueryExecuteAdapter.QueryParam("double", param.rawat_inap.rawat_inap_kelas1));
	    listParam.add(QueryExecuteAdapter.QueryParam("double", param.rawat_inap.rawat_inap_kelas2));
	    listParam.add(QueryExecuteAdapter.QueryParam("double", param.rawat_inap.rawat_inap_kelas3));
	    listParam.add(QueryExecuteAdapter.QueryParam("string", param.tipe_rekanan == null ? "3" : param.tipe_rekanan));

	    listParam.add(QueryExecuteAdapter.QueryParam("string", param.provider_id == null ? "0" : param.provider_id));

	    listParam.add(QueryExecuteAdapter.QueryParam("string", param.tipe_provider));
	    listParam.add(QueryExecuteAdapter.QueryParam("string", param.nama_provider));
	    listParam.add(QueryExecuteAdapter.QueryParam("string", param.alamat));
	    listParam.add(QueryExecuteAdapter.QueryParam("string", param.no_telp1));
	    listParam.add(QueryExecuteAdapter.QueryParam("string", param.no_telp2));

	    listParam.add(QueryExecuteAdapter.QueryParam("string", param.no_telp3));
	    listParam.add(QueryExecuteAdapter.QueryParam("string", param.no_telp4));
	    listParam.add(QueryExecuteAdapter.QueryParam("string", param.kota));
	    listParam.add(QueryExecuteAdapter.QueryParam("string", param.provinsi));
	    listParam.add(QueryExecuteAdapter.QueryParam("string", param.latitude));

	    listParam.add(QueryExecuteAdapter.QueryParam("string", param.longitude));

	    listParam.add(QueryExecuteAdapter.QueryParam("double", param.perawatan.perawatan_presiden));
	    listParam.add(QueryExecuteAdapter.QueryParam("double", param.perawatan.perawatan_svip));
	    listParam.add(QueryExecuteAdapter.QueryParam("double", param.perawatan.perawatan_vvip));
	    listParam.add(QueryExecuteAdapter.QueryParam("double", param.perawatan.perawatan_vip));
	    listParam.add(QueryExecuteAdapter.QueryParam("double", param.perawatan.perawatan_utama));

	    listParam.add(QueryExecuteAdapter.QueryParam("double", param.perawatan.perawatan_kelas1));
	    listParam.add(QueryExecuteAdapter.QueryParam("double", param.perawatan.perawatan_kelas2));
	    listParam.add(QueryExecuteAdapter.QueryParam("double", param.perawatan.perawatan_kelas3));

	    listParam.add(QueryExecuteAdapter.QueryParam("double", param.persalinan.persalinan_presiden));
	    listParam.add(QueryExecuteAdapter.QueryParam("double", param.persalinan.persalinan_svip));
	    listParam.add(QueryExecuteAdapter.QueryParam("double", param.persalinan.persalinan_vvip));
	    listParam.add(QueryExecuteAdapter.QueryParam("double", param.persalinan.persalinan_vip));
	    listParam.add(QueryExecuteAdapter.QueryParam("double", param.persalinan.persalinan_utama));

	    listParam.add(QueryExecuteAdapter.QueryParam("double", param.persalinan.persalinan_kelas1));
	    listParam.add(QueryExecuteAdapter.QueryParam("double", param.persalinan.persalinan_kelas2));
	    listParam.add(QueryExecuteAdapter.QueryParam("double", param.persalinan.persalinan_kelas3));

	    listParam.add(QueryExecuteAdapter.QueryParam("double", param.persalinan_sectio.persalinan_sectio_presiden));
	    listParam.add(QueryExecuteAdapter.QueryParam("double", param.persalinan_sectio.persalinan_sectio_svip));
	    listParam.add(QueryExecuteAdapter.QueryParam("double", param.persalinan_sectio.persalinan_sectio_vvip));
	    listParam.add(QueryExecuteAdapter.QueryParam("double", param.persalinan_sectio.persalinan_sectio_vip));
	    listParam.add(QueryExecuteAdapter.QueryParam("double", param.persalinan_sectio.persalinan_sectio_utama));

	    listParam.add(QueryExecuteAdapter.QueryParam("double", param.persalinan_sectio.persalinan_sectio_kelas1));
	    listParam.add(QueryExecuteAdapter.QueryParam("double", param.persalinan_sectio.persalinan_sectio_kelas2));
	    listParam.add(QueryExecuteAdapter.QueryParam("double", param.persalinan_sectio.persalinan_sectio_kelas3));

	    listParam.add(QueryExecuteAdapter.QueryParam("double", param.rawat_jalan.rawat_jalan_dokter_umum));
	    listParam.add(QueryExecuteAdapter.QueryParam("double", param.rawat_jalan.rawat_jalan_dokter_spesialis));

	    listParam.add(QueryExecuteAdapter.QueryParam("double", param.rawat_inap.rawat_inap_presiden));
	    listParam.add(QueryExecuteAdapter.QueryParam("double", param.rawat_inap.rawat_inap_svip));
	    listParam.add(QueryExecuteAdapter.QueryParam("double", param.rawat_inap.rawat_inap_vvip));
	    listParam.add(QueryExecuteAdapter.QueryParam("double", param.rawat_inap.rawat_inap_vip));
	    listParam.add(QueryExecuteAdapter.QueryParam("double", param.rawat_inap.rawat_inap_utama));

	    listParam.add(QueryExecuteAdapter.QueryParam("double", param.rawat_inap.rawat_inap_kelas1));
	    listParam.add(QueryExecuteAdapter.QueryParam("double", param.rawat_inap.rawat_inap_kelas2));
	    listParam.add(QueryExecuteAdapter.QueryParam("double", param.rawat_inap.rawat_inap_kelas3));
	    
	    listParam.add(QueryExecuteAdapter.QueryParam("string", param.tipe_rekanan == null ? "3" : param.tipe_rekanan));

	    success = QueryExecuteAdapter.QueryManipulate(sql, listParam, functionName);
	} catch (Exception ex) {
	    LogAdapter.InsertLog(ex.toString(), functionName, sql, source);
	    logger.error("FAILED. Function : " + functionName + ", provider_id : " + param.provider_id + ", Exception:" + ex.toString(), ex);
	}
	return success;
    }

    public static boolean DeleteProvider(String provider_id) {
	String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();
	List<mdlQueryExecute> listParam = new ArrayList<mdlQueryExecute>();
	String sql = "";
	boolean success = false;
	try {
	    sql = "DELETE FROM ms_provider WHERE provider_id = ?";
	    listParam.add(QueryExecuteAdapter.QueryParam("string", provider_id));
	    success = QueryExecuteAdapter.QueryManipulate(sql, listParam, functionName);
	} catch (Exception ex) {
	    logger.error("FAILED. Function : " + functionName + ", provider_id : " + provider_id + ", Exception:" + ex.toString(), ex);
	}
	return success;
    }
    
    public static model.mdlProviderCount GetProviderCountData(){
	String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();
	model.mdlProviderCount mdlProviderCount = new model.mdlProviderCount();
	CachedRowSet jrs = null;
	String sql = "";
	try {
	    sql = "SELECT (select count(tipe_provider) from ms_provider where tipe_provider = 'Rumah Sakit') as rumah_sakit_count,"
	    	+ "(select count(tipe_provider) from ms_provider where tipe_provider = 'Optik') as optik_count,"
	    	+ "(select count(tipe_provider) from ms_provider where tipe_provider = 'Klinik') as klinik_count,"
	    	+ "(select count(tipe_provider) from ms_provider where tipe_provider = 'Laboratorium') as lab_count,"
	    	+ "(select count(tipe_provider) from ms_provider) as provider_count";
	    jrs = QueryExecuteAdapter.QueryExecute(sql, functionName);
	    while (jrs.next()) {
		mdlProviderCount.rumah_sakit_count = jrs.getString(1);
		mdlProviderCount.optik_count = jrs.getString(2);
		mdlProviderCount.klinik_count = jrs.getString(3);
		mdlProviderCount.lab_count = jrs.getString(4);
		mdlProviderCount.provider_count = jrs.getString(5);
	    }
	} catch (Exception ex) {
	    logger.error("FAILED. Function : " + functionName + ", Exception:" + ex.toString(), ex);
	}
	return mdlProviderCount;
	
    }
}
