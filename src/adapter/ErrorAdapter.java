package adapter;

import model.ErrorStatus;

public class ErrorAdapter {
    public static model.mdlErrorSchema GetErrorSchema(ErrorStatus error) {
	model.mdlErrorSchema mdlErrorSchema = new model.mdlErrorSchema();
	model.mdlMessage mdlMessage = new model.mdlMessage();
	switch (error) {
	// 01 = API /login
	// 02 = API /get-slideshow
	// 03 = API /register
	// 04 = API /request-otp
	// 05 = API /verify-otp
	// 06 = API /set-password
	// 07 = API /forgot-password
	// 08 = API /get-nearest-provider
	// 09 = API /get-provider-by-name
	// 10 = API /term-condition
	// 11 = API /check-version
	// 12 = API /get-menu-member
	// 13 = API /get-help
	// 14 = API /get-news
	// 15 = API /get-message
	// 16 = API /update-message-read
	// 17 = API /upload-claim-document
	// 18 = API /logout
	// 19 = API /change-password
	// 20 = API /send-message
	// 21 = API /upload-claim
	// 22 = API /insert-update-news and /delete-news
	// 23 = API /get-claim-type
	// 24 = API /update-claim-status
	// 25 = API /get-claim-document-type
	// 26 = API /change-email
	// 27 = API /update-claim-document
	// 28 = API /add-account
	// 29 = API /get-claim
	// 30 = API /insert-update-provider
	// 31 = API /get-document-type-list
	// 32 = API /insert-update-document-type
	// 33 = API /insert-update-help
	// 34 = API /delete-help
	// 35 = API /get-account-list
	// 36 = API /get-mobile-config
	// 37 = API /insert-rating
	// 38 = API /confirm-password
	// 39 = API /get-nab
	// 40 = API /get-ffs
	// 41 = API /send-email
	// 42 = API /get-product
	// 43 = API /get-product-list
	// 44 = API /insert-update-product
	// 45 = API /delete-product
	// 46 = API /get-product-category-list
	// 47 = API /insert-update-product-category
	// 48 = API /delete-product-category
	// 49 = API /get-service
	// 50 = API /isnert-update-service
	// 51 = API /delete-service
	// 52 = API /get-username-list
	// 53 = API /get-broadcast-list

	case ERR_00_000:
	    mdlErrorSchema.error_code = "ERR-00-000";
	    mdlMessage.indonesian = "Sukses";
	    mdlMessage.english = "Success";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;
	case ERR_01_001:
	    mdlErrorSchema.error_code = "ERR-01-001";
	    mdlMessage.indonesian = "Nama Pengguna tidak valid.";
	    mdlMessage.english = "Username Invalid.";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;
	case ERR_01_002:
	    mdlErrorSchema.error_code = "ERR-01-002";
	    mdlMessage.indonesian = "Kata Sandi tidak valid.";
	    mdlMessage.english = "Password invalid.";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;
	case ERR_01_003:
	    mdlErrorSchema.error_code = "ERR-01-003";
	    mdlMessage.indonesian = "Nama Pengguna / Email / No. HP atau Kata Sandi salah.";
	    mdlMessage.english = "Wrong Username / Email / Phone no or password.";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;
	case ERR_01_004:
	    mdlErrorSchema.error_code = "ERR-01-004";
	    mdlMessage.indonesian = "Login gagal.";
	    mdlMessage.english = "Login failed.";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;
	case ERR_01_005:
	    mdlErrorSchema.error_code = "ERR-01-005";
	    mdlMessage.indonesian = "Login gagal.";
	    mdlMessage.english = "Login failed.";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;
	case ERR_01_006:
	    mdlErrorSchema.error_code = "ERR-01-006";
	    mdlMessage.indonesian = "Login gagal.";
	    mdlMessage.english = "Login failed.";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;
	case ERR_01_007:
	    mdlErrorSchema.error_code = "ERR-01-007";
	    mdlMessage.indonesian = "Login gagal.";
	    mdlMessage.english = "Login failed.";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;
	case ERR_02_001:
	    mdlErrorSchema.error_code = "ERR-02-001";
	    mdlMessage.indonesian = "Tidak ada data slideshow.";
	    mdlMessage.english = "No slideshow data.";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;
	case ERR_03_001:
	    mdlErrorSchema.error_code = "ERR-03-001";
	    mdlMessage.indonesian = "Mohon masukkan No. Peserta / Polis / Agen yang valid.";
	    mdlMessage.english = "Please input valid Member / Policy / Agent ID.";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;
	case ERR_03_002:
	    mdlErrorSchema.error_code = "ERR-03-002";
	    mdlMessage.indonesian = "Mohon masukkan Alamat Email yang valid.";
	    mdlMessage.english = "Please input valid Email address.";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;
	case ERR_03_003:
	    mdlErrorSchema.error_code = "ERR-03-003";
	    mdlMessage.indonesian = "Mohon masukkan No. HP yang valid.";
	    mdlMessage.english = "please input valid Phone number.";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;
	case ERR_03_004:
	    mdlErrorSchema.error_code = "ERR-03-004";
	    mdlMessage.indonesian = "Mohon masukkan Tanggal Lahir yang valid.";
	    mdlMessage.english = "Please input valid Date of Birth.";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;
	case ERR_03_005:
	    mdlErrorSchema.error_code = "ERR-03-005";
	    mdlMessage.indonesian = "Mohon masukkan Nama Pengguna yang valid.";
	    mdlMessage.english = "Please input valid Username.";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;
	case ERR_03_006:
	    mdlErrorSchema.error_code = "ERR-03-006";
	    mdlMessage.indonesian = "Nama Pengguna belum terverifikasi. Silahkan melakukan proses verifikasi.";
	    mdlMessage.english = "Username has not verified. Please do verification process.";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;
	case ERR_03_007:
	    mdlErrorSchema.error_code = "ERR-03-007";
	    mdlMessage.indonesian = "Kata Sandi belum di set. Silahkan tentukan Kata Sandi Anda.";
	    mdlMessage.english = "Password has not been set. Please set your Password.";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;
	case ERR_03_008:
	    mdlErrorSchema.error_code = "ERR-03-008";
	    mdlMessage.indonesian = "Nama Pengguna sudah ada.";
	    mdlMessage.english = "Username already exists.";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;
	case ERR_03_009:
	    mdlErrorSchema.error_code = "ERR-03-009";
	    mdlMessage.indonesian = "Nasabah sudah pernah teregistrasi. Silahkan login.";
	    mdlMessage.english = "Member already registered. Please login.";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;
	case ERR_03_010:
	    mdlErrorSchema.error_code = "ERR-03-010";
	    mdlMessage.indonesian = "Registrasi gagal.";
	    mdlMessage.english = "Register failed.";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;
	case ERR_03_011:
	    mdlErrorSchema.error_code = "ERR-03-011";
	    mdlMessage.indonesian = "No. HP ini sudah pernah digunakan.";
	    mdlMessage.english = "This Phone number already in use.";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;

	case ERR_03_012:
	    mdlErrorSchema.error_code = "ERR-03-012";
	    mdlMessage.indonesian = "Email ini sudah pernah digunakan.";
	    mdlMessage.english = "This Email already in use.";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;
	case ERR_03_013:
	    mdlErrorSchema.error_code = "ERR-03-013";
	    mdlMessage.indonesian = "Nama Pengguna ini sudah pernah terdaftar oleh nasabah lain.";
	    mdlMessage.english = "This Username already registered using another member.";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;
	case ERR_03_014:
	    mdlErrorSchema.error_code = "ERR-03-014";
	    mdlMessage.indonesian = "Mohon masukkan No. Peserta / Polis / Agen atau Tanggal Lahir yang valid.";
	    mdlMessage.english = "Please input valid Member / Policy / Agent ID or Birth Date.";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;
	case ERR_03_015:
	    mdlErrorSchema.error_code = "ERR-03-015";
	    mdlMessage.indonesian = "Mohon masukkan Tanggal Lahir / Nomor Mobile / Email yang valid.";
	    mdlMessage.english = "Please input valid Birth Date / Mobile Number / Email";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;
	case ERR_03_016:
	    mdlErrorSchema.error_code = "ERR-03-016";
	    mdlMessage.indonesian = "Pendaftaran hanya bisa menggunakan Member Utama.";
	    mdlMessage.english = "Registration can only done using Main Member.";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;
	case ERR_04_001:
	    mdlErrorSchema.error_code = "ERR-04-001";
	    mdlMessage.indonesian = "Nama Pengguna tidak valid.";
	    mdlMessage.english = "Username invalid.";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;
	case ERR_04_002:
	    mdlErrorSchema.error_code = "ERR-04-002";
	    mdlMessage.indonesian = "No. HP tidak valid.";
	    mdlMessage.english = "Phone number invalid.";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;
	case ERR_04_003:
	    mdlErrorSchema.error_code = "ERR-04-003";
	    mdlMessage.indonesian = "No. Peserta / Polis / Agen tidak valid.";
	    mdlMessage.english = "Member / Policy / Agent ID invalid.";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;
	case ERR_04_004:
	    mdlErrorSchema.error_code = "ERR-04-004";
	    mdlMessage.indonesian = "Permintaan OTP sudah mencapai limit hari ini.";
	    mdlMessage.english = "Request OTP already reached today's maximum limit.";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;
	case ERR_04_005:
	    mdlErrorSchema.error_code = "ERR-04-005";
	    mdlMessage.indonesian = "Pengiriman OTP gagal. Silahkan coba beberapa saat lagi.";
	    mdlMessage.english = "Send OTP failed. Please try again later.";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;
	case ERR_05_001:
	    mdlErrorSchema.error_code = "ERR-05-001";
	    mdlMessage.indonesian = "Nama Pengguna tidak valid.";
	    mdlMessage.english = "Username invalid.";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;
	case ERR_05_002:
	    mdlErrorSchema.error_code = "ERR-05-002";
	    mdlMessage.indonesian = "Kode OTP tidak valid.";
	    mdlMessage.english = "Kode OTP invalid.";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;

	case ERR_05_003:
	    mdlErrorSchema.error_code = "ERR-05-003";
	    mdlMessage.indonesian = "No. Peserta / Polis / Agen tidak valid.";
	    mdlMessage.english = "Member / Policy / Agent ID invalid.";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;
	case ERR_05_004:
	    mdlErrorSchema.error_code = "ERR-05-004";
	    mdlMessage.indonesian = "Kode OTP salah atau kadarluarsa.";
	    mdlMessage.english = "Wrong / Expired OTP code.";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;
	case ERR_05_005:
	    mdlErrorSchema.error_code = "ERR-05-005";
	    mdlMessage.indonesian = "Terjadi gangguan pada sistem. Silahkan coba beberapa saat lagi.";
	    mdlMessage.english = "There is system problem. Please try again later.";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;
	case ERR_05_006:
	    mdlErrorSchema.error_code = "ERR-05-006";
	    mdlMessage.indonesian = "Verifikasi OTP sudah mencapai limit hari ini.";
	    mdlMessage.english = "OTP Verification already reached today's maximum limit.";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;
	case ERR_06_001:
	    mdlErrorSchema.error_code = "ERR-06-001";
	    mdlMessage.indonesian = "Nama Pengguna tidak valid.";
	    mdlMessage.english = "Username invalid.";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;
	case ERR_06_002:
	    mdlErrorSchema.error_code = "ERR-06-002";
	    mdlMessage.indonesian = "Kata Sandi tidak valid.";
	    mdlMessage.english = "Password invalid.";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;
	case ERR_06_003:
	    mdlErrorSchema.error_code = "ERR-06-003";
	    mdlMessage.indonesian = "No HP tidak valid.";
	    mdlMessage.english = "Phone number invalid.";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;
	case ERR_06_004:
	    mdlErrorSchema.error_code = "ERR-06-004";
	    mdlMessage.indonesian = "Email tidak valid.";
	    mdlMessage.english = "Email invalid.";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;
	case ERR_06_005:
	    mdlErrorSchema.error_code = "ERR-06-005";
	    mdlMessage.indonesian = "Tanggal Lahir tidak valid.";
	    mdlMessage.english = "Date of Birth invalid.";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;
	case ERR_06_006:
	    mdlErrorSchema.error_code = "ERR-06-006";
	    mdlMessage.indonesian = "No. Peserta / Polis / Agen tidak valid.";
	    mdlMessage.english = "Member / Policy / Agent ID invalid.";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;
	case ERR_06_007:
	    mdlErrorSchema.error_code = "ERR-06-007";
	    mdlMessage.indonesian = "Penentuan Kata Sandi gagal.";
	    mdlMessage.english = "Set password failed.";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;
	case ERR_06_008:
	    mdlErrorSchema.error_code = "ERR-06-008";
	    mdlMessage.indonesian = "Tipe nasabah tidak valid.";
	    mdlMessage.english = "Member type invalid.";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;
	case ERR_06_009:
	    mdlErrorSchema.error_code = "ERR-06-009";
	    mdlMessage.indonesian = "Nasabah belum verivikasi OTP atau sudah pernah teregistrasi.";
	    mdlMessage.english = "Member has not verify OTP or has been registered.";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;
	case ERR_07_001:
	    mdlErrorSchema.error_code = "ERR-07-001";
	    mdlMessage.indonesian = "Email tidak valid.";
	    mdlMessage.english = "Email invalid.";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;
	case ERR_07_002:
	    mdlErrorSchema.error_code = "ERR-07-002";
	    mdlMessage.indonesian = "Tidak ada pengguna dengan email ini.";
	    mdlMessage.english = "There is no user with this email.";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;
	case ERR_07_003:
	    mdlErrorSchema.error_code = "ERR-07-003";
	    mdlMessage.indonesian = "Pengiriman Email Lupa Kata Sandi gagal.";
	    mdlMessage.english = "Forgot Password mail send failed.";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;
	case ERR_08_001:
	    mdlErrorSchema.error_code = "ERR-08-001";
	    mdlMessage.indonesian = "Latitude atau longitude tidak valid.";
	    mdlMessage.english = "Latitude or longitude is not valid.";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;

	case ERR_08_002:
	    mdlErrorSchema.error_code = "ERR-08-002";
	    mdlMessage.indonesian = "Tidak ada Provider di daerah ini.";
	    mdlMessage.english = "There is no Provider within this area.";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;
	case ERR_09_001:
	    mdlErrorSchema.error_code = "ERR-09-001";
	    mdlMessage.indonesian = "Nama Provider tidak valid.";
	    mdlMessage.english = "Provider Name is not valid.";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;
	case ERR_09_002:
	    mdlErrorSchema.error_code = "ERR-09-002";
	    mdlMessage.indonesian = "Tidak ada Provider dengan nama tersebut.";
	    mdlMessage.english = "There is no Provider with that name.";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;
	case ERR_10_001:
	    mdlErrorSchema.error_code = "ERR-10-001";
	    mdlMessage.indonesian = "Tipe syarat dan ketentuan tidak valid.";
	    mdlMessage.english = "Term and condition type is not valid.";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;
	case ERR_10_002:
	    mdlErrorSchema.error_code = "ERR-10-002";
	    mdlMessage.indonesian = "Tidak ada syarat dan ketentuan dengan tipe tersebut.";
	    mdlMessage.english = "There is no term and condition with that type.";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;

	case ERR_11_001:
	    mdlErrorSchema.error_code = "ERR-11-001";
	    mdlMessage.indonesian = "Pengecekan versi APK gagal.";
	    mdlMessage.english = "APK version check failed.";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;
	case ERR_12_001:
	    mdlErrorSchema.error_code = "ERR-12-001";
	    mdlMessage.indonesian = "Tipe menu nasabah tidak valid.";
	    mdlMessage.english = "Menu member type is not valid.";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;
	case ERR_12_002:
	    mdlErrorSchema.error_code = "ERR-12-002";
	    mdlMessage.indonesian = "Tidak ada menu dengan tipe nasabah tersebut.";
	    mdlMessage.english = "There is no menu with that member type.";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;
	case ERR_13_001:
	    mdlErrorSchema.error_code = "ERR-13-001";
	    mdlMessage.indonesian = "Tidak ada konten Bantuan.";
	    mdlMessage.english = "There is no Help content.";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;
	case ERR_14_001:
	    mdlErrorSchema.error_code = "ERR-14-001";
	    mdlMessage.indonesian = "Tidak ada konten Berita.";
	    mdlMessage.english = "There is no News content.";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;
	case ERR_15_001:
	    mdlErrorSchema.error_code = "ERR-15-001";
	    mdlMessage.indonesian = "Username tidak valid.";
	    mdlMessage.english = "Username invalid.";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;
	case ERR_15_002:
	    mdlErrorSchema.error_code = "ERR-15-002";
	    mdlMessage.indonesian = "Tidak ada pesan untuk Nama Pengguna ini.";
	    mdlMessage.english = "There is no messages for this Username.";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;
	case ERR_16_001:
	    mdlErrorSchema.error_code = "ERR-16-001";
	    mdlMessage.indonesian = "ID tidak valid";
	    mdlMessage.english = "ID invalid.";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;
	case ERR_16_002:
	    mdlErrorSchema.error_code = "ERR-16-002";
	    mdlMessage.indonesian = "Gagal update tanda baca pesan.";
	    mdlMessage.english = "Update message read failed.";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;
	case ERR_17_001:
	    mdlErrorSchema.error_code = "ERR-17-001";
	    mdlMessage.indonesian = "Tidak ada gambar untuk di upload.";
	    mdlMessage.english = "There is no image to be uploaded.";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;
	case ERR_17_002:
	    mdlErrorSchema.error_code = "ERR-17-002";
	    mdlMessage.indonesian = "Ekstensi gambar tidak valid.";
	    mdlMessage.english = "Image etension invalid.";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;
	case ERR_17_003:
	    mdlErrorSchema.error_code = "ERR-17-003";
	    mdlMessage.indonesian = "Upload gambar gagal.";
	    mdlMessage.english = "Upload image failed.";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;
	case ERR_17_004:
	    mdlErrorSchema.error_code = "ERR-17-004";
	    mdlMessage.indonesian = "Terdapat gambar yang gagal di upload. Mohon coba lagi.";
	    mdlMessage.english = "There are some images failed to upload. Please try again.";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;
	case ERR_17_005:
	    mdlErrorSchema.error_code = "ERR-17-005";
	    mdlMessage.indonesian = "Jumlah gambar yang di upload tidak sesuai.";
	    mdlMessage.english = "Uploaded Image amount is invalid.";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;
	case ERR_17_006:
	    mdlErrorSchema.error_code = "ERR-17-006";
	    mdlMessage.indonesian = "Data gambar yang di upload kosong.";
	    mdlMessage.english = "Empty uploaded image data.";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;

	case ERR_17_007:
	    mdlErrorSchema.error_code = "ERR-17-007";
	    mdlMessage.indonesian = "Pembuatan PDF gagal.";
	    mdlMessage.english = "PDF creation failed.";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;
	case ERR_17_008:
	    mdlErrorSchema.error_code = "ERR-17-008";
	    mdlMessage.indonesian = "Pembuatan direktori gambar gagal.";
	    mdlMessage.english = "Image directory creation failed.";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;
	case ERR_18_001:
	    mdlErrorSchema.error_code = "ERR-18-001";
	    mdlMessage.indonesian = "Nama Pengguna tidak valid.";
	    mdlMessage.english = "Username invalid.";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;
	case ERR_18_002:
	    mdlErrorSchema.error_code = "ERR-18-002";
	    mdlMessage.indonesian = "Device ID tidak valid.";
	    mdlMessage.english = "Device ID invalid.";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;
	case ERR_18_003:
	    mdlErrorSchema.error_code = "ERR-18-003";
	    mdlMessage.indonesian = "Tipe OS tidak valid.";
	    mdlMessage.english = "OS type invalid.";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;

	case ERR_18_004:
	    mdlErrorSchema.error_code = "ERR-18-004";
	    mdlMessage.indonesian = "Logout gagal.";
	    mdlMessage.english = "Logout failed.";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;
	case ERR_19_001:
	    mdlErrorSchema.error_code = "ERR-19-001";
	    mdlMessage.indonesian = "Nama Pengguna tidak valid.";
	    mdlMessage.english = "Username invalid.";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;
	case ERR_19_002:
	    mdlErrorSchema.error_code = "ERR-19-002";
	    mdlMessage.indonesian = "Kata Sandi tidak valid.";
	    mdlMessage.english = "Password invalid.";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;
	case ERR_19_003:
	    mdlErrorSchema.error_code = "ERR-19-003";
	    mdlMessage.indonesian = "Kata Sandi baru tidak valid.";
	    mdlMessage.english = "New Password invalid.";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;
	case ERR_19_004:
	    mdlErrorSchema.error_code = "ERR-19-004";
	    mdlMessage.indonesian = "Kata Sandi lama salah.";
	    mdlMessage.english = "Wrong Old Password.";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;

	case ERR_19_005:
	    mdlErrorSchema.error_code = "ERR-19-005";
	    mdlMessage.indonesian = "Perubahan Kata Sandi gagal.";
	    mdlMessage.english = "Change Password failed.";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;
	case ERR_20_001:
	    mdlErrorSchema.error_code = "ERR-20-001";
	    mdlMessage.indonesian = "Tipe pesan tidak valid.";
	    mdlMessage.english = "Message type invalid.";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;
	case ERR_20_002:
	    mdlErrorSchema.error_code = "ERR-20-002";
	    mdlMessage.indonesian = "Judul Pesan tidak valid.";
	    mdlMessage.english = "Message title invalid.";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;
	case ERR_20_003:
	    mdlErrorSchema.error_code = "ERR-20-003";
	    mdlMessage.indonesian = "Konten Pesan tidak valid.";
	    mdlMessage.english = "Message content invalid.";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;
	case ERR_20_004:
	    mdlErrorSchema.error_code = "ERR-20-004";
	    mdlMessage.indonesian = "Nama pengguna tidak valid.";
	    mdlMessage.english = "Username invalid.";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;
	case ERR_20_005:
	    mdlErrorSchema.error_code = "ERR-20-005";
	    mdlMessage.indonesian = "Pengiriman pesan broadcast gagal.";
	    mdlMessage.english = "Send broadcast message failed.";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;
	case ERR_21_001:
	    mdlErrorSchema.error_code = "ERR-21-001";
	    mdlMessage.indonesian = "Nama Pengguna tidak valid.";
	    mdlMessage.english = "Username invalid.";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;
	case ERR_21_002:
	    mdlErrorSchema.error_code = "ERR-21-002";
	    mdlMessage.indonesian = "No ID nasabah tidak valid.";
	    mdlMessage.english = "Member employee no. invalid";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;
	case ERR_21_003:
	    mdlErrorSchema.error_code = "ERR-21-003";
	    mdlMessage.indonesian = "Nomor Rekening tidak valid.";
	    mdlMessage.english = "Account Number not valid.";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;
	case ERR_21_004:
	    mdlErrorSchema.error_code = "ERR-21-004";
	    mdlMessage.indonesian = "Tanggal Claim tidak valid";
	    mdlMessage.english = "Claim Date invalid.";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;
	case ERR_21_005:
	    mdlErrorSchema.error_code = "ERR-21-005";
	    mdlMessage.indonesian = "Tipe nasabah tidak valid";
	    mdlMessage.english = "Member type invalid.";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;

	case ERR_21_006:
	    mdlErrorSchema.error_code = "ERR-21-006";
	    mdlMessage.indonesian = "Upload claim gagal.";
	    mdlMessage.english = "Upload claim failed.";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;
	case ERR_21_007:
	    mdlErrorSchema.error_code = "ERR-21-007";
	    mdlMessage.indonesian = "Member no. tidak valid.";
	    mdlMessage.english = "Member no. invalid.";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;
	case ERR_21_008:
	    mdlErrorSchema.error_code = "ERR-21-008";
	    mdlMessage.indonesian = "Klaim Anda sudah pernah diproses. Silahkan cek kembali.";
	    mdlMessage.english = "Your Claim had been processed. Please check again.";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;
	case ERR_22_001:
	    mdlErrorSchema.error_code = "ERR-22-001";
	    mdlMessage.indonesian = "Berita ID tidak valid.";
	    mdlMessage.english = "News ID invalid.";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;
	case ERR_22_002:
	    mdlErrorSchema.error_code = "ERR-22-002";
	    mdlMessage.indonesian = "Judul Berita tidak valid.";
	    mdlMessage.english = "News title invalid";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;
	case ERR_22_003:
	    mdlErrorSchema.error_code = "ERR-22-003";
	    mdlMessage.indonesian = "Gambar Berita tidak valid.";
	    mdlMessage.english = "News image not valid.";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;

	case ERR_22_004:
	    mdlErrorSchema.error_code = "ERR-22-004";
	    mdlMessage.indonesian = "Konten Berita tidak valid.";
	    mdlMessage.english = "News Content invalid.";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;
	case ERR_22_005:
	    mdlErrorSchema.error_code = "ERR-22-005";
	    mdlMessage.indonesian = "Link Berita tidak valid.";
	    mdlMessage.english = "News Link invalid.";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;
	case ERR_22_006:
	    mdlErrorSchema.error_code = "ERR-22-006";
	    mdlMessage.indonesian = " News ID tidak valid.";
	    mdlMessage.english = "News ID invalid.";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;
	case ERR_23_001:
	    mdlErrorSchema.error_code = "ERR-23-001";
	    mdlMessage.indonesian = "Member no. tidak valid";
	    mdlMessage.english = "member no. invalid.";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;
	case ERR_23_002:
	    mdlErrorSchema.error_code = "ERR-23-002";
	    mdlMessage.indonesian = "Tidak ada Benefit Type untuk member ini.";
	    mdlMessage.english = "There is no Benefit Type for this member.";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;

	case ERR_24_001:
	    mdlErrorSchema.error_code = "ERR-24-001";
	    mdlMessage.indonesian = "Nomor Claim tidak valid.";
	    mdlMessage.english = "Claim No. invalid.";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;
	case ERR_24_002:
	    mdlErrorSchema.error_code = "ERR-24-002";
	    mdlMessage.indonesian = "Member employee no. tidak valid.";
	    mdlMessage.english = "Member employee no. invalid.";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;
	case ERR_24_003:
	    mdlErrorSchema.error_code = "ERR-24-003";
	    mdlMessage.indonesian = "Member no. tidak valid.";
	    mdlMessage.english = "Member no. invalid.";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;
	case ERR_24_004:
	    mdlErrorSchema.error_code = "ERR-24-004";
	    mdlMessage.indonesian = "Tipe member tidak valid.";
	    mdlMessage.english = "Member type invalid.";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;
	case ERR_24_005:
	    mdlErrorSchema.error_code = "ERR-24-005";
	    mdlMessage.indonesian = "Status claim tidak valid.";
	    mdlMessage.english = "Claim status invalid.";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;
	case ERR_24_006:
	    mdlErrorSchema.error_code = "ERR-24-006";
	    mdlMessage.indonesian = "Update status claim gagal.";
	    mdlMessage.english = "Update status claim failed.";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;
	case ERR_25_001:
	    mdlErrorSchema.error_code = "ERR-25-001";
	    mdlMessage.indonesian = "Tipe claim tidak valid";
	    mdlMessage.english = "Claim type invalid.";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;
	case ERR_25_002:
	    mdlErrorSchema.error_code = "ERR-25-002";
	    mdlMessage.indonesian = "Tidak ada tipe dokumen claim untuk tipe claim ini.";
	    mdlMessage.english = "There is no claim document type for this claim type.";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;
	case ERR_26_001:
	    mdlErrorSchema.error_code = "ERR-26-001";
	    mdlMessage.indonesian = "Nama Pengguna tidak valid.";
	    mdlMessage.english = "Username invalid.";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;
	case ERR_26_002:
	    mdlErrorSchema.error_code = "ERR-26-002";
	    mdlMessage.indonesian = "Email tidak valid.";
	    mdlMessage.english = "Email invalid.";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;

	case ERR_26_003:
	    mdlErrorSchema.error_code = "ERR-26-003";
	    mdlMessage.indonesian = "Email Baru tidak valid.";
	    mdlMessage.english = "New Email invalid.";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;
	case ERR_26_004:
	    mdlErrorSchema.error_code = "ERR-26-004";
	    mdlMessage.indonesian = "Email Lama salah.";
	    mdlMessage.english = "Wrong Old email.";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;
	case ERR_26_005:
	    mdlErrorSchema.error_code = "ERR-26-005";
	    mdlMessage.indonesian = "Perubahan Email gagal.";
	    mdlMessage.english = "Change Email failed.";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;
	case ERR_27_001:
	    mdlErrorSchema.error_code = "ERR-27-001";
	    mdlMessage.indonesian = "Nomor Claim tidak valid.";
	    mdlMessage.english = "Claim No. invalid.";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;
	case ERR_27_002:
	    mdlErrorSchema.error_code = "ERR-27-002";
	    mdlMessage.indonesian = "Klaim ini tidak memiliki dokumen.";
	    mdlMessage.english = "This claim does not have any document.";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;
	case ERR_27_003:
	    mdlErrorSchema.error_code = "ERR-27-003";
	    mdlMessage.indonesian = "Pembuatan direktori gambar gagal.";
	    mdlMessage.english = "Image directory creation failed.";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;
	case ERR_27_004:
	    mdlErrorSchema.error_code = "ERR-27-004";
	    mdlMessage.indonesian = "Ekstensi gambar tidak valid.";
	    mdlMessage.english = "Image etension invalid.";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;
	case ERR_27_005:
	    mdlErrorSchema.error_code = "ERR-27-005";
	    mdlMessage.indonesian = "Upload gambar gagal.";
	    mdlMessage.english = "Upload image failed.";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;
	case ERR_27_006:
	    mdlErrorSchema.error_code = "ERR-27-006";
	    mdlMessage.indonesian = "Terdapat gambar yang gagal di upload. Mohon coba lagi.";
	    mdlMessage.english = "There are some images failed to upload. Please try again.";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;
	case ERR_27_007:
	    mdlErrorSchema.error_code = "ERR-27-007";
	    mdlMessage.indonesian = "Jumlah gambar yang di upload tidak sesuai.";
	    mdlMessage.english = "Uploaded Image amount is invalid.";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;
	case ERR_27_008:
	    mdlErrorSchema.error_code = "ERR-27-008";
	    mdlMessage.indonesian = "Data gambar yang di upload kosong.";
	    mdlMessage.english = "Empty uploaded image data.";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;

	case ERR_27_009:
	    mdlErrorSchema.error_code = "ERR-27-009";
	    mdlMessage.indonesian = "Pembuatan PDF gagal.";
	    mdlMessage.english = "PDF creation failed.";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;
	case ERR_27_010:
	    mdlErrorSchema.error_code = "ERR-27-010";
	    mdlMessage.indonesian = "Tidak ada gambar untuk di upload.";
	    mdlMessage.english = "There is no image to be uploaded.";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;
	case ERR_28_001:
	    mdlErrorSchema.error_code = "ERR-28-001";
	    mdlMessage.indonesian = "Mohon masukkan No. Peserta / Polis / Agen yang valid.";
	    mdlMessage.english = "Please input valid Member / Policy / Agent ID.";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;
	case ERR_28_002:
	    mdlErrorSchema.error_code = "ERR-28-002";
	    mdlMessage.indonesian = "Mohon masukkan Alamat Email yang valid.";
	    mdlMessage.english = "Please input valid Email address.";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;
	case ERR_28_003:
	    mdlErrorSchema.error_code = "ERR-28-003";
	    mdlMessage.indonesian = "Mohon masukkan No. HP yang valid.";
	    mdlMessage.english = "please input valid Phone number.";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;
	case ERR_28_004:
	    mdlErrorSchema.error_code = "ERR-28-004";
	    mdlMessage.indonesian = "Mohon masukkan Tanggal Lahir yang valid.";
	    mdlMessage.english = "Please input valid Date of Birth.";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;
	case ERR_28_005:
	    mdlErrorSchema.error_code = "ERR-28-005";
	    mdlMessage.indonesian = "Mohon masukkan Nama Pengguna yang valid.";
	    mdlMessage.english = "Please input valid Username.";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;
	case ERR_28_006:
	    mdlErrorSchema.error_code = "ERR-28-006";
	    mdlMessage.indonesian = "Mohon masukkan No. Peserta / Polis / Agen yang valid.";
	    mdlMessage.english = "Please input valid Member / Policy / Agent ID.";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;
	case ERR_28_007:
	    mdlErrorSchema.error_code = "ERR-28-007";
	    mdlMessage.indonesian = "Tidak ada data peserta untuk Pengguna ini.";
	    mdlMessage.english = "There is no member data for this User.";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;
	case ERR_28_008:
	    mdlErrorSchema.error_code = "ERR-28-008";
	    mdlMessage.indonesian = "Penambahan akun gagal.";
	    mdlMessage.english = "Add account failed.";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;
	case ERR_28_009:
	    mdlErrorSchema.error_code = "ERR-28-009";
	    mdlMessage.indonesian = "Mohon masukkan Tanggal Lahir yang valid.";
	    mdlMessage.english = "Please input valid Birth Date.";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;
	case ERR_28_010:
	    mdlErrorSchema.error_code = "ERR-28-010";
	    mdlMessage.indonesian = "Member ini sudah teregistrasi sebelumnya.";
	    mdlMessage.english = "This member is already registered before.";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;
	case ERR_29_001:
	    mdlErrorSchema.error_code = "ERR-29-001";
	    mdlMessage.indonesian = "Nomor Klaim tidak valid.";
	    mdlMessage.english = "Claim No not valid.";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;
	case ERR_29_002:
	    mdlErrorSchema.error_code = "ERR-29-002";
	    mdlMessage.indonesian = "Tidak ada data klaim dengan nomor klaim ini.";
	    mdlMessage.english = "There is no claim data with this claim no.";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;
	case ERR_30_001:
	    mdlErrorSchema.error_code = "ERR-30-001";
	    mdlMessage.indonesian = "Tipe provider tidak valid";
	    mdlMessage.english = "Provider type invalid.";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;
	case ERR_30_002:
	    mdlErrorSchema.error_code = "ERR-30-002";
	    mdlMessage.indonesian = "Nama provider tidak valid";
	    mdlMessage.english = "Provider name invalid.";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;
	case ERR_30_003:
	    mdlErrorSchema.error_code = "ERR-30-003";
	    mdlMessage.indonesian = "Alamat tidak valid";
	    mdlMessage.english = "Address invalid.";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;
	case ERR_30_004:
	    mdlErrorSchema.error_code = "ERR-30-004";
	    mdlMessage.indonesian = "Nomor telepon tidak valid";
	    mdlMessage.english = "Phone number invalid.";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;
	case ERR_30_005:
	    mdlErrorSchema.error_code = "ERR-30-005";
	    mdlMessage.indonesian = "Kota tidak valid";
	    mdlMessage.english = "City invalid.";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;
	case ERR_30_006:
	    mdlErrorSchema.error_code = "ERR-30-006";
	    mdlMessage.indonesian = "Provinsi tidak valid";
	    mdlMessage.english = "Province invalid.";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;
	case ERR_30_007:
	    mdlErrorSchema.error_code = "ERR-30-007";
	    mdlMessage.indonesian = "Koordinat tidak valid";
	    mdlMessage.english = "Coordinate invalid.";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;
	case ERR_30_008:
	    mdlErrorSchema.error_code = "ERR-30-008";
	    mdlMessage.indonesian = "Jarak tidak valid";
	    mdlMessage.english = "Distance invalid.";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;
	case ERR_30_009:
	    mdlErrorSchema.error_code = "ERR-30-009";
	    mdlMessage.indonesian = "Insert provider gagal";
	    mdlMessage.english = "Provider insert failed.";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;
	case ERR_31_001:
	    mdlErrorSchema.error_code = "ERR-31-001";
	    mdlMessage.indonesian = "Tidak ada data tipe dokumen.";
	    mdlMessage.english = "There is no document type data.";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;
	case ERR_32_001:
	    mdlErrorSchema.error_code = "ERR-32-001";
	    mdlMessage.indonesian = "Tipe klaim tidak valid.";
	    mdlMessage.english = "Claim type invalid.";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;
	case ERR_32_002:
	    mdlErrorSchema.error_code = "ERR-32-002";
	    mdlMessage.indonesian = "ID Tipe Dokumen Klaim tidak valid.";
	    mdlMessage.english = "Claim Document Type ID invalid.";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;
	case ERR_32_003:
	    mdlErrorSchema.error_code = "ERR-32-003";
	    mdlMessage.indonesian = "Nama Tipe Dokumen Klaim tidak valid.";
	    mdlMessage.english = "claim Document Type Name invalid.";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;
	case ERR_32_004:
	    mdlErrorSchema.error_code = "ERR-32-004";
	    mdlMessage.indonesian = "Detil Tipe Dokumen Klaim tidak valid.";
	    mdlMessage.english = "claim Document Type Details invalid.";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;
	case ERR_32_005:
	    mdlErrorSchema.error_code = "ERR-32-005";
	    mdlMessage.indonesian = "Informasi 1 Tipe Dokumen Klaim tidak valid.";
	    mdlMessage.english = "claim Document Type Information 1 invalid.";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;
	case ERR_32_006:
	    mdlErrorSchema.error_code = "ERR-32-006";
	    mdlMessage.indonesian = "Penambahan atau perubahan Tipe Dokumen Klaim gagal.";
	    mdlMessage.english = "Insert or update Claim Document Type failed.";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;
	case ERR_33_001:
	    mdlErrorSchema.error_code = "ERR-33-001";
	    mdlMessage.indonesian = "Judul bantuan tidak valid.";
	    mdlMessage.english = "Help title invalid.";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;
	case ERR_33_002:
	    mdlErrorSchema.error_code = "ERR-33-002";
	    mdlMessage.indonesian = "Konten bantuan tidak valid";
	    mdlMessage.english = "Help content invalid.";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;
	case ERR_33_003:
	    mdlErrorSchema.error_code = "ERR-33-003";
	    mdlMessage.indonesian = "Urutan bantuan tidak valid";
	    mdlMessage.english = "Order content invalid.";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;
	case ERR_33_004:
	    mdlErrorSchema.error_code = "ERR-33-004";
	    mdlMessage.indonesian = "Penambahan atau perubahan Bantuan gagal.";
	    mdlMessage.english = "Insert or update Help failed.";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;
	case ERR_34_001:
	    mdlErrorSchema.error_code = "ERR-34-001";
	    mdlMessage.indonesian = "ID Bantuan tidak valid";
	    mdlMessage.english = "Help ID invalid.";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;
	case ERR_34_002:
	    mdlErrorSchema.error_code = "ERR-34-002";
	    mdlMessage.indonesian = "Penghapusan bantuan gagal.";
	    mdlMessage.english = "Delete Help failed.";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;
	case ERR_35_001:
	    mdlErrorSchema.error_code = "ERR-35-001";
	    mdlMessage.indonesian = "Nama Pengguna tidak valid.";
	    mdlMessage.english = "Username Invalid.";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;
	case ERR_35_002:
	    mdlErrorSchema.error_code = "ERR-35-002";
	    mdlMessage.indonesian = "Tidak ada data dengan Nama Pengguna ini.";
	    mdlMessage.english = "There is no data with this Username.";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;
	case ERR_35_003:
	    mdlErrorSchema.error_code = "ERR-35-003";
	    mdlMessage.indonesian = "Tidak ada data peserta untuk Pengguna ini.";
	    mdlMessage.english = "There is no member data for this Username.";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;
	case ERR_36_001:
	    mdlErrorSchema.error_code = "ERR-36-001";
	    mdlMessage.indonesian = "Tidak ada konfigurasi mobile.";
	    mdlMessage.english = "There is no mobile config.";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;
	case ERR_37_001:
	    mdlErrorSchema.error_code = "ERR-37-001";
	    mdlMessage.indonesian = "Nama pengguna tidak valid.";
	    mdlMessage.english = "Username invalid.";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;
	case ERR_37_002:
	    mdlErrorSchema.error_code = "ERR-37-002";
	    mdlMessage.indonesian = "Rating tidak valid.";
	    mdlMessage.english = "Rating invalid.";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;
	case ERR_37_003:
	    mdlErrorSchema.error_code = "ERR-37-003";
	    mdlMessage.indonesian = "Penyimpanan rating gagal.";
	    mdlMessage.english = "Save rating failed.";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;
	case ERR_38_001:
	    mdlErrorSchema.error_code = "ERR-38-001";
	    mdlMessage.indonesian = "Nama pengguna tidak valid.";
	    mdlMessage.english = "Username invalid.";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;
	case ERR_38_002:
	    mdlErrorSchema.error_code = "ERR-38-002";
	    mdlMessage.indonesian = "Kata Sandi tidak valid.";
	    mdlMessage.english = "Password invalid.";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;
	case ERR_38_003:
	    mdlErrorSchema.error_code = "ERR-38-003";
	    mdlMessage.indonesian = "Kata sandi salah.";
	    mdlMessage.english = "Wrong password.";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;
	case ERR_39_001:
	    mdlErrorSchema.error_code = "ERR-39-001";
	    mdlMessage.indonesian = "Tidak ada data Nilai Unit untuk member ini.";
	    mdlMessage.english = "There is no NAB data for this member.";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;
	case ERR_40_001:
	    mdlErrorSchema.error_code = "ERR-40-001";
	    mdlMessage.indonesian = "Tidak ada data Kinerja Dana untuk perusahaan ini.";
	    mdlMessage.english = "There is no Fund Fact Sheet data for this company.";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;
	case ERR_41_001:
	    mdlErrorSchema.error_code = "ERR-41-001";
	    mdlMessage.indonesian = "Subyek Email tidak valid.";
	    mdlMessage.english = "Email Subject is not valid.";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;
	case ERR_41_002:
	    mdlErrorSchema.error_code = "ERR-41-002";
	    mdlMessage.indonesian = "Tujuan email tidak valid.";
	    mdlMessage.english = "Email destination is not valid.";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;
	case ERR_41_003:
	    mdlErrorSchema.error_code = "ERR-41-003";
	    mdlMessage.indonesian = "Pengiriman email gagal.";
	    mdlMessage.english = "Send email failed.";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;
	case ERR_42_001:
	    mdlErrorSchema.error_code = "ERR-42-001";
	    mdlMessage.indonesian = "Tidak ada konten produk.";
	    mdlMessage.english = "There is no product content.";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;
	case ERR_43_001:
	    mdlErrorSchema.error_code = "ERR-43-001";
	    mdlMessage.indonesian = "Tidak ada produk.";
	    mdlMessage.english = "There is no product.";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;
	case ERR_44_001:
	    mdlErrorSchema.error_code = "ERR-44-001";
	    mdlMessage.indonesian = "Product ID tidak valid.";
	    mdlMessage.english = "Product ID is not valid.";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;
	case ERR_44_002:
	    mdlErrorSchema.error_code = "ERR-44-002";
	    mdlMessage.indonesian = "Ketegori produk tidak valid.";
	    mdlMessage.english = "Product category is not valid.";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;
	case ERR_44_003:
	    mdlErrorSchema.error_code = "ERR-44-003";
	    mdlMessage.indonesian = "Judul produk tidak valid";
	    mdlMessage.english = "Product title is not valid.";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;
	case ERR_44_004:
	    mdlErrorSchema.error_code = "ERR-44-004";
	    mdlMessage.indonesian = "Deskripsi produk tidak valid.";
	    mdlMessage.english = "Product description is not valid.";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;
	case ERR_44_005:
	    mdlErrorSchema.error_code = "ERR-44-005";
	    mdlMessage.indonesian = "Konten produk tidak valid.";
	    mdlMessage.english = "Product content is not valid.";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;
	case ERR_44_006:
	    mdlErrorSchema.error_code = "ERR-44-006";
	    mdlMessage.indonesian = "Penambahan atau perubahan produk gagal.";
	    mdlMessage.english = "Insert or update product failed.";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;
	case ERR_45_001:
	    mdlErrorSchema.error_code = "ERR-45-001";
	    mdlMessage.indonesian = "ID Produk tidak valid";
	    mdlMessage.english = "Product ID is no valid.";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;
	case ERR_45_002:
	    mdlErrorSchema.error_code = "ERR-45-002";
	    mdlMessage.indonesian = "Penghapusan produk gagal.";
	    mdlMessage.english = "Delete product failed.";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;
	case ERR_46_001:
	    mdlErrorSchema.error_code = "ERR-46-001";
	    mdlMessage.indonesian = "Tidak ada kategori produk.";
	    mdlMessage.english = "There is no product category.";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;
	case ERR_47_001:
	    mdlErrorSchema.error_code = "ERR-47-001";
	    mdlMessage.indonesian = "ID kategori produk tidak valid.";
	    mdlMessage.english = "Product Category ID is not valid.";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;
	case ERR_47_002:
	    mdlErrorSchema.error_code = "ERR-47-002";
	    mdlMessage.indonesian = "Nama kategori produk tidak valid.";
	    mdlMessage.english = "Product category name is not valid.";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;
	case ERR_47_003:
	    mdlErrorSchema.error_code = "ERR-47-003";
	    mdlMessage.indonesian = "Ikon kategori produk tidak valid.";
	    mdlMessage.english = "Product category icon is not valid.";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;
	case ERR_47_004:
	    mdlErrorSchema.error_code = "ERR-47-004";
	    mdlMessage.indonesian = "Penambahan atau perubahan kategori produk gagal.";
	    mdlMessage.english = "Insert or update product category failed.";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;
	case ERR_48_001:
	    mdlErrorSchema.error_code = "ERR-48-001";
	    mdlMessage.indonesian = "ID kategori produk tidak valid";
	    mdlMessage.english = "Product category ID is no valid.";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;
	case ERR_48_002:
	    mdlErrorSchema.error_code = "ERR-48-002";
	    mdlMessage.indonesian = "Penghapusan kategori produk gagal.";
	    mdlMessage.english = "Delete product category failed.";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;
	case ERR_49_001:
	    mdlErrorSchema.error_code = "ERR-49-001";
	    mdlMessage.indonesian = "Tidak ada layanan.";
	    mdlMessage.english = "There is no service.";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;
	case ERR_50_001:
	    mdlErrorSchema.error_code = "ERR-50-001";
	    mdlMessage.indonesian = "Judul layanan tidak valid.";
	    mdlMessage.english = "Service title is not valid.";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;
	case ERR_50_002:
	    mdlErrorSchema.error_code = "ERR-50-002";
	    mdlMessage.indonesian = "Konten layanan tidak valid.";
	    mdlMessage.english = "Service content is not valid.";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;
	case ERR_50_003:
	    mdlErrorSchema.error_code = "ERR-50-003";
	    mdlMessage.indonesian = "Urutan layanan tidak valid";
	    mdlMessage.english = "Service order is not valid.";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;
	case ERR_50_004:
	    mdlErrorSchema.error_code = "ERR-50-004";
	    mdlMessage.indonesian = "Penambahan atau perubahan layanan gagal.";
	    mdlMessage.english = "Insert or update service failed.";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;
	case ERR_51_001:
	    mdlErrorSchema.error_code = "ERR-51-001";
	    mdlMessage.indonesian = "ID layanan tidak valid";
	    mdlMessage.english = "Service ID is no valid.";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;
	case ERR_51_002:
	    mdlErrorSchema.error_code = "ERR-51-002";
	    mdlMessage.indonesian = "Penghapusan layanan gagal.";
	    mdlMessage.english = "Delete service failed.";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;
	case ERR_52_001:
	    mdlErrorSchema.error_code = "ERR-51-001";
	    mdlMessage.indonesian = "Tidak ada daftar nama pengguna";
	    mdlMessage.english = "There is no username list.";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;
	case ERR_53_001:
	    mdlErrorSchema.error_code = "ERR-53-001";
	    mdlMessage.indonesian = "Tidak ada pesan broadcast.";
	    mdlMessage.english = "There is no broadcast message.";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;
	case ERR_99_001:
	    mdlErrorSchema.error_code = "ERR-99-001";
	    mdlMessage.indonesian = "Parameter input tidak valid";
	    mdlMessage.english = "Invalid input parameter";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;
	case ERR_99_002:
	    mdlErrorSchema.error_code = "ERR-99-002";
	    mdlMessage.indonesian = "Client_id/client_secret/grant_type tidak valid";
	    mdlMessage.english = "Invalid client_id/client_secret/grant_type";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;
	case ERR_99_003:
	    mdlErrorSchema.error_code = "ERR-99-003";
	    mdlMessage.indonesian = "Permintaan tidak valid";
	    mdlMessage.english = "Invalid request";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;
	case ERR_99_004:
	    mdlErrorSchema.error_code = "ERR-99-004";
	    mdlMessage.indonesian = "Tidak berhak";
	    mdlMessage.english = "Unauthorized";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;
	case ERR_99_005:
	    mdlErrorSchema.error_code = "ERR-99-005";
	    mdlMessage.indonesian = "Token tidak valid";
	    mdlMessage.english = "Token invalid";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;
	case ERR_99_006:
	    mdlErrorSchema.error_code = "ERR-99-006";
	    mdlMessage.indonesian = "Token expired";
	    mdlMessage.english = "Token expired";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;

	case ERR_99_007:
	    mdlErrorSchema.error_code = "ERR-99-007";
	    mdlMessage.indonesian = "HMAC tidak cocok";
	    mdlMessage.english = "HMAC mismatch";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;
	case ERR_99_999:
	    mdlErrorSchema.error_code = "ERR-99-999";
	    mdlMessage.indonesian = "Sistem sedang tidak tersedia";
	    mdlMessage.english = "System unavailable";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;
	default:
	    mdlErrorSchema.error_code = "ERR-99-999";
	    mdlMessage.indonesian = "Sistem sedang tidak tersedia";
	    mdlMessage.english = "System unavailable";
	    mdlErrorSchema.error_message = mdlMessage;
	    break;
	}
	return mdlErrorSchema;
    }
}
