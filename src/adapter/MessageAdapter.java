package adapter;

import java.util.ArrayList;
import java.util.List;
import java.util.StringJoiner;
import java.util.stream.Collectors;

import javax.sql.rowset.CachedRowSet;

import org.apache.log4j.Logger;
import org.springframework.util.StringUtils;

import com.google.gson.Gson;

import database.QueryExecuteAdapter;
import model.mdlQueryExecute;

public class MessageAdapter {
    final static Logger logger = Logger.getLogger(ProviderAdapter.class);
    static Gson gson = new Gson();
    static String source = "Web Server";

    public static List<model.mdlUserMessage> GetMessageList(String username) {
	String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();
	List<model.mdlUserMessage> userMessageList = new ArrayList<model.mdlUserMessage>();
	List<mdlQueryExecute> listParam = new ArrayList<mdlQueryExecute>();
	CachedRowSet jrs = null;
	String sql = "";
	try {
	    sql = "SELECT TOP 20 message_id, username, message_title, message_content_indonesian, message_content_english, " 
		    + "message_value, message_value2, message_read, message_category " + "FROM ms_message WHERE username = ? ORDER BY create_date DESC";
	    listParam.add(QueryExecuteAdapter.QueryParam("string", username));
	    jrs = QueryExecuteAdapter.QueryExecute(sql, listParam, functionName);
	    while (jrs.next()) {
		model.mdlUserMessage mdlUserMessage = new model.mdlUserMessage();
		mdlUserMessage.message_id = jrs.getString("message_id");
		mdlUserMessage.username = jrs.getString("username");
		mdlUserMessage.message_title = jrs.getString("message_title");
		mdlUserMessage.message_content_indonesian = jrs.getString("message_content_indonesian");
		mdlUserMessage.message_content_english = jrs.getString("message_content_english");
		mdlUserMessage.message_value = jrs.getString("message_value");
		mdlUserMessage.message_value2 = jrs.getString("message_value2");
		mdlUserMessage.message_read = jrs.getString("message_read");
		mdlUserMessage.message_category = jrs.getString("message_category");
		userMessageList.add(mdlUserMessage);
	    }
	} catch (Exception ex) {
	    logger.error("FAILED. Function : " + functionName + ", username : " + username + ", Exception:" + ex.toString(), ex);
	}
	return userMessageList;
    }

    public static boolean UpdateMessageRead(String message_id) {
	String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();
	boolean success = false;
	List<mdlQueryExecute> listParam = new ArrayList<mdlQueryExecute>();
	String sql = "";
	int id = Integer.parseInt(message_id);

	try {
	    sql = "UPDATE ms_message SET message_read = 1 WHERE message_id = ? ";
	    listParam.add(QueryExecuteAdapter.QueryParam("integer", id));
	    success = QueryExecuteAdapter.QueryManipulate(sql, listParam, functionName);
	} catch (Exception ex) {
	    logger.error("FAILED. Function : " + functionName + ", message_id : " + message_id + ",Exception:" + ex.toString(), ex);
	}
	return success;
    }

    public static boolean CreateUserMessage(model.mdlUserMessage mdlUserMessage, String messageCategory, String language) {
	List<mdlQueryExecute> listParam = new ArrayList<mdlQueryExecute>();
	String sql = "";
	boolean success = false;
	// String message = "";
	language = language == null || language.equals("") ? "in" : language;
	try {
	    model.mdlClaim mdlClaim = mdlUserMessage.message_value == null || mdlUserMessage.message_value.equals("") ? null : ClaimAdapter.GetClaimData(mdlUserMessage.message_value);
	    mdlUserMessage.message_value3 = mdlClaim == null || mdlClaim.member_name == null ? "" : mdlClaim.member_name;
	    mdlUserMessage.message_value4 = mdlClaim == null || mdlClaim.claim_date == null ? "" : mdlClaim.claim_date;
	    switch (messageCategory) {
	    case "login":
		mdlUserMessage.message_title = "Login sukses";
		mdlUserMessage.message_content_indonesian = "Selamat datang di Aplikasi BNI LIfe Mobile.";
		mdlUserMessage.message_content_english = "Welcome to BNI LIfe Mobile Application.";
		// message = "Selamat datang di Aplikasi BNI LIfe Mobile.";
		if (language.equalsIgnoreCase("en")) {
		    mdlUserMessage.message_title = "Login success";
		    // message = "Welcome to BNI LIfe Mobile Application.";
		}
		break;
	    case "claim-create":

		mdlUserMessage.message_title = "Claim telah dibuat";
		mdlUserMessage.message_content_indonesian = "Claim #" + mdlUserMessage.message_value + " pada tanggal " + mdlUserMessage.message_value4 + " atas nama " + mdlUserMessage.message_value3 + " telah berhasil dibuat.";
		mdlUserMessage.message_content_english = "Claim #" + mdlUserMessage.message_value + " has submitted on " + mdlUserMessage.message_value4 + " on behalf of " + mdlUserMessage.message_value3 + " has been created successfully.";
		// message = "<b>Claim #" + mdlUserMessage.message_value + "</b> telah berhasil dibuat.";
		if (language.equalsIgnoreCase("en")) {
		    mdlUserMessage.message_title = "Claim created";
		    // message = "<b>Claim #" + mdlUserMessage.message_value + "</b> telah berhasil dibuat.";
		}
		break;
	    case "claim-in-progress":
		mdlUserMessage.message_title = "Claim sedang dalam proses";
		mdlUserMessage.message_content_indonesian = "Claim #" + mdlUserMessage.message_value + " pada tanggal " + mdlUserMessage.message_value4 + " atas nama " + mdlUserMessage.message_value3 + " sedang dalam proses.";
		mdlUserMessage.message_content_english = "Claim #" + mdlUserMessage.message_value + " has submitted on " + mdlUserMessage.message_value4 + " on behalf of " + mdlUserMessage.message_value3 + " is in progress.";
		// message = "<b>Claim #" + mdlUserMessage.message_value + "</b> sedang diproses.";
		if (language.equalsIgnoreCase("en")) {
		    mdlUserMessage.message_title = "Claim in progress";
		    // message = "<b>Claim #" + mdlUserMessage.message_value + "</b> is in progress.";
		}
		break;
	    case "claim-update":
		mdlUserMessage.message_title = "Claim telah diperbaharui";
		mdlUserMessage.message_content_indonesian = "Claim #" + mdlUserMessage.message_value + " pada tanggal " + mdlUserMessage.message_value4 + " atas nama " + mdlUserMessage.message_value3 + " telah diperbaharui.";
		mdlUserMessage.message_content_english = "Claim #" + mdlUserMessage.message_value + " has submitted on " + mdlUserMessage.message_value4 + " on behalf of " + mdlUserMessage.message_value3 + " has been updated.";
		// message = "<b>Claim #" + mdlUserMessage.message_value + "</b> telah diperbaharui.";
		if (language.equalsIgnoreCase("en")) {
		    mdlUserMessage.message_title = "Claim updated";
		    // message = "<b>Claim #" + mdlUserMessage.message_value + "</b> has been updated.";
		}
		break;
	    case "claim-pending":
		mdlUserMessage.message_title = "Claim pending";
		mdlUserMessage.message_content_indonesian = "Claim #" + mdlUserMessage.message_value + " pada tanggal " + mdlUserMessage.message_value4 + " atas nama " + mdlUserMessage.message_value3 + " membutuhkan tambahan dokumen.";
		mdlUserMessage.message_content_english = "Claim #" + mdlUserMessage.message_value + " has submitted on " + mdlUserMessage.message_value4 + " on behalf of " + mdlUserMessage.message_value3 + " needs more documents.";
		// message = "<b>Claim #" + mdlUserMessage.message_value + "</b> membutuhkan tambahan dokumen.";
		if (language.equalsIgnoreCase("en")) {
		    mdlUserMessage.message_title = "Claim pending";
		    // message = "<b>Claim #" + mdlUserMessage.message_value + "</b> needs more documents.";
		}
		break;
	    case "claim-rejected":
		mdlUserMessage.message_title = "Claim ditolak";
		mdlUserMessage.message_content_indonesian = "Claim #" + mdlUserMessage.message_value + " pada tanggal " + mdlUserMessage.message_value4 + " atas nama " + mdlUserMessage.message_value3 + " telah ditolak.";
		mdlUserMessage.message_content_english = "Claim #" + mdlUserMessage.message_value + " has submitted on " + mdlUserMessage.message_value4 + " on behalf of " + mdlUserMessage.message_value3 + " has been rejected.";
		// message = "<b>Claim #" + mdlUserMessage.message_value + "</b> telah ditolak.";
		if (language.equalsIgnoreCase("en")) {
		    mdlUserMessage.message_title = "Claim rejected";
		    // message = "<b>Claim #" + mdlUserMessage.message_value + "</b> has been rejected.";
		}
		break;
	    case "claim-paid":
		mdlUserMessage.message_title = "Claim sudah dibayar";
		mdlUserMessage.message_content_indonesian = "Claim #" + mdlUserMessage.message_value + " pada tanggal " + mdlUserMessage.message_value4 + " atas nama " + mdlUserMessage.message_value3 + " telah dibayar.";
		mdlUserMessage.message_content_english = "Claim #" + mdlUserMessage.message_value + " has submitted on " + mdlUserMessage.message_value4 + " on behalf of " + mdlUserMessage.message_value3 + " has been paid.";
		// message = "<b>Claim #" + mdlUserMessage.message_value + "</b> telah dibayar.";
		if (language.equalsIgnoreCase("en")) {
		    mdlUserMessage.message_title = "Claim paid";
		    // message = "<b>Claim #" + mdlUserMessage.message_value + "</b> has been paid.";
		}
		break;
	    case "notification":
		mdlUserMessage.message_title = "Notifikasi baru";
		mdlUserMessage.message_content_indonesian = "Anda memiliki notifikasi baru.";
		mdlUserMessage.message_content_english = "You have new notification.";
		// message = "Anda memiliki notifikasi baru.";
		if (language.equalsIgnoreCase("en")) {
		    mdlUserMessage.message_title = "New notification";
		    // message = "You have new notification.";
		}
		break;
	    default:
		// message = mdlUserMessage.message_content_indonesian;
		break;
	    }

	    sql = "INSERT INTO ms_message (username, message_title, message_content_indonesian, message_content_english, message_read, " + "message_category, message_value, message_value2) " + "VALUES (?, ?, ?, ?, 0, " + "?, ?, ?) ";
	    listParam.add(QueryExecuteAdapter.QueryParam("string", mdlUserMessage.username));
	    listParam.add(QueryExecuteAdapter.QueryParam("string", mdlUserMessage.message_title));
	    listParam.add(QueryExecuteAdapter.QueryParam("string", mdlUserMessage.message_content_indonesian));
	    listParam.add(QueryExecuteAdapter.QueryParam("string", mdlUserMessage.message_content_english));

	    listParam.add(QueryExecuteAdapter.QueryParam("string", mdlUserMessage.message_category));
	    listParam.add(QueryExecuteAdapter.QueryParam("string", mdlUserMessage.message_value));
	    listParam.add(QueryExecuteAdapter.QueryParam("string", mdlUserMessage.message_value2));
	    boolean successInsertMessage = false;
	    successInsertMessage = QueryExecuteAdapter.QueryManipulate(sql, listParam, "CreateUserMessage");

	    // if success, send notification to user devices
	    if (successInsertMessage) {
		success = NotificationAdapter.PushNotificationToDevice(mdlUserMessage, language);
		// success = true;
	    }

	} catch (Exception ex) {
	    logger.error("FAILED. Function : CreateUserMessage, mdlUserMessage : " + gson.toJson(mdlUserMessage) + ", messageCategory : " + messageCategory + ", language : " + language + ", Exception:" + ex.toString(), ex);
	}
	return success;
    }

    public static boolean CreateBroadcastMessage(model.mdlBroadcastMessage mdlBroadcastMessage) {
	String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();
	List<mdlQueryExecute> listParam = new ArrayList<mdlQueryExecute>();
	String sql = "";
	List<String> usernameList = new ArrayList<String>();
	List<String> tokenList = new ArrayList<String>();
	CachedRowSet jrs = null;
	boolean successInsertBroadcast = false;
	boolean successInsertMessage = false;
	boolean success = false;
	try {
	    switch (mdlBroadcastMessage.broadcast_type) {
	    case "ALL":
	    case "all":
		sql = "SELECT distinct usr.username, device.firebase_token FROM ms_user usr INNER JOIN ms_user_device device ON usr.username = device.username " 
			+ "WHERE usr.is_active = 1 AND usr.password IS NOT NULL";

		break;
	    case "OGH":
	    case "ogh":
		sql = "SELECT distinct usr.username, device.firebase_token FROM ms_user_member member INNER JOIN ms_user usr ON member.username = usr.username " 
			+ "INNER JOIN ms_user_device device ON member.username = device.username " 
			+ "WHERE member.member_type LIKE 'OGH%' AND usr.is_active = 1 AND usr.[password] IS NOT NULL ORDER BY 1";
		break;
	    case "OGS":
	    case "ogs":
		sql = "SELECT distinct usr.username, device.firebase_token FROM ms_user_member member INNER JOIN ms_user usr ON member.username = usr.username " 
			+ "INNER JOIN ms_user_device device ON member.username = device.username " 
			+ "WHERE member.member_type LIKE 'OGS%' AND usr.is_active = 1 AND usr.[password] IS NOT NULL ORDER BY 1";
		break;
	    case "PERSONAL":
	    case "personal":
		sql = "SELECT usr.username, device.firebase_token FROM ms_user usr INNER JOIN ms_user_device device ON usr.username = device.username " 
			+ "WHERE usr.username = ? AND usr.is_active = 1 AND usr.[password] IS NOT NULL";
		break;
	    default:
		// if no type, then assume that this is for all user
		sql = "SELECT distinct usr.username, device.firebase_token FROM ms_user usr INNER JOIN ms_user_device device ON usr.username = device.username " 
			+ "WHERE usr.is_active = 1 AND usr.[password] IS NOT NULL";
		break;
	    }
	    if (mdlBroadcastMessage.broadcast_type.equalsIgnoreCase("PERSONAL")) {
		listParam.add(QueryExecuteAdapter.QueryParam("string", mdlBroadcastMessage.username));
		jrs = QueryExecuteAdapter.QueryExecute(sql, listParam, functionName);
	    } else {
		jrs = QueryExecuteAdapter.QueryExecute(sql, functionName);
	    }
	    while (jrs.next()) {
		usernameList.add(jrs.getString("username"));
		tokenList.add(jrs.getString("firebase_token"));
	    }
	    List<String> usernameDistinctList = usernameList.stream().distinct().collect(Collectors.toList()); 
	    StringJoiner teamNames = new StringJoiner(", ", "(", ")");
	    teamNames.setEmptyValue("(\'\')");
	    //fill joiner with data
	    for (String username : usernameDistinctList) {
		teamNames.add(StringUtils.quote(username));
	    }
	    String usernameListString = teamNames.toString();
	    sql = "INSERT INTO ms_broadcast (broadcast_type, username, broadcast_title, broadcast_content) VALUES (?, ?, ?, ?)";
	    listParam = new ArrayList<mdlQueryExecute>();
	    listParam.add(QueryExecuteAdapter.QueryParam("string", mdlBroadcastMessage.broadcast_type));
	    listParam.add(QueryExecuteAdapter.QueryParam("string", mdlBroadcastMessage.username == null ? "" : mdlBroadcastMessage.username));
	    listParam.add(QueryExecuteAdapter.QueryParam("string", mdlBroadcastMessage.broadcast_title));
	    listParam.add(QueryExecuteAdapter.QueryParam("string", mdlBroadcastMessage.broadcast_content));
	    successInsertBroadcast = QueryExecuteAdapter.QueryManipulate(sql, listParam, functionName);
	    if (successInsertBroadcast){
		sql = "INSERT INTO ms_message (username, message_title, message_content_indonesian, message_content_english, message_read, message_category, message_value) "
		    	+ "SELECT username, ?, ?, ?, 0, 'notification' , '' "
		    	+ "FROM ms_user WHERE  username IN " + usernameListString;
		listParam = new ArrayList<mdlQueryExecute>();
		listParam.add(QueryExecuteAdapter.QueryParam("string", mdlBroadcastMessage.broadcast_title));
		listParam.add(QueryExecuteAdapter.QueryParam("string", mdlBroadcastMessage.broadcast_content));
		listParam.add(QueryExecuteAdapter.QueryParam("string", mdlBroadcastMessage.broadcast_content));
		successInsertMessage = QueryExecuteAdapter.QueryManipulate(sql, listParam, functionName);
		if (successInsertMessage){
		    success = NotificationAdapter.PushBroadcastNotificationToDevice(mdlBroadcastMessage, tokenList);
		}
	    }
	} catch (Exception ex) {
	    logger.error("FAILED. Function : " + functionName + ", mdlBroadcastMessage : " + gson.toJson(mdlBroadcastMessage) + ", Exception:" + ex.toString(), ex);
	}

	return success;
    }
    
    public static List<model.mdlBroadcastMessage> GetBroadcastList(String id, String pageNumber, String pageSize, String search) {
	String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();
	List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
	List<model.mdlBroadcastMessage> broadcastList = new ArrayList<model.mdlBroadcastMessage>();
	CachedRowSet jrs = null;
	String search_part = search != null ? search_part = "AND (broadcast_title LIKE ? OR broadcast_content LIKE ? "
		+ "OR broadcast_type LIKE ? OR username LIKE ? ) " : "";
	String sql = "";

	try {	    
	    sql = "SELECT broadcast_id, broadcast_type, username, broadcast_title, broadcast_content FROM ms_broadcast "
	    		+ "WHERE broadcast_id = COALESCE(?, broadcast_id) " + search_part 
	    		+ " ORDER BY create_date DESC OFFSET ? ROWS FETCH NEXT ? ROWS ONLY;";
	    listParam.add(id != null ? QueryExecuteAdapter.QueryParam("int", Integer.parseInt(id)) : QueryExecuteAdapter.QueryParam("null", java.sql.Types.NULL));
	    
	    if (search != null) {
		String searchString = "%" + search.trim().replace(" ", "%") + "%";
		listParam.add(QueryExecuteAdapter.QueryParam("string", searchString));
		listParam.add(QueryExecuteAdapter.QueryParam("string", searchString));
		listParam.add(QueryExecuteAdapter.QueryParam("string", searchString));
		listParam.add(QueryExecuteAdapter.QueryParam("string", searchString));
	    }
	    
	    listParam.add(QueryExecuteAdapter.QueryParam("int", ((Integer.parseInt(pageNumber) - 1) * Integer.parseInt(pageSize))));
	    listParam.add(QueryExecuteAdapter.QueryParam("int", Integer.parseInt(pageSize)));
	    
	    jrs = QueryExecuteAdapter.QueryExecute(sql, listParam, functionName);
	    while (jrs.next()) {
		model.mdlBroadcastMessage mdlBroadcastMessage = new model.mdlBroadcastMessage();
		mdlBroadcastMessage.broadcast_id = jrs.getString("broadcast_id");
		mdlBroadcastMessage.broadcast_type = jrs.getString("broadcast_type");
		mdlBroadcastMessage.username = jrs.getString("username");
		mdlBroadcastMessage.broadcast_title = jrs.getString("broadcast_title");
		mdlBroadcastMessage.broadcast_content = jrs.getString("broadcast_content");
		broadcastList.add(mdlBroadcastMessage);
	    }
	} catch (Exception ex) {
	    LogAdapter.InsertLog(ex.toString(), functionName, sql, source);
	    logger.error("FAILED. Function : " + functionName + ", Exception:" + ex.toString(), ex);
	}
	return broadcastList;
    }
    
    public static int GetBroadcastTotalList(String id, String search) {
	String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();
	List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
	CachedRowSet crs = null;
	String sql = "";
	int return_value = 0;
	String search_part = search != null ? search_part = "AND (broadcast_title LIKE ? OR broadcast_content LIKE ? "
		+ "OR broadcast_type LIKE ? OR username LIKE ? ) " : "";
	try {
	    sql = "SELECT COUNT(1) AS total FROM ms_broadcast WHERE broadcast_id = COALESCE(?, broadcast_id) " + search_part;
	    listParam.add(id != null ? QueryExecuteAdapter.QueryParam("string", id) : QueryExecuteAdapter.QueryParam("null", java.sql.Types.NULL));
	    
	    if (search != null) {
		String searchString = "%" + search.trim().replace(" ", "%") + "%";
		listParam.add(QueryExecuteAdapter.QueryParam("string", searchString));
		listParam.add(QueryExecuteAdapter.QueryParam("string", searchString));
		listParam.add(QueryExecuteAdapter.QueryParam("string", searchString));
		listParam.add(QueryExecuteAdapter.QueryParam("string", searchString));
	    }
	    crs = QueryExecuteAdapter.QueryExecute(sql, listParam, functionName);

	    while (crs.next()) {
		return_value = crs.getInt("total");
	    }
	} catch (Exception ex) {
	    LogAdapter.InsertLog(ex.toString(), functionName, sql, source);
	    logger.error("FAILED. Function : " + functionName + ", Exception:" + ex.toString(), ex);
	}
	return return_value;
    }
}
