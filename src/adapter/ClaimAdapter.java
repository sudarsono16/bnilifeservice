package adapter;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.sql.rowset.CachedRowSet;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.graphics.image.PDImageXObject;

import com.google.gson.Gson;

import database.QueryExecuteAdapter;
import helper.DateHelper;
import model.mdlQueryExecute;

public class ClaimAdapter {
    final static Logger logger = Logger.getLogger(ClaimAdapter.class);
    static Gson gson = new Gson();

    public static String GetPdfClaimDocumentName(String claimNo) {
	List<mdlQueryExecute> listParam = new ArrayList<mdlQueryExecute>();
	CachedRowSet jrs = null;
	String sql = "";
	String pdfFilename = "";
	try {
	    sql = "SELECT document_filename FROM ms_claim_document WHERE claim_no = ? ";
	    listParam.add(QueryExecuteAdapter.QueryParam("string", claimNo));
	    jrs = QueryExecuteAdapter.QueryExecute(sql, listParam, "GetPdfClaimDocumentName");
	    while (jrs.next()) {
		pdfFilename = jrs.getString("document_filename");
	    }
	} catch (Exception ex) {
	    logger.error("FAILED. Function : GetPdfClaimDocumentName, claimNo : " + claimNo + ", Exception:" + ex.toString(), ex);
	}
	return pdfFilename;
    }

    public static String GetPdfLink(String claimNo) {
	List<mdlQueryExecute> listParam = new ArrayList<mdlQueryExecute>();
	CachedRowSet jrs = null;
	String sql = "";
	String pdfLink = "";
	try {
	    sql = "SELECT document_link FROM ms_claim_document WHERE claim_no = ? ";
	    listParam.add(QueryExecuteAdapter.QueryParam("string", claimNo));
	    jrs = QueryExecuteAdapter.QueryExecute(sql, listParam, "GetPdfLink");
	    while (jrs.next()) {
		pdfLink = jrs.getString("document_link");
	    }
	} catch (Exception ex) {
	    logger.error("FAILED. Function : GetPdfLink, claimNo : " + claimNo + ", Exception:" + ex.toString(), ex);
	}
	return pdfLink;
    }

    public static List<model.mdlImage> GetClaimImageData(String claimNo) {
	String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();
	List<model.mdlImage> imageList = new ArrayList<model.mdlImage>();
	List<mdlQueryExecute> listParam = new ArrayList<mdlQueryExecute>();
	CachedRowSet jrs = null;
	String sql = "";
	try {
	    sql = "SELECT claim_image_id, claim_no, claim_document_type_id, username, image_original_name, image_uploaded_name, " + "image_directory, image_link, uploaded FROM ms_claim_image WHERE claim_no = ? ";
	    listParam.add(QueryExecuteAdapter.QueryParam("string", claimNo));
	    jrs = QueryExecuteAdapter.QueryExecute(sql, listParam, functionName);
	    while (jrs.next()) {
		model.mdlImage mdlImage = new model.mdlImage();
		mdlImage.claim_image_id = jrs.getString("claim_image_id");
		mdlImage.claim_no = jrs.getString("claim_no");
		mdlImage.claim_document_type_id = jrs.getString("claim_document_type_id");
		mdlImage.username = jrs.getString("username");
		mdlImage.image_original_name = jrs.getString("image_original_name");
		mdlImage.image_uploaded_name = jrs.getString("image_uploaded_name");
		mdlImage.image_directory = jrs.getString("image_directory");
		mdlImage.image_link = jrs.getString("image_link");
		mdlImage.uploaded = jrs.getInt("uploaded");
		imageList.add(mdlImage);
	    }
	} catch (Exception ex) {
	    logger.error("FAILED. Function : " + functionName + ", claimNo : " + claimNo + ", Exception:" + ex.toString(), ex);
	}
	return imageList;
    }

    public static boolean RemoveClaimImageData(String claimNo) {
	String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();
	List<mdlQueryExecute> listParam = new ArrayList<mdlQueryExecute>();
	boolean success = false;
	String sql = "";
	try {
	    sql = "DELETE FROM ms_claim_image WHERE claim_no = ? ";
	    listParam.add(QueryExecuteAdapter.QueryParam("string", claimNo));
	    success = QueryExecuteAdapter.QueryManipulate(sql, listParam, functionName);
	} catch (Exception ex) {
	    logger.error("FAILED. Function : " + functionName + ", claimNo : " + claimNo + ", Exception:" + ex.toString(), ex);
	}
	return success;
    }

    public static String GenerateClaimNo() {
	// TODO : FOR DEVELOPMENT ONLY! PLEASE COMMENT WHEN DEPLOY TO UAT OR PRODUCTION
	// String claimNo = "";
	// String dateNow = DateHelper.GetDateTimeNowCustomFormat("yyMMddHHmmssSSS");
	// StringBuilder sb = new StringBuilder();
	// sb.append("D901").append(dateNow);
	// claimNo = sb.toString();
	// return claimNo;

	String claimNo = "";
	String dateNow = DateHelper.GetDateTimeNowCustomFormat("yyyyMMddHHmmssSSS");
	StringBuilder sb = new StringBuilder();
	sb.append("901").append(dateNow);
	claimNo = sb.toString();
	return claimNo;
    }

    public static String GetLastClaimNo(String date) {
	List<mdlQueryExecute> listParam = new ArrayList<mdlQueryExecute>();
	CachedRowSet jrs = null;
	String sql = "";
	String claimNo = "";
	try {
	    sql = "SELECT ISNULL(MAX(claim_no),'') as claim_no FROM ms_claim WHERE SUBSTRING(claim_no,4,8) = ? ";
	    listParam.add(QueryExecuteAdapter.QueryParam("string", date));
	    jrs = QueryExecuteAdapter.QueryExecute(sql, listParam, "GetLastClaimNo");

	    while (jrs.next()) {
		claimNo = jrs.getString(1);
	    }
	} catch (Exception ex) {
	    logger.error("FAILED. Function : GetLastClaimNo, date : " + date + ", Exception:" + ex.toString(), ex);
	}
	return claimNo;
    }

    public static boolean UploadClaimData(model.mdlClaim mdlClaim) {
	String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();
	boolean success = false;
	try {
	    boolean successHitAPI = APIAdapter.HitAPIInsertClaim(mdlClaim);
	    if (successHitAPI) {
		List<model.mdlQueryTransaction> listmdlQueryTransaction = new ArrayList<model.mdlQueryTransaction>();
		model.mdlQueryTransaction mdlQueryTransaction = new model.mdlQueryTransaction();

		mdlQueryTransaction.sql = "INSERT INTO ms_claim (claim_no, username, member_id, member_type, member_child_id," + "member_name, claim_type, claim_type_name, claim_date, start_date," + "end_date, amount, policy_period, phone_number, bank_name," + "account_no, account_name, claim_status) VALUES (" + "?, ?, ?, ?, ?," + "?, ?, ?, ?, ?," + "?, ?, ?, ?, ?," + "?, ?, ?)";
		mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", mdlClaim.claim_no));
		mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", mdlClaim.username));
		mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", mdlClaim.member_id));
		mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", mdlClaim.member_type));
		mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", mdlClaim.member_child_id));

		mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", mdlClaim.member_name));
		mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", mdlClaim.claim_type));
		mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", mdlClaim.claim_type_name));
		mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", mdlClaim.claim_date));
		mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", mdlClaim.start_date));

		mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", mdlClaim.end_date));
		mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", mdlClaim.amount));
		mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", mdlClaim.policy_period));
		mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", mdlClaim.phone_number));
		mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", mdlClaim.bank_name));

		mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", mdlClaim.account_no));
		mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", mdlClaim.account_name));
		mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", "INPROGRESS"));
		listmdlQueryTransaction.add(mdlQueryTransaction);

		success = QueryExecuteAdapter.QueryTransaction(listmdlQueryTransaction, functionName);

		if (success) {
		    model.mdlUserMessage mdlUserMessage = new model.mdlUserMessage();
		    mdlUserMessage.username = mdlClaim.username;
		    mdlUserMessage.message_category = "claim-create";
		    mdlUserMessage.message_value = "";
		    mdlUserMessage.message_value2 = "";
		    MessageAdapter.CreateUserMessage(mdlUserMessage, mdlUserMessage.message_category, mdlClaim.language);
		    CreateClaimHistory(mdlClaim.claim_no, "submitted");
		    // SendClaimSubmittedEmail(mdlClaim, mdlClaim.language);
		}
	    }
	} catch (Exception ex) {
	    logger.error("FAILED. Function : SaveClaimImageData, mdlClaim : " + gson.toJson(mdlClaim) + ", Exception:" + ex.toString(), ex);
	}
	return success;
    }

    public static boolean SaveClaimImageData(model.mdlUploadClaim mdlUploadClaim, model.mdlImage mdlImage) {
	boolean success = false;
	List<mdlQueryExecute> listParam = new ArrayList<mdlQueryExecute>();
	String sql = "";
	try {
	    sql = "INSERT INTO ms_claim_image (claim_no,claim_document_type_id,username,image_original_name,image_uploaded_name,image_directory," + "image_link,uploaded) " + "VALUES (?,?,?,?,?,?," + "?,1 )";
	    listParam.add(QueryExecuteAdapter.QueryParam("string", mdlUploadClaim.claim_no));
	    listParam.add(QueryExecuteAdapter.QueryParam("string", mdlImage.claim_document_type_id));
	    listParam.add(QueryExecuteAdapter.QueryParam("string", mdlUploadClaim.username));
	    listParam.add(QueryExecuteAdapter.QueryParam("string", mdlImage.image_original_name));
	    listParam.add(QueryExecuteAdapter.QueryParam("string", mdlImage.image_uploaded_name));
	    listParam.add(QueryExecuteAdapter.QueryParam("string", mdlImage.image_directory));
	    listParam.add(QueryExecuteAdapter.QueryParam("string", mdlImage.image_link));
	    success = QueryExecuteAdapter.QueryManipulate(sql, listParam, "SaveClaimImageData");
	} catch (Exception ex) {
	    String jsonClaim = gson.toJson(mdlUploadClaim);
	    String jsonImage = gson.toJson(mdlImage);
	    logger.error("FAILED. Function : SaveClaimImageData, mdlUploadClaim : " + jsonClaim + ", mdlImage : " + jsonImage + ", Exception:" + ex.toString(), ex);
	}
	return success;
    }

    public static String ConvertJpegToPDF(List<model.mdlImage> imageLocationList, model.mdlUploadClaim mdlUploadClaim, String dateNow, String baseDirectoryPath) throws IOException {
	String pdfLocation = "";
	String pdfFilename = "";
	PDDocument doc = new PDDocument();
	String localPdfFileLocation = "";

	try {
	    String previousPdfFileName = GetPdfClaimDocumentName(mdlUploadClaim.claim_no);
	    if (!previousPdfFileName.equals("")) {
		pdfFilename = previousPdfFileName;
	    } else {
		String captiveOrNoncaptive = mdlUploadClaim.member_type.toUpperCase().contains(" NON ") ? "_noncaptive_" : "_captive_";
		pdfFilename = mdlUploadClaim.claim_no + captiveOrNoncaptive + dateNow + ".pdf";
	    }

	    for (model.mdlImage image : imageLocationList) {
		Files.find(Paths.get(image.image_directory), Integer.MAX_VALUE, (path, basicFileAttributes) -> Files.isRegularFile(path)).forEachOrdered(path -> addImageAsNewPage(doc, path.toString()));
	    }

	    Context context = (Context) new InitialContext().lookup("java:comp/env");
	    String pdfFileLocation = baseDirectoryPath + "/" + pdfFilename;
	    String urlWebServer = (String) context.lookup("url_web_server_for_upload");
	    String sourcePath = (String) context.lookup("source_path_claim");
	    localPdfFileLocation = sourcePath + pdfFileLocation;

	    doc.save(localPdfFileLocation);
	    doc.close();
	    boolean success = false;
	    // if there is no previous pdf for claim no
	    if (!previousPdfFileName.equals("")) {
		boolean successUpdateClaimPdfData = UpdateClaimStatus(mdlUploadClaim.claim_no, "INPROGRESS");
		if (successUpdateClaimPdfData) {
		    boolean successHitAPI = APIAdapter.HitAPIInsertClaimDocument(mdlUploadClaim.claim_no, localPdfFileLocation);
		    if (successHitAPI) {
			success = true;
		    }
		}
	    } else {
		boolean successSaveClaimPdfData = SaveClaimPDFData(mdlUploadClaim, pdfFilename, pdfFileLocation);
		if (successSaveClaimPdfData) {
		    boolean successHitAPI = APIAdapter.HitAPIInsertClaimDocument(mdlUploadClaim.claim_no, localPdfFileLocation);
		    if (successHitAPI) {
			success = true;
		    }
		}
	    }
	    if (success) {
		SetClaimDocumentUploaded(mdlUploadClaim.claim_no);
		model.mdlUserMessage mdlUserMessage = new model.mdlUserMessage();
		mdlUserMessage.username = mdlUploadClaim.username;
		mdlUserMessage.message_category = "claim-in-progress";
		mdlUserMessage.message_value = mdlUploadClaim.claim_no;
		mdlUserMessage.message_value2 = "INPROGRESS";
		MessageAdapter.CreateUserMessage(mdlUserMessage, mdlUserMessage.message_category, mdlUploadClaim.language);
		CreateClaimHistory(mdlUploadClaim.claim_no, "inprogress");
		pdfLocation = urlWebServer + pdfFileLocation;
	    }
	} catch (Exception ex) {
	    doc.close();
	    logger.error("FAILED. Function : ConvertJpegToPDF, imageLocationList = " + gson.toJson(imageLocationList) + ", mdlUploadClaim = " + gson.toJson(mdlUploadClaim) + ", dateNow = " + dateNow + ", baseDirectoryPath = " + baseDirectoryPath + ", baseDirectoryPath = " + baseDirectoryPath + ", Exception:" + ex.toString(), ex);
	}
	return pdfLocation;
    }

    public static boolean SaveClaimPDFData(model.mdlUploadClaim mdlUploadClaim, String pdfFilename, String pdfFileLocation) {
	boolean success = false;
	List<mdlQueryExecute> listParam = new ArrayList<mdlQueryExecute>();
	String sql = "";
	try {
	    Context context = (Context) new InitialContext().lookup("java:comp/env");
	    String urlWebServer = (String) context.lookup("url_web_server_for_upload");
	    String sourcePath = (String) context.lookup("source_path_claim");
	    sql = "INSERT INTO ms_claim_document(username,claim_no,member_id,member_child_id,document_filename, document_directory,document_link,uploaded) " + "VALUES (?,?,?,?,?," + "?,?,0)";
	    listParam.add(QueryExecuteAdapter.QueryParam("string", mdlUploadClaim.username));
	    listParam.add(QueryExecuteAdapter.QueryParam("string", mdlUploadClaim.claim_no));
	    listParam.add(QueryExecuteAdapter.QueryParam("string", mdlUploadClaim.member_id));
	    listParam.add(QueryExecuteAdapter.QueryParam("string", mdlUploadClaim.member_child_id));
	    listParam.add(QueryExecuteAdapter.QueryParam("string", pdfFilename));

	    String localPdfFileLocation = sourcePath + pdfFileLocation;
	    String serverPdfLocation = urlWebServer + pdfFileLocation;
	    listParam.add(QueryExecuteAdapter.QueryParam("string", localPdfFileLocation));
	    listParam.add(QueryExecuteAdapter.QueryParam("string", serverPdfLocation));
	    success = QueryExecuteAdapter.QueryManipulate(sql, listParam, "SaveClaimPDFData");
	} catch (Exception ex) {
	    String jsonClaim = gson.toJson(mdlUploadClaim);
	    logger.error("FAILED. Function : SaveClaimPDFData, mdlUploadClaim : " + jsonClaim + ", pdfFileLocation : " + pdfFileLocation + ", Exception:" + ex.toString(), ex);
	}
	return success;
    }

    public static boolean UpdateClaimStatus(String claimNo, String claimStatus) {
	String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();
	boolean success = false;
	List<mdlQueryExecute> listParam = new ArrayList<mdlQueryExecute>();
	String sql = "";
	try {
	    sql = "UPDATE ms_claim SET claim_status = ? WHERE claim_no = ? ";
	    listParam.add(QueryExecuteAdapter.QueryParam("string", claimStatus));
	    listParam.add(QueryExecuteAdapter.QueryParam("string", claimNo));
	    success = QueryExecuteAdapter.QueryManipulate(sql, listParam, functionName);
	} catch (Exception ex) {
	    logger.error("FAILED. Function : " + functionName + ", claim_no : " + claimNo + ", claimStatus : " + claimStatus + ", Exception:" + ex.toString(), ex);
	}
	return success;
    }

    public static boolean SetClaimDocumentUploaded(String claim_no) {
	String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();
	boolean success = false;
	List<mdlQueryExecute> listParam = new ArrayList<mdlQueryExecute>();
	String sql = "";
	try {
	    sql = "UPDATE ms_claim_document SET uploaded = 1 WHERE claim_no = ? ";
	    listParam.add(QueryExecuteAdapter.QueryParam("string", claim_no));
	    success = QueryExecuteAdapter.QueryManipulate(sql, listParam, functionName);
	} catch (Exception ex) {
	    logger.error("FAILED. Function : " + functionName + ", claim_no : " + claim_no + ", Exception:" + ex.toString(), ex);
	}
	return success;
    }

    public static boolean CreateClaimHistory(String claim_no, String claim_history_type) {
	String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();
	List<mdlQueryExecute> listParam = new ArrayList<mdlQueryExecute>();
	String sql = "";
	boolean success = false;
	String indonesian, english = "";
	try {
	    switch (claim_history_type.toUpperCase()) {
	    case "SUBMITTED":
		indonesian = "Claim berhasil dikirim";
		english = "Claim submitted successfully";
		break;
	    case "INPROGRESS":
		indonesian = "Pengajuan claim sedang dalam proses dan di review";
		english = "Claim submission is in progress and being reviewed";
		break;
	    case "PENDING":
		indonesian = "Claim menunggu untuk dokumen tambahan";
		english = "Claim waiting for additional documents";
		break;
	    case "PAID":
		indonesian = "Pengajuan claim disetujui";
		english = "Claim submission approved";
		break;
	    case "REJECTED":
		indonesian = "Pengajuan claim ditolak";
		english = "Claim submission rejected";
		break;
	    default:
		logger.error("FAILED. Function : CreateClaimHistory, claim_no = " + claim_no + ", claim_history_type = " + claim_history_type);
		return false;
	    }

	    sql = "INSERT ms_claim_history (claim_no, claim_history_text_english, claim_history_text_indonesian, claim_history_type) " + "VALUES (?, ?, ?, ?) ";
	    listParam.add(QueryExecuteAdapter.QueryParam("string", claim_no));
	    listParam.add(QueryExecuteAdapter.QueryParam("string", english));
	    listParam.add(QueryExecuteAdapter.QueryParam("string", indonesian));
	    listParam.add(QueryExecuteAdapter.QueryParam("string", claim_history_type));
	    success = QueryExecuteAdapter.QueryManipulate(sql, listParam, functionName);
	} catch (Exception ex) {
	    logger.error("FAILED. Function : " + functionName + ", Exception:" + ex.toString(), ex);
	}
	return success;
    }

    public static model.mdlClaim GetClaimData(String claimNo) {
	List<mdlQueryExecute> listParam = new ArrayList<mdlQueryExecute>();
	CachedRowSet jrs = null;
	String sql = "";
	model.mdlClaim mdlClaim = new model.mdlClaim();
	try {
	    sql = "SELECT claim_no, claim_status, username, member_id, member_type, " + "member_child_id, member_name, claim_type, claim_type_name, claim_date, " + "hospital_name, start_date, end_date, amount, policy_period, " + "phone_number, bank_name, account_no, account_name FROM ms_claim WHERE claim_no = ? ";
	    listParam.add(QueryExecuteAdapter.QueryParam("string", claimNo));
	    jrs = QueryExecuteAdapter.QueryExecute(sql, listParam, "GetClaimData");
	    while (jrs.next()) {
		mdlClaim.claim_no = jrs.getString("claim_no");
		mdlClaim.claim_status = jrs.getString("claim_status");
		mdlClaim.username = jrs.getString("username");
		mdlClaim.member_id = jrs.getString("member_id");
		mdlClaim.member_type = jrs.getString("member_type");
		mdlClaim.member_child_id = jrs.getString("member_child_id");
		mdlClaim.member_name = jrs.getString("member_name");
		mdlClaim.claim_type = jrs.getString("claim_type");
		mdlClaim.claim_type_name = jrs.getString("claim_type_name");
		mdlClaim.claim_date = jrs.getString("claim_date");
		mdlClaim.start_date = jrs.getString("start_date");
		mdlClaim.end_date = jrs.getString("end_date");
		mdlClaim.amount = jrs.getString("amount");
		mdlClaim.policy_period = jrs.getString("policy_period");
		mdlClaim.phone_number = jrs.getString("phone_number");
		mdlClaim.bank_name = jrs.getString("bank_name");
		mdlClaim.account_no = jrs.getString("account_no");
		mdlClaim.account_name = jrs.getString("account_name");
		mdlClaim.document_type_list = GetClaimDocumentTypeList(mdlClaim.claim_no);
		mdlClaim.image_list = GetClaimImageData(mdlClaim.claim_no);
		mdlClaim.pdf_link = GetPdfLink(mdlClaim.claim_no);
		mdlClaim.claim_history = GetClaimHistory(mdlClaim.claim_no);
		mdlClaim.update_document_link = GetUpdateDocumentLink(mdlClaim.claim_no, mdlClaim.claim_status);
	    }
	} catch (Exception ex) {
	    logger.error("FAILED. Function : GetClaimData, claimNo : " + claimNo + ", Exception:" + ex.toString(), ex);
	}
	return mdlClaim;
    }

    public static String UpdateClaimStatus(model.mdlUpdateClaim mdlUpdateClaim, String username) {
	String updateDocumentLink = "";
	List<mdlQueryExecute> listParam = new ArrayList<mdlQueryExecute>();
	String sql = "";
	try {
	    sql = "UPDATE ms_claim SET claim_status = ? WHERE claim_no = ? ";
	    listParam.add(QueryExecuteAdapter.QueryParam("string", mdlUpdateClaim.claim_status.toUpperCase()));
	    listParam.add(QueryExecuteAdapter.QueryParam("string", mdlUpdateClaim.claim_no));
	    boolean updateClaimStatus = QueryExecuteAdapter.QueryManipulate(sql, listParam, "UpdateClaimData");

	    if (updateClaimStatus) {
		updateDocumentLink = CreateUpdateDocument(mdlUpdateClaim);

		if (!updateDocumentLink.equals("")) {
		    model.mdlUserMessage mdlUserMessage = new model.mdlUserMessage();
		    mdlUserMessage.username = username;
		    mdlUserMessage.message_value = mdlUpdateClaim.claim_no;
		    mdlUserMessage.message_value2 = mdlUpdateClaim.claim_status.toUpperCase();
		    if (mdlUpdateClaim.claim_status.equalsIgnoreCase("PENDING")) {
			mdlUserMessage.message_category = "claim-pending";
			MessageAdapter.CreateUserMessage(mdlUserMessage, mdlUserMessage.message_category, mdlUpdateClaim.language);
			CreateClaimHistory(mdlUpdateClaim.claim_no, "pending");
		    } else if (mdlUpdateClaim.claim_status.equalsIgnoreCase("REJECTED")) {
			mdlUserMessage.message_category = "claim-rejected";
			MessageAdapter.CreateUserMessage(mdlUserMessage, mdlUserMessage.message_category, mdlUpdateClaim.language);
			CreateClaimHistory(mdlUpdateClaim.claim_no, "rejected");
		    } else if (mdlUpdateClaim.claim_status.equalsIgnoreCase("PAID")) {
			mdlUserMessage.message_category = "claim-paid";
			MessageAdapter.CreateUserMessage(mdlUserMessage, mdlUserMessage.message_category, mdlUpdateClaim.language);
			CreateClaimHistory(mdlUpdateClaim.claim_no, "paid");
		    }
		}
	    }
	} catch (Exception ex) {
	    String jsonIn = gson.toJson(mdlUpdateClaim);
	    logger.error("FAILED. Function : UpdateClaimData, mdlUpdateClaim : " + jsonIn + ", Exception:" + ex.toString(), ex);
	}
	return updateDocumentLink;
    }

    private static void addImageAsNewPage(PDDocument doc, String imagePath) {
	try {
	    PDImageXObject image = PDImageXObject.createFromFile(imagePath, doc);
	    PDRectangle pageSize = PDRectangle.A4;

	    int originalWidth = image.getWidth();
	    int originalHeight = image.getHeight();
	    float pageWidth = pageSize.getWidth();
	    float pageHeight = pageSize.getHeight();
	    float ratio = Math.min(pageWidth / originalWidth, pageHeight / originalHeight);
	    float scaledWidth = originalWidth * ratio;
	    float scaledHeight = originalHeight * ratio;
	    float x = (pageWidth - scaledWidth) / 2;
	    float y = (pageHeight - scaledHeight) / 2;

	    PDPage page = new PDPage(pageSize);
	    doc.addPage(page);
	    try (PDPageContentStream contents = new PDPageContentStream(doc, page)) {
		contents.drawImage(image, x, y, scaledWidth, scaledHeight);
	    }
	} catch (Exception ex) {
	    logger.error("FAILED. Function : addImageAsNewPage, imagePath = " + imagePath + ", Exception:" + ex.toString(), ex);
	}
    }

    public static int GetClaimTotalList(String username, String search) {
	String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();
	CachedRowSet crs = null;
	String sql = "";
	int return_value = 0;
	String search_part = "";
	try {
	    if (search != null) {
		search_part = "WHERE claim_no LIKE ? OR account_name LIKE ? ";
	    }
	    sql = "SELECT COUNT(*) total " + "FROM ms_claim " + search_part;
	    List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
	    if (search != null) {
		String searchString = "%" + search.trim().replace(" ", "%") + "%";
		listParam.add(QueryExecuteAdapter.QueryParam("string", searchString));
		listParam.add(QueryExecuteAdapter.QueryParam("string", searchString));
	    }

	    crs = QueryExecuteAdapter.QueryExecute(sql, listParam, functionName);

	    while (crs.next()) {
		return_value = crs.getInt("total");
	    }
	} catch (Exception ex) {
	    logger.error("FAILED. Function : " + functionName + ", Exception:" + ex.toString(), ex);
	}
	return return_value;
    }

    public static List<model.mdlClaim> GetClaimListByDate(String username, String pageNumber, String pageSize, String search) {
	String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();
	List<model.mdlClaim> claimList = new ArrayList<model.mdlClaim>();
	CachedRowSet jrs = null;
	String sql = "";
	String search_part = "";
	try {
	    if (search != null) {
		search_part = "WHERE claim_no LIKE ? OR account_name LIKE ? ";
	    }
	    sql = "SELECT claim_no, claim_status, username, " + "member_id, member_type, member_child_id, member_name, " + "claim_type, claim_type_name, claim_date, " + "hospital_name, start_date, end_date, amount, " + "policy_period, phone_number, " + "bank_name, account_no, " + "account_name " + "FROM ms_claim " + search_part + "ORDER BY claim_no DESC OFFSET ? ROWS FETCH NEXT ? ROWS ONLY;";
	    List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
	    // if (username != null) {
	    // listParam.add(QueryExecuteAdapter.QueryParam("string", username));
	    // } else {
	    // listParam.add(QueryExecuteAdapter.QueryParam("null", java.sql.Types.NULL));
	    // }

	    if (search != null) {
		String searchString = "%" + search.trim().replace(" ", "%") + "%";
		listParam.add(QueryExecuteAdapter.QueryParam("string", searchString));
		listParam.add(QueryExecuteAdapter.QueryParam("string", searchString));
	    }

	    listParam.add(QueryExecuteAdapter.QueryParam("int", ((Integer.parseInt(pageNumber) - 1) * Integer.parseInt(pageSize))));
	    listParam.add(QueryExecuteAdapter.QueryParam("int", Integer.parseInt(pageSize)));
	    jrs = QueryExecuteAdapter.QueryExecute(sql, listParam, functionName);
	    while (jrs.next()) {
		model.mdlClaim mdlClaim = new model.mdlClaim();
		mdlClaim.claim_no = jrs.getString("claim_no");
		mdlClaim.claim_status = jrs.getString("claim_status");
		mdlClaim.username = jrs.getString("username");
		mdlClaim.member_id = jrs.getString("member_id");
		mdlClaim.member_type = jrs.getString("member_type");
		mdlClaim.member_child_id = jrs.getString("member_child_id");
		mdlClaim.member_name = jrs.getString("member_name");
		mdlClaim.claim_type = jrs.getString("claim_type");
		mdlClaim.claim_type_name = jrs.getString("claim_type_name");
		mdlClaim.claim_date = jrs.getString("claim_date");
		mdlClaim.start_date = jrs.getString("start_date");
		mdlClaim.end_date = jrs.getString("end_date");
		mdlClaim.amount = jrs.getString("amount");
		mdlClaim.policy_period = jrs.getString("policy_period");
		mdlClaim.phone_number = jrs.getString("phone_number");
		mdlClaim.bank_name = jrs.getString("bank_name");
		mdlClaim.account_no = jrs.getString("account_no");
		mdlClaim.account_name = jrs.getString("account_name");
		// mdlClaim.document_type_list = GetClaimDocumentTypeList(mdlClaim.claim_no);
		// mdlClaim.image_list = GetClaimImageData(mdlClaim.claim_no);
		mdlClaim.pdf_link = GetPdfLink(mdlClaim.claim_no);
		// mdlClaim.claim_history = GetClaimHistory(mdlClaim.claim_no);
		// mdlClaim.update_document_link = GetUpdateDocumentLink(mdlClaim.claim_no, mdlClaim.claim_status);
		claimList.add(mdlClaim);
	    }
	} catch (Exception ex) {
	    logger.error("FAILED. Function : " + functionName + ", Exception:" + ex.toString(), ex);
	}
	return claimList;
    }

    public static List<model.mdlClaim> GetClaimList(String username, String status, String pageString) {
	String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();
	int page = Integer.valueOf(pageString);
	int pagesize = 10;
	int page_start = page * pagesize - (pagesize - 1);
	int page_end = page * pagesize;

	List<model.mdlClaim> claimList = new ArrayList<model.mdlClaim>();
	List<mdlQueryExecute> listParam = new ArrayList<mdlQueryExecute>();
	CachedRowSet jrs = null;
	String sql = "";
	try {
	    String claimStatus = "";
	    if (!status.equalsIgnoreCase("all")) {
		claimStatus = " AND claim_status = ? ";
	    }
	    sql = "select * from (SELECT ROW_NUMBER() OVER(ORDER BY create_date DESC) AS row_number, " + "claim_no, claim_status, username, member_id, member_type, " + "member_child_id, member_name, claim_type, claim_type_name, claim_date, " + "hospital_name, start_date, end_date, amount, policy_period, " + "phone_number, bank_name, account_no, account_name FROM ms_claim WHERE username = ? " + claimStatus + ") tbl WHERE tbl.row_number between ? and ? ";
	    listParam.add(QueryExecuteAdapter.QueryParam("string", username));
	    if (!status.equalsIgnoreCase("all")) {
		listParam.add(QueryExecuteAdapter.QueryParam("string", status));
	    }
	    listParam.add(QueryExecuteAdapter.QueryParam("int", page_start));
	    listParam.add(QueryExecuteAdapter.QueryParam("int", page_end));
	    jrs = QueryExecuteAdapter.QueryExecute(sql, listParam, functionName);
	    while (jrs.next()) {
		model.mdlClaim mdlClaim = new model.mdlClaim();
		mdlClaim.claim_no = jrs.getString("claim_no");
		mdlClaim.claim_status = jrs.getString("claim_status");
		mdlClaim.username = jrs.getString("username");
		mdlClaim.member_id = jrs.getString("member_id");
		mdlClaim.member_type = jrs.getString("member_type");
		mdlClaim.member_child_id = jrs.getString("member_child_id");
		mdlClaim.member_name = jrs.getString("member_name");
		mdlClaim.claim_type = jrs.getString("claim_type");
		mdlClaim.claim_type_name = jrs.getString("claim_type_name");
		mdlClaim.claim_date = jrs.getString("claim_date");
		mdlClaim.start_date = jrs.getString("start_date");
		mdlClaim.end_date = jrs.getString("end_date");
		mdlClaim.amount = jrs.getString("amount");
		mdlClaim.policy_period = jrs.getString("policy_period");
		mdlClaim.phone_number = jrs.getString("phone_number");
		mdlClaim.bank_name = jrs.getString("bank_name");
		mdlClaim.account_no = jrs.getString("account_no");
		mdlClaim.account_name = jrs.getString("account_name");
		mdlClaim.document_type_list = GetClaimDocumentTypeList(mdlClaim.claim_no);
		mdlClaim.image_list = GetClaimImageData(mdlClaim.claim_no);
		mdlClaim.pdf_link = GetPdfLink(mdlClaim.claim_no);
		mdlClaim.claim_history = GetClaimHistory(mdlClaim.claim_no);
		mdlClaim.update_document_link = GetUpdateDocumentLink(mdlClaim.claim_no, mdlClaim.claim_status);
		claimList.add(mdlClaim);
	    }
	} catch (Exception ex) {
	    logger.error("FAILED. Function : " + functionName + ", username : " + username + ", status : " + status + ", Exception:" + ex.toString(), ex);
	}
	return claimList;
    }

    public static List<model.mdlClaimHistory> GetClaimHistory(String claimNo) {
	List<model.mdlClaimHistory> claimHistoryList = new ArrayList<model.mdlClaimHistory>();
	List<mdlQueryExecute> listParam = new ArrayList<mdlQueryExecute>();
	CachedRowSet jrs = null;
	String sql = "";
	try {
	    sql = "SELECT claim_history_id, claim_no, claim_history_text_english, claim_history_text_indonesian, claim_history_type, create_date " + "FROM ms_claim_history WHERE claim_no = ? ORDER BY create_date DESC ";
	    listParam.add(QueryExecuteAdapter.QueryParam("string", claimNo));
	    jrs = QueryExecuteAdapter.QueryExecute(sql, listParam, "GetClaimData");
	    while (jrs.next()) {
		model.mdlClaimHistory mdlClaimHistory = new model.mdlClaimHistory();
		mdlClaimHistory.claim_history_id = jrs.getString("claim_history_id");
		mdlClaimHistory.claim_no = jrs.getString("claim_no");
		mdlClaimHistory.claim_history_text_english = jrs.getString("claim_history_text_english");
		mdlClaimHistory.claim_history_text_indonesian = jrs.getString("claim_history_text_indonesian");
		mdlClaimHistory.claim_history_type = jrs.getString("claim_history_type");
		mdlClaimHistory.create_date = jrs.getString("create_date");
		claimHistoryList.add(mdlClaimHistory);
	    }
	} catch (Exception ex) {
	    logger.error("FAILED. Function : GetClaimHistory, claimNo : " + claimNo + ", Exception:" + ex.toString(), ex);
	}
	return claimHistoryList;
    }

    public static List<model.mdlClaimDocumentType> GetClaimDocumentType(String claimType) {
	String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();
	List<model.mdlClaimDocumentType> claimDocumentTypeList = new ArrayList<model.mdlClaimDocumentType>();
	List<mdlQueryExecute> listParam = new ArrayList<mdlQueryExecute>();
	CachedRowSet jrs = null;
	String sql = "";
	try {
	    sql = "SELECT claim_type_code, claim_type_name, claim_document_type_id, claim_document_type_name, claim_document_type_details, " + "claim_document_type_information1, claim_document_type_information2, mandatory FROM ms_claim_document_type WHERE claim_type_code = ? ";
	    listParam.add(QueryExecuteAdapter.QueryParam("string", claimType));
	    jrs = QueryExecuteAdapter.QueryExecute(sql, listParam, functionName);
	    while (jrs.next()) {
		model.mdlClaimDocumentType mdlClaimDocumentType = new model.mdlClaimDocumentType();
		mdlClaimDocumentType.claim_type_code = jrs.getString("claim_type_code");
		mdlClaimDocumentType.claim_type_name = jrs.getString("claim_type_name");
		mdlClaimDocumentType.claim_document_type_id = jrs.getString("claim_document_type_id");
		mdlClaimDocumentType.claim_document_type_name = jrs.getString("claim_document_type_name");
		mdlClaimDocumentType.claim_document_type_details = jrs.getString("claim_document_type_details");
		mdlClaimDocumentType.claim_document_type_information1 = jrs.getString("claim_document_type_information1");
		mdlClaimDocumentType.claim_document_type_information2 = jrs.getString("claim_document_type_information2");
		mdlClaimDocumentType.mandatory = jrs.getString("mandatory");
		mdlClaimDocumentType.claim_document_type_count = 0;
		claimDocumentTypeList.add(mdlClaimDocumentType);
	    }
	} catch (Exception ex) {
	    logger.error("FAILED. Function : " + functionName + ", claimType : " + claimType + ", Exception:" + ex.toString(), ex);
	}
	return claimDocumentTypeList;
    }

    public static List<model.mdlClaimDocumentType> GetClaimDocumentTypeList(String claimNo) {
	String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();
	List<model.mdlClaimDocumentType> claimDocumentTypeList = new ArrayList<model.mdlClaimDocumentType>();
	List<mdlQueryExecute> listParam = new ArrayList<mdlQueryExecute>();
	CachedRowSet jrs = null;
	String sql = "";
	try {
	    sql = "SELECT type.*, ISNULL(image.image_count, 0) AS image_count FROM ms_claim_document_type type " + "LEFT JOIN (SELECT claim_no,claim_document_type_id, COUNT(claim_document_type_id) AS image_count " + "FROM ms_claim_image where claim_no = ? GROUP BY claim_no,claim_document_type_id ) image " + "ON type.claim_document_type_id = image.claim_document_type_id " + "LEFT JOIN ms_claim claim ON claim.claim_no = image.claim_no " + "WHERE type.claim_type_code = (SELECT claim_type FROM ms_claim WHERE claim_no = ?)";
	    listParam.add(QueryExecuteAdapter.QueryParam("string", claimNo));
	    listParam.add(QueryExecuteAdapter.QueryParam("string", claimNo));
	    jrs = QueryExecuteAdapter.QueryExecute(sql, listParam, functionName);
	    while (jrs.next()) {
		model.mdlClaimDocumentType mdlClaimDocumentType = new model.mdlClaimDocumentType();
		mdlClaimDocumentType.claim_type_code = jrs.getString(1);
		mdlClaimDocumentType.claim_type_name = jrs.getString(2);
		mdlClaimDocumentType.claim_document_type_id = jrs.getString(3);
		mdlClaimDocumentType.claim_document_type_name = jrs.getString(4);
		mdlClaimDocumentType.claim_document_type_details = jrs.getString(5);
		mdlClaimDocumentType.claim_document_type_information1 = jrs.getString(6);
		mdlClaimDocumentType.claim_document_type_information2 = jrs.getString(7);
		mdlClaimDocumentType.claim_document_type_count = jrs.getInt(9);
		claimDocumentTypeList.add(mdlClaimDocumentType);
	    }
	} catch (Exception ex) {
	    logger.error("FAILED. Function : " + functionName + ", claimNo : " + claimNo + ", Exception:" + ex.toString(), ex);
	}
	return claimDocumentTypeList;
    }

    @SuppressWarnings("deprecation")
    public static String CreateUpdateDocument(model.mdlUpdateClaim mdlUpdateClaim) {
	String htmlLink = "";
	try {
	    String documentTemplate = GetUpdateDocumentTemplate(mdlUpdateClaim.claim_status);
	    if (documentTemplate.equals("")) {
		return htmlLink;
	    } else {
		Context context = (Context) new InitialContext().lookup("java:comp/env");
		String sourcePath = (String) context.lookup("source_path_claim");
		String urlWebServer = (String) context.lookup("url_web_server_for_upload");
		String filename = mdlUpdateClaim.claim_no + "_" + mdlUpdateClaim.member_child_id + "_" + mdlUpdateClaim.claim_status + ".html";
		String outputFile = sourcePath + "bnilife/letters/" + filename;
		String letterLink = urlWebServer + "bnilife/letters/" + filename;

		documentTemplate = documentTemplate.replace("@letter_number", mdlUpdateClaim.udpate_claim_data.letter_number);
		documentTemplate = documentTemplate.replace("@approve_date", mdlUpdateClaim.udpate_claim_data.approve_date);
		documentTemplate = documentTemplate.replace("@member_name", mdlUpdateClaim.udpate_claim_data.member_name);
		documentTemplate = documentTemplate.replace("@claim_no", mdlUpdateClaim.udpate_claim_data.claim_no);
		documentTemplate = documentTemplate.replace("@claim_date", mdlUpdateClaim.udpate_claim_data.claim_date);
		documentTemplate = documentTemplate.replace("@title", mdlUpdateClaim.udpate_claim_data.title);
		documentTemplate = documentTemplate.replace("@status_info_detail", mdlUpdateClaim.udpate_claim_data.status_info_detail);
		documentTemplate = documentTemplate.replace("@status_info", mdlUpdateClaim.udpate_claim_data.status_info);
		documentTemplate = documentTemplate.replace("@additional_info", mdlUpdateClaim.udpate_claim_data.additional_info);
		documentTemplate = documentTemplate.replace("@member_child_name", mdlUpdateClaim.udpate_claim_data.member_child_name);
		documentTemplate = documentTemplate.replace("@member_child_id", mdlUpdateClaim.udpate_claim_data.member_child_id);
		documentTemplate = documentTemplate.replace("@amount", mdlUpdateClaim.udpate_claim_data.amount);
		documentTemplate = documentTemplate.replace("@bnil_claim_manager_name", mdlUpdateClaim.udpate_claim_data.bnil_claim_manager_name);
		documentTemplate = documentTemplate.replace("@bnil_position", mdlUpdateClaim.udpate_claim_data.bnil_position);
		File newHtmlFile = new File(outputFile);
		FileUtils.writeStringToFile(newHtmlFile, documentTemplate);

		// ITextRenderer renderer = new ITextRenderer();
		// renderer.setDocumentFromString(documentTemplate, "C:/Program Files/Apache Software Foundation/Tomcat
		// 8.0/webapps/Images/letters");
		// renderer.layout();
		//
		// try (OutputStream os = Files.newOutputStream(Paths.get(outputFile))) {
		// renderer.createPDF(os);
		// }catch (Exception ex){
		// logger.error("FAILED. Function :Generate PDF GetUpdateDocumentTemplate, outputFile : " + outputFile + ", Exception:" +
		// ex.toString(), ex);
		// }

		// String html = documentTemplate;
		// final Document document = Jsoup.parse(html);
		// document.outputSettings().syntax(Document.OutputSettings.Syntax.xml);
		//
		// ITextRenderer renderer = new ITextRenderer();
		// renderer.setDocumentFromString(document.html());
		// renderer.layout();
		//
		// try (OutputStream os = Files.newOutputStream(Paths.get(outputFile))) {
		// renderer.createPDF(os);
		// }

		SaveUpdateDocumentData(mdlUpdateClaim, filename, outputFile, letterLink, documentTemplate);
		htmlLink = letterLink;

	    }

	} catch (Exception ex) {
	    logger.error("FAILED. Function : CreateUpdateDocument, mdlUpdateClaim : " + gson.toJson(mdlUpdateClaim) + ", Exception:" + ex.toString(), ex);
	}
	return htmlLink;
    }

    public static String GetUpdateDocumentTemplate(String claimStatus) {
	String template = "";
	// List<mdlQueryExecute> listParam = new ArrayList<mdlQueryExecute>();
	CachedRowSet jrs = null;
	String sql = "";
	try {
	    sql = "SELECT document_template_text FROM ms_document_template";
	    jrs = QueryExecuteAdapter.QueryExecute(sql, "GetUpdateDocumentTemplate");
	    while (jrs.next()) {
		template = jrs.getString(1);
	    }
	} catch (Exception ex) {
	    logger.error("FAILED. Function : GetUpdateDocumentTemplate, claimStatus : " + claimStatus + ", Exception:" + ex.toString(), ex);
	}
	return template;
    }

    public static boolean SaveUpdateDocumentData(model.mdlUpdateClaim mdlUpdateClaim, String filename, String localHtmlDirectory, String htmlLink, String htmlContent) {
	String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();
	boolean success = false;
	List<mdlQueryExecute> listParam = new ArrayList<mdlQueryExecute>();
	String sql = "";
	try {
	    sql = "INSERT ms_claim_update_document (claim_no, member_id, member_child_id, member_type, claim_status, " + "update_document_filename, update_document_directory, update_document_link, update_document_text) " + "VALUES (?, ?, ?, ?, ?, " + "?, ?, ?, ?) ";
	    listParam.add(QueryExecuteAdapter.QueryParam("string", mdlUpdateClaim.claim_no));
	    listParam.add(QueryExecuteAdapter.QueryParam("string", mdlUpdateClaim.member_id));
	    listParam.add(QueryExecuteAdapter.QueryParam("string", mdlUpdateClaim.member_child_id));
	    listParam.add(QueryExecuteAdapter.QueryParam("string", mdlUpdateClaim.member_type));
	    listParam.add(QueryExecuteAdapter.QueryParam("string", mdlUpdateClaim.claim_status));
	    listParam.add(QueryExecuteAdapter.QueryParam("string", filename));
	    listParam.add(QueryExecuteAdapter.QueryParam("string", localHtmlDirectory));
	    listParam.add(QueryExecuteAdapter.QueryParam("string", htmlLink));
	    listParam.add(QueryExecuteAdapter.QueryParam("string", htmlContent));
	    success = QueryExecuteAdapter.QueryManipulate(sql, listParam, functionName);
	} catch (Exception ex) {
	    String jsonIn = gson.toJson(mdlUpdateClaim);
	    logger.error("FAILED. Function : " + functionName + ", mdlUpdateClaim : " + jsonIn + ", filename : " + filename + ", localHtmlDirectory : " + localHtmlDirectory + ", htmlLink : " + htmlLink + ", Exception:" + ex.toString(), ex);
	}
	return success;
    }

    public static String GetUpdateDocumentLink(String claimNo, String claimStatus) {
	String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();
	String updateDocumentLink = "";
	List<mdlQueryExecute> listParam = new ArrayList<mdlQueryExecute>();
	CachedRowSet jrs = null;
	String sql = "";
	try {
	    sql = "SELECT TOP 1 update_document_link FROM ms_claim_update_document WHERE claim_no = ? AND claim_status = ? ORDER BY create_date DESC ";
	    listParam.add(QueryExecuteAdapter.QueryParam("string", claimNo));
	    listParam.add(QueryExecuteAdapter.QueryParam("string", claimStatus));
	    jrs = QueryExecuteAdapter.QueryExecute(sql, listParam, functionName);
	    while (jrs.next()) {
		updateDocumentLink = jrs.getString("update_document_link");
	    }
	} catch (Exception ex) {
	    logger.error("FAILED. Function : " + functionName + ", claimNo : " + claimNo + ", claimStatus : " + claimStatus + ", Exception:" + ex.toString(), ex);
	}
	return updateDocumentLink;
    }

    public static Integer CheckClaimNo(String claimNo) {
	String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();
	Integer imageCount = 0;
	List<mdlQueryExecute> listParam = new ArrayList<mdlQueryExecute>();
	CachedRowSet jrs = null;
	String sql = "";
	try {
	    sql = "SELECT ISNULL(COUNT(image_uploaded_name),0) FROM ms_claim_image WHERE claim_no = ? ";
	    listParam.add(QueryExecuteAdapter.QueryParam("string", claimNo));
	    jrs = QueryExecuteAdapter.QueryExecute(sql, listParam, functionName);
	    while (jrs.next()) {
		imageCount = jrs.getInt(1);
	    }
	} catch (Exception ex) {
	    logger.error("FAILED. Function : " + functionName + ", claimNo : " + claimNo + ", Exception:" + ex.toString(), ex);
	}
	return imageCount;
    }

    public static model.mdlClaim CheckClaimDuplicateData(model.mdlClaim mdlClaim) {
	String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();
	model.mdlClaim mdlClaimExists = new model.mdlClaim();
	List<mdlQueryExecute> listParam = new ArrayList<mdlQueryExecute>();
	CachedRowSet jrs = null;
	String sql = "";
	try {
	    sql = "SELECT claim_no, claim_status FROM ms_claim " + "WHERE member_id = ? and member_child_id = ? and start_date = ? and end_date = ? and amount = ? and claim_date = ? and claim_type = ? ";
	    listParam.add(QueryExecuteAdapter.QueryParam("string", mdlClaim.member_id));
	    listParam.add(QueryExecuteAdapter.QueryParam("string", mdlClaim.member_child_id));
	    listParam.add(QueryExecuteAdapter.QueryParam("string", mdlClaim.start_date));
	    listParam.add(QueryExecuteAdapter.QueryParam("string", mdlClaim.end_date));
	    listParam.add(QueryExecuteAdapter.QueryParam("string", mdlClaim.amount));
	    listParam.add(QueryExecuteAdapter.QueryParam("string", mdlClaim.claim_date));
	    listParam.add(QueryExecuteAdapter.QueryParam("string", mdlClaim.claim_type));
	    jrs = QueryExecuteAdapter.QueryExecute(sql, listParam, functionName);
	    while (jrs.next()) {
		mdlClaimExists.claim_no = jrs.getString("claim_no");
		mdlClaimExists.claim_status = jrs.getString("claim_status");
	    }
	} catch (Exception ex) {
	    logger.error("FAILED. Function : " + functionName + ", mdlClaim : " + gson.toJson(mdlClaim) + ", Exception:" + ex.toString(), ex);
	}
	return mdlClaimExists;
    }

    public static boolean SendClaimSubmittedEmail(model.mdlClaim mdlClaim, String language) {
	String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();
	model.mdlUser mdlUser = new model.mdlUser();
	boolean success = false;
	try {
	    mdlUser = UserAdapter.CheckUser(mdlClaim.username);
	    if (mdlUser.email == null || mdlUser.email.equals("")) {
		success = false;
	    } else {
		String mailTo = mdlUser.email;
		String mailTitle, mailContent = "";
		mailTitle = "BNI Life - Klaim Anda Dengan ID " + mdlClaim.claim_no + " Sedang Diproses";
		mailContent = "<h3>Yth. " + mdlUser.username + ",<br><br>Klaim Anda dengan ID " + mdlClaim.claim_no + " sedang dalam proses." + "<br><br>Mohon menunggu sampai proses klaim selesai.</h3>";
		if (language.equalsIgnoreCase("en")) {
		    mailTitle = "BNI Life - Ypur Claim With ID " + mdlClaim.claim_no + " Is Inprogress";
		    mailContent = "<h3>Dear " + mdlUser.username + ",<br><br>Your Claim with ID " + mdlClaim.claim_no + " is inprogress." + "<br><br>Please wait until your claim process is done.</h3>";
		}
		success = APIAdapter.HitAPISendEmail(mailTo, mailTitle, mailContent);
	    }
	} catch (Exception ex) {
	    logger.error("FAILED. Function : " + functionName + ", mdlClaim : " + gson.toJson(mdlClaim) + ", Exception:" + ex.toString(), ex);
	}
	return success;
    }

    public static List<model.mdlClaimCount> GetClaimCountData() {
	String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();
	List<model.mdlClaimCount> claimCountList = new ArrayList<model.mdlClaimCount>();
	CachedRowSet jrs = null;
	String sql = "";
	try {
	    sql = "SELECT claim_status, MONTH(claim_date) as claim_month, COUNT(claim_no) as claim_count FROM ms_claim " + "WHERE YEAR(claim_date) = YEAR(getdate()) AND DATEDIFF(month, claim_date, getdate()) <= 5 " + "GROUP BY claim_status, MONTH(claim_date) " + "ORDER BY claim_status, MONTH(claim_date) DESC";
	    jrs = QueryExecuteAdapter.QueryExecute(sql, functionName);
	    while (jrs.next()) {
		model.mdlClaimCount mdlClaimCount = new model.mdlClaimCount();
		mdlClaimCount.claim_status = jrs.getString("claim_status");
		mdlClaimCount.claim_month = jrs.getString("claim_month");
		mdlClaimCount.claim_count = jrs.getString("claim_count");
		claimCountList.add(mdlClaimCount);
	    }

	} catch (Exception ex) {
	    logger.error("FAILED. Function : " + functionName + ", Exception:" + ex.toString(), ex);
	}
	return claimCountList;
    }
}