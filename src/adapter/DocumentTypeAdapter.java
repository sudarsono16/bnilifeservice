package adapter;

import java.util.ArrayList;
import java.util.List;

import javax.sql.rowset.CachedRowSet;

import org.apache.log4j.Logger;

import com.google.gson.Gson;

import database.QueryExecuteAdapter;
import model.mdlQueryExecute;

public class DocumentTypeAdapter {
    final static Logger logger = Logger.getLogger(ClaimAdapter.class);
    static Gson gson = new Gson();
    
    public static List<model.mdlClaimDocumentType> GetDocumentTypeList(){
	String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();
	List<model.mdlClaimDocumentType> documentTypeList = new ArrayList<model.mdlClaimDocumentType>();
	List<mdlQueryExecute> listParam = new ArrayList<mdlQueryExecute>();
	CachedRowSet jrs = null;
	String sql = "";
	
	try {
	    sql = "SELECT claim_type_code, claim_type_name, claim_document_type_id, claim_document_type_name, claim_document_type_details, "
	    	+ "claim_document_type_information1, claim_document_type_information2 FROM ms_claim_document_type";
	    jrs = QueryExecuteAdapter.QueryExecute(sql, listParam, "GetClaimData");
	    while (jrs.next()) {
		model.mdlClaimDocumentType mdlClaimDocumentType = new model.mdlClaimDocumentType();
		mdlClaimDocumentType.claim_type_code = jrs.getString("claim_type_code");
		mdlClaimDocumentType.claim_type_name = jrs.getString("claim_type_name");
		mdlClaimDocumentType.claim_document_type_id = jrs.getString("claim_document_type_id");
		mdlClaimDocumentType.claim_document_type_name = jrs.getString("claim_document_type_name");
		mdlClaimDocumentType.claim_document_type_details = jrs.getString("claim_document_type_details");
		mdlClaimDocumentType.claim_document_type_information1 = jrs.getString("claim_document_type_information1");
		mdlClaimDocumentType.claim_document_type_information2 = jrs.getString("claim_document_type_information2");
		documentTypeList.add(mdlClaimDocumentType);
	    }
	} catch (Exception ex) {
	    logger.error("FAILED. Function : " + functionName + ", Exception:" + ex.toString(), ex);
	}
	return documentTypeList;
    }
    
    public static boolean InsertUpdateDocumentType(model.mdlClaimDocumentType mdlClaimDocumentType){
	String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();
	List<mdlQueryExecute> listParam = new ArrayList<mdlQueryExecute>();
	String sql = "";
	boolean success = false;
	try {
		sql = "BEGIN TRAN "
			+ "UPDATE ms_claim_document_type SET "
			+ "claim_document_type_name = ?, claim_type_code = ?, claim_type_name = ?, claim_document_type_details = ?, claim_document_type_information1 = ?, "
			+ "claim_document_type_information2 = ? WHERE claim_document_type_id = ? "
			+ "IF @@rowcount = 0 "
			+ "BEGIN INSERT INTO ms_claim_document_type "
			+ "(claim_document_type_name, claim_type_code, claim_type_name, claim_document_type_details, claim_document_type_information1, "
			+ "claim_document_type_information2) "
			+ "VALUES "
			+ "(?, ?, ?, ?, ?, "
			+ "?) END COMMIT TRAN";
		listParam.add(QueryExecuteAdapter.QueryParam("string", mdlClaimDocumentType.claim_document_type_name));
		listParam.add(QueryExecuteAdapter.QueryParam("string", mdlClaimDocumentType.claim_type_code));
		listParam.add(QueryExecuteAdapter.QueryParam("string", mdlClaimDocumentType.claim_type_name));
		listParam.add(QueryExecuteAdapter.QueryParam("string", mdlClaimDocumentType.claim_document_type_details));
		listParam.add(QueryExecuteAdapter.QueryParam("string", mdlClaimDocumentType.claim_document_type_information1));
		
		listParam.add(QueryExecuteAdapter.QueryParam("string", mdlClaimDocumentType.claim_document_type_information2));
		listParam.add(QueryExecuteAdapter.QueryParam("string", mdlClaimDocumentType.claim_document_type_id == null ? "" : mdlClaimDocumentType.claim_document_type_id));
		
		listParam.add(QueryExecuteAdapter.QueryParam("string", mdlClaimDocumentType.claim_document_type_name));
		listParam.add(QueryExecuteAdapter.QueryParam("string", mdlClaimDocumentType.claim_type_code));
		listParam.add(QueryExecuteAdapter.QueryParam("string", mdlClaimDocumentType.claim_type_name));
		listParam.add(QueryExecuteAdapter.QueryParam("string", mdlClaimDocumentType.claim_document_type_details));
		listParam.add(QueryExecuteAdapter.QueryParam("string", mdlClaimDocumentType.claim_document_type_information1));
		
		listParam.add(QueryExecuteAdapter.QueryParam("string", mdlClaimDocumentType.claim_document_type_information2));
		
		success = QueryExecuteAdapter.QueryManipulate(sql, listParam, functionName);
	} catch (Exception ex) {
		logger.error("FAILED. Function : " + functionName + ", mdlClaimDocumentType : " + gson.toJson(mdlClaimDocumentType) + ", Exception:" + ex.toString(), ex);
	}
	
	return success;
    }
    
    public static boolean DeleteDocumentType(String id){
	String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();
	List<mdlQueryExecute> listParam = new ArrayList<mdlQueryExecute>();
	String sql = "";
	boolean success = false;
	try {
		sql = "DELETE FROM ms_claim_document_type WHERE claim_document_type_id = ? ";
		listParam.add(QueryExecuteAdapter.QueryParam("string", id));
		success = QueryExecuteAdapter.QueryManipulate(sql, listParam, functionName);
	} catch (Exception ex) {
		logger.error("FAILED. Function : " + functionName + ", claim_document_type_id : " + id + ", Exception:" + ex.toString(), ex);
	}
	return success;
    }
}
