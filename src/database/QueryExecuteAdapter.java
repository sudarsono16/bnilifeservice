package database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.sql.rowset.CachedRowSet;
import javax.sql.rowset.RowSetFactory;
import javax.sql.rowset.RowSetProvider;

import org.apache.log4j.Logger;

import adapter.LogAdapter;
import adapter.ProviderAdapter;

public class QueryExecuteAdapter {
    static String source = "Web Server";
    final static Logger logger = Logger.getLogger(QueryExecuteAdapter.class);

    public static CachedRowSet QueryExecute(String sql, String function) {
	String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();
	Connection connection = null;
	PreparedStatement pstm = null;
	ResultSet jrs = null;
	CachedRowSet rs = null;
	RowSetFactory rowSetFactory = null;

	try {
	    connection = database.RowSetAdapter.getConnection();
	    pstm = connection.prepareStatement(sql);

	    jrs = pstm.executeQuery();

	    rowSetFactory = RowSetProvider.newFactory();
	    rs = rowSetFactory.createCachedRowSet();
	    rs.populate(jrs);
	} catch (Exception ex) {
	    LogAdapter.InsertLog(ex.toString(), function, pstm.toString(), source);
	    logger.error("FAILED. Function : " + functionName + ", sql : " + sql + ", Exception:" + ex.toString(), ex);
	} finally {
	    try {
		if (pstm != null)
		    pstm.close();
		if (connection != null)
		    connection.close();
		if (jrs != null)
		    jrs.close();
	    } catch (Exception e) {
		LogAdapter.InsertLog(e.toString(), function, "close opened connection protocol", source);
	    }
	}
	return rs;
    }

    public static CachedRowSet QueryExecute(String sql, List<model.mdlQueryExecute> queryParam, String function) {
	String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();
	Connection connection = null;
	PreparedStatement pstm = null;
	ResultSet jrs = null;
	CachedRowSet rs = null;
	RowSetFactory rowSetFactory = null;

	try {
	    connection = database.RowSetAdapter.getConnection();
	    pstm = connection.prepareStatement(sql);

	    if (queryParam.size() >= 0) {
		for (int i = 1; i <= queryParam.size(); i++) {
		    System.out.println("Parameter " + i + " : " + queryParam.get(i - 1).paramValue + " || Type : " + queryParam.get(i - 1).paramType.toLowerCase());

		    switch (queryParam.get(i - 1).paramType.toLowerCase()) {
		    case "string":
			pstm.setString(i, (String) queryParam.get(i - 1).paramValue);
			break;
		    case "int":
		    case "integer":
			pstm.setInt(i, (int) queryParam.get(i - 1).paramValue);
			break;
		    case "decimal":
		    case "double":
			pstm.setDouble(i, (double) queryParam.get(i - 1).paramValue);
			break;
		    case "boolean":
			pstm.setBoolean(i, (boolean) queryParam.get(i - 1).paramValue);
			break;
		    case "null":
			pstm.setNull(i, java.sql.Types.NULL);
			break;
		    default:
			pstm.setString(i, (String) queryParam.get(i - 1).paramValue);
			break;
		    }
		}
	    }

	    jrs = pstm.executeQuery();

	    rowSetFactory = RowSetProvider.newFactory();
	    rs = rowSetFactory.createCachedRowSet();
	    rs.populate(jrs);
	} catch (Exception ex) {
	    LogAdapter.InsertLog(ex.toString(), function, pstm.toString(), source);
	    logger.error("FAILED. Function : " + functionName + ", sql : " + sql + ", Exception:" + ex.toString(), ex);
	} finally {
	    try {
		if (pstm != null)
		    pstm.close();
		if (connection != null)
		    connection.close();
		if (jrs != null)
		    jrs.close();
	    } catch (Exception e) {
		LogAdapter.InsertLog(e.toString(), function, "close opened connection protocol", source);
	    }
	}
	return rs;
    }

    public static Boolean QueryManipulate(String sql, List<model.mdlQueryExecute> queryParam, String function) {
	String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();
	Connection connection = null;
	PreparedStatement pstm = null;
	Boolean success = false;

	try {
	    connection = database.RowSetAdapter.getConnection();
	    pstm = connection.prepareStatement(sql);

	    if (queryParam.size() >= 0) {
		for (int i = 1; i <= queryParam.size(); i++) {
		    switch (queryParam.get(i - 1).paramType.toLowerCase()) {
		    case "string":
			pstm.setString(i, (String) queryParam.get(i - 1).paramValue);
			break;
		    case "int":
		    case "integer":
			pstm.setInt(i, (int) queryParam.get(i - 1).paramValue);
			break;
		    case "decimal":
		    case "double":
			pstm.setDouble(i, (double) queryParam.get(i - 1).paramValue);
			break;
		    case "boolean":
			pstm.setBoolean(i, (boolean) queryParam.get(i - 1).paramValue);
			break;
		    case "null":
			pstm.setNull(i, java.sql.Types.NULL);
			break;
		    default:
			pstm.setString(i, (String) queryParam.get(i - 1).paramValue);
			break;
		    }
		}
	    }

	    pstm.execute();
	    success = true;
	} catch (Exception ex) {
	    LogAdapter.InsertLog(ex.toString(), function, pstm.toString(), source);
	    logger.error("FAILED. Function : " + functionName + ", sql : " + sql + ", Exception:" + ex.toString(), ex);
	} finally {
	    try {
		if (pstm != null)
		    pstm.close();
		if (connection != null)
		    connection.close();
	    } catch (Exception e) {
		LogAdapter.InsertLog(e.toString(), function, "close opened connection protocol", source);
	    }
	}
	return success;
    }
    
    public static Boolean QueryManipulate(String sql, String function) {
	String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();
	Connection connection = null;
	PreparedStatement pstm = null;
	Boolean success = false;

	try {
	    connection = database.RowSetAdapter.getConnection();
	    pstm = connection.prepareStatement(sql);
	    pstm.execute();
	    success = true;
	} catch (Exception ex) {
	    LogAdapter.InsertLog(ex.toString(), function, pstm.toString(), source);
	    logger.error("FAILED. Function : " + functionName + ", sql : " + sql + ", Exception:" + ex.toString(), ex);
	} finally {
	    try {
		if (pstm != null)
		    pstm.close();
		if (connection != null)
		    connection.close();
	    } catch (Exception e) {
		LogAdapter.InsertLog(e.toString(), function, "close opened connection protocol", source);
	    }
	}
	return success;
    }

    public static model.mdlQueryExecute QueryParam(String type, Object value) {
	model.mdlQueryExecute param = new model.mdlQueryExecute();
	param.paramType = type;
	param.paramValue = value;
	return param;
    }

    public static Boolean QueryTransaction(List<model.mdlQueryTransaction> listMdlQueryTrans, String function) {
	Connection connection = null;
	PreparedStatement pstm = null;
	Boolean success = false;
	String sql;
	List<model.mdlQueryExecute> queryParam;

	try {
	    connection = database.RowSetAdapter.getConnection();
	    connection.setAutoCommit(false);

	    for (model.mdlQueryTransaction mdlQueryTrans : listMdlQueryTrans) {
		pstm = null;
		sql = mdlQueryTrans.sql;
		queryParam = new ArrayList<model.mdlQueryExecute>();
		queryParam.addAll(mdlQueryTrans.listParam);
		pstm = connection.prepareStatement(sql);

		for (int i = 1; i <= queryParam.size(); i++) {
		    switch (queryParam.get(i - 1).paramType.toLowerCase()) {
		    case "string":
			pstm.setString(i, (String) queryParam.get(i - 1).paramValue);
			break;
		    case "int":
		    case "integer":
			pstm.setInt(i, (int) queryParam.get(i - 1).paramValue);
			break;
		    case "decimal":
		    case "double":
			pstm.setDouble(i, (double) queryParam.get(i - 1).paramValue);
			break;
		    case "boolean":
			pstm.setBoolean(i, (boolean) queryParam.get(i - 1).paramValue);
			break;
		    case "null":
			pstm.setNull(i, java.sql.Types.NULL);
			break;
		    default:
			pstm.setString(i, (String) queryParam.get(i - 1).paramValue);
			break;
		    }
		}
		pstm.executeUpdate();
	    }

	    connection.commit(); // commit transaction if all of the proccess is running well
	    success = true;
	} catch (Exception ex) {
	    if (connection != null) {
		try {
		    System.err.print("Transaction is being rolled back");
		    connection.rollback();
		} catch (SQLException excep) {
		    LogAdapter.InsertLog(excep.toString(), "Transaction" + function, pstm.toString(), source);
		}
	    }
	    LogAdapter.InsertLog(ex.toString(), function, pstm.toString(), source);
	    success = false;
	} finally {
	    try {
		if (pstm != null)
		    pstm.close();
		connection.setAutoCommit(true);
		if (connection != null)
		    connection.close();
	    } catch (Exception e) {
		LogAdapter.InsertLog(e.toString(), function, "close opened connection protocol", source);
	    }
	}

	return success;
    }

}
