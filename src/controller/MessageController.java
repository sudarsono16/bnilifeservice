package controller;

import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;

import adapter.AuthorizationAdapter;
import adapter.ErrorAdapter;
import adapter.LogAdapter;
import adapter.MessageAdapter;
import adapter.ProductAdapter;
import model.ErrorStatus;

@RestController
@RequestMapping("/message")
public class MessageController {
    String baseURL = "/message";
    static String apiSource = "Web Server";
    final static Logger logger = Logger.getLogger(NewsController.class);
    static Gson gson = new Gson();

    @RequestMapping(value = "/get-message", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody  model.mdlAPIArrayResult GetMessage(@RequestHeader String Authorization, @RequestHeader String Key, @RequestHeader String Timestamp, @RequestHeader String Signature, @RequestParam(value = "username", defaultValue = "") String username, HttpServletResponse response) {
	model.mdlAPIArrayResult mdlAPIResult = new model.mdlAPIArrayResult();
	String jsonIn = username;
	String jsonOut = "";
	String apiMethod = "GET";
	String url = baseURL + "/get-message";

	try {
	    // check token, key and signature	    
	    model.mdlErrorSchema mdlAPIStatus = AuthorizationAdapter.CheckAuthorizationArray(Authorization, Key, Timestamp, Signature, response, jsonIn, apiMethod, url, mdlAPIResult);
	    if (!mdlAPIStatus.error_code.equals("ERR-00-000")){
		mdlAPIResult.error_schema = mdlAPIStatus;
		return mdlAPIResult;
	    }

	    if (username == null || username.equals("")) {
		mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_15_001);
		response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		jsonOut = gson.toJson(mdlAPIResult);
		logger.error("FAILED = API : " + url + ", method : " + apiMethod + ", jsonIn :" + jsonIn + ", jsonOut : " + jsonOut);
		return mdlAPIResult;
	    }

	    List<model.mdlUserMessage> messageList = MessageAdapter.GetMessageList(username);
	    if (messageList == null || messageList.size() == 0) {
		mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_15_002);
		response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		jsonOut = gson.toJson(mdlAPIResult);
		logger.error("FAILED = API : " + url + ", method : " + apiMethod + ", jsonIn :" + jsonIn + ", jsonOut : " + jsonOut);
		return mdlAPIResult;
	    } else {
		mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_00_000);
		model.mdlArrayResult arrayResult = new model.mdlArrayResult();
		arrayResult.items = messageList;
		mdlAPIResult.output_schema = arrayResult;
		response.setStatus(HttpServletResponse.SC_OK);
		jsonOut = gson.toJson(mdlAPIResult);
		logger.info("SUCCESS. API : " + url + ", method : " + apiMethod + ", jsonIn :" + jsonIn + ", jsonOut : " + jsonOut);
	    }
	} catch (Exception ex) {
	    mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_99_999);
	    response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
	    jsonOut = gson.toJson(mdlAPIResult);
	    logger.error("FAILED. API : " + url + ", method : " + apiMethod + ", jsonIn : " + jsonIn + ", jsonOut : " + jsonOut + ",Exception:" + ex.toString());
	}
	return mdlAPIResult;
    }

    @RequestMapping(value = "/update-message-read", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody model.mdlAPIObjectResult UpdateMessageRead(@RequestHeader String Authorization, @RequestHeader String Key, @RequestHeader String Timestamp, @RequestHeader String Signature, @RequestParam(value = "id", defaultValue = "") String message_id, HttpServletResponse response) {
	model.mdlAPIObjectResult mdlAPIResult = new model.mdlAPIObjectResult();
	String jsonIn = message_id;
	String jsonOut = "";
	String apiMethod = "GET";
	String url = baseURL + "/update-message-read";

	try {
	    // check token, key and signature	    
	    model.mdlErrorSchema mdlAPIStatus = AuthorizationAdapter.CheckAuthorization(Authorization, Key, Timestamp, Signature, response, jsonIn, apiMethod, url, mdlAPIResult);
	    if (!mdlAPIStatus.error_code.equals("ERR-00-000")){
		mdlAPIResult.error_schema = mdlAPIStatus;
		return mdlAPIResult;
	    }

	    if (message_id == null || message_id.equals("")) {
		mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_16_001);
		response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		jsonOut = gson.toJson(mdlAPIResult);
		logger.error("FAILED = API : " + url + ", method : " + apiMethod + ", jsonIn :" + jsonIn + ", jsonOut : " + jsonOut);
		return mdlAPIResult;
	    }

	    boolean success = MessageAdapter.UpdateMessageRead(message_id);
	    if (!success) {
		mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_16_002);
		response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		jsonOut = gson.toJson(mdlAPIResult);
		logger.error("FAILED = API : " + url + ", method : " + apiMethod + ", jsonIn :" + jsonIn + ", jsonOut : " + jsonOut);
		return mdlAPIResult;
	    } else {
		mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_00_000);
		response.setStatus(HttpServletResponse.SC_OK);
		jsonOut = gson.toJson(mdlAPIResult);
		logger.info("SUCCESS. API : " + url + ", method : " + apiMethod + ", jsonIn :" + jsonIn + ", jsonOut : " + jsonOut);
	    }
	} catch (Exception ex) {
	    mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_99_999);
	    response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
	    jsonOut = gson.toJson(mdlAPIResult);
	    logger.error("FAILED. API : " + url + ", method : " + apiMethod + ", jsonIn : " + jsonIn + ", jsonOut : " + jsonOut + ",Exception:" + ex.toString());
	}
	return mdlAPIResult;
    }

    @RequestMapping(value = "/send-broadcast-message", method = RequestMethod.POST, consumes = "application/json", headers = "Accept=application/json", produces = "application/json")
    public @ResponseBody model.mdlAPIObjectResult SendMessageToDevice(@RequestHeader String Authorization, @RequestHeader String Key, @RequestHeader String Timestamp, 
	    @RequestHeader String Signature, @RequestBody model.mdlBroadcastMessage mdlBroadcastMessage, HttpServletResponse response) {
	model.mdlAPIObjectResult mdlAPIResult = new model.mdlAPIObjectResult();
	String jsonIn = gson.toJson(mdlBroadcastMessage);
	String jsonOut = "";
	String apiMethod = "POST";
	String url = baseURL + "/send-broadcast-message";

	try {
	    // check token, key and signature	    
	    model.mdlErrorSchema mdlAPIStatus = AuthorizationAdapter.CheckAuthorization(Authorization, Key, Timestamp, Signature, response, jsonIn, apiMethod, url, mdlAPIResult);
	    if (!mdlAPIStatus.error_code.equals("ERR-00-000")){
		mdlAPIResult.error_schema = mdlAPIStatus;
		return mdlAPIResult;
	    }

	    boolean isValid = true;
	    if (mdlBroadcastMessage.broadcast_type == null || mdlBroadcastMessage.broadcast_type.equals("")) {
		mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_20_001);
		isValid = false;
	    } else if (mdlBroadcastMessage.broadcast_title == null || mdlBroadcastMessage.broadcast_title.equals("")) {
		mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_20_002);
		isValid = false;
	    } else if (mdlBroadcastMessage.broadcast_content == null || mdlBroadcastMessage.broadcast_content.equals("")) {
		mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_20_003);
		isValid = false;
	    }
	    
	    if (mdlBroadcastMessage.broadcast_type.equalsIgnoreCase("PERSONAL") && (mdlBroadcastMessage.username == null || mdlBroadcastMessage.username.equals("") ) ){
		mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_20_004);
		isValid = false;
	    }

	    if (!isValid) {
		response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		jsonOut = gson.toJson(mdlAPIResult);
		logger.error("FAILED = API : " + url + ", method : " + apiMethod + ", jsonIn :" + jsonIn + ", jsonOut : " + jsonOut);
		return mdlAPIResult;
	    }

	    boolean success = MessageAdapter.CreateBroadcastMessage(mdlBroadcastMessage);

	    if (!success) {
		mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_20_005);
		response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		jsonOut = gson.toJson(mdlAPIResult);
		logger.error("FAILED = API : " + url + ", method : " + apiMethod + ", jsonIn :" + jsonIn + ", jsonOut : " + jsonOut);
		return mdlAPIResult;
	    } else {
		mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_00_000);
		response.setStatus(HttpServletResponse.SC_OK);
		jsonOut = gson.toJson(mdlAPIResult);
		logger.info("SUCCESS. API : " + url + ", method : " + apiMethod + ", jsonIn :" + jsonIn + ", jsonOut : " + jsonOut);
	    }
	} catch (Exception ex) {
	    mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_99_999);
	    response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
	    jsonOut = gson.toJson(mdlAPIResult);
	    logger.error("FAILED. API : " + url + ", method : " + apiMethod + ", jsonIn : " + jsonIn + ", jsonOut : " + jsonOut + ",Exception:" + ex.toString());
	}
	return mdlAPIResult;
    }
    
    @RequestMapping(value = "/get-broadcast-list", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody String GetBroadcastList(@RequestHeader String Authorization, @RequestHeader String Key, @RequestHeader String Timestamp, @RequestHeader String Signature, 
	    @RequestParam(value = "id", required = false) String id, @RequestParam(value = "pageNumber", defaultValue = "1") String pageNumber, 
	    @RequestParam(value = "pageSize", defaultValue = "10") String pageSize, @RequestParam(value = "draw", defaultValue = "0") String draw, 
	    @RequestParam(value = "search", required = false) String search, HttpServletResponse response) {
	String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();
	model.mdlAPIArrayResult mdlAPIResult = new model.mdlAPIArrayResult();
	String jsonIn = "";
	String jsonOut = "";
	String apiMethod = "GET";
	String url = baseURL + "/get-broadcast-list";

	try {
	    // check token, key and signature
	    model.mdlErrorSchema mdlAPIStatus = AuthorizationAdapter.CheckAuthorizationArray(Authorization, Key, Timestamp, Signature, response, jsonIn, apiMethod, url, mdlAPIResult);
	    if (!mdlAPIStatus.error_code.equals("ERR-00-000")) {
		mdlAPIResult.error_schema = mdlAPIStatus;
		return gson.toJson(mdlAPIResult);
	    }

	    List<model.mdlBroadcastMessage> broadcastList = MessageAdapter.GetBroadcastList(id, pageNumber, pageSize, search);
	    int productListTotal = MessageAdapter.GetBroadcastTotalList(id, search);
	    
	    if (broadcastList == null || broadcastList.size() == 0) {
		mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_53_001);
		response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		jsonOut = gson.toJson(mdlAPIResult);
		LogAdapter.InsertAPILog(apiSource, "FAILED", url, functionName, jsonIn, jsonOut);
		logger.info("FAILED = API : " + url + ", method : " + apiMethod + ", jsonIn :" + jsonIn + ", jsonOut : " + jsonOut);
		return gson.toJson(mdlAPIResult);
	    } else {
		mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_00_000);
		model.mdlArrayResult arrayResult = new model.mdlArrayResult();
		arrayResult.items = broadcastList;
		arrayResult.recordsTotal = productListTotal;
		arrayResult.recordsFiltered = productListTotal;
		arrayResult.draw = Integer.parseInt(draw) + 1;
		mdlAPIResult.output_schema = arrayResult;
		response.setStatus(HttpServletResponse.SC_OK);
		jsonOut = gson.toJson(mdlAPIResult);
		LogAdapter.InsertAPILog(apiSource, "SUCCESS", url, functionName, jsonIn, jsonOut);
		logger.info("SUCCESS. API : " + url + ", method : " + apiMethod + ", jsonIn :" + jsonIn + ", jsonOut : " + jsonOut);
	    }
	} catch (Exception ex) {
	    mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_99_999);
	    response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
	    jsonOut = gson.toJson(mdlAPIResult);
	    LogAdapter.InsertAPILog(apiSource, "FAILED", url, functionName, jsonIn, jsonOut, ex.toString());
	    logger.error("FAILED. API : " + url + ", method : " + apiMethod + ", jsonIn : " + jsonIn + ", jsonOut : " + jsonOut + ",Exception:" + ex.toString(), ex);
	}
	return gson.toJson(mdlAPIResult);
    }
}
