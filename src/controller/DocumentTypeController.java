package controller;

import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;

import adapter.AuthorizationAdapter;
import adapter.DocumentTypeAdapter;
import adapter.ErrorAdapter;
import model.ErrorStatus;

@RestController
@RequestMapping("/document")
public class DocumentTypeController {
    final static Logger logger = Logger.getLogger(ClaimController.class);
    static Gson gson = new Gson();
    String baseURL = "/document";

    // get-document-type-list
    @RequestMapping(value = "/get-document-type-list", method = RequestMethod.GET)
    public @ResponseBody model.mdlAPIArrayResult GetDocumentTypeList(@RequestHeader String Authorization, @RequestHeader String Key, @RequestHeader String Timestamp, @RequestHeader String Signature, HttpServletResponse response) {
	long startTime = System.currentTimeMillis();
	long stopTime = 0;
	long elapsedTime = 0;
	model.mdlAPIArrayResult mdlAPIResult = new model.mdlAPIArrayResult();
	String jsonIn = "";
	String jsonOut = "";
	String apiMethod = "GET";
	String url = baseURL + "/get-document-type-list";

	try {
	    // check token, key and signature	    
	    model.mdlErrorSchema mdlAPIStatus = AuthorizationAdapter.CheckAuthorizationArray(Authorization, Key, Timestamp, Signature, response, jsonIn, apiMethod, url, mdlAPIResult);
	    if (!mdlAPIStatus.error_code.equals("ERR-00-000")){
		mdlAPIResult.error_schema = mdlAPIStatus;
		return mdlAPIResult;
	    }

	    List<model.mdlClaimDocumentType> documentTypeList = DocumentTypeAdapter.GetDocumentTypeList();

	    if (documentTypeList.size() == 0) {
		mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_31_001);
		response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		jsonOut = gson.toJson(mdlAPIResult);
		logger.error("FAILED = API : " + url + ", method : " + apiMethod + ", jsonIn :" + jsonIn + ", jsonOut : " + jsonOut);
		return mdlAPIResult;
	    } else {
		mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_00_000);
		model.mdlArrayResult mdlOutputSchemaArray = new model.mdlArrayResult();
		mdlOutputSchemaArray.items = documentTypeList;
		mdlAPIResult.output_schema = mdlOutputSchemaArray;
		response.setStatus(HttpServletResponse.SC_OK);
		jsonOut = gson.toJson(mdlAPIResult);
		stopTime = System.currentTimeMillis();
		elapsedTime = stopTime - startTime;
		logger.info("SUCCESS. Response Time : " + elapsedTime + ", API : " + url + ", method : " + apiMethod + ", jsonIn :" + jsonIn + ", jsonOut : " + jsonOut);
	    }
	} catch (Exception ex) {
	    mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_99_999);
	    response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
	    jsonOut = gson.toJson(mdlAPIResult);
	    stopTime = System.currentTimeMillis();
	    elapsedTime = stopTime - startTime;
	    logger.error("FAILED. Response Time : " + elapsedTime + ", API : " + url + ", method : " + apiMethod + ", jsonIn : " + jsonIn + ", jsonOut : " + jsonOut + ",Exception:" + ex.toString());
	}
	return mdlAPIResult;
    }

    @RequestMapping(value = "/insert-update-document-type", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
    public @ResponseBody model.mdlAPIObjectResult InsertUpdateDocumentType(@RequestHeader String Authorization, @RequestHeader String Key, @RequestHeader String Timestamp, @RequestHeader String Signature, @RequestBody model.mdlClaimDocumentType mdlClaimDocumentType, HttpServletResponse response) {
	long startTime = System.currentTimeMillis();
	long stopTime = 0;
	long elapsedTime = 0;
	model.mdlAPIObjectResult mdlAPIResult = new model.mdlAPIObjectResult();
	String jsonIn = gson.toJson(mdlClaimDocumentType);
	String jsonOut = "";
	String apiMethod = "POST";
	String url = baseURL + "/insert-update-document-type";

	try {
	    // check token, key and signature	    
	    model.mdlErrorSchema mdlAPIStatus = AuthorizationAdapter.CheckAuthorization(Authorization, Key, Timestamp, Signature, response, jsonIn, apiMethod, url, mdlAPIResult);
	    if (!mdlAPIStatus.error_code.equals("ERR-00-000")){
		mdlAPIResult.error_schema = mdlAPIStatus;
		return mdlAPIResult;
	    }

	    boolean isValid = true;

	    if (mdlClaimDocumentType.claim_type_code == null || mdlClaimDocumentType.claim_type_code.equals("")) {
		mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_32_001);
		isValid = false;
	    } else if (mdlClaimDocumentType.claim_document_type_id == null || mdlClaimDocumentType.claim_document_type_id.equals("")) {
		mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_32_002);
		isValid = false;
	    } else if (mdlClaimDocumentType.claim_document_type_name == null || mdlClaimDocumentType.claim_document_type_name.equals("")) {
		mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_32_003);
		isValid = false;
	    } else if (mdlClaimDocumentType.claim_document_type_details == null || mdlClaimDocumentType.claim_document_type_details.equals("")) {
		mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_32_004);
		isValid = false;
	    } else if (mdlClaimDocumentType.claim_document_type_information1 == null || mdlClaimDocumentType.claim_document_type_information1.equals("")) {
		mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_32_005);
		isValid = false;
	    }

	    if (!isValid) {
		response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		jsonOut = gson.toJson(mdlAPIResult);
		logger.error("FAILED = API : " + url + ", method : " + apiMethod + ", jsonIn :" + jsonIn + ", jsonOut : " + jsonOut);
		return mdlAPIResult;
	    }

	    boolean success = DocumentTypeAdapter.InsertUpdateDocumentType(mdlClaimDocumentType);

	    if (!success) {
		mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_32_006);
		response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		jsonOut = gson.toJson(mdlAPIResult);
		logger.error("FAILED = API : " + url + ", method : " + apiMethod + ", jsonIn :" + jsonIn + ", jsonOut : " + jsonOut);
		return mdlAPIResult;
	    } else {
		mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_00_000);
		response.setStatus(HttpServletResponse.SC_OK);
		jsonOut = gson.toJson(mdlAPIResult);
		stopTime = System.currentTimeMillis();
		elapsedTime = stopTime - startTime;
		logger.info("SUCCESS. Response Time : " + elapsedTime + ", API : " + url + ", method : " + apiMethod + ", jsonIn :" + jsonIn + ", jsonOut : " + jsonOut);
	    }
	} catch (Exception ex) {
	    mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_99_999);
	    response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
	    jsonOut = gson.toJson(mdlAPIResult);
	    stopTime = System.currentTimeMillis();
	    elapsedTime = stopTime - startTime;
	    logger.error("FAILED. Response Time : " + elapsedTime + ", API : " + url + ", method : " + apiMethod + ", jsonIn : " + jsonIn + ", jsonOut : " + jsonOut + ",Exception:" + ex.toString());
	}
	return mdlAPIResult;
    }
    
    @RequestMapping(value = "/delete-document-type", method = RequestMethod.GET)
    public @ResponseBody model.mdlAPIObjectResult DeleteDocumentType(@RequestHeader String Authorization, @RequestHeader String Key, @RequestHeader String Timestamp, @RequestHeader String Signature, @RequestParam(value = "id", defaultValue = "") String id, HttpServletResponse response) {
	long startTime = System.currentTimeMillis();
	long stopTime = 0;
	long elapsedTime = 0;
	model.mdlAPIObjectResult mdlAPIResult = new model.mdlAPIObjectResult();
	String jsonIn = id;
	String jsonOut = "";
	String apiMethod = "GET";
	String url = baseURL + "/delete-document-type";

	try {
	    // check token, key and signature	    
	    model.mdlErrorSchema mdlAPIStatus = AuthorizationAdapter.CheckAuthorization(Authorization, Key, Timestamp, Signature, response, jsonIn, apiMethod, url, mdlAPIResult);
	    if (!mdlAPIStatus.error_code.equals("ERR-00-000")){
		mdlAPIResult.error_schema = mdlAPIStatus;
		return mdlAPIResult;
	    }

	    boolean success = DocumentTypeAdapter.DeleteDocumentType(id);

	    if (!success) {
		mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_33_001);
		response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		jsonOut = gson.toJson(mdlAPIResult);
		logger.error("FAILED = API : " + url + ", method : " + apiMethod + ", jsonIn :" + jsonIn + ", jsonOut : " + jsonOut);
		return mdlAPIResult;
	    } else {
		mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_00_000);
		response.setStatus(HttpServletResponse.SC_OK);
		jsonOut = gson.toJson(mdlAPIResult);
		stopTime = System.currentTimeMillis();
		elapsedTime = stopTime - startTime;
		logger.info("SUCCESS. Response Time : " + elapsedTime + ", API : " + url + ", method : " + apiMethod + ", jsonIn :" + jsonIn + ", jsonOut : " + jsonOut);
	    }
	} catch (Exception ex) {
	    mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_99_999);
	    response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
	    jsonOut = gson.toJson(mdlAPIResult);
	    stopTime = System.currentTimeMillis();
	    elapsedTime = stopTime - startTime;
	    logger.error("FAILED. Response Time : " + elapsedTime + ", API : " + url + ", method : " + apiMethod + ", jsonIn : " + jsonIn + ", jsonOut : " + jsonOut + ",Exception:" + ex.toString());
	}
	return mdlAPIResult;
    }
}
