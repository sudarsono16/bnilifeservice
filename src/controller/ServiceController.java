package controller;

import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;

import adapter.AuthorizationAdapter;
import adapter.ErrorAdapter;
import adapter.LogAdapter;
import adapter.ServiceAdapter;
import model.ErrorStatus;

@RestController
@RequestMapping("/service")
public class ServiceController {
    String baseURL = "/service";
    static String apiSource = "Web Server";
    final static Logger logger = Logger.getLogger(ServiceController.class);
    static Gson gson = new Gson();
    
    @RequestMapping(value = "/get-service-list", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody model.mdlAPIArrayResult GetService(@RequestHeader String Authorization, @RequestHeader String Key, @RequestHeader String Timestamp, 
	    @RequestHeader String Signature, @RequestParam(value = "id", required = false) String id, @RequestParam(value = "pageNumber", defaultValue = "1") String pageNumber, 
	    @RequestParam(value = "pageSize", defaultValue = "10") String pageSize, @RequestParam(value = "draw", defaultValue = "0") String draw, 
	    @RequestParam(value = "search", required = false) String search, HttpServletResponse response) {
	String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();
	String jsonIn = "";
	String jsonOut = "";
	String apiMethod = "GET";
	String url = "/get-service-list";
	model.mdlAPIArrayResult mdlAPIResult = new model.mdlAPIArrayResult();

	try {
	    // check token, key and signature	    
	    model.mdlErrorSchema mdlAPIStatus = AuthorizationAdapter.CheckAuthorizationArray(Authorization, Key, Timestamp, Signature, response, jsonIn, apiMethod, url, mdlAPIResult);
	    if (!mdlAPIStatus.error_code.equals("ERR-00-000")){
		mdlAPIResult.error_schema = mdlAPIStatus;
		return mdlAPIResult;
	    }

	    List<model.mdlService> serviceList = ServiceAdapter.GetServiceList(id, pageNumber, pageSize, search);
	    int serviceListTotal = ServiceAdapter.GetServiceTotalList(id, search);
	    
	    if (serviceList == null || serviceList.size() == 0) {
		mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_49_001);
		response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		jsonOut = gson.toJson(mdlAPIResult);
		LogAdapter.InsertAPILog(apiSource, "FAILED", url, functionName, jsonIn, jsonOut);
		logger.info("FAILED = API : " + url + ", method : " + apiMethod + ", jsonIn :" + jsonIn + ", jsonOut : " + jsonOut);
		return mdlAPIResult;
	    } else {
		mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_00_000);
		model.mdlArrayResult arrayResult = new model.mdlArrayResult();
		arrayResult.items = serviceList;
		arrayResult.recordsTotal = serviceListTotal;
		arrayResult.recordsFiltered = serviceListTotal;
		arrayResult.draw = Integer.parseInt(draw) + 1;
		mdlAPIResult.output_schema = arrayResult;
		response.setStatus(HttpServletResponse.SC_OK);
		jsonOut = gson.toJson(mdlAPIResult);
		LogAdapter.InsertAPILog(apiSource, "SUCCESS", url, functionName, jsonIn, jsonOut);
		logger.info("SUCCESS. API : " + url + ", method : " + apiMethod + ", jsonIn :" + jsonIn + ", jsonOut : " + jsonOut);
	    }
	} catch (Exception ex) {
	    mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_99_999);
	    response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
	    jsonOut = gson.toJson(mdlAPIResult);
	    LogAdapter.InsertAPILog(apiSource, "FAILED", url, functionName, jsonIn, jsonOut, ex.toString());
	    logger.error("FAILED. API : " + url + ", method : " + apiMethod + ", jsonIn : " + jsonIn + ", jsonOut : " + jsonOut + ",Exception:" + ex.toString());
	}

	return mdlAPIResult;
    }

    @RequestMapping(value = "/insert-update-service", method = RequestMethod.POST, consumes = "application/json", headers = "Accept=application/json")
    public @ResponseBody String InsertUpdateService(@RequestHeader String Authorization, @RequestHeader String Key, @RequestHeader String Timestamp, @RequestHeader String Signature, @RequestBody model.mdlService mdlService, HttpServletResponse response) {
	String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();
	String jsonIn = gson.toJson(mdlService);
	String jsonOut = "";
	String apiMethod = "POST";
	String url = "/insert-update-service";
	model.mdlAPIObjectResult mdlAPIResult = new model.mdlAPIObjectResult();

	try {
	    // check token, key and signature	    
	    model.mdlErrorSchema mdlAPIStatus = AuthorizationAdapter.CheckAuthorization(Authorization, Key, Timestamp, Signature, response, jsonIn, apiMethod, url, mdlAPIResult);
	    if (!mdlAPIStatus.error_code.equals("ERR-00-000")){
		mdlAPIResult.error_schema = mdlAPIStatus;
		return gson.toJson(mdlAPIResult);
	    }

	    boolean isValid = true;
	    if (mdlService.title == null || mdlService.title.equals("")) {
		mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_50_001);
		isValid = false;
	    } else if (mdlService.content == null || mdlService.content.equals("") ) {
		mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_50_002);
		isValid = false;
	    } else if (mdlService.order == null || mdlService.order.equals("")) {
		mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_50_003);
		isValid = false;
	    }

	    if (!isValid) {
		response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		jsonOut = gson.toJson(mdlAPIResult);
		LogAdapter.InsertAPILog(apiSource, "FAILED", url, functionName, jsonIn, jsonOut);
		logger.error("FAILED = API : " + url + ", method : " + apiMethod + ", jsonIn :" + jsonIn + ", jsonOut : " + jsonOut);
		return gson.toJson(mdlAPIResult);
	    }

	    boolean isSuccess = ServiceAdapter.InsertUpdateService(mdlService);
	    if (isSuccess) {
		mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_00_000);
		response.setStatus(HttpServletResponse.SC_OK);
		jsonOut = gson.toJson(mdlAPIResult);
		LogAdapter.InsertAPILog(apiSource, "SUCCESS", url, functionName, jsonIn, jsonOut);
		logger.info("SUCCESS. API : " + url + ", method : " + apiMethod + ", jsonIn :" + jsonIn + ", jsonOut : " + jsonOut);
	    } else {
		mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_50_004);
		response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		jsonOut = gson.toJson(mdlAPIResult);
		LogAdapter.InsertAPILog(apiSource, "FAILED", url, functionName, jsonIn, jsonOut);
		logger.error("FAILED = API : " + url + ", method : " + apiMethod + ", jsonIn :" + jsonIn + ", jsonOut : " + jsonOut);
		return gson.toJson(mdlAPIResult);
	    }

	} catch (Exception ex) {
	    mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_99_999);
	    response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
	    jsonOut = gson.toJson(mdlAPIResult);
	    LogAdapter.InsertAPILog(apiSource, "FAILED", url, functionName, jsonIn, jsonOut, ex.toString());
	    logger.error("FAILED. API : " + url + ", method : " + apiMethod + ", jsonIn : " + jsonIn + ", jsonOut : " + jsonOut + ",Exception:" + ex.toString());
	}
	return gson.toJson(mdlAPIResult);
    }

    @RequestMapping(value = "/delete-service", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody String DeleteService(@RequestHeader String Authorization, @RequestHeader String Key, @RequestHeader String Timestamp, @RequestHeader String Signature, @RequestParam(value = "id", defaultValue = "") String id, HttpServletResponse response) {
	String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();
	String jsonIn = id;
	String jsonOut = "";
	String apiMethod = "GET";
	String url = "/delete-service";
	model.mdlAPIObjectResult mdlAPIResult = new model.mdlAPIObjectResult();

	try {
	    // check token, key and signature	    
	    model.mdlErrorSchema mdlAPIStatus = AuthorizationAdapter.CheckAuthorization(Authorization, Key, Timestamp, Signature, response, jsonIn, apiMethod, url, mdlAPIResult);
	    if (!mdlAPIStatus.error_code.equals("ERR-00-000")){
		mdlAPIResult.error_schema = mdlAPIStatus;
		return gson.toJson(mdlAPIResult);
	    }

	    if (id == null || id.equals("")) {
		mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_51_001);
		response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		jsonOut = gson.toJson(mdlAPIResult);
		LogAdapter.InsertAPILog(apiSource, "FAILED", url, functionName, jsonIn, jsonOut);
		logger.error("FAILED = API : " + url + ", method : " + apiMethod + ", jsonIn :" + jsonIn + ", jsonOut : " + jsonOut);
		return gson.toJson(mdlAPIResult);
	    }

	    boolean success = ServiceAdapter.DeleteService(id);
	    if (!success) {
		mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_51_002);
		response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		jsonOut = gson.toJson(mdlAPIResult);
		LogAdapter.InsertAPILog(apiSource, "FAILED", url, functionName, jsonIn, jsonOut);
		logger.error("FAILED = API : " + url + ", method : " + apiMethod + ", jsonIn :" + jsonIn + ", jsonOut : " + jsonOut);
		return gson.toJson(mdlAPIResult);
	    } else {
		mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_00_000);
		response.setStatus(HttpServletResponse.SC_OK);
		jsonOut = gson.toJson(mdlAPIResult);
		LogAdapter.InsertAPILog(apiSource, "SUCCESS", url, functionName, jsonIn, jsonOut);
		logger.info("SUCCESS. API : " + url + ", method : " + apiMethod + ", jsonIn :" + jsonIn + ", jsonOut : " + jsonOut);
	    }
	} catch (Exception ex) {
	    mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_99_999);
	    response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
	    jsonOut = gson.toJson(mdlAPIResult);
	    LogAdapter.InsertAPILog(apiSource, "FAILED", url, functionName, jsonIn, jsonOut, ex.toString());
	    logger.error("FAILED. API : " + url + ", method : " + apiMethod + ", jsonIn : " + jsonIn + ", jsonOut : " + jsonOut + ",Exception:" + ex.toString());
	}
	return gson.toJson(mdlAPIResult);
    }
}
