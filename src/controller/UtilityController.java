package controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.FilenameUtils;
import org.apache.log4j.Logger;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.google.gson.Gson;

import adapter.AuthorizationAdapter;
import adapter.ErrorAdapter;
import adapter.LogAdapter;
import adapter.UtilityAdapter;
import model.ErrorStatus;

@RestController
public class UtilityController {
    final static Logger logger = Logger.getLogger(UtilityController.class);
    static Gson gson = new Gson();
    static String apiSource = "Web Server";

    @RequestMapping(value = "/get-help", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody model.mdlAPIArrayResult GetHelp(@RequestHeader String Authorization, @RequestHeader String Key, @RequestHeader String Timestamp, @RequestHeader String Signature, @RequestParam(value = "id", required = false) String id, HttpServletResponse response) {
	String jsonIn = "";
	String jsonOut = "";
	String apiMethod = "GET";
	String url = "/get-help";
	model.mdlAPIArrayResult mdlAPIResult = new model.mdlAPIArrayResult();

	try {
	    // check token, key and signature	    
	    model.mdlErrorSchema mdlAPIStatus = AuthorizationAdapter.CheckAuthorizationArray(Authorization, Key, Timestamp, Signature, response, jsonIn, apiMethod, url, mdlAPIResult);
	    if (!mdlAPIStatus.error_code.equals("ERR-00-000")){
		mdlAPIResult.error_schema = mdlAPIStatus;
		return mdlAPIResult;
	    }

	    List<model.mdlHelp> helpList = UtilityAdapter.GetHelpContent(id);
	    if (helpList == null || helpList.size() == 0) {
		mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_13_001);
		response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		jsonOut = gson.toJson(mdlAPIResult);
		logger.info("FAILED = API : " + url + ", method : " + apiMethod + ", jsonIn :" + jsonIn + ", jsonOut : " + jsonOut);
		return mdlAPIResult;
	    } else {
		mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_00_000);
		model.mdlArrayResult arrayResult = new model.mdlArrayResult();
		arrayResult.items = helpList;
		mdlAPIResult.output_schema = arrayResult;
		response.setStatus(HttpServletResponse.SC_OK);
		jsonOut = gson.toJson(mdlAPIResult);
		logger.info("SUCCESS. API : " + url + ", method : " + apiMethod + ", jsonIn :" + jsonIn + ", jsonOut : " + jsonOut);
	    }
	} catch (Exception ex) {
	    mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_99_999);
	    response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
	    jsonOut = gson.toJson(mdlAPIResult);
	    logger.error("FAILED. API : " + url + ", method : " + apiMethod + ", jsonIn : " + jsonIn + ", jsonOut : " + jsonOut + ",Exception:" + ex.toString());
	}

	return mdlAPIResult;
    }

    @RequestMapping(value = "/insert-update-help", method = RequestMethod.POST, consumes = "application/json", headers = "Accept=application/json")
    public @ResponseBody String InsertUpdateHelp(@RequestHeader String Authorization, @RequestHeader String Key, @RequestHeader String Timestamp, @RequestHeader String Signature, @RequestBody model.mdlHelp mdlHelp, HttpServletResponse response) {
	String jsonIn = gson.toJson(mdlHelp);
	String jsonOut = "";
	String apiMethod = "POST";
	String url = "/insert-update-help";
	model.mdlAPIObjectResult mdlAPIResult = new model.mdlAPIObjectResult();

	try {
	    // check token, key and signature	    
	    model.mdlErrorSchema mdlAPIStatus = AuthorizationAdapter.CheckAuthorization(Authorization, Key, Timestamp, Signature, response, jsonIn, apiMethod, url, mdlAPIResult);
	    if (!mdlAPIStatus.error_code.equals("ERR-00-000")){
		mdlAPIResult.error_schema = mdlAPIStatus;
		return gson.toJson(mdlAPIResult);
	    }

	    boolean isValid = true;
	    if (mdlHelp.title == null || mdlHelp.title.equals("")) {
		mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_33_001);
		isValid = false;
	    } else if (mdlHelp.content == null || mdlHelp.content.equals("") ) {
		mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_33_002);
		isValid = false;
	    } else if (mdlHelp.order == null || mdlHelp.order.equals("")) {
		mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_33_003);
		isValid = false;
	    }

	    if (!isValid) {
		response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		jsonOut = gson.toJson(mdlAPIResult);
		logger.error("FAILED = API : " + url + ", method : " + apiMethod + ", jsonIn :" + jsonIn + ", jsonOut : " + jsonOut);
		return gson.toJson(mdlAPIResult);
	    }

	    boolean isSuccess = UtilityAdapter.InsertUpdateHelp(mdlHelp);
	    if (isSuccess) {
		mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_00_000);
		response.setStatus(HttpServletResponse.SC_OK);
		jsonOut = gson.toJson(mdlAPIResult);
		logger.info("SUCCESS. API : " + url + ", method : " + apiMethod + ", jsonIn :" + jsonIn + ", jsonOut : " + jsonOut);
	    } else {
		mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_33_004);
		response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		jsonOut = gson.toJson(mdlAPIResult);
		logger.error("FAILED = API : " + url + ", method : " + apiMethod + ", jsonIn :" + jsonIn + ", jsonOut : " + jsonOut);
		return gson.toJson(mdlAPIResult);
	    }

	} catch (Exception ex) {
	    mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_99_999);
	    response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
	    jsonOut = gson.toJson(mdlAPIResult);
	    logger.error("FAILED. API : " + url + ", method : " + apiMethod + ", jsonIn : " + jsonIn + ", jsonOut : " + jsonOut + ",Exception:" + ex.toString());
	}
	return gson.toJson(mdlAPIResult);
    }

    @RequestMapping(value = "/delete-help", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody String DeleteHelp(@RequestHeader String Authorization, @RequestHeader String Key, @RequestHeader String Timestamp, @RequestHeader String Signature, @RequestParam(value = "id", defaultValue = "") String id, HttpServletResponse response) {
	String jsonIn = id;
	String jsonOut = "";
	String apiMethod = "GET";
	String url = "/delete-help";
	model.mdlAPIObjectResult mdlAPIResult = new model.mdlAPIObjectResult();

	try {
	    // check token, key and signature	    
	    model.mdlErrorSchema mdlAPIStatus = AuthorizationAdapter.CheckAuthorization(Authorization, Key, Timestamp, Signature, response, jsonIn, apiMethod, url, mdlAPIResult);
	    if (!mdlAPIStatus.error_code.equals("ERR-00-000")){
		mdlAPIResult.error_schema = mdlAPIStatus;
		return gson.toJson(mdlAPIResult);
	    }

	    if (id == null || id.equals("")) {
		mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_34_001);
		response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		jsonOut = gson.toJson(mdlAPIResult);
		logger.error("FAILED = API : " + url + ", method : " + apiMethod + ", jsonIn :" + jsonIn + ", jsonOut : " + jsonOut);
		return gson.toJson(mdlAPIResult);
	    }

	    boolean success = UtilityAdapter.DeleteHelp(id);
	    if (!success) {
		mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_34_002);
		response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		jsonOut = gson.toJson(mdlAPIResult);
		logger.error("FAILED = API : " + url + ", method : " + apiMethod + ", jsonIn :" + jsonIn + ", jsonOut : " + jsonOut);
		return gson.toJson(mdlAPIResult);
	    } else {
		mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_00_000);
		response.setStatus(HttpServletResponse.SC_OK);
		jsonOut = gson.toJson(mdlAPIResult);
		logger.info("SUCCESS. API : " + url + ", method : " + apiMethod + ", jsonIn :" + jsonIn + ", jsonOut : " + jsonOut);
	    }
	} catch (Exception ex) {
	    mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_99_999);
	    response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
	    jsonOut = gson.toJson(mdlAPIResult);
	    logger.error("FAILED. API : " + url + ", method : " + apiMethod + ", jsonIn : " + jsonIn + ", jsonOut : " + jsonOut + ",Exception:" + ex.toString());
	}
	return gson.toJson(mdlAPIResult);
    }

    @RequestMapping(value = "/update-image", method = RequestMethod.POST, consumes = "multipart/form-data")
    public @ResponseBody String UpdateImage(@RequestHeader String Authorization, @RequestHeader String Key, @RequestHeader String Timestamp, @RequestHeader String Signature, @RequestParam(name = "file", required = false) final MultipartFile image, @RequestParam("json") final String json, HttpServletResponse response) {
	model.mdlUpdateImage mdlUpdateImage = new Gson().fromJson(json, model.mdlUpdateImage.class);
	String jsonIn = gson.toJson(json);
	String jsonOut = "";
	String apiMethod = "POST";
	String url = "/update-image";
	model.mdlAPIObjectResult mdlAPIResult = new model.mdlAPIObjectResult();

	try {
	    // check token, key and signature	    
	    model.mdlErrorSchema mdlAPIStatus = AuthorizationAdapter.CheckAuthorization(Authorization, Key, Timestamp, Signature, response, jsonIn, apiMethod, url, mdlAPIResult);
	    if (!mdlAPIStatus.error_code.equals("ERR-00-000")){
		mdlAPIResult.error_schema = mdlAPIStatus;
		return gson.toJson(mdlAPIResult);
	    }

	    boolean isValid = true;
	    if (mdlUpdateImage.folder == null || mdlUpdateImage.folder.equals("")) {
		mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_99_999);
		isValid = false;
	    } else if (mdlUpdateImage.filename == null || mdlUpdateImage.filename.equals("")) {
		mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_99_999);
		isValid = false;
	    }

	    if (image != null) {
		String fileName = image.getOriginalFilename();
		String ext = FilenameUtils.getExtension(fileName);
		if (!ext.isEmpty() && (ext.equals("png"))) {
		    try {
			StringBuilder sb = new StringBuilder();

			String paramFilePath = "C://Program Files/Apache Software Foundation/Tomcat 8.0/webapps/Images/bnilife/";
			String directory = sb.append(paramFilePath).append(mdlUpdateImage.folder).toString();
			String filenameString = mdlUpdateImage.filename;
			String uploadedFileLocation = sb.append("/").append(filenameString).toString();

			helper.FileHelper.writeToFile(image.getInputStream(), directory, uploadedFileLocation);
		    } catch (Exception e) {
			isValid = false;
		    }
		} else {
		    isValid = false;
		}
	    } else {
		isValid = false;
	    }

	    if (!isValid) {
		mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_99_999);
		response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		jsonOut = gson.toJson(mdlAPIResult);
		logger.error("FAILED = API : " + url + ", method : " + apiMethod + ", jsonIn :" + jsonIn + ", jsonOut : " + jsonOut);
		return gson.toJson(mdlAPIResult);
	    } else {
		mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_00_000);
		response.setStatus(HttpServletResponse.SC_OK);
		jsonOut = gson.toJson(mdlAPIResult);
		logger.info("SUCCESS. API : " + url + ", method : " + apiMethod + ", jsonIn :" + jsonIn + ", jsonOut : " + jsonOut);
	    }
	} catch (Exception ex) {
	    mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_99_999);
	    response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
	    jsonOut = gson.toJson(mdlAPIResult);
	    logger.error("FAILED. API : " + url + ", method : " + apiMethod + ", jsonIn : " + jsonIn + ", jsonOut : " + jsonOut + ",Exception:" + ex.toString());
	}
	return gson.toJson(mdlAPIResult);
    }
    
    @RequestMapping(value = "/insert-rating", method = RequestMethod.POST, consumes = "application/json")
    public @ResponseBody model.mdlAPIObjectResult InsertRating(@RequestHeader String Authorization, @RequestHeader String Key, @RequestHeader String Timestamp, @RequestHeader String Signature, @RequestBody model.mdlRating mdlRating, HttpServletResponse response) {
	String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();
	model.mdlAPIObjectResult mdlAPIResult = new model.mdlAPIObjectResult();
	String jsonIn = gson.toJson(mdlRating);
	String jsonOut = "";
	String apiMethod = "POST";
	String url = "/insert-rating";

	try {
	    // check token, key and signature	    
	    model.mdlErrorSchema mdlAPIStatus = AuthorizationAdapter.CheckAuthorization(Authorization, Key, Timestamp, Signature, response, jsonIn, apiMethod, url, mdlAPIResult);
	    if (!mdlAPIStatus.error_code.equals("ERR-00-000")){
		mdlAPIResult.error_schema = mdlAPIStatus;
		return mdlAPIResult;
	    }

	    boolean isValid = true;
	    if (mdlRating.username == null || mdlRating.username.trim().equals("")) {
		mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_37_001);
		isValid = false;
	    } else if (mdlRating.rating == null || mdlRating.rating.equals("")) {
		mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_37_002);
		isValid = false;
	    }

	    if (!isValid) {
		response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		jsonOut = gson.toJson(mdlAPIResult);
		LogAdapter.InsertAPILog(apiSource, "FAILED", url, functionName, jsonIn, jsonOut);
		logger.error("FAILED = API : " + url + ", method : " + apiMethod + ", jsonIn :" + jsonIn + ", jsonOut : " + jsonOut);
		return mdlAPIResult;
	    }

	    boolean success = UtilityAdapter.InsertRating(mdlRating);

	    if (success) {
		mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_00_000);
		response.setStatus(HttpServletResponse.SC_OK);
		jsonOut = gson.toJson(mdlAPIResult);
		LogAdapter.InsertAPILog(apiSource, "SUCCESS", url, functionName, jsonIn, jsonOut);
		logger.info("SUCCESS. API : " + url + ", method : " + apiMethod + ", jsonIn :" + jsonIn + ", jsonOut : " + jsonOut);
	    } else {
		mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_37_003);
		response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		jsonOut = gson.toJson(mdlAPIResult);
		LogAdapter.InsertAPILog(apiSource, "FAILED", url, functionName, jsonIn, jsonOut);
		logger.error("FAILED = API : " + url + ", method : " + apiMethod + ", jsonIn :" + jsonIn + ", jsonOut : " + jsonOut);
		return mdlAPIResult;
	    }
	} catch (Exception ex) {
	    mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_99_999);
	    response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
	    jsonOut = gson.toJson(mdlAPIResult);
	    LogAdapter.InsertAPILog(apiSource, "FAILED", url, functionName, jsonIn, jsonOut, ex.toString());
	    logger.error("FAILED. API : " + url + ", method : " + apiMethod + ", jsonIn : " + jsonIn + ", jsonOut : " + jsonOut + ",Exception:" + ex.toString());
	}
	return mdlAPIResult;
    }
    
    @RequestMapping(value = "/get-rating", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody String GetRating(@RequestHeader String Authorization, @RequestHeader String Key, @RequestHeader String Timestamp, @RequestHeader String Signature, @RequestParam(value = "pageNumber", defaultValue = "1") String pageNumber, @RequestParam(value = "pageSize", defaultValue = "10") String pageSize, @RequestParam(value = "draw", defaultValue = "0") String draw, @RequestParam(value = "search", required = false) String search, HttpServletResponse response) {
	model.mdlAPIArrayResult mdlAPIResult = new model.mdlAPIArrayResult();
	String jsonIn = "";
	String jsonOut = "";
	String apiMethod = "GET";
	String url = "/get-rating";

	try {
	    // check token, key and signature	    
	    model.mdlErrorSchema mdlAPIStatus = AuthorizationAdapter.CheckAuthorizationArray(Authorization, Key, Timestamp, Signature, response, jsonIn, apiMethod, url, mdlAPIResult);
	    if (!mdlAPIStatus.error_code.equals("ERR-00-000")){
		mdlAPIResult.error_schema = mdlAPIStatus;
		return gson.toJson(mdlAPIResult);
	    }

	    List<model.mdlRating> ratingList = UtilityAdapter.GetRating(pageNumber, pageSize, search);
	    int ratingListTotal = UtilityAdapter.GetRatingTotalList(search);
	    
	    if (ratingList == null || ratingList.size() == 0) {
			mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_99_999);
			
			model.mdlArrayResult arrayResult = new model.mdlArrayResult();
			arrayResult.recordsTotal = 0;
			arrayResult.recordsFiltered = 0;
			arrayResult.draw = Integer.parseInt(draw) + 1;
			arrayResult.data = new ArrayList<model.mdlRating>() ;
			mdlAPIResult.output_schema = arrayResult;
			
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			jsonOut = gson.toJson(mdlAPIResult);
			logger.info("FAILED = API : " + url + ", method : " + apiMethod + ", jsonIn :" + jsonIn + ", jsonOut : " + jsonOut);
			return gson.toJson(mdlAPIResult);
	    } else {
			mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_00_000);
			model.mdlArrayResult arrayResult = new model.mdlArrayResult();
			arrayResult.recordsTotal = ratingListTotal;
			arrayResult.recordsFiltered = ratingListTotal;
			arrayResult.draw = Integer.parseInt(draw) + 1;
			arrayResult.data = ratingList;
			mdlAPIResult.output_schema = arrayResult;
			response.setStatus(HttpServletResponse.SC_OK);
			jsonOut = gson.toJson(mdlAPIResult);
			logger.info("SUCCESS. API : " + url + ", method : " + apiMethod + ", jsonIn :" + jsonIn + ", jsonOut : " + jsonOut);
	    }
	} catch (Exception ex) {
	    mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_99_999);
	    response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
	    jsonOut = gson.toJson(mdlAPIResult);
	    logger.error("FAILED. API : " + url + ", method : " + apiMethod + ", jsonIn : " + jsonIn + ", jsonOut : " + jsonOut + ",Exception:" + ex.toString());
	}
	return gson.toJson(mdlAPIResult);
    }
    
    @RequestMapping(value = "/send-email", method = RequestMethod.POST, consumes = "application/json")
    public @ResponseBody model.mdlAPIObjectResult SendEmail(@RequestHeader String Authorization, @RequestHeader String Key, @RequestHeader String Timestamp, @RequestHeader String Signature, @RequestBody model.mdlEmail mdlEmail, HttpServletResponse response) {
	String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();
	model.mdlAPIObjectResult mdlAPIResult = new model.mdlAPIObjectResult();
	String jsonIn = gson.toJson(mdlEmail);
	String jsonOut = "";
	String apiMethod = "POST";
	String url = "/send-email";

	try {
	    // check token, key and signature	    
//	    model.mdlErrorSchema mdlAPIStatus = AuthorizationAdapter.CheckAuthorization(Authorization, Key, Timestamp, Signature, response, jsonIn, apiMethod, url, mdlAPIResult);
//	    if (!mdlAPIStatus.error_code.equals("ERR-00-000")){
//		mdlAPIResult.error_schema = mdlAPIStatus;
//		return mdlAPIResult;
//	    }

	    boolean isValid = true;
	    if (mdlEmail.email_subject == null || mdlEmail.email_subject.trim().equals("")) {
		mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_41_001);
		isValid = false;
	    } else if (mdlEmail.email_to == null || mdlEmail.email_to.equals("")) {
		mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_41_002);
		isValid = false;
	    }

	    if (!isValid) {
		response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		jsonOut = gson.toJson(mdlAPIResult);
		LogAdapter.InsertAPILog(apiSource, "FAILED", url, functionName, jsonIn, jsonOut);
		logger.error("FAILED = API : " + url + ", method : " + apiMethod + ", jsonIn :" + jsonIn + ", jsonOut : " + jsonOut);
		return mdlAPIResult;
	    }

	    boolean success = UtilityAdapter.SendEmail(mdlEmail);

	    if (success) {
		mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_00_000);
		response.setStatus(HttpServletResponse.SC_OK);
		jsonOut = gson.toJson(mdlAPIResult);
		LogAdapter.InsertAPILog(apiSource, "SUCCESS", url, functionName, jsonIn, jsonOut);
		logger.info("SUCCESS. API : " + url + ", method : " + apiMethod + ", jsonIn :" + jsonIn + ", jsonOut : " + jsonOut);
	    } else {
		mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_41_003);
		response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		jsonOut = gson.toJson(mdlAPIResult);
		LogAdapter.InsertAPILog(apiSource, "FAILED", url, functionName, jsonIn, jsonOut);
		logger.error("FAILED = API : " + url + ", method : " + apiMethod + ", jsonIn :" + jsonIn + ", jsonOut : " + jsonOut);
		return mdlAPIResult;
	    }
	} catch (Exception ex) {
	    mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_99_999);
	    response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
	    jsonOut = gson.toJson(mdlAPIResult);
	    LogAdapter.InsertAPILog(apiSource, "FAILED", url, functionName, jsonIn, jsonOut, ex.toString());
	    logger.error("FAILED. API : " + url + ", method : " + apiMethod + ", jsonIn : " + jsonIn + ", jsonOut : " + jsonOut + ",Exception:" + ex.toString());
	}
	return mdlAPIResult;
    }
    
}
