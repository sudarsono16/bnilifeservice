package controller;

import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;

import adapter.APIAdapter;
import adapter.AuthorizationAdapter;

import adapter.ErrorAdapter;
import adapter.FFSAdapter;
import adapter.LogAdapter;
import model.ErrorStatus;

@RestController
@RequestMapping("/ogs")
public class OGSController {
    final static Logger logger = Logger.getLogger(ClaimController.class);
    static Gson gson = new Gson();
    static String baseURL = "/ogs";
    static String apiSource = "Web Server";

    @RequestMapping(value = "/get-nab/{member_no}", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody String GetNAB(@RequestHeader String Authorization, @RequestHeader String Key, @RequestHeader String Timestamp, @RequestHeader String Signature, @PathVariable("member_no") String memberNo, HttpServletResponse response) {
	String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();
	model.mdlAPIArrayResult mdlAPIResult = new model.mdlAPIArrayResult();
	String jsonIn = "";
	String jsonOut = "";
	String apiMethod = "GET";
	String url = baseURL + "/get-nab";

	try {
	    // check token, key and signature
	    model.mdlErrorSchema mdlAPIStatus = AuthorizationAdapter.CheckAuthorizationArray(Authorization, Key, Timestamp, Signature, response, jsonIn, apiMethod, url, mdlAPIResult);
	    if (!mdlAPIStatus.error_code.equals("ERR-00-000")) {
		mdlAPIResult.error_schema = mdlAPIStatus;
		return gson.toJson(mdlAPIResult);
	    }

	    List<model.mdlNABDetails> nabDetailsList = APIAdapter.HitAPIGetNAB(memberNo);

	    if (nabDetailsList == null || nabDetailsList.size() == 0) {
		mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_39_001);
		response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		jsonOut = gson.toJson(mdlAPIResult);
		LogAdapter.InsertAPILog(apiSource, "FAILED", url, functionName, jsonIn, jsonOut);
		logger.error("FAILED = API : " + url + ", method : " + apiMethod + ", jsonIn :" + jsonIn + ", jsonOut : " + jsonOut);
		gson.toJson(mdlAPIResult);
	    } else {
		mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_00_000);
		model.mdlArrayResult mdlOutputSchemaArray = new model.mdlArrayResult();
		mdlOutputSchemaArray.items = nabDetailsList;
		mdlAPIResult.output_schema = mdlOutputSchemaArray;
		response.setStatus(HttpServletResponse.SC_OK);
		jsonOut = gson.toJson(mdlAPIResult);
		LogAdapter.InsertAPILog(apiSource, "SUCCESS", url, functionName, jsonIn, jsonOut);
		logger.info("SUCCESS. API : " + url + ", method : " + apiMethod + ", jsonIn :" + jsonIn + ", jsonOut : " + jsonOut);
	    }
	} catch (Exception ex) {
	    mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_99_999);
	    response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
	    jsonOut = gson.toJson(mdlAPIResult);
	    logger.error("FAILED. API : " + url + ", method : " + apiMethod + ", jsonIn : " + jsonIn + ", jsonOut : " + jsonOut + ",Exception:" + ex.toString());
	}
	return gson.toJson(mdlAPIResult);
    }
    
    @RequestMapping(value = "/get-ffs/{company_code}", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody String GetFFS(@RequestHeader String Authorization, @RequestHeader String Key, @RequestHeader String Timestamp, @RequestHeader String Signature, @PathVariable("company_code") String companyCode, HttpServletResponse response) {
	String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();
	model.mdlAPIObjectResult mdlAPIResult = new model.mdlAPIObjectResult();
	String jsonIn = "";
	String jsonOut = "";
	String apiMethod = "GET";
	String url = baseURL + "/get-ffs";

	try {
	    // check token, key and signature
	    model.mdlErrorSchema mdlAPIStatus = AuthorizationAdapter.CheckAuthorization(Authorization, Key, Timestamp, Signature, response, jsonIn, apiMethod, url, mdlAPIResult);
	    if (!mdlAPIStatus.error_code.equals("ERR-00-000")) {
		mdlAPIResult.error_schema = mdlAPIStatus;
		return gson.toJson(mdlAPIResult);
	    }

	    model.mdlFFS mdlFFS = FFSAdapter.GetFFS(companyCode);

	    if (mdlFFS == null || mdlFFS.ffs_link == null || mdlFFS.ffs_link.equals("") ) {
		mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_40_001);
		response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		jsonOut = gson.toJson(mdlAPIResult);
		LogAdapter.InsertAPILog(apiSource, "FAILED", url, functionName, jsonIn, jsonOut);
		logger.error("FAILED = API : " + url + ", method : " + apiMethod + ", jsonIn :" + jsonIn + ", jsonOut : " + jsonOut);
		gson.toJson(mdlAPIResult);
	    } else {
		mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_00_000);
		mdlAPIResult.output_schema = mdlFFS;
		response.setStatus(HttpServletResponse.SC_OK);
		jsonOut = gson.toJson(mdlAPIResult);
		LogAdapter.InsertAPILog(apiSource, "SUCCESS", url, functionName, jsonIn, jsonOut);
		logger.info("SUCCESS. API : " + url + ", method : " + apiMethod + ", jsonIn :" + jsonIn + ", jsonOut : " + jsonOut);
	    }
	} catch (Exception ex) {
	    mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_99_999);
	    response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
	    jsonOut = gson.toJson(mdlAPIResult);
	    logger.error("FAILED. API : " + url + ", method : " + apiMethod + ", jsonIn : " + jsonIn + ", jsonOut : " + jsonOut + ",Exception:" + ex.toString());
	}
	return gson.toJson(mdlAPIResult);
    }

}
