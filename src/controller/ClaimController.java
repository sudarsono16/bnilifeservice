package controller;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.FilenameUtils;
import org.apache.log4j.Logger;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.google.gson.Gson;

import adapter.APIAdapter;
import adapter.AuthorizationAdapter;
import adapter.ClaimAdapter;
import adapter.ErrorAdapter;
import adapter.LogAdapter;
import helper.DateHelper;
import helper.FileHelper;
import model.ErrorStatus;

@RestController
@RequestMapping("/claim")
public class ClaimController {
    final static Logger logger = Logger.getLogger(ClaimController.class);
    static Gson gson = new Gson();
    static String baseURL = "/claim";
    static String apiSource = "Web Server";

    // upload-claim
    @RequestMapping(value = "/upload-claim", method = RequestMethod.POST, consumes = "application/json")
    public @ResponseBody model.mdlAPIObjectResult UploadClaim(@RequestHeader String Authorization, @RequestHeader String Key, @RequestHeader String Timestamp, @RequestHeader String Signature, @RequestBody model.mdlClaim mdlClaim, HttpServletResponse response) {
	String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();
	long startTime = System.currentTimeMillis();
	long stopTime = 0;
	long elapsedTime = 0;
	model.mdlAPIObjectResult mdlAPIResult = new model.mdlAPIObjectResult();
	String jsonIn = gson.toJson(mdlClaim);
	String jsonOut = "";
	String apiMethod = "POST";
	String url = baseURL + "/upload-claim";

	try {
	    // check token, key and signature	    
	    model.mdlErrorSchema mdlAPIStatus = AuthorizationAdapter.CheckAuthorization(Authorization, Key, Timestamp, Signature, response, jsonIn, apiMethod, url, mdlAPIResult);
	    if (!mdlAPIStatus.error_code.equals("ERR-00-000")){
		mdlAPIResult.error_schema = mdlAPIStatus;
		return mdlAPIResult;
	    }

	    boolean isValid = true;
	    if (mdlClaim.username == null || mdlClaim.username.trim().equals("")) {
		mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_21_001);
		isValid = false;
	    } else if (mdlClaim.member_id == null || mdlClaim.member_id.equals("")) {
		mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_21_002);
		isValid = false;
	    } else if (mdlClaim.account_no == null || mdlClaim.account_no.equals("")) {
		mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_21_003);
		isValid = false;
	    } else if (mdlClaim.claim_date == null || mdlClaim.claim_date.equals("")) {
		mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_21_004);
		isValid = false;
	    } else if (mdlClaim.member_type == null || mdlClaim.member_type.equals("")) {
		mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_21_005);
		isValid = false;
	    } else if (mdlClaim.member_id == null || mdlClaim.member_id.equals("")) {
		mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_21_007);
		isValid = false;
	    }
	    
	    if (!isValid) {
		response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		jsonOut = gson.toJson(mdlAPIResult);
		stopTime = System.currentTimeMillis();
		elapsedTime = stopTime - startTime;
		LogAdapter.InsertAPILog(apiSource, "FAILED", url, functionName, jsonIn, jsonOut);
		logger.error("FAILED = Response Time : " + elapsedTime + ", API : " + url + ", method : " + apiMethod + ", jsonIn :" + jsonIn + ", jsonOut : " + jsonOut);
		return mdlAPIResult;
	    }
	    
	    model.mdlClaim mdlClaimExists = ClaimAdapter.CheckClaimDuplicateData(mdlClaim);
	    if (mdlClaimExists.claim_no != null && !mdlClaimExists.claim_no.equals("")){
		mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_21_008);
		mdlAPIResult.output_schema = mdlClaimExists;
		response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		jsonOut = gson.toJson(mdlAPIResult);
		stopTime = System.currentTimeMillis();
		elapsedTime = stopTime - startTime;
		LogAdapter.InsertAPILog(apiSource, "FAILED", url, functionName, jsonIn, jsonOut);
		logger.error("FAILED = Response Time : " + elapsedTime + ", API : " + url + ", method : " + apiMethod + ", jsonIn :" + jsonIn + ", jsonOut : " + jsonOut);
		return mdlAPIResult;
	    }

	    String claimNo = ClaimAdapter.GenerateClaimNo();
	    mdlClaim.claim_no = claimNo;

	    boolean success = ClaimAdapter.UploadClaimData(mdlClaim);

	    if (!success) {
		mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_21_006);
		response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		jsonOut = gson.toJson(mdlAPIResult);
		LogAdapter.InsertAPILog(apiSource, "FAILED", url, functionName, jsonIn, jsonOut);
		logger.error("FAILED = API : " + url + ", method : " + apiMethod + ", jsonIn :" + jsonIn + ", jsonOut : " + jsonOut);
		return mdlAPIResult;
	    } else {
		mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_00_000);
		mdlAPIResult.output_schema = mdlClaim;
		response.setStatus(HttpServletResponse.SC_OK);
		jsonOut = gson.toJson(mdlAPIResult);
		stopTime = System.currentTimeMillis();
		elapsedTime = stopTime - startTime;
		LogAdapter.InsertAPILog(apiSource, "SUCCESS", url, functionName, jsonIn, jsonOut);
		logger.info("SUCCESS. Response Time : " + elapsedTime + ", API : " + url + ", method : " + apiMethod + ", jsonIn :" + jsonIn + ", jsonOut : " + jsonOut);
	    }
	} catch (Exception ex) {
	    mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_99_999);
	    response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
	    jsonOut = gson.toJson(mdlAPIResult);
	    stopTime = System.currentTimeMillis();
	    elapsedTime = stopTime - startTime;
	    LogAdapter.InsertAPILog(apiSource, "FAILED", url, functionName, jsonIn, jsonOut, ex.toString());
	    logger.error("FAILED. Response Time : " + elapsedTime + ", API : " + url + ", method : " + apiMethod + ", jsonIn : " + jsonIn + ", jsonOut : " + jsonOut + ",Exception:" + ex.toString());
	}
	return mdlAPIResult;
    }

    // upload-claim-document
    @RequestMapping(value = "/upload-claim-document", method = RequestMethod.POST, consumes = "multipart/form-data")
    public @ResponseBody String UploadClaimDocument(@RequestHeader String Authorization, @RequestHeader String Key, @RequestHeader String Timestamp, @RequestHeader String Signature, @RequestParam(name = "file", required = false) final MultipartFile[] imageList, @RequestParam("json") final String json, HttpServletResponse response) {
	String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();
	long startTime = System.currentTimeMillis();
	long stopTime = 0;
	long elapsedTime = 0;
	model.mdlAPIObjectResult mdlAPIResult = new model.mdlAPIObjectResult();
	model.mdlUploadClaim mdlUploadClaim = gson.fromJson(json, model.mdlUploadClaim.class);
	model.mdlClaimDocument mdlClaimDocument = new model.mdlClaimDocument();
	String jsonIn = "";
	jsonIn = gson.toJson(mdlUploadClaim);
	String jsonOut = "";
	String apiMethod = "POST";
	String url = baseURL + "/upload-claim-document";
	List<model.mdlImage> imageLocationList = new ArrayList<model.mdlImage>();
	Integer successUploadImage = 0;
	Integer failedUploadImage = 0;
	String dateNow = DateHelper.GetDateTimeNowCustomFormat("yyyyMMdd");
	String baseDirectoryPath = "";
	try {
	    // check token, key and signature	    
	    model.mdlErrorSchema mdlAPIStatus = AuthorizationAdapter.CheckAuthorization(Authorization, Key, Timestamp, Signature, response, jsonIn, apiMethod, url, mdlAPIResult);
	    if (!mdlAPIStatus.error_code.equals("ERR-00-000")){
		mdlAPIResult.error_schema = mdlAPIStatus;
		return gson.toJson(mdlAPIResult);
	    }

	    if (imageList != null) {
		boolean wrongExtension = false;
		Integer imageCount = 1;
		ClaimAdapter.RemoveClaimImageData(mdlUploadClaim.claim_no);
		for (MultipartFile image : imageList) { // loop to save images to web server

		    String fileName = image.getOriginalFilename();

		    model.mdlFilenameList mdlFilename = mdlUploadClaim.filename_list.stream().filter(file -> file.filename.equalsIgnoreCase(fileName)).findAny().orElse(null);
		    String claimDocumentTypeID = mdlFilename.claim_document_type_id;

		    String ext = FilenameUtils.getExtension(fileName);
		    // check if ext is jpeg or jpg, if not,
		    if (!ext.isEmpty() && (ext.toString().equalsIgnoreCase("jpg") || ext.toString().equalsIgnoreCase("jpeg"))) {
			StringBuilder sb = new StringBuilder();
			Context context = (Context) new InitialContext().lookup("java:comp/env");
			String sourcePath = (String) context.lookup("source_path_claim");
			String basePath = "bnilife/claim";
			String urlWebServer = (String) context.lookup("url_web_server_for_upload");
			String uploadPath = sourcePath + basePath;
			baseDirectoryPath = basePath + "/" + mdlUploadClaim.username + "/" + mdlUploadClaim.claim_no;
			String directory = sb.append(uploadPath).append("/").append(mdlUploadClaim.username).append("/").append(mdlUploadClaim.claim_no).toString();

			File filedir = new File(directory);
			boolean successCreateDir = false;
			if (!filedir.exists()) {
			    successCreateDir = filedir.mkdirs();
			} else {
			    successCreateDir = true;
			}
			if (!successCreateDir) {
			    mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_17_008);
			    response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			    jsonOut = gson.toJson(mdlAPIResult);
			    stopTime = System.currentTimeMillis();
			    elapsedTime = stopTime - startTime;
			    LogAdapter.InsertAPILog(apiSource, "FAILED", url, functionName, jsonIn, jsonOut);
			    logger.error("FAILED = Response Time : " + elapsedTime + ", API : " + url + ", method : " + apiMethod + ", jsonIn :" + jsonIn + ", jsonOut : " + jsonOut);
			    return gson.toJson(mdlAPIResult);
			}
			String fileCount = String.format("%03d", imageCount);
			String uploadedFileName = mdlUploadClaim.username + "_" + mdlUploadClaim.member_id + "_" + dateNow + "_" + fileCount + ".jpg";
			String uploadedFileLocation = directory + "/" + uploadedFileName;
			File fileImage = new File(uploadedFileLocation);
			// Files.deleteIfExists(fileImage.toPath());
			if (fileImage.exists()) {
			    fileImage.delete();
			}
			boolean successCreateFile = FileHelper.writeToFile(image.getInputStream(), directory, uploadedFileLocation);
			if (successCreateFile) {
			    model.mdlImage mdlImage = new model.mdlImage();
			    mdlImage.claim_document_type_id = claimDocumentTypeID;
			    mdlImage.image_original_name = fileName;
			    mdlImage.image_uploaded_name = uploadedFileName;
			    mdlImage.image_directory = uploadedFileLocation;
			    mdlImage.image_link = urlWebServer + baseDirectoryPath + "/" + uploadedFileName;
			    mdlImage.uploaded = 0;
			    boolean saveImageData = ClaimAdapter.SaveClaimImageData(mdlUploadClaim, mdlImage);
			    if (saveImageData) {
				successUploadImage++;
				mdlImage.uploaded = 1;
				imageLocationList.add(mdlImage);
			    }
			} else {
			    failedUploadImage++;
			}
		    } else {
			wrongExtension = true;
			break;
		    }
		    imageCount++;
		}

		logger.info("SUCCESS = Response Time : " + elapsedTime + ", API : " + url + ", Upload Image To Web Server Time");

		mdlClaimDocument.username = mdlUploadClaim.username;
		mdlClaimDocument.member_id = mdlUploadClaim.member_id;
		mdlClaimDocument.member_child_id = mdlUploadClaim.member_child_id;
		mdlClaimDocument.claim_no = mdlUploadClaim.claim_no;
		mdlClaimDocument.pdf_link = "";

		boolean isValid = true;
		if (wrongExtension) {
		    mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_17_002);
		    isValid = false;
		} else if (successUploadImage == 0) {
		    mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_17_003);
		    mdlClaimDocument.image_link = imageLocationList;
		    mdlAPIResult.output_schema = mdlClaimDocument;
		    isValid = false;
		} else if (failedUploadImage != 0) {
		    mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_17_004);
		    mdlClaimDocument.image_link = imageLocationList;
		    mdlAPIResult.output_schema = mdlClaimDocument;
		    isValid = false;
		} else if (successUploadImage != imageLocationList.size()) {
		    mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_17_005);
		    isValid = false;
		}

		if (!isValid) {
		    response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		    jsonOut = gson.toJson(mdlAPIResult);
		    stopTime = System.currentTimeMillis();
		    elapsedTime = stopTime - startTime;
		    LogAdapter.InsertAPILog(apiSource, "FAILED", url, functionName, jsonIn, jsonOut);
		    logger.error("FAILED = Response Time : " + elapsedTime + ", API : " + url + ", method : " + apiMethod + ", jsonIn :" + jsonIn + ", jsonOut : " + jsonOut);
		    return gson.toJson(mdlAPIResult);
		}

		String pdfFileLocation = "";
		if (imageLocationList == null || imageLocationList.size() == 0) {
		    mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_17_006);
		    response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		    jsonOut = gson.toJson(mdlAPIResult);
		    stopTime = System.currentTimeMillis();
		    elapsedTime = stopTime - startTime;
		    LogAdapter.InsertAPILog(apiSource, "FAILED", url, functionName, jsonIn, jsonOut);
		    logger.error("FAILED = Response Time : " + elapsedTime + ", API : " + url + ", method : " + apiMethod + ", jsonIn :" + jsonIn + ", jsonOut : " + jsonOut);
		    return gson.toJson(mdlAPIResult);
		} else {
		    pdfFileLocation = ClaimAdapter.ConvertJpegToPDF(imageLocationList, mdlUploadClaim, dateNow, baseDirectoryPath);
		}

		if (pdfFileLocation.equalsIgnoreCase("")) {
		    mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_17_007);
		    response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		    jsonOut = gson.toJson(mdlAPIResult);
		    stopTime = System.currentTimeMillis();
		    elapsedTime = stopTime - startTime;
		    LogAdapter.InsertAPILog(apiSource, "FAILED", url, functionName, jsonIn, jsonOut);
		    logger.error("FAILED = Response Time : " + elapsedTime + ", API : " + url + ", method : " + apiMethod + ", jsonIn :" + jsonIn + ", jsonOut : " + jsonOut);
		    return gson.toJson(mdlAPIResult);
		} else {
		    mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_00_000);
		    mdlClaimDocument.image_link = imageLocationList;
		    mdlClaimDocument.pdf_link = pdfFileLocation;
		    mdlAPIResult.output_schema = mdlClaimDocument;
		    response.setStatus(HttpServletResponse.SC_OK);
		    jsonOut = gson.toJson(mdlAPIResult);
		    stopTime = System.currentTimeMillis();
		    elapsedTime = stopTime - startTime;
		    LogAdapter.InsertAPILog(apiSource, "SUCCESS", url, functionName, jsonIn, jsonOut);
		    logger.info("SUCCESS = Response Time : " + elapsedTime + ", API : " + url + ", method : " + apiMethod + ", jsonIn :" + jsonIn + ", jsonOut : " + jsonOut);
		}

	    } else {
		mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_17_001);
		response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		jsonOut = gson.toJson(mdlAPIResult);
		stopTime = System.currentTimeMillis();
		elapsedTime = stopTime - startTime;
		LogAdapter.InsertAPILog(apiSource, "FAILED", url, functionName, jsonIn, jsonOut);
		logger.error("FAILED = Response Time : " + elapsedTime + ", API : " + url + ", method : " + apiMethod + ", jsonIn :" + jsonIn + ", jsonOut : " + jsonOut);
		return gson.toJson(mdlAPIResult);
	    }

	} catch (Exception ex) {
	    mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_99_999);
	    response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
	    jsonOut = gson.toJson(mdlAPIResult);
	    stopTime = System.currentTimeMillis();
	    elapsedTime = stopTime - startTime;
	    LogAdapter.InsertAPILog(apiSource, "FAILED", url, functionName, jsonIn, jsonOut, ex.toString());
	    logger.error("FAILED = Response Time : " + elapsedTime + ", API : " + url + ", method : " + apiMethod + ", jsonIn : " + jsonIn + ", jsonOut : " + jsonOut + ",Exception:" + ex.toString());
	}
	return gson.toJson(mdlAPIResult);
    }

    @RequestMapping(value = "/get-claim-type/{member-no}", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody model.mdlAPIArrayResult GetClaimType(@RequestHeader String Authorization, @RequestHeader String Key, @RequestHeader String Timestamp, @RequestHeader String Signature, @PathVariable("member-no") String memberNo, HttpServletResponse response) {
	String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();
	model.mdlAPIArrayResult mdlAPIResult = new model.mdlAPIArrayResult();
	String jsonIn = "member-no : " + memberNo;
	String jsonOut = "";
	String apiMethod = "GET";
	String url = baseURL + "/get-claim-type";

	try {
	    // check token, key and signature
	    model.mdlErrorSchema mdlAPIStatus = AuthorizationAdapter.CheckAuthorization(Authorization, Key, url, Timestamp, Signature, apiMethod);
	    if (mdlAPIStatus == null) {
		mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_99_999);
		response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		jsonOut = gson.toJson(mdlAPIResult);
		logger.error("FAILED = API : " + url + ", method : " + apiMethod + ", jsonIn :" + jsonIn + ", jsonOut : " + jsonOut);
		return mdlAPIResult;
	    } else if (!mdlAPIStatus.error_code.equalsIgnoreCase("ERR-00-000")) {
		mdlAPIResult.error_schema = mdlAPIStatus;
		if (mdlAPIStatus.error_code.equalsIgnoreCase("ERR-99-005") || mdlAPIStatus.error_code.equalsIgnoreCase("ERR-99-006")) {
		    // if token invalid or token expired
		    response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
		} else {
		    response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		}
		jsonOut = gson.toJson(mdlAPIStatus);
		logger.error("FAILED = API : " + url + ", method : " + apiMethod + ", jsonIn :" + jsonIn + ", jsonOut : " + jsonOut);
		return mdlAPIResult;
	    }

	    if (memberNo == null || memberNo.trim().equals("")) {
		mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_23_001);
		response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		jsonOut = gson.toJson(mdlAPIResult);
		LogAdapter.InsertAPILog(apiSource, "FAILED", url, functionName, jsonIn, jsonOut);
		logger.error("FAILED = API : " + url + ", method : " + apiMethod + ", jsonIn :" + jsonIn + ", jsonOut : " + jsonOut);
		return mdlAPIResult;
	    }

	    List<model.mdlBenefitType> benefitTypeList = APIAdapter.HitAPIBenefitType(memberNo);
	    // model.mdlBenefitType james = benefitTypeList.stream().filter(benefitType ->
	    // benefitType.type.equals("James")).findAny().orElse(null);
	    if (benefitTypeList == null || benefitTypeList.size() == 0) {
		mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_23_002);
		response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		jsonOut = gson.toJson(mdlAPIResult);
		LogAdapter.InsertAPILog(apiSource, "FAILED", url, functionName, jsonIn, jsonOut);
		logger.error("FAILED = API : " + url + ", method : " + apiMethod + ", jsonIn :" + jsonIn + ", jsonOut : " + jsonOut);
		return mdlAPIResult;
	    } else {
		mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_00_000);
		model.mdlArrayResult mdlOutputSchemaArray = new model.mdlArrayResult();
		mdlOutputSchemaArray.items = benefitTypeList;
		mdlAPIResult.output_schema = mdlOutputSchemaArray;
		response.setStatus(HttpServletResponse.SC_OK);
		jsonOut = gson.toJson(mdlAPIResult);
		LogAdapter.InsertAPILog(apiSource, "SUCCESS", url, functionName, jsonIn, jsonOut);
		logger.info("SUCCESS. API : " + url + ", method : " + apiMethod + ", jsonIn :" + jsonIn + ", jsonOut : " + jsonOut);
	    }
	} catch (Exception ex) {
	    mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_99_999);
	    response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
	    jsonOut = gson.toJson(mdlAPIResult);
	    LogAdapter.InsertAPILog(apiSource, "FAILED", url, functionName, jsonIn, jsonOut, ex.toString());
	    logger.error("FAILED. API : " + url + ", method : " + apiMethod + ", jsonIn : " + jsonIn + ", jsonOut : " + jsonOut + ",Exception:" + ex.toString());
	}
	return mdlAPIResult;
    }

    // update-claim-status
    @RequestMapping(value = "/update-claim-status", method = RequestMethod.POST, consumes = "application/json")
    public @ResponseBody model.mdlAPIObjectResult UpdateClaimStatus(@RequestHeader String Authorization, @RequestHeader String Key, @RequestHeader String Timestamp, @RequestHeader String Signature, @RequestBody model.mdlUpdateClaim mdlUpdateClaim, HttpServletResponse response) {
	String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();
	model.mdlAPIObjectResult mdlAPIResult = new model.mdlAPIObjectResult();
	String jsonIn = gson.toJson(mdlUpdateClaim);
	String jsonOut = "";
	String apiMethod = "POST";
	String url = baseURL + "/update-claim-status";

	try {
	    // check token, key and signature	    
	    model.mdlErrorSchema mdlAPIStatus = AuthorizationAdapter.CheckAuthorization(Authorization, Key, Timestamp, Signature, response, jsonIn, apiMethod, url, mdlAPIResult);
	    if (!mdlAPIStatus.error_code.equals("ERR-00-000")){
		mdlAPIResult.error_schema = mdlAPIStatus;
		return mdlAPIResult;
	    }

	    boolean isValid = true;
	    if (mdlUpdateClaim.claim_no == null || mdlUpdateClaim.claim_no.trim().equals("")) {
		mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_24_001);
		isValid = false;
	    } else if (mdlUpdateClaim.member_id == null || mdlUpdateClaim.member_id.equals("")) {
		mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_24_002);
		isValid = false;
	    } else if (mdlUpdateClaim.member_child_id == null || mdlUpdateClaim.member_child_id.equals("")) {
		mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_24_003);
		isValid = false;
	    } else if (mdlUpdateClaim.member_type == null || mdlUpdateClaim.member_type.equals("")) {
		mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_24_004);
		isValid = false;
	    } else if (mdlUpdateClaim.claim_status == null || mdlUpdateClaim.claim_status.equals("")) {
		mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_24_005);
		isValid = false;
	    }

	    if (!isValid) {
		response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		jsonOut = gson.toJson(mdlAPIResult);
		LogAdapter.InsertAPILog(apiSource, "FAILED", url, functionName, jsonIn, jsonOut);
		logger.error("FAILED = API : " + url + ", method : " + apiMethod + ", jsonIn :" + jsonIn + ", jsonOut : " + jsonOut);
		return mdlAPIResult;
	    }

	    model.mdlClaim mdlClaim = ClaimAdapter.GetClaimData(mdlUpdateClaim.claim_no);
	    if (mdlClaim == null || mdlClaim.claim_no == null || mdlClaim.claim_no.equals("")) {
		mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_24_006);
		isValid = false;
	    }
	    String updateDocumentLink = ClaimAdapter.UpdateClaimStatus(mdlUpdateClaim, mdlClaim.username);

	    if (updateDocumentLink.equals("")) {
		mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_24_007);
		response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		jsonOut = gson.toJson(mdlAPIResult);
		LogAdapter.InsertAPILog(apiSource, "FAILED", url, functionName, jsonIn, jsonOut);
		logger.error("FAILED = API : " + url + ", method : " + apiMethod + ", jsonIn :" + jsonIn + ", jsonOut : " + jsonOut);
		return mdlAPIResult;
	    } else {
		mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_00_000);
		model.mdlHTMLLink mdlHTMLLink = new model.mdlHTMLLink();
		mdlHTMLLink.html_link = updateDocumentLink;
		mdlAPIResult.output_schema = mdlHTMLLink;
		response.setStatus(HttpServletResponse.SC_OK);
		jsonOut = gson.toJson(mdlAPIResult);
		LogAdapter.InsertAPILog(apiSource, "SUCCESS", url, functionName, jsonIn, jsonOut);
		logger.info("SUCCESS. API : " + url + ", method : " + apiMethod + ", jsonIn :" + jsonIn + ", jsonOut : " + jsonOut);
	    }
	} catch (Exception ex) {
	    mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_99_999);
	    response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
	    jsonOut = gson.toJson(mdlAPIResult);
	    LogAdapter.InsertAPILog(apiSource, "FAILED", url, functionName, jsonIn, jsonOut, ex.toString());
	    logger.error("FAILED. API : " + url + ", method : " + apiMethod + ", jsonIn : " + jsonIn + ", jsonOut : " + jsonOut + ",Exception:" + ex.toString());
	}
	return mdlAPIResult;
    }

    @RequestMapping(value = "/get-claim-document-type/{claim-type}", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody String GetClaimDocumentType(@RequestHeader String Authorization, @RequestHeader String Key, @RequestHeader String Timestamp, @RequestHeader String Signature, @PathVariable("claim-type") String claimType, HttpServletResponse response) {
	String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();
	model.mdlAPIArrayResult mdlAPIResult = new model.mdlAPIArrayResult();
	String jsonIn = "claim-type : " + claimType;
	String jsonOut = "";
	String apiMethod = "GET";
	String url = baseURL + "/get-claim-document-type";

	try {
	    // check token, key and signature	    
	    model.mdlErrorSchema mdlAPIStatus = AuthorizationAdapter.CheckAuthorizationArray(Authorization, Key, Timestamp, Signature, response, jsonIn, apiMethod, url, mdlAPIResult);
	    if (!mdlAPIStatus.error_code.equals("ERR-00-000")){
		mdlAPIResult.error_schema = mdlAPIStatus;
		return gson.toJson(mdlAPIResult);
	    }

	    if (claimType == null || claimType.trim().equals("")) {
		mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_25_001);
		response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		jsonOut = gson.toJson(mdlAPIResult);
		LogAdapter.InsertAPILog(apiSource, "FAILED", url, functionName, jsonIn, jsonOut);
		logger.error("FAILED = API : " + url + ", method : " + apiMethod + ", jsonIn :" + jsonIn + ", jsonOut : " + jsonOut);
		return gson.toJson(mdlAPIResult);
	    }

	    List<model.mdlClaimDocumentType> claimDocumentTypeList = ClaimAdapter.GetClaimDocumentType(claimType);
	    if (claimDocumentTypeList == null || claimDocumentTypeList.size() == 0) {
		mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_25_002);
		response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		jsonOut = gson.toJson(mdlAPIResult);
		LogAdapter.InsertAPILog(apiSource, "FAILED", url, functionName, jsonIn, jsonOut);
		logger.error("FAILED = API : " + url + ", method : " + apiMethod + ", jsonIn :" + jsonIn + ", jsonOut : " + jsonOut);
		return gson.toJson(mdlAPIResult);
	    } else {
		mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_00_000);
		model.mdlArrayResult mdlOutputSchemaArray = new model.mdlArrayResult();
		mdlOutputSchemaArray.items = claimDocumentTypeList;
		mdlAPIResult.output_schema = mdlOutputSchemaArray;
		response.setStatus(HttpServletResponse.SC_OK);
		jsonOut = gson.toJson(mdlAPIResult);
		LogAdapter.InsertAPILog(apiSource, "SUCCESS", url, functionName, jsonIn, jsonOut);
		logger.info("SUCCESS. API : " + url + ", method : " + apiMethod + ", jsonIn :" + jsonIn + ", jsonOut : " + jsonOut);
	    }
	} catch (Exception ex) {
	    mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_99_999);
	    response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
	    jsonOut = gson.toJson(mdlAPIResult);
	    LogAdapter.InsertAPILog(apiSource, "FAILED", url, functionName, jsonIn, jsonOut, ex.toString());
	    logger.error("FAILED. API : " + url + ", method : " + apiMethod + ", jsonIn : " + jsonIn + ", jsonOut : " + jsonOut + ",Exception:" + ex.toString());
	}
	return gson.toJson(mdlAPIResult);
    }

    @RequestMapping(value = "/get-claim-list-by-date", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody String GetClaimListByDate(@RequestHeader String Authorization, @RequestHeader String Key, @RequestHeader String Timestamp, @RequestHeader String Signature, @RequestParam(value = "username", required = false) String username, @RequestParam(value = "pageNumber", defaultValue = "1") String pageNumber, @RequestParam(value = "pageSize", defaultValue = "10") String pageSize, @RequestParam(value = "draw", defaultValue = "0") String draw, @RequestParam(value = "search", required = false) String search, HttpServletResponse response) {
	model.mdlAPIArrayResult mdlAPIResult = new model.mdlAPIArrayResult();
	String jsonIn = "";
	String jsonOut = "";
	String apiMethod = "GET";
	String url = baseURL + "/get-claim-list-by-date";

	try {
	    // check token, key and signature	    
	    model.mdlErrorSchema mdlAPIStatus = AuthorizationAdapter.CheckAuthorizationArray(Authorization, Key, Timestamp, Signature, response, jsonIn, apiMethod, url, mdlAPIResult);
	    if (!mdlAPIStatus.error_code.equals("ERR-00-000")){
		mdlAPIResult.error_schema = mdlAPIStatus;
		return gson.toJson(mdlAPIResult);
	    }

	    List<model.mdlClaim> dataList = ClaimAdapter.GetClaimListByDate(username, pageNumber, pageSize, search);
	    int dataListTotal = ClaimAdapter.GetClaimTotalList(username, search);
	    
	    if (dataList == null || dataList.size() == 0) {
			mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_99_999);
			
			model.mdlArrayResult arrayResult = new model.mdlArrayResult();
			arrayResult.recordsTotal = 0;
			arrayResult.recordsFiltered = 0;
			arrayResult.draw = Integer.parseInt(draw) + 1;
			arrayResult.data = new ArrayList<model.mdlClaim>() ;
			mdlAPIResult.output_schema = arrayResult;
			
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			jsonOut = gson.toJson(mdlAPIResult);
			logger.info("FAILED = API : " + url + ", method : " + apiMethod + ", jsonIn :" + jsonIn + ", jsonOut : " + jsonOut);
			return gson.toJson(mdlAPIResult);
	    } else {
			mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_00_000);
			model.mdlArrayResult arrayResult = new model.mdlArrayResult();
			arrayResult.recordsTotal = dataListTotal;
			arrayResult.recordsFiltered = dataListTotal;
			arrayResult.draw = Integer.parseInt(draw) + 1;
			arrayResult.data = dataList;
			mdlAPIResult.output_schema = arrayResult;
			response.setStatus(HttpServletResponse.SC_OK);
			jsonOut = gson.toJson(mdlAPIResult);
			logger.info("SUCCESS. API : " + url + ", method : " + apiMethod + ", jsonIn :" + jsonIn + ", jsonOut : " + jsonOut);
	    }
	} catch (Exception ex) {
	    mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_99_999);
	    response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
	    jsonOut = gson.toJson(mdlAPIResult);
	    logger.error("FAILED. API : " + url + ", method : " + apiMethod + ", jsonIn : " + jsonIn + ", jsonOut : " + jsonOut + ",Exception:" + ex.toString());
	}
	return gson.toJson(mdlAPIResult);
    }
    
    @RequestMapping(value = "/get-claim-list", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody model.mdlAPIArrayResult GetClaimList(@RequestHeader String Authorization, @RequestHeader String Key, @RequestHeader String Timestamp, @RequestHeader String Signature, @RequestParam(value = "username", defaultValue = "") String username, @RequestParam(value = "status", defaultValue = "all") String status, @RequestParam(value = "page", defaultValue = "1") String page, HttpServletResponse response) {
	String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();
	model.mdlAPIArrayResult mdlAPIResult = new model.mdlAPIArrayResult();
	String jsonIn = "claim-status : " + status + ", page : " + page;
	String jsonOut = "";
	String apiMethod = "GET";
	String url = baseURL + "/get-claim-list";

	try {
	    // check token, key and signature	    
	    model.mdlErrorSchema mdlAPIStatus = AuthorizationAdapter.CheckAuthorizationArray(Authorization, Key, Timestamp, Signature, response, jsonIn, apiMethod, url, mdlAPIResult);
	    if (!mdlAPIStatus.error_code.equals("ERR-00-000")){
		mdlAPIResult.error_schema = mdlAPIStatus;
		return mdlAPIResult;
	    }

	    if (username == null || username.equalsIgnoreCase("")) {
		mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_26_001);
		response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		jsonOut = gson.toJson(mdlAPIResult);
		LogAdapter.InsertAPILog(apiSource, "FAILED", url, functionName, jsonIn, jsonOut);
		logger.error("FAILED = API : " + url + ", method : " + apiMethod + ", jsonIn :" + jsonIn + ", jsonOut : " + jsonOut);
		return mdlAPIResult;
	    }

	    List<model.mdlClaim> claimList = ClaimAdapter.GetClaimList(username, status, page);

	    mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_00_000);
	    model.mdlArrayResult mdlOutputSchemaArray = new model.mdlArrayResult();
	    mdlOutputSchemaArray.items = claimList;
	    mdlAPIResult.output_schema = mdlOutputSchemaArray;
	    response.setStatus(HttpServletResponse.SC_OK);
	    jsonOut = gson.toJson(mdlAPIResult);
	    LogAdapter.InsertAPILog(apiSource, "SUCCESS", url, functionName, jsonIn, jsonOut);
	    logger.info("SUCCESS. API : " + url + ", method : " + apiMethod + ", jsonIn :" + jsonIn + ", jsonOut : " + jsonOut);
	} catch (Exception ex) {
	    mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_99_999);
	    response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
	    jsonOut = gson.toJson(mdlAPIResult);
	    LogAdapter.InsertAPILog(apiSource, "FAILED", url, functionName, jsonIn, jsonOut, ex.toString());
	    logger.error("FAILED. API : " + url + ", method : " + apiMethod + ", jsonIn : " + jsonIn + ", jsonOut : " + jsonOut + ",Exception:" + ex.toString());
	}
	return mdlAPIResult;
    }

    // update-claim-document
    @RequestMapping(value = "/update-claim-document", method = RequestMethod.POST, consumes = "multipart/form-data")
    public @ResponseBody String UpdateClaimDocument(@RequestHeader String Authorization, @RequestHeader String Key, @RequestHeader String Timestamp, @RequestHeader String Signature, @RequestParam(name = "file", required = false) final MultipartFile[] imageList, @RequestParam("json") final String json, HttpServletResponse response) {
	String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();
	model.mdlAPIObjectResult mdlAPIResult = new model.mdlAPIObjectResult();
	model.mdlUploadClaim mdlUploadClaim = gson.fromJson(json, model.mdlUploadClaim.class);
	model.mdlClaimDocument mdlClaimDocument = new model.mdlClaimDocument();
	String jsonIn = "";
	jsonIn = gson.toJson(mdlUploadClaim);
	String jsonOut = "";
	String apiMethod = "POST";
	String url = baseURL + "/update-claim-document";
	String dateNow = DateHelper.GetDateTimeNowCustomFormat("yyyyMMdd");
	String baseDirectoryPath = "";

	try {
	    // check token, key and signature	    
	    model.mdlErrorSchema mdlAPIStatus = AuthorizationAdapter.CheckAuthorization(Authorization, Key, Timestamp, Signature, response, jsonIn, apiMethod, url, mdlAPIResult);
	    if (!mdlAPIStatus.error_code.equals("ERR-00-000")){
		mdlAPIResult.error_schema = mdlAPIStatus;
		return gson.toJson(mdlAPIResult);
	    }

	    if (mdlUploadClaim.claim_no == null || mdlUploadClaim.claim_no.equals("")) {
		mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_27_001);
		response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		jsonOut = gson.toJson(mdlAPIResult);
		LogAdapter.InsertAPILog(apiSource, "FAILED", url, functionName, jsonIn, jsonOut);
		logger.error("FAILED = API : " + url + ", method : " + apiMethod + ", jsonIn :" + jsonIn + ", jsonOut : " + jsonOut);
		return gson.toJson(mdlAPIResult);
	    }

	    Integer claimImageCount = ClaimAdapter.CheckClaimNo(mdlUploadClaim.claim_no);

	    if (claimImageCount == 0) {
		mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_27_002);
		response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		jsonOut = gson.toJson(mdlAPIResult);
		LogAdapter.InsertAPILog(apiSource, "FAILED", url, functionName, jsonIn, jsonOut);
		logger.error("FAILED = API : " + url + ", method : " + apiMethod + ", jsonIn :" + jsonIn + ", jsonOut : " + jsonOut);
		return gson.toJson(mdlAPIResult);
	    }

	    List<model.mdlImage> imageLocationList = ClaimAdapter.GetClaimImageData(mdlUploadClaim.claim_no);
	    List<model.mdlImage> updateImageLocationList = new ArrayList<model.mdlImage>();

	    if (imageList != null) {
		boolean wrongExtension = false;
		Integer imageCount = claimImageCount;
		Integer successUploadImage = 0;
		Integer failedUploadImage = 0;
		for (MultipartFile image : imageList) { // loop to save images to web server

		    String fileName = image.getOriginalFilename();

		    model.mdlFilenameList mdlFilename = mdlUploadClaim.filename_list.stream().filter(file -> file.filename.equalsIgnoreCase(fileName)).findAny().orElse(null);
		    String claimDocumentTypeID = mdlFilename.claim_document_type_id;

		    String ext = FilenameUtils.getExtension(fileName);
		    // check if ext is jpeg or jpg, if not,
		    if (!ext.isEmpty() && (ext.toString().equalsIgnoreCase("jpg") || ext.toString().equalsIgnoreCase("jpeg"))) {
			StringBuilder sb = new StringBuilder();
			Context context = (Context) new InitialContext().lookup("java:comp/env");
			String sourcePath = (String) context.lookup("source_path_claim");
			String basePath = "bnilife/claim";
			String urlWebServer = (String) context.lookup("url_web_server_for_upload");
			String uploadPath = sourcePath + basePath;
			baseDirectoryPath = basePath + "/" + mdlUploadClaim.username + "/" + mdlUploadClaim.claim_no;
			String directory = sb.append(uploadPath).append("/").append(mdlUploadClaim.username).append("/").append(mdlUploadClaim.claim_no).toString();

			File filedir = new File(directory);
			boolean successCreateDir = false;
			if (!filedir.exists()) {
			    successCreateDir = filedir.mkdirs();
			} else {
			    successCreateDir = true;
			}
			if (!successCreateDir) {
			    mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_27_003);
			    response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			    jsonOut = gson.toJson(mdlAPIResult);
			    LogAdapter.InsertAPILog(apiSource, "FAILED", url, functionName, jsonIn, jsonOut);
			    logger.error("FAILED = API : " + url + ", method : " + apiMethod + ", jsonIn :" + jsonIn + ", jsonOut : " + jsonOut);
			    return gson.toJson(mdlAPIResult);
			}
			String fileCount = String.format("%03d", imageCount + 1);
			String uploadedFileName = mdlUploadClaim.username + "_" + mdlUploadClaim.member_id + "_" + dateNow + "_" + fileCount + ".jpg";
			String uploadedFileLocation = directory + "/" + uploadedFileName;
			boolean successCreateFile = FileHelper.writeToFile(image.getInputStream(), directory, uploadedFileLocation);
			if (successCreateFile) {
			    model.mdlImage mdlImage = new model.mdlImage();
			    mdlImage.claim_document_type_id = claimDocumentTypeID;
			    mdlImage.image_original_name = fileName;
			    mdlImage.image_uploaded_name = uploadedFileName;
			    mdlImage.image_directory = uploadedFileLocation;
			    mdlImage.image_link = urlWebServer + baseDirectoryPath + "/" + uploadedFileName;
			    mdlImage.uploaded = 0;
			    boolean saveImageData = ClaimAdapter.SaveClaimImageData(mdlUploadClaim, mdlImage);
			    if (saveImageData) {
				successUploadImage++;
				mdlImage.uploaded = 1;
				imageLocationList.add(mdlImage);
				updateImageLocationList.add(mdlImage);
			    }
			} else {
			    failedUploadImage++;
			}
		    } else {
			wrongExtension = true;
			break;
		    }
		    imageCount++;
		}

		mdlClaimDocument.username = mdlUploadClaim.username;
		mdlClaimDocument.member_id = mdlUploadClaim.member_id;
		mdlClaimDocument.member_child_id = mdlUploadClaim.member_child_id;
		mdlClaimDocument.claim_no = mdlUploadClaim.claim_no;
		mdlClaimDocument.pdf_link = "";

		boolean isValid = true;
		if (wrongExtension) {
		    mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_27_004);
		    isValid = false;
		} else if (successUploadImage == 0) {
		    mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_27_005);
		    // mdlClaimDocument.image_link = imageLocationList;
		    // mdlAPIResult.output_schema = mdlClaimDocument;
		    isValid = false;
		} else if (failedUploadImage != 0) {
		    mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_27_006);
		    // mdlClaimDocument.image_link = imageLocationList;
		    // mdlAPIResult.output_schema = mdlClaimDocument;
		    isValid = false;
		} else if (successUploadImage != updateImageLocationList.size()) {
		    mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_27_007);
		    isValid = false;
		}

		if (!isValid) {
		    response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		    jsonOut = gson.toJson(mdlAPIResult);
		    LogAdapter.InsertAPILog(apiSource, "FAILED", url, functionName, jsonIn, jsonOut);
		    logger.error("FAILED = API : " + url + ", method : " + apiMethod + ", jsonIn :" + jsonIn + ", jsonOut : " + jsonOut);
		    return gson.toJson(mdlAPIResult);
		}

		String pdfFileLocation = "";
		if (updateImageLocationList == null || updateImageLocationList.size() == 0) {
		    mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_27_008);
		    response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		    jsonOut = gson.toJson(mdlAPIResult);
		    LogAdapter.InsertAPILog(apiSource, "FAILED", url, functionName, jsonIn, jsonOut);
		    logger.error("FAILED = API : " + url + ", method : " + apiMethod + ", jsonIn :" + jsonIn + ", jsonOut : " + jsonOut);
		    return gson.toJson(mdlAPIResult);
		} else {
		    pdfFileLocation = ClaimAdapter.ConvertJpegToPDF(imageLocationList, mdlUploadClaim, dateNow, baseDirectoryPath);
		}

		if (pdfFileLocation.equalsIgnoreCase("")) {
		    mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_27_009);
		    response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		    jsonOut = gson.toJson(mdlAPIResult);
		    LogAdapter.InsertAPILog(apiSource, "FAILED", url, functionName, jsonIn, jsonOut);
		    logger.error("FAILED = API : " + url + ", method : " + apiMethod + ", jsonIn :" + jsonIn + ", jsonOut : " + jsonOut);
		    return gson.toJson(mdlAPIResult);
		} else {
		    mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_00_000);
		    mdlClaimDocument.image_link = imageLocationList;
		    mdlClaimDocument.pdf_link = pdfFileLocation;
		    mdlAPIResult.output_schema = mdlClaimDocument;
		    response.setStatus(HttpServletResponse.SC_OK);
		    jsonOut = gson.toJson(mdlAPIResult);
		    LogAdapter.InsertAPILog(apiSource, "SUCCESS", url, functionName, jsonIn, jsonOut);
		    logger.info("SUCCESS. API : " + url + ", method : " + apiMethod + ", jsonIn :" + jsonIn + ", jsonOut : " + jsonOut);
		}

	    } else {
		mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_27_010);
		response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		jsonOut = gson.toJson(mdlAPIResult);
		LogAdapter.InsertAPILog(apiSource, "FAILED", url, functionName, jsonIn, jsonOut);
		logger.error("FAILED = API : " + url + ", method : " + apiMethod + ", jsonIn :" + jsonIn + ", jsonOut : " + jsonOut);
		return gson.toJson(mdlAPIResult);
	    }

	} catch (Exception ex) {
	    mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_99_999);
	    response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
	    jsonOut = gson.toJson(mdlAPIResult);
	    LogAdapter.InsertAPILog(apiSource, "FAILED", url, functionName, jsonIn, jsonOut, ex.toString());
	    logger.error("FAILED. API : " + url + ", method : " + apiMethod + ", jsonIn : " + jsonIn + ", jsonOut : " + jsonOut + ",Exception:" + ex.toString());
	}
	return gson.toJson(mdlAPIResult);
    }
    
    @RequestMapping(value = "/get-claim", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody model.mdlAPIObjectResult GetClaimDetails(@RequestHeader String Authorization, @RequestHeader String Key, @RequestHeader String Timestamp, @RequestHeader String Signature, @RequestParam(value = "id", defaultValue = "") String claimNo, HttpServletResponse response) {
	String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();
	model.mdlAPIObjectResult mdlAPIResult = new model.mdlAPIObjectResult();
	String jsonIn = "claimNo : " + claimNo;
	String jsonOut = "";
	String apiMethod = "GET";
	String url = baseURL + "/get-claim";

	try {
	    // check token, key and signature	    
	    model.mdlErrorSchema mdlAPIStatus = AuthorizationAdapter.CheckAuthorization(Authorization, Key, Timestamp, Signature, response, jsonIn, apiMethod, url, mdlAPIResult);
	    if (!mdlAPIStatus.error_code.equals("ERR-00-000")){
		mdlAPIResult.error_schema = mdlAPIStatus;
		return mdlAPIResult;
	    }

	    if (claimNo == null || claimNo.equalsIgnoreCase("")) {
		mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_29_001);
		response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		jsonOut = gson.toJson(mdlAPIResult);
		LogAdapter.InsertAPILog(apiSource, "FAILED", url, functionName, jsonIn, jsonOut);
		logger.error("FAILED = API : " + url + ", method : " + apiMethod + ", jsonIn :" + jsonIn + ", jsonOut : " + jsonOut);
		return mdlAPIResult;
	    }

	    model.mdlClaim mdlClaim = ClaimAdapter.GetClaimData(claimNo);

	    if (mdlClaim == null || mdlClaim.claim_no == null || mdlClaim.claim_no.equals("")) {
		mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_29_002);
		response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		jsonOut = gson.toJson(mdlAPIResult);
		LogAdapter.InsertAPILog(apiSource, "FAILED", url, functionName, jsonIn, jsonOut);
		logger.error("FAILED = API : " + url + ", method : " + apiMethod + ", jsonIn :" + jsonIn + ", jsonOut : " + jsonOut);
		return mdlAPIResult;
	    } else {
		mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_00_000);
		mdlAPIResult.output_schema = mdlClaim;
		response.setStatus(HttpServletResponse.SC_OK);
		jsonOut = gson.toJson(mdlAPIResult);
		LogAdapter.InsertAPILog(apiSource, "SUCCESS", url, functionName, jsonIn, jsonOut);
		logger.info("SUCCESS. API : " + url + ", method : " + apiMethod + ", jsonIn :" + jsonIn + ", jsonOut : " + jsonOut);
	    }
	} catch (Exception ex) {
	    mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_99_999);
	    response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
	    jsonOut = gson.toJson(mdlAPIResult);
	    LogAdapter.InsertAPILog(apiSource, "FAILED", url, functionName, jsonIn, jsonOut, ex.toString());
	    logger.error("FAILED. API : " + url + ", method : " + apiMethod + ", jsonIn : " + jsonIn + ", jsonOut : " + jsonOut + ",Exception:" + ex.toString());
	}
	return mdlAPIResult;
    }
    
    @RequestMapping(value = "/get-claim-count", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody model.mdlAPIArrayResult GetClaimCountData(@RequestHeader String Authorization, @RequestHeader String Key, @RequestHeader String Timestamp, @RequestHeader String Signature, HttpServletResponse response) {
	String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();
	model.mdlAPIArrayResult mdlAPIResult = new model.mdlAPIArrayResult();
	String jsonIn = "";
	String jsonOut = "";
	String apiMethod = "GET";
	String url = baseURL + "/get-claim-count";

	try {
	    // check token, key and signature	    
	    model.mdlErrorSchema mdlAPIStatus = AuthorizationAdapter.CheckAuthorizationArray(Authorization, Key, Timestamp, Signature, response, jsonIn, apiMethod, url, mdlAPIResult);
	    if (!mdlAPIStatus.error_code.equals("ERR-00-000")){
		mdlAPIResult.error_schema = mdlAPIStatus;
		return mdlAPIResult;
	    }

	    List<model.mdlClaimCount> claimCountList = ClaimAdapter.GetClaimCountData();

	    if (claimCountList == null || claimCountList.size() == 0 ) {
		mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_99_999);
		response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		jsonOut = gson.toJson(mdlAPIResult);
		LogAdapter.InsertAPILog(apiSource, "FAILED", url, functionName, jsonIn, jsonOut);
		logger.error("FAILED = API : " + url + ", method : " + apiMethod + ", jsonIn :" + jsonIn + ", jsonOut : " + jsonOut);
		return mdlAPIResult;
	    } else {
		mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_00_000);
		model.mdlArrayResult mdlOutputSchemaArray = new model.mdlArrayResult();
		mdlOutputSchemaArray.items = claimCountList;
		mdlAPIResult.output_schema = mdlOutputSchemaArray;
		response.setStatus(HttpServletResponse.SC_OK);
		jsonOut = gson.toJson(mdlAPIResult);
		LogAdapter.InsertAPILog(apiSource, "SUCCESS", url, functionName, jsonIn, jsonOut);
		logger.info("SUCCESS. API : " + url + ", method : " + apiMethod + ", jsonIn :" + jsonIn + ", jsonOut : " + jsonOut);
	    }
	} catch (Exception ex) {
	    mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_99_999);
	    response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
	    jsonOut = gson.toJson(mdlAPIResult);
	    LogAdapter.InsertAPILog(apiSource, "FAILED", url, functionName, jsonIn, jsonOut, ex.toString());
	    logger.error("FAILED. API : " + url + ", method : " + apiMethod + ", jsonIn : " + jsonIn + ", jsonOut : " + jsonOut + ",Exception:" + ex.toString());
	}
	return mdlAPIResult;
    }
}
