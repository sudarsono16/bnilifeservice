package controller;

import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;

import adapter.AuthorizationAdapter;
import adapter.ErrorAdapter;
import adapter.TermConditionAdapter;
import model.ErrorStatus;

@RestController
@EnableScheduling
public class TermConditionController {
    final static Logger logger = Logger.getLogger(controller.class);
    static Gson gson = new Gson();

    @RequestMapping(value = "/get-term-condition-list", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody String GetTermConditionList(@RequestHeader String Authorization, @RequestHeader String Key, @RequestHeader String Timestamp, @RequestHeader String Signature, HttpServletResponse response) {
	model.mdlAPIArrayResult mdlAPIResult = new model.mdlAPIArrayResult();
	String jsonIn = "";
	String jsonOut = "";
	String apiMethod = "GET";
	String url = "/get-term-condition-list";

	try {
	    // check token, key and signature	    
	    model.mdlErrorSchema mdlAPIStatus = AuthorizationAdapter.CheckAuthorizationArray(Authorization, Key, Timestamp, Signature, response, jsonIn, apiMethod, url, mdlAPIResult);
	    if (!mdlAPIStatus.error_code.equals("ERR-00-000")){
		mdlAPIResult.error_schema = mdlAPIStatus;
		return gson.toJson(mdlAPIResult);
	    }

	    List<model.mdlTermCondition> termConditionList = TermConditionAdapter.GetTermConditionList();
	    if (termConditionList == null || termConditionList.size() == 0) {
		mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_99_999);
		response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		jsonOut = gson.toJson(mdlAPIResult);
		logger.info("FAILED = API : " + url + ", method : " + apiMethod + ", jsonIn :" + jsonIn + ", jsonOut : " + jsonOut);
		return gson.toJson(mdlAPIResult);
	    } else {
		mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_00_000);
		model.mdlArrayResult arrayResult = new model.mdlArrayResult();
		arrayResult.items = termConditionList;
		mdlAPIResult.output_schema = arrayResult;
		response.setStatus(HttpServletResponse.SC_OK);
		jsonOut = gson.toJson(mdlAPIResult);
		logger.info("SUCCESS. API : " + url + ", method : " + apiMethod + ", jsonIn :" + jsonIn + ", jsonOut : " + jsonOut);
	    }
	} catch (Exception ex) {
	    mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_99_999);
	    response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
	    jsonOut = gson.toJson(mdlAPIResult);
	    logger.error("FAILED. API : " + url + ", method : " + apiMethod + ", jsonIn : " + jsonIn + ", jsonOut : " + jsonOut + ",Exception:" + ex.toString());
	}
	return gson.toJson(mdlAPIResult);
    }

    @RequestMapping(value = "/insert-update-term-condition", method = RequestMethod.POST, consumes = "application/json", headers = "Accept=application/json")
    public @ResponseBody String InsertUpdateTermCondition(@RequestHeader String Authorization, @RequestHeader String Key, @RequestHeader String Timestamp, @RequestHeader String Signature, @RequestBody model.mdlTermCondition mdlTermCondition, HttpServletResponse response) {
	String jsonIn = gson.toJson(mdlTermCondition);
	String jsonOut = "";
	String apiMethod = "POST";
	String url = "/insert-update-term-condition";
	model.mdlAPIObjectResult mdlAPIResult = new model.mdlAPIObjectResult();

	try {
	    // check token, key and signature	    
	    model.mdlErrorSchema mdlAPIStatus = AuthorizationAdapter.CheckAuthorization(Authorization, Key, Timestamp, Signature, response, jsonIn, apiMethod, url, mdlAPIResult);
	    if (!mdlAPIStatus.error_code.equals("ERR-00-000")){
		mdlAPIResult.error_schema = mdlAPIStatus;
		return gson.toJson(mdlAPIResult);
	    }

	    boolean isValid = true;
	    if (mdlTermCondition.type == null || mdlTermCondition.type.equals("")) {
		mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_99_999);
		isValid = false;
	    } else if (mdlTermCondition.indonesian_text == null || mdlTermCondition.indonesian_text.equals("")) {
		mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_99_999);
		isValid = false;
	    } else if (mdlTermCondition.english_text == null || mdlTermCondition.english_text.equals("")) {
		mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_99_999);
		isValid = false;
	    }

	    if (!isValid) {
		response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		jsonOut = gson.toJson(mdlAPIResult);
		logger.error("FAILED = API : " + url + ", method : " + apiMethod + ", jsonIn :" + jsonIn + ", jsonOut : " + jsonOut);
		return gson.toJson(mdlAPIResult);
	    }

	    boolean isSuccess = TermConditionAdapter.InsertUpdateTermCondition(mdlTermCondition);
	    if (isSuccess) {
		mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_00_000);
		response.setStatus(HttpServletResponse.SC_OK);
		jsonOut = gson.toJson(mdlAPIResult);
		logger.info("SUCCESS. API : " + url + ", method : " + apiMethod + ", jsonIn :" + jsonIn + ", jsonOut : " + jsonOut);
	    } else {
		mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_99_999);
		response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		jsonOut = gson.toJson(mdlAPIResult);
		logger.error("FAILED = API : " + url + ", method : " + apiMethod + ", jsonIn :" + jsonIn + ", jsonOut : " + jsonOut);
		return gson.toJson(mdlAPIResult);
	    }

	} catch (Exception ex) {
	    mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_99_999);
	    response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
	    jsonOut = gson.toJson(mdlAPIResult);
	    logger.error("FAILED. API : " + url + ", method : " + apiMethod + ", jsonIn : " + jsonIn + ", jsonOut : " + jsonOut + ",Exception:" + ex.toString());
	}
	return gson.toJson(mdlAPIResult);
    }

    @RequestMapping(value = "/delete-term-condition", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody String DeleteTermCondition(@RequestHeader String Authorization, @RequestHeader String Key, @RequestHeader String Timestamp, @RequestHeader String Signature, @RequestParam(value = "id", defaultValue = "") String id, HttpServletResponse response) {
	String jsonIn = id;
	String jsonOut = "";
	String apiMethod = "GET";
	String url = "/delete-term-condition";
	model.mdlAPIObjectResult mdlAPIResult = new model.mdlAPIObjectResult();

	try {
	    // check token, key and signature	    
	    model.mdlErrorSchema mdlAPIStatus = AuthorizationAdapter.CheckAuthorization(Authorization, Key, Timestamp, Signature, response, jsonIn, apiMethod, url, mdlAPIResult);
	    if (!mdlAPIStatus.error_code.equals("ERR-00-000")){
		mdlAPIResult.error_schema = mdlAPIStatus;
		return gson.toJson(mdlAPIResult);
	    }

	    if (id == null || id.equals("")) {
		mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_99_999);
		response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		jsonOut = gson.toJson(mdlAPIResult);
		logger.error("FAILED = API : " + url + ", method : " + apiMethod + ", jsonIn :" + jsonIn + ", jsonOut : " + jsonOut);
		return gson.toJson(mdlAPIResult);
	    }

	    boolean success = TermConditionAdapter.DeleteTermCondition(id);
	    if (!success) {
		mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_99_999);
		response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		jsonOut = gson.toJson(mdlAPIResult);
		logger.error("FAILED = API : " + url + ", method : " + apiMethod + ", jsonIn :" + jsonIn + ", jsonOut : " + jsonOut);
		return gson.toJson(mdlAPIResult);
	    } else {
		mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_00_000);
		response.setStatus(HttpServletResponse.SC_OK);
		jsonOut = gson.toJson(mdlAPIResult);
		logger.info("SUCCESS. API : " + url + ", method : " + apiMethod + ", jsonIn :" + jsonIn + ", jsonOut : " + jsonOut);
	    }
	} catch (Exception ex) {
	    mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_99_999);
	    response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
	    jsonOut = gson.toJson(mdlAPIResult);
	    logger.error("FAILED. API : " + url + ", method : " + apiMethod + ", jsonIn : " + jsonIn + ", jsonOut : " + jsonOut + ",Exception:" + ex.toString());
	}
	return gson.toJson(mdlAPIResult);
    }
}
