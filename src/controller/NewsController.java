package controller;

import java.util.List;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.FilenameUtils;
import org.apache.log4j.Logger;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.google.gson.Gson;

import adapter.AuthorizationAdapter;
import adapter.ErrorAdapter;
import adapter.NewsAdapter;
import helper.DateHelper;
import model.ErrorStatus;

@RestController
@RequestMapping("/news")
public class NewsController {
    String baseURL = "/news";
    final static Logger logger = Logger.getLogger(NewsController.class);
    static Gson gson = new Gson();

    @RequestMapping(value = "/get-news", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody String GetNews(@RequestHeader String Authorization, @RequestHeader String Key, @RequestHeader String Timestamp, @RequestHeader String Signature, 
	    @RequestParam(value = "language", defaultValue = "id") String language, @RequestParam(value = "id", required = false) String id, 
	    @RequestParam(value = "type", defaultValue = "OGH") String news_type, HttpServletResponse response) {
	model.mdlAPIArrayResult mdlAPIResult = new model.mdlAPIArrayResult();
	String jsonIn = "";
	String jsonOut = "";
	String apiMethod = "GET";
	String url = baseURL + "/get-news";

	try {
	    // check token, key and signature	    
	    model.mdlErrorSchema mdlAPIStatus = AuthorizationAdapter.CheckAuthorizationArray(Authorization, Key, Timestamp, Signature, response, jsonIn, apiMethod, url, mdlAPIResult);
	    if (!mdlAPIStatus.error_code.equals("ERR-00-000")){
		mdlAPIResult.error_schema = mdlAPIStatus;
		return gson.toJson(mdlAPIResult);
	    }

	    List<model.mdlNews> helpList = NewsAdapter.GetNewsList(language, id, news_type);
	    if (helpList == null || helpList.size() == 0) {
		mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_14_001);
		response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		jsonOut = gson.toJson(mdlAPIResult);
		logger.info("FAILED = API : " + url + ", method : " + apiMethod + ", jsonIn :" + jsonIn + ", jsonOut : " + jsonOut);
		return gson.toJson(mdlAPIResult);
	    } else {
		mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_00_000);
		model.mdlArrayResult arrayResult = new model.mdlArrayResult();
		arrayResult.items = helpList;
		mdlAPIResult.output_schema = arrayResult;
		response.setStatus(HttpServletResponse.SC_OK);
		jsonOut = gson.toJson(mdlAPIResult);
		logger.info("SUCCESS. API : " + url + ", method : " + apiMethod + ", jsonIn :" + jsonIn + ", jsonOut : " + jsonOut);
	    }
	} catch (Exception ex) {
	    mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_99_999);
	    response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
	    jsonOut = gson.toJson(mdlAPIResult);
	    logger.error("FAILED. API : " + url + ", method : " + apiMethod + ", jsonIn : " + jsonIn + ", jsonOut : " + jsonOut + ",Exception:" + ex.toString());
	}
	return gson.toJson(mdlAPIResult);
    }

    @RequestMapping(value = "/insert-update-news", method = RequestMethod.POST, consumes = "multipart/form-data")
    public @ResponseBody String InsertUpdateNews(@RequestHeader String Authorization, @RequestHeader String Key, @RequestHeader String Timestamp, @RequestHeader String Signature, @RequestParam(name = "file", required = false) final MultipartFile image, @RequestParam("json") final String json, HttpServletResponse response) {
	// String jsonIn = gson.toJson(mdlNews);
	model.mdlNews mdlNews = new Gson().fromJson(json, model.mdlNews.class);
	String jsonIn = gson.toJson(mdlNews);
	String jsonOut = "";
	String apiMethod = "POST";
	String url = baseURL + "/insert-update-news";
	model.mdlAPIObjectResult mdlAPIResult = new model.mdlAPIObjectResult();

	try {
	    // check token, key and signature	    
	    model.mdlErrorSchema mdlAPIStatus = AuthorizationAdapter.CheckAuthorization(Authorization, Key, Timestamp, Signature, response, jsonIn, apiMethod, url, mdlAPIResult);
	    if (!mdlAPIStatus.error_code.equals("ERR-00-000")){
		mdlAPIResult.error_schema = mdlAPIStatus;
		return gson.toJson(mdlAPIResult);
	    }

	    boolean isValid = true;
	    if (mdlNews.news_title_id == null || mdlNews.news_title_id.equals("") || mdlNews.news_title_en == null || mdlNews.news_title_en.equals("")) {
		mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_22_002); // news title tidak valid
		isValid = false;
	    } else if (mdlNews.news_content_id == null || mdlNews.news_content_id.equals("") || mdlNews.news_content_en == null || mdlNews.news_content_en.equals("")) {
		mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_22_004); // news content tidak valid
		isValid = false;
	    }
	    // else if (mdlNews.news_link == null || mdlNews.news_link.equals("")) {
	    // mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_22_005); // news link tidak valid
	    // isValid = false;
	    // }

	    if (image != null) {
		String fileName = image.getOriginalFilename();
		String ext = FilenameUtils.getExtension(fileName);
		if (!ext.isEmpty() && (ext.equals("jpg") || ext.equals("jpeg"))) {
		    try {
			StringBuilder sb = new StringBuilder();

			String paramFilePath = "C://Program Files/Apache Software Foundation/Tomcat 8.0/webapps/Images/bnilife";
			String directory = sb.append(paramFilePath).append("/News/").toString();
			String news_time_string = "NEWS_" + DateHelper.GetDateTimeNowCustomFormat("yyyyMMddHHmmssSSS");
			String uploadedFileLocation = sb.append("/").append(news_time_string).append(".").append(ext).toString();

			helper.FileHelper.writeToFile(image.getInputStream(), directory, uploadedFileLocation);
			Context context = (Context) new InitialContext().lookup("java:comp/env");
			String urlWebServer = (String) context.lookup("url_web_server_for_upload");
			mdlNews.news_image = new StringBuilder().append(urlWebServer).append("bnilife/news/").append(news_time_string).append(".").append("jpg").toString();

		    } catch (Exception e) {
			isValid = false;
		    }
		} else {
		    isValid = false;
		}
	    }

	    if (!isValid) {
		response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		jsonOut = gson.toJson(mdlAPIResult);
		logger.error("FAILED = API : " + url + ", method : " + apiMethod + ", jsonIn :" + jsonIn + ", jsonOut : " + jsonOut);
		return gson.toJson(mdlAPIResult);
	    }

	    boolean isSuccess = NewsAdapter.InsertUpdateNews(mdlNews);
	    if (isSuccess) {
		mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_00_000);
		response.setStatus(HttpServletResponse.SC_OK);
		jsonOut = gson.toJson(mdlAPIResult);
		logger.info("SUCCESS. API : " + url + ", method : " + apiMethod + ", jsonIn :" + jsonIn + ", jsonOut : " + jsonOut);
	    } else {
		mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_03_010); // Register gagal.
		response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		jsonOut = gson.toJson(mdlAPIResult);
		logger.error("FAILED = API : " + url + ", method : " + apiMethod + ", jsonIn :" + jsonIn + ", jsonOut : " + jsonOut);
		return gson.toJson(mdlAPIResult);
	    }

	} catch (Exception ex) {
	    mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_99_999);
	    response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
	    jsonOut = gson.toJson(mdlAPIResult);
	    logger.error("FAILED. API : " + url + ", method : " + apiMethod + ", jsonIn : " + jsonIn + ", jsonOut : " + jsonOut + ",Exception:" + ex.toString());
	}
	return gson.toJson(mdlAPIResult);
    }

    @RequestMapping(value = "/delete-news", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody String DeleteHelp(@RequestHeader String Authorization, @RequestHeader String Key, @RequestHeader String Timestamp, @RequestHeader String Signature, @RequestParam(value = "id", defaultValue = "") String id, HttpServletResponse response) {
	String jsonIn = id;
	String jsonOut = "";
	String apiMethod = "GET";
	String url = baseURL + "/delete-news";
	model.mdlAPIObjectResult mdlAPIResult = new model.mdlAPIObjectResult();

	try {
	    // check token, key and signature	    
	    model.mdlErrorSchema mdlAPIStatus = AuthorizationAdapter.CheckAuthorization(Authorization, Key, Timestamp, Signature, response, jsonIn, apiMethod, url, mdlAPIResult);
	    if (!mdlAPIStatus.error_code.equals("ERR-00-000")){
		mdlAPIResult.error_schema = mdlAPIStatus;
		return gson.toJson(mdlAPIResult);
	    }

	    if (id == null || id.equals("")) {
		mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_34_001);
		response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		jsonOut = gson.toJson(mdlAPIResult);
		logger.error("FAILED = API : " + url + ", method : " + apiMethod + ", jsonIn :" + jsonIn + ", jsonOut : " + jsonOut);
		return gson.toJson(mdlAPIResult);
	    }

	    boolean success = NewsAdapter.DeleteNews(id);
	    if (!success) {
		mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_34_002);
		response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		jsonOut = gson.toJson(mdlAPIResult);
		logger.error("FAILED = API : " + url + ", method : " + apiMethod + ", jsonIn :" + jsonIn + ", jsonOut : " + jsonOut);
		return gson.toJson(mdlAPIResult);
	    } else {
		mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_00_000);
		response.setStatus(HttpServletResponse.SC_OK);
		jsonOut = gson.toJson(mdlAPIResult);
		logger.info("SUCCESS. API : " + url + ", method : " + apiMethod + ", jsonIn :" + jsonIn + ", jsonOut : " + jsonOut);
	    }
	} catch (Exception ex) {
	    mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_99_999);
	    response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
	    jsonOut = gson.toJson(mdlAPIResult);
	    logger.error("FAILED. API : " + url + ", method : " + apiMethod + ", jsonIn : " + jsonIn + ", jsonOut : " + jsonOut + ",Exception:" + ex.toString());
	}
	return gson.toJson(mdlAPIResult);
    }
}
