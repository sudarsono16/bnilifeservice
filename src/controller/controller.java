package controller;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.io.FilenameUtils;
import org.apache.log4j.Logger;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import controller.controller;
import database.QueryExecuteAdapter;
import helper.DateHelper;

import com.google.gson.Gson;

import adapter.APIAdapter;
import adapter.AuthorizationAdapter;
import adapter.Base64Adapter;
import adapter.ClientIdAdapter;
import adapter.EncryptAdapter;
import adapter.UtilityAdapter;
import adapter.ErrorAdapter;
import adapter.HomeAdapter;
import adapter.NewsAdapter;
import adapter.TermConditionAdapter;
import model.ErrorStatus;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.servlet.http.HttpServletResponse;
import javax.sql.rowset.CachedRowSet;

@RestController
@EnableScheduling
public class controller {
    final static Logger logger = Logger.getLogger(controller.class);
    static Gson gson = new Gson();

    @RequestMapping(value = "/ping", method = RequestMethod.POST, consumes = "application/json", headers = "Accept=application/json")
    public @ResponseBody void GetPing() {
	return;
    }

    // get token
    @RequestMapping(value = "/get-token", method = RequestMethod.POST, consumes = "application/x-www-form-urlencoded")
    public @ResponseBody model.mdlAPIObjectResult GetToken(@RequestHeader("Authorization") String authorization, @RequestHeader("Content-Type") String contentType, @RequestParam(value = "grant_type", defaultValue = "") String grantType, HttpServletResponse response) {
	model.mdlAPIObjectResult mdlAPIResult = new model.mdlAPIObjectResult();
	model.mdlToken mdlToken = new model.mdlToken();
	String authString = authorization.replace("Basic ", "");
	List<String> clientData = AuthorizationAdapter.decryptAuthorizationKey(authString);
	if (clientData.size() != 2) {
	    mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_99_001);
	    response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
	    return mdlAPIResult;
	}

	List<String> appData = ClientIdAdapter.CheckClientID(clientData.get(0), clientData.get(1));
	if (appData.size() != 2) {
	    mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_99_001);
	    response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
	    return mdlAPIResult;
	} else if (appData.get(0).equals("")) {
	    mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_99_002);
	    response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
	    return mdlAPIResult;
	}
	String appName = appData.get(0);
	String apiKey = appData.get(1);

	try {
	    if (!grantType.equals("client_credentials")) {
		mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_99_003);
		response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		return mdlAPIResult;
	    } else {
		Date now = new Date();
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
		String startDate = df.format(now);
		String expiresIn = "3600"; // in second
		String token = authString + ";" + startDate + ";" + expiresIn;
		String encrytedToken = EncryptAdapter.encrypt(token, apiKey);
		String hashedToken = Base64Adapter.EncryptBase64(encrytedToken);

		mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_00_000);
		mdlToken.token_type = "Bearer";
		mdlToken.grant_type = grantType;
		mdlToken.expires_in = expiresIn;
		mdlToken.access_token = hashedToken;
		mdlToken.scope = "resource.WRITE resource.READ";
		mdlToken.api_key = apiKey;
		mdlAPIResult.output_schema = mdlToken;
		response.setStatus(HttpServletResponse.SC_OK);
	    }
	    // String jsonOut = gson.toJson(mdlAPIResult);
	    // logger.info("SUCCESS. API : GetToken, method: POST, AppName : " + appName + ", authorization: " + authorization + ", grant type:"
	    // + grantType + ", jsonOut : " + jsonOut);
	} catch (Exception ex) {
	    mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_99_999);
	    response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
	    logger.error("FAILED. API : GetToken, method: POST, AppName : " + appName + ", authorization: " + authorization + ", grant type:" + grantType + "Exception:" + ex.toString());
	}
	return mdlAPIResult;
    }

    @RequestMapping(value = "/get-slideshow", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody model.mdlAPIArrayResult GetSlideshow(@RequestHeader String Authorization, @RequestHeader String Key, @RequestHeader String Timestamp, @RequestHeader String Signature, HttpServletResponse response) {
	model.mdlAPIArrayResult mdlAPIResult = new model.mdlAPIArrayResult();
	String jsonIn = "";
	String jsonOut = "";
	String apiMethod = "GET";
	String url = "/get-slideshow";

	try {
	    // check token, key and signature
	    model.mdlErrorSchema mdlAPIStatus = AuthorizationAdapter.CheckAuthorization(Authorization, Key, url, Timestamp, Signature, apiMethod);
	    if (mdlAPIStatus == null) {
		mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_99_999);
		response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		jsonOut = gson.toJson(mdlAPIResult);
		logger.info("FAILED = API : " + url + ", method : " + apiMethod + ", jsonIn :" + jsonIn + ", jsonOut : " + jsonOut);
		return mdlAPIResult;
	    } else if (!mdlAPIStatus.error_code.equalsIgnoreCase("ERR-00-000")) {
		mdlAPIResult.error_schema = mdlAPIStatus;
		if (mdlAPIStatus.error_code.equalsIgnoreCase("ERR-99-005") || mdlAPIStatus.error_code.equalsIgnoreCase("ERR-99-006")) {
		    // if token invalid or token expired
		    response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
		} else {
		    response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		}
		jsonOut = gson.toJson(mdlAPIStatus);
		logger.info("FAILED = API : " + url + ", method : " + apiMethod + ", jsonIn :" + jsonIn + ", jsonOut : " + jsonOut);
		return mdlAPIResult;
	    }

	    List<model.mdlSlideshow> slideshowList = HomeAdapter.GetSlideshowData();
	    if (slideshowList == null) {
		mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_02_001);
		response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
	    } else {
		mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_00_000);
		mdlAPIResult.output_schema.items = slideshowList; // model slideshow
		response.setStatus(HttpServletResponse.SC_OK);
		jsonOut = gson.toJson(mdlAPIResult);
		logger.info("SUCCESS. API : " + url + ", method : " + apiMethod + ", jsonIn :" + jsonIn + ", jsonOut : " + jsonOut);
	    }
	} catch (Exception ex) {
	    mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_99_999);
	    response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
	    jsonOut = gson.toJson(mdlAPIResult);
	    logger.error("FAILED. API : " + url + ", method : " + apiMethod + ", jsonIn : " + jsonIn + ", jsonOut : " + jsonOut + ",Exception:" + ex.toString());
	}
	return mdlAPIResult;
    }

    @RequestMapping(value = "/term-condition", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody model.mdlAPIObjectResult GetTermAndConditions(@RequestHeader String Authorization, @RequestHeader String Key, @RequestHeader String Timestamp, @RequestHeader String Signature, @RequestParam(value = "type", defaultValue = "") String type, HttpServletResponse response) {
	String jsonIn = gson.toJson(type);
	String jsonOut = "";
	String apiMethod = "GET";
	String url = "/term-condition";
	model.mdlAPIObjectResult mdlAPIResult = new model.mdlAPIObjectResult();

	try {
	    // check token, key and signature
	    model.mdlErrorSchema mdlAPIStatus = AuthorizationAdapter.CheckAuthorization(Authorization, Key, url, Timestamp, Signature, apiMethod);
	    if (mdlAPIStatus == null) {
		mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_99_999);
		response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		jsonOut = gson.toJson(mdlAPIResult);
		logger.info("FAILED = API : " + url + ", method : " + apiMethod + ", jsonIn :" + jsonIn + ", jsonOut : " + jsonOut);
		return mdlAPIResult;
	    } else if (!mdlAPIStatus.error_code.equalsIgnoreCase("ERR-00-000")) {
		mdlAPIResult.error_schema = mdlAPIStatus;
		if (mdlAPIStatus.error_code.equalsIgnoreCase("ERR-99-005") || mdlAPIStatus.error_code.equalsIgnoreCase("ERR-99-006")) {
		    // if token invalid or token expired
		    response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
		} else {
		    response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		}
		jsonOut = gson.toJson(mdlAPIStatus);
		logger.info("FAILED = API : " + url + ", method : " + apiMethod + ", jsonIn :" + jsonIn + ", jsonOut : " + jsonOut);
		return mdlAPIResult;
	    }

	    if (type == null || type.equals("")) { // term type is not valid
		mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_10_001);
		response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		jsonOut = gson.toJson(mdlAPIResult);
		logger.info("FAILED = API : " + url + ", method : " + apiMethod + ", jsonIn :" + jsonIn + ", jsonOut : " + jsonOut);
		return mdlAPIResult;
	    }

	    // check if term condition type content is exists
	    model.mdlTermCondition mdlTermCondition = TermConditionAdapter.GetTermAndCondition(type);
	    if (mdlTermCondition == null || mdlTermCondition.english_text == null || mdlTermCondition.english_text.equals("")) {
		mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_10_002);
		response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		jsonOut = gson.toJson(mdlAPIResult);
		logger.info("FAILED = API : " + url + ", method : " + apiMethod + ", jsonIn :" + jsonIn + ", jsonOut : " + jsonOut);
		return mdlAPIResult;
	    } else {
		mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_00_000);
		mdlAPIResult.output_schema = mdlTermCondition;
		response.setStatus(HttpServletResponse.SC_OK);
		jsonOut = gson.toJson(mdlAPIResult);
		logger.info("SUCCESS. API : " + url + ", method : " + apiMethod + ", jsonIn :" + jsonIn + ", jsonOut : " + jsonOut);
	    }

	} catch (Exception ex) {
	    mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_99_999);
	    response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
	    jsonOut = gson.toJson(mdlAPIResult);
	    logger.error("FAILED. API : " + url + ", method : " + apiMethod + ", jsonIn : " + jsonIn + ", jsonOut : " + jsonOut + ",Exception:" + ex.toString());
	}
	return mdlAPIResult;
    }


    @RequestMapping(value = "/check-version", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody model.mdlAPIObjectResult GetAPKVersion(@RequestHeader String Authorization, @RequestHeader String Key, @RequestHeader String Timestamp, @RequestHeader String Signature, HttpServletResponse response) {
	String jsonIn = "version";
	String jsonOut = "";
	String apiMethod = "GET";
	String url = "/check-version";
	model.mdlAPIObjectResult mdlAPIResult = new model.mdlAPIObjectResult();

	try {
	    // check token, key and signature
	    model.mdlErrorSchema mdlAPIStatus = AuthorizationAdapter.CheckAuthorization(Authorization, Key, url, Timestamp, Signature, apiMethod);
	    if (mdlAPIStatus == null) {
		mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_99_999);
		response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		jsonOut = gson.toJson(mdlAPIResult);
		logger.info("FAILED = API : " + url + ", method : " + apiMethod + ", jsonIn :" + jsonIn + ", jsonOut : " + jsonOut);
		return mdlAPIResult;
	    } else if (!mdlAPIStatus.error_code.equalsIgnoreCase("ERR-00-000")) {
		mdlAPIResult.error_schema = mdlAPIStatus;
		if (mdlAPIStatus.error_code.equalsIgnoreCase("ERR-99-005") || mdlAPIStatus.error_code.equalsIgnoreCase("ERR-99-006")) {
		    // if token invalid or token expired
		    response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
		} else {
		    response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		}
		jsonOut = gson.toJson(mdlAPIStatus);
		logger.info("FAILED = API : " + url + ", method : " + apiMethod + ", jsonIn :" + jsonIn + ", jsonOut : " + jsonOut);
		return mdlAPIResult;
	    }

	    // check if term condition type content is exists
	    model.mdlAPKVersion mdlAPKVersion = UtilityAdapter.GetAPKVersion();
	    if (mdlAPKVersion == null || mdlAPKVersion.version == null || mdlAPKVersion.version.equals("")) {
		mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_11_001);
		response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		jsonOut = gson.toJson(mdlAPIResult);
		logger.info("FAILED = API : " + url + ", method : " + apiMethod + ", jsonIn :" + jsonIn + ", jsonOut : " + jsonOut);
		return mdlAPIResult;
	    } else {
		mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_00_000);
		mdlAPIResult.output_schema = mdlAPKVersion;
		response.setStatus(HttpServletResponse.SC_OK);
		jsonOut = gson.toJson(mdlAPIResult);
		logger.info("SUCCESS. API : " + url + ", method : " + apiMethod + ", jsonIn :" + jsonIn + ", jsonOut : " + jsonOut);
	    }

	} catch (Exception ex) {
	    mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_99_999);
	    response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
	    jsonOut = gson.toJson(mdlAPIResult);
	    logger.error("FAILED. API : " + url + ", method : " + apiMethod + ", jsonIn : " + jsonIn + ", jsonOut : " + jsonOut + ",Exception:" + ex.toString());
	}

	return mdlAPIResult;
    }

    @RequestMapping(value = "/get-menu-member", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody model.mdlAPIArrayResult GetMenuMember(@RequestHeader String Authorization, @RequestHeader String Key, @RequestHeader String Timestamp, @RequestHeader String Signature, @RequestParam(value = "type", defaultValue = "") String type, HttpServletResponse response) {
	String jsonIn = type;
	String jsonOut = "";
	String apiMethod = "GET";
	String url = "/get-menu-member";
	model.mdlAPIArrayResult mdlAPIResult = new model.mdlAPIArrayResult();

	try {
	    // check token, key and signature
	    model.mdlErrorSchema mdlAPIStatus = AuthorizationAdapter.CheckAuthorization(Authorization, Key, url, Timestamp, Signature, apiMethod);
	    if (mdlAPIStatus == null) {
		mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_99_999);
		response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		jsonOut = gson.toJson(mdlAPIResult);
		logger.info("FAILED = API : " + url + ", method : " + apiMethod + ", jsonIn :" + jsonIn + ", jsonOut : " + jsonOut);
		return mdlAPIResult;
	    } else if (!mdlAPIStatus.error_code.equalsIgnoreCase("ERR-00-000")) {
		mdlAPIResult.error_schema = mdlAPIStatus;
		if (mdlAPIStatus.error_code.equalsIgnoreCase("ERR-99-005") || mdlAPIStatus.error_code.equalsIgnoreCase("ERR-99-006")) {
		    // if token invalid or token expired
		    response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
		} else {
		    response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		}
		jsonOut = gson.toJson(mdlAPIStatus);
		logger.info("FAILED = API : " + url + ", method : " + apiMethod + ", jsonIn :" + jsonIn + ", jsonOut : " + jsonOut);
		return mdlAPIResult;
	    }

	    if (type == null || type.equals("")) { // term type is not valid
		mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_12_001);
		response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		jsonOut = gson.toJson(mdlAPIResult);
		logger.info("FAILED = API : " + url + ", method : " + apiMethod + ", jsonIn :" + jsonIn + ", jsonOut : " + jsonOut);
		return mdlAPIResult;
	    }
	    // check if type contains OGH for "OGH NON BNI" and "OGH BNI"
	    if (type.contains("OGH")){
		type = "OGH";
	    }else if (type.contains("OGS")){
		type = "OGS";
	    }

	    // check if term condition type content is exists
	    List<model.mdlMenuMember> menuMemberList = UtilityAdapter.GetMenuMemberList(type);
	    if (menuMemberList == null || menuMemberList.size() == 0) {
		mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_12_002);
		response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		jsonOut = gson.toJson(mdlAPIResult);
		logger.info("FAILED = API : " + url + ", method : " + apiMethod + ", jsonIn :" + jsonIn + ", jsonOut : " + jsonOut);
		return mdlAPIResult;
	    } else {
		mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_00_000);
		model.mdlArrayResult arrayResult = new model.mdlArrayResult();
		arrayResult.items = menuMemberList;
		mdlAPIResult.output_schema = arrayResult;
		response.setStatus(HttpServletResponse.SC_OK);
		jsonOut = gson.toJson(mdlAPIResult);
		logger.info("SUCCESS. API : " + url + ", method : " + apiMethod + ", jsonIn :" + jsonIn + ", jsonOut : " + jsonOut);
	    }
	} catch (Exception ex) {
	    mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_99_999);
	    response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
	    jsonOut = gson.toJson(mdlAPIResult);
	    logger.error("FAILED. API : " + url + ", method : " + apiMethod + ", jsonIn : " + jsonIn + ", jsonOut : " + jsonOut + ",Exception:" + ex.toString());
	}

	return mdlAPIResult;
    }
    
    @RequestMapping(value = "/get-menu-member-list", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody String GetMenuMemberList(@RequestHeader String Authorization, @RequestHeader String Key, @RequestHeader String Timestamp, @RequestHeader String Signature, @RequestParam(value = "pageNumber", defaultValue = "1") String pageNumber, @RequestParam(value = "pageSize", defaultValue = "10") String pageSize, @RequestParam(value = "draw", defaultValue = "0") String draw, @RequestParam(value = "search", required = false) String search, HttpServletResponse response) {
	model.mdlAPIArrayResult mdlAPIResult = new model.mdlAPIArrayResult();
	String jsonIn = "";
	String jsonOut = "";
	String apiMethod = "GET";
	String url = "/get-menu-member-list";

	try {
	    // check token, key and signature	    
	    model.mdlErrorSchema mdlAPIStatus = AuthorizationAdapter.CheckAuthorizationArray(Authorization, Key, Timestamp, Signature, response, jsonIn, apiMethod, url, mdlAPIResult);
	    if (!mdlAPIStatus.error_code.equals("ERR-00-000")){
		mdlAPIResult.error_schema = mdlAPIStatus;
		return gson.toJson(mdlAPIResult);
	    }

	    List<model.mdlMenuMember> menuMemberList = UtilityAdapter.GetMenuMember(pageNumber, pageSize, search);
	    int menuMemberListTotal = UtilityAdapter.GetMenuMemberTotalList(search);
	    
	    if (menuMemberList == null || menuMemberList.size() == 0) {
			mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_99_999);
			model.mdlArrayResult arrayResult = new model.mdlArrayResult();
			arrayResult.recordsTotal = 0;
			arrayResult.recordsFiltered = 0;
			arrayResult.draw = Integer.parseInt(draw) + 1;
			arrayResult.data = new ArrayList<model.mdlRating>() ;
			mdlAPIResult.output_schema = arrayResult;
			
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			jsonOut = gson.toJson(mdlAPIResult);
			logger.info("FAILED = API : " + url + ", method : " + apiMethod + ", jsonIn :" + jsonIn + ", jsonOut : " + jsonOut);
			return gson.toJson(mdlAPIResult);
	    } else {
			mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_00_000);
			model.mdlArrayResult arrayResult = new model.mdlArrayResult();
			arrayResult.recordsTotal = menuMemberListTotal;
			arrayResult.recordsFiltered = menuMemberListTotal;
			arrayResult.draw = Integer.parseInt(draw) + 1;
			arrayResult.data = menuMemberList;
			mdlAPIResult.output_schema = arrayResult;
			response.setStatus(HttpServletResponse.SC_OK);
			jsonOut = gson.toJson(mdlAPIResult);
			logger.info("SUCCESS. API : " + url + ", method : " + apiMethod + ", jsonIn :" + jsonIn + ", jsonOut : " + jsonOut);
	    }
	} catch (Exception ex) {
	    mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_99_999);
	    response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
	    jsonOut = gson.toJson(mdlAPIResult);
	    logger.error("FAILED. API : " + url + ", method : " + apiMethod + ", jsonIn : " + jsonIn + ", jsonOut : " + jsonOut + ",Exception:" + ex.toString());
	}
	return gson.toJson(mdlAPIResult);
    }
    
    @RequestMapping(value = "/update-menu-member", method = RequestMethod.POST, consumes = "application/json", headers = "Accept=application/json")
    public @ResponseBody String UpdateMenuMember(@RequestHeader String Authorization, @RequestHeader String Key, @RequestHeader String Timestamp, 
	    @RequestHeader String Signature, @RequestBody model.mdlMenuMember mdlMenuMember, HttpServletResponse response) {
	String jsonIn = gson.toJson(mdlMenuMember);
	String jsonOut = "";
	String apiMethod = "POST";
	String url = "/update-menu-member";
	model.mdlAPIObjectResult mdlAPIResult = new model.mdlAPIObjectResult();

	try {
	 // check token, key and signature	    
	    model.mdlErrorSchema mdlAPIStatus = AuthorizationAdapter.CheckAuthorization(Authorization, Key, Timestamp, Signature, response, jsonIn, apiMethod, url, mdlAPIResult);
	    if (!mdlAPIStatus.error_code.equals("ERR-00-000")){
		mdlAPIResult.error_schema = mdlAPIStatus;
		return gson.toJson(mdlAPIResult);
	    }

	    boolean isValid = true;
	    if (mdlMenuMember.menu_member_id == null || mdlMenuMember.menu_member_id.equals("")) {
		mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_99_999);
		isValid = false;
	    } else if (mdlMenuMember.show == null) {
		mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_99_999);
		isValid = false;
	    }

	    if (!isValid) {
		response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		jsonOut = gson.toJson(mdlAPIResult);
		logger.error("FAILED = API : " + url + ", method : " + apiMethod + ", jsonIn :" + jsonIn + ", jsonOut : " + jsonOut);
		return gson.toJson(mdlAPIResult);
	    }

	    boolean isSuccess = UtilityAdapter.UpdateMenuMember(mdlMenuMember);
	    if (isSuccess) {
		mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_00_000);
		response.setStatus(HttpServletResponse.SC_OK);
		jsonOut = gson.toJson(mdlAPIResult);
		logger.info("SUCCESS. API : " + url + ", method : " + apiMethod + ", jsonIn :" + jsonIn + ", jsonOut : " + jsonOut);
	    } else {
		mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_33_004); // Register gagal.
		response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		jsonOut = gson.toJson(mdlAPIResult);
		logger.error("FAILED = API : " + url + ", method : " + apiMethod + ", jsonIn :" + jsonIn + ", jsonOut : " + jsonOut);
		return gson.toJson(mdlAPIResult);
	    }

	} catch (Exception ex) {
	    mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_99_999);
	    response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
	    jsonOut = gson.toJson(mdlAPIResult);
	    logger.error("FAILED. API : " + url + ", method : " + apiMethod + ", jsonIn : " + jsonIn + ", jsonOut : " + jsonOut + ",Exception:" + ex.toString());
	}
	return gson.toJson(mdlAPIResult);
    }

    @RequestMapping(value = "/get-mobile-config", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody model.mdlAPIArrayResult GetMobileConfig(@RequestHeader String Authorization, @RequestHeader String Key, @RequestHeader String Timestamp, @RequestHeader String Signature, HttpServletResponse response) {
	String jsonIn = "";
	String jsonOut = "";
	String apiMethod = "GET";
	String url = "/get-mobile-config";
	model.mdlAPIArrayResult mdlAPIResult = new model.mdlAPIArrayResult();

	try {
	    // check token, key and signature
	    model.mdlErrorSchema mdlAPIStatus = AuthorizationAdapter.CheckAuthorization(Authorization, Key, url, Timestamp, Signature, apiMethod);
	    if (mdlAPIStatus == null) {
		mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_99_999);
		response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		jsonOut = gson.toJson(mdlAPIResult);
		logger.info("FAILED = API : " + url + ", method : " + apiMethod + ", jsonIn :" + jsonIn + ", jsonOut : " + jsonOut);
		return mdlAPIResult;
	    } else if (!mdlAPIStatus.error_code.equalsIgnoreCase("ERR-00-000")) {
		mdlAPIResult.error_schema = mdlAPIStatus;
		if (mdlAPIStatus.error_code.equalsIgnoreCase("ERR-99-005") || mdlAPIStatus.error_code.equalsIgnoreCase("ERR-99-006")) {
		    // if token invalid or token expired
		    response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
		} else {
		    response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		}
		jsonOut = gson.toJson(mdlAPIStatus);
		logger.info("FAILED = API : " + url + ", method : " + apiMethod + ", jsonIn :" + jsonIn + ", jsonOut : " + jsonOut);
		return mdlAPIResult;
	    }

	    // check if term condition type content is exists
	    List<model.mdlMobileConfig> mobileConfigList = UtilityAdapter.GetMobileConfigList();
	    if (mobileConfigList == null || mobileConfigList.size() == 0) {
		mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_36_001);
		response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		jsonOut = gson.toJson(mdlAPIResult);
		logger.info("FAILED = API : " + url + ", method : " + apiMethod + ", jsonIn :" + jsonIn + ", jsonOut : " + jsonOut);
		return mdlAPIResult;
	    } else {
		mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_00_000);
		model.mdlArrayResult arrayResult = new model.mdlArrayResult();
		arrayResult.items = mobileConfigList;
		mdlAPIResult.output_schema = arrayResult;
		response.setStatus(HttpServletResponse.SC_OK);
		jsonOut = gson.toJson(mdlAPIResult);
		logger.info("SUCCESS. API : " + url + ", method : " + apiMethod + ", jsonIn :" + jsonIn + ", jsonOut : " + jsonOut);
	    }
	} catch (Exception ex) {
	    mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_99_999);
	    response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
	    jsonOut = gson.toJson(mdlAPIResult);
	    logger.error("FAILED. API : " + url + ", method : " + apiMethod + ", jsonIn : " + jsonIn + ", jsonOut : " + jsonOut + ",Exception:" + ex.toString());
	}

	return mdlAPIResult;
    }

    @RequestMapping(value = "/insert-update-mobile-config", method = RequestMethod.POST, consumes = "application/json", headers = "Accept=application/json")
    public @ResponseBody String InsertUpdateMobileConfig(@RequestHeader String Authorization, @RequestHeader String Key, @RequestHeader String Timestamp, @RequestHeader String Signature, @RequestBody model.mdlMobileConfig mdlMobileConfig, HttpServletResponse response) {
	String jsonIn = gson.toJson(mdlMobileConfig);
	String jsonOut = "";
	String apiMethod = "POST";
	String url = "/insert-update-mobile-config";
	model.mdlAPIObjectResult mdlAPIResult = new model.mdlAPIObjectResult();

	try {
	    // check token, key and signature
	    model.mdlErrorSchema mdlAPIStatus = AuthorizationAdapter.CheckAuthorization(Authorization, Key, url, Timestamp, Signature, apiMethod);
	    if (mdlAPIStatus == null) {
		mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_99_999);
		response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		jsonOut = gson.toJson(mdlAPIResult);
		logger.error("FAILED = API : " + url + ", method : " + apiMethod + ", jsonIn :" + jsonIn + ", jsonOut : " + jsonOut);
		return gson.toJson(mdlAPIResult);
	    } else if (!mdlAPIStatus.error_code.equalsIgnoreCase("ERR-00-000")) {
		mdlAPIResult.error_schema = mdlAPIStatus;
		if (mdlAPIStatus.error_code.equalsIgnoreCase("ERR-99-005") || mdlAPIStatus.error_code.equalsIgnoreCase("ERR-99-006")) {
		    // if token invalid or token expired
		    response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
		} else {
		    response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		}
		jsonOut = gson.toJson(mdlAPIStatus);
		logger.error("FAILED = API : " + url + ", method : " + apiMethod + ", jsonIn :" + jsonIn + ", jsonOut : " + jsonOut);
		return gson.toJson(mdlAPIResult);
	    }

	    boolean isValid = true;
	    if (mdlMobileConfig.config_type == null || mdlMobileConfig.config_type.equals("")) {
		mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_99_999);
		isValid = false;
	    } else if (mdlMobileConfig.config_value == null || mdlMobileConfig.config_value.equals("")) {
		mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_99_999);
		isValid = false;
	    }

	    if (!isValid) {
		response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		jsonOut = gson.toJson(mdlAPIResult);
		logger.error("FAILED = API : " + url + ", method : " + apiMethod + ", jsonIn :" + jsonIn + ", jsonOut : " + jsonOut);
		return gson.toJson(mdlAPIResult);
	    }

	    boolean isSuccess = UtilityAdapter.InsertUpdateMobileConfig(mdlMobileConfig);
	    if (isSuccess) {
		mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_00_000);
		response.setStatus(HttpServletResponse.SC_OK);
		jsonOut = gson.toJson(mdlAPIResult);
		logger.info("SUCCESS. API : " + url + ", method : " + apiMethod + ", jsonIn :" + jsonIn + ", jsonOut : " + jsonOut);
	    } else {
		mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_33_004); // Register gagal.
		response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		jsonOut = gson.toJson(mdlAPIResult);
		logger.error("FAILED = API : " + url + ", method : " + apiMethod + ", jsonIn :" + jsonIn + ", jsonOut : " + jsonOut);
		return gson.toJson(mdlAPIResult);
	    }

	} catch (Exception ex) {
	    mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_99_999);
	    response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
	    jsonOut = gson.toJson(mdlAPIResult);
	    logger.error("FAILED. API : " + url + ", method : " + apiMethod + ", jsonIn : " + jsonIn + ", jsonOut : " + jsonOut + ",Exception:" + ex.toString());
	}
	return gson.toJson(mdlAPIResult);
    }

    @RequestMapping(value = "/delete-mobile-config", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody String DeleteMobileConfig(@RequestHeader String Authorization, @RequestHeader String Key, @RequestHeader String Timestamp, @RequestHeader String Signature, @RequestParam(value = "id", defaultValue = "") String id, HttpServletResponse response) {
	String jsonIn = id;
	String jsonOut = "";
	String apiMethod = "GET";
	String url = "/delete-mobile-config";
	model.mdlAPIObjectResult mdlAPIResult = new model.mdlAPIObjectResult();

	try {
	    // check token, key and signature
	    model.mdlErrorSchema mdlAPIStatus = AuthorizationAdapter.CheckAuthorization(Authorization, Key, url, Timestamp, Signature, apiMethod);
	    if (mdlAPIStatus == null) {
		mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_99_999);
		response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		jsonOut = gson.toJson(mdlAPIResult);
		logger.error("FAILED = API : " + url + ", method : " + apiMethod + ", jsonIn :" + jsonIn + ", jsonOut : " + jsonOut);
		return gson.toJson(mdlAPIResult);
	    } else if (!mdlAPIStatus.error_code.equalsIgnoreCase("ERR-00-000")) {
		mdlAPIResult.error_schema = mdlAPIStatus;
		if (mdlAPIStatus.error_code.equalsIgnoreCase("ERR-99-005") || mdlAPIStatus.error_code.equalsIgnoreCase("ERR-99-006")) {
		    // if token invalid or token expired
		    response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
		} else {
		    response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		}
		jsonOut = gson.toJson(mdlAPIStatus);
		logger.error("FAILED = API : " + url + ", method : " + apiMethod + ", jsonIn :" + jsonIn + ", jsonOut : " + jsonOut);
		return gson.toJson(mdlAPIResult);
	    }

	    if (id == null || id.equals("")) {
		mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_34_001);
		response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		jsonOut = gson.toJson(mdlAPIResult);
		logger.error("FAILED = API : " + url + ", method : " + apiMethod + ", jsonIn :" + jsonIn + ", jsonOut : " + jsonOut);
		return gson.toJson(mdlAPIResult);
	    }

	    boolean success = UtilityAdapter.DeleteMobileConfig(id);
	    if (!success) {
		mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_99_999);
		response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		jsonOut = gson.toJson(mdlAPIResult);
		logger.error("FAILED = API : " + url + ", method : " + apiMethod + ", jsonIn :" + jsonIn + ", jsonOut : " + jsonOut);
		return gson.toJson(mdlAPIResult);
	    } else {
		mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_00_000);
		response.setStatus(HttpServletResponse.SC_OK);
		jsonOut = gson.toJson(mdlAPIResult);
		logger.info("SUCCESS. API : " + url + ", method : " + apiMethod + ", jsonIn :" + jsonIn + ", jsonOut : " + jsonOut);
	    }
	} catch (Exception ex) {
	    mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_99_999);
	    response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
	    jsonOut = gson.toJson(mdlAPIResult);
	    logger.error("FAILED. API : " + url + ", method : " + apiMethod + ", jsonIn : " + jsonIn + ", jsonOut : " + jsonOut + ",Exception:" + ex.toString());
	}
	return gson.toJson(mdlAPIResult);
    }

    @Scheduled(fixedDelay = 600000)
    public void CheckPendingClaimDocument() {
	String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();
	List<model.mdlClaimDocumentDetails> claimDocumentList = new ArrayList<model.mdlClaimDocumentDetails>();
	CachedRowSet jrs = null;
	try {
	    String sql = "SELECT claim_no, document_directory FROM ms_claim_document WHERE uploaded = 0 ";

	    jrs = QueryExecuteAdapter.QueryExecute(sql, functionName);
	    while (jrs.next()) {
		model.mdlClaimDocumentDetails claimDocument = new model.mdlClaimDocumentDetails();
		claimDocument.claim_no = jrs.getString(1);
		claimDocument.document_directory = jrs.getString(2);
		claimDocumentList.add(claimDocument);
	    }

	    if (claimDocumentList.size() > 0) {
		for (model.mdlClaimDocumentDetails claimDocument : claimDocumentList) {
		    logger.info("STARTED. Upload Claim Document Schedule. claim_no : " + claimDocument.claim_no + ", document_directory : " + claimDocument.document_directory);
		    APIAdapter.HitAPIInsertClaimDocument(claimDocument.claim_no, claimDocument.document_directory);
		}
	    }
	} catch (Exception ex) {
	    logger.error("FAILED. Function : " + functionName + ", Exception:" + ex.toString(), ex);
	}
    }
}
