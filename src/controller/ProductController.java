package controller;

import java.util.List;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.FilenameUtils;
import org.apache.log4j.Logger;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.google.gson.Gson;

import adapter.AuthorizationAdapter;
import adapter.ErrorAdapter;
import adapter.LogAdapter;
import adapter.ProductAdapter;
import helper.DateHelper;
import model.ErrorStatus;

@RestController
@RequestMapping("/product")
public class ProductController {
    String baseURL = "/product";
    static String apiSource = "Web Server";
    final static Logger logger = Logger.getLogger(NewsController.class);
    static Gson gson = new Gson();

    @RequestMapping(value = "/get-product", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody String GetProduct(@RequestHeader String Authorization, @RequestHeader String Key, @RequestHeader String Timestamp, @RequestHeader String Signature, @RequestParam(value = "category", defaultValue = "", required = false) String category, @RequestParam(value = "id", required = false) String id, HttpServletResponse response) {
	String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();
	model.mdlAPIArrayResult mdlAPIResult = new model.mdlAPIArrayResult();
	String jsonIn = "";
	String jsonOut = "";
	String apiMethod = "GET";
	String url = baseURL + "/get-product";

	try {
	    // check token, key and signature
	    model.mdlErrorSchema mdlAPIStatus = AuthorizationAdapter.CheckAuthorizationArray(Authorization, Key, Timestamp, Signature, response, jsonIn, apiMethod, url, mdlAPIResult);
	    if (!mdlAPIStatus.error_code.equals("ERR-00-000")) {
		mdlAPIResult.error_schema = mdlAPIStatus;
		return gson.toJson(mdlAPIResult);
	    }

	    List<model.mdlProductCategory> productCategoryList = ProductAdapter.GetProductCategoryWithProductList();
	    if (productCategoryList == null || productCategoryList.size() == 0) {
		mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_42_001);
		response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		jsonOut = gson.toJson(mdlAPIResult);
		LogAdapter.InsertAPILog(apiSource, "FAILED", url, functionName, jsonIn, jsonOut);
		logger.info("FAILED = API : " + url + ", method : " + apiMethod + ", jsonIn :" + jsonIn + ", jsonOut : " + jsonOut);
		return gson.toJson(mdlAPIResult);
	    } else {
		mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_00_000);
		model.mdlArrayResult arrayResult = new model.mdlArrayResult();
		arrayResult.items = productCategoryList;
		mdlAPIResult.output_schema = arrayResult;
		response.setStatus(HttpServletResponse.SC_OK);
		jsonOut = gson.toJson(mdlAPIResult);
		LogAdapter.InsertAPILog(apiSource, "SUCCESS", url, functionName, jsonIn, jsonOut);
		logger.info("SUCCESS. API : " + url + ", method : " + apiMethod + ", jsonIn :" + jsonIn + ", jsonOut : " + jsonOut);
	    }
	} catch (Exception ex) {
	    mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_99_999);
	    response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
	    jsonOut = gson.toJson(mdlAPIResult);
	    LogAdapter.InsertAPILog(apiSource, "FAILED", url, functionName, jsonIn, jsonOut, ex.toString());
	    logger.error("FAILED. API : " + url + ", method : " + apiMethod + ", jsonIn : " + jsonIn + ", jsonOut : " + jsonOut + ",Exception:" + ex.toString(), ex);
	}
	return gson.toJson(mdlAPIResult);
    }

    @RequestMapping(value = "/get-product-list", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody String GetProductList(@RequestHeader String Authorization, @RequestHeader String Key, @RequestHeader String Timestamp, @RequestHeader String Signature, 
	    @RequestParam(value = "id", required = false) String id, @RequestParam(value = "pageNumber", defaultValue = "1") String pageNumber, 
	    @RequestParam(value = "pageSize", defaultValue = "10") String pageSize, @RequestParam(value = "draw", defaultValue = "0") String draw, 
	    @RequestParam(value = "search", required = false) String search, HttpServletResponse response) {
	String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();
	model.mdlAPIArrayResult mdlAPIResult = new model.mdlAPIArrayResult();
	String jsonIn = "";
	String jsonOut = "";
	String apiMethod = "GET";
	String url = baseURL + "/get-product-list";

	try {
	    // check token, key and signature
	    model.mdlErrorSchema mdlAPIStatus = AuthorizationAdapter.CheckAuthorizationArray(Authorization, Key, Timestamp, Signature, response, jsonIn, apiMethod, url, mdlAPIResult);
	    if (!mdlAPIStatus.error_code.equals("ERR-00-000")) {
		mdlAPIResult.error_schema = mdlAPIStatus;
		return gson.toJson(mdlAPIResult);
	    }

	    List<model.mdlProduct> productList = ProductAdapter.GetProductList(id, pageNumber, pageSize, search);
	    int productListTotal = ProductAdapter.GetProductTotalList(id, search);
	    
	    if (productList == null || productList.size() == 0) {
		mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_43_001);
		response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		jsonOut = gson.toJson(mdlAPIResult);
		LogAdapter.InsertAPILog(apiSource, "FAILED", url, functionName, jsonIn, jsonOut);
		logger.info("FAILED = API : " + url + ", method : " + apiMethod + ", jsonIn :" + jsonIn + ", jsonOut : " + jsonOut);
		return gson.toJson(mdlAPIResult);
	    } else {
		mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_00_000);
		model.mdlArrayResult arrayResult = new model.mdlArrayResult();
		arrayResult.items = productList;
		arrayResult.recordsTotal = productListTotal;
		arrayResult.recordsFiltered = productListTotal;
		arrayResult.draw = Integer.parseInt(draw) + 1;
		mdlAPIResult.output_schema = arrayResult;
		response.setStatus(HttpServletResponse.SC_OK);
		jsonOut = gson.toJson(mdlAPIResult);
		LogAdapter.InsertAPILog(apiSource, "SUCCESS", url, functionName, jsonIn, jsonOut);
		logger.info("SUCCESS. API : " + url + ", method : " + apiMethod + ", jsonIn :" + jsonIn + ", jsonOut : " + jsonOut);
	    }
	} catch (Exception ex) {
	    mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_99_999);
	    response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
	    jsonOut = gson.toJson(mdlAPIResult);
	    LogAdapter.InsertAPILog(apiSource, "FAILED", url, functionName, jsonIn, jsonOut, ex.toString());
	    logger.error("FAILED. API : " + url + ", method : " + apiMethod + ", jsonIn : " + jsonIn + ", jsonOut : " + jsonOut + ",Exception:" + ex.toString(), ex);
	}
	return gson.toJson(mdlAPIResult);
    }

    @RequestMapping(value = "/insert-update-product", method = RequestMethod.POST, consumes = "multipart/form-data")
    public @ResponseBody String InsertUpdateProduct(@RequestHeader String Authorization, @RequestHeader String Key, @RequestHeader String Timestamp, @RequestHeader String Signature, @RequestParam(name = "file", required = false) final MultipartFile image, @RequestParam("json") final String json, HttpServletResponse response) {
	String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();
	model.mdlProduct mdlProduct = new Gson().fromJson(json, model.mdlProduct.class);
	String jsonIn = gson.toJson(mdlProduct);
	String jsonOut = "";
	String apiMethod = "POST";
	String url = baseURL + "/insert-update-product-category";
	model.mdlAPIObjectResult mdlAPIResult = new model.mdlAPIObjectResult();

	try {
	    // check token, key and signature
	    model.mdlErrorSchema mdlAPIStatus = AuthorizationAdapter.CheckAuthorization(Authorization, Key, Timestamp, Signature, response, jsonIn, apiMethod, url, mdlAPIResult);
	    if (!mdlAPIStatus.error_code.equals("ERR-00-000")) {
		mdlAPIResult.error_schema = mdlAPIStatus;
		return gson.toJson(mdlAPIResult);
	    }

	    boolean isValid = true;
	    if (mdlProduct.product_category_id == null || mdlProduct.product_category_id.equals("")) {
		mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_44_002); // product category tidak valid
		isValid = false;
	    } else if (mdlProduct.product_title == null || mdlProduct.product_title.equals("") || mdlProduct.product_title_english == null || mdlProduct.product_title_english.equals("")) {
		mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_44_003); // product title tidak valid
		isValid = false;
	    } else if (mdlProduct.product_desc == null || mdlProduct.product_desc.equals("") || mdlProduct.product_desc_english == null || mdlProduct.product_desc_english.equals("")) {
		mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_44_004); // product description tidak valid
		isValid = false;
	    } else if (mdlProduct.product_content == null || mdlProduct.product_content.equals("") || mdlProduct.product_content_english == null || mdlProduct.product_content_english.equals("")) {
		mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_44_005); // product content tidak valid
		isValid = false;
	    }

	    if (image != null) {
		String fileName = image.getOriginalFilename();
		String ext = FilenameUtils.getExtension(fileName);
		if (!ext.isEmpty() && (ext.equals("jpg") || ext.equals("jpeg") || ext.equals("png"))) {
		    try {
			// If extension is JPEG or JPG, make it default to JPG
			if (ext.equals("jpg") || ext.equals("jpeg")) {
			    ext = "jpg";
			}

			StringBuilder sb = new StringBuilder();
			String paramFilePath = "C://Program Files/Apache Software Foundation/Tomcat 8.0/webapps/Images/bnilife";
			String directory = sb.append(paramFilePath).append("/product/product-details/").toString();
			String news_time_string = "PRODUCT_" + DateHelper.GetDateTimeNowCustomFormat("yyyyMMddHHmmssSSS");
			String uploadedFileLocation = sb.append("/").append(news_time_string).append(".").append(ext).toString();

			helper.FileHelper.writeToFile(image.getInputStream(), directory, uploadedFileLocation);
			Context context = (Context) new InitialContext().lookup("java:comp/env");
			String urlWebServer = (String) context.lookup("url_web_server_for_upload");

			mdlProduct.product_image = new StringBuilder().append(urlWebServer).append("bnilife/product/product-details/").append(news_time_string).append(".").append(ext).toString();

		    } catch (Exception e) {
			LogAdapter.InsertAPILog(apiSource, "FAILED Upload Image", url, functionName, jsonIn, jsonOut);
			logger.error("FAILED Upload Image = API : " + url + ", method : " + apiMethod + ", jsonIn :" + jsonIn + ", jsonOut : " + jsonOut, e);
			isValid = false;
		    }
		} else {
		    isValid = false;
		}
	    }

	    if (!isValid) {
		response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		jsonOut = gson.toJson(mdlAPIResult);
		LogAdapter.InsertAPILog(apiSource, "FAILED", url, functionName, jsonIn, jsonOut);
		logger.error("FAILED = API : " + url + ", method : " + apiMethod + ", jsonIn :" + jsonIn + ", jsonOut : " + jsonOut);
		return gson.toJson(mdlAPIResult);
	    }

	    boolean isSuccess = ProductAdapter.InsertUpdateProduct(mdlProduct);
	    if (isSuccess) {
		mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_00_000);
		response.setStatus(HttpServletResponse.SC_OK);
		jsonOut = gson.toJson(mdlAPIResult);
		LogAdapter.InsertAPILog(apiSource, "SUCCESS", url, functionName, jsonIn, jsonOut);
		logger.info("SUCCESS. API : " + url + ", method : " + apiMethod + ", jsonIn :" + jsonIn + ", jsonOut : " + jsonOut);
	    } else {
		mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_44_006); // insert update product gagal.
		response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		jsonOut = gson.toJson(mdlAPIResult);
		LogAdapter.InsertAPILog(apiSource, "FAILED", url, functionName, jsonIn, jsonOut);
		logger.error("FAILED = API : " + url + ", method : " + apiMethod + ", jsonIn :" + jsonIn + ", jsonOut : " + jsonOut);
		return gson.toJson(mdlAPIResult);
	    }

	} catch (Exception ex) {
	    mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_99_999);
	    response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
	    jsonOut = gson.toJson(mdlAPIResult);
	    LogAdapter.InsertAPILog(apiSource, "FAILED", url, functionName, jsonIn, jsonOut, ex.toString());
	    logger.error("FAILED. API : " + url + ", method : " + apiMethod + ", jsonIn : " + jsonIn + ", jsonOut : " + jsonOut + ",Exception:" + ex.toString(), ex);
	}
	return gson.toJson(mdlAPIResult);
    }

    @RequestMapping(value = "/delete-product", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody String DeleteProduct(@RequestHeader String Authorization, @RequestHeader String Key, @RequestHeader String Timestamp, @RequestHeader String Signature, @RequestParam(value = "id", defaultValue = "") String id, HttpServletResponse response) {
	String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();
	String jsonIn = id;
	String jsonOut = "";
	String apiMethod = "GET";
	String url = baseURL + "/delete-product";
	model.mdlAPIObjectResult mdlAPIResult = new model.mdlAPIObjectResult();

	try {
	    // check token, key and signature
	    model.mdlErrorSchema mdlAPIStatus = AuthorizationAdapter.CheckAuthorization(Authorization, Key, Timestamp, Signature, response, jsonIn, apiMethod, url, mdlAPIResult);
	    if (!mdlAPIStatus.error_code.equals("ERR-00-000")) {
		mdlAPIResult.error_schema = mdlAPIStatus;
		return gson.toJson(mdlAPIResult);
	    }

	    if (id == null || id.equals("")) {
		mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_45_001);
		response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		jsonOut = gson.toJson(mdlAPIResult);
		LogAdapter.InsertAPILog(apiSource, "FAILED", url, functionName, jsonIn, jsonOut);
		logger.error("FAILED = API : " + url + ", method : " + apiMethod + ", jsonIn :" + jsonIn + ", jsonOut : " + jsonOut);
		return gson.toJson(mdlAPIResult);
	    }

	    boolean success = ProductAdapter.DeleteProduct(id);
	    if (!success) {
		mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_45_002);
		response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		jsonOut = gson.toJson(mdlAPIResult);
		LogAdapter.InsertAPILog(apiSource, "FAILED", url, functionName, jsonIn, jsonOut);
		logger.error("FAILED = API : " + url + ", method : " + apiMethod + ", jsonIn :" + jsonIn + ", jsonOut : " + jsonOut);
		return gson.toJson(mdlAPIResult);
	    } else {
		mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_00_000);
		response.setStatus(HttpServletResponse.SC_OK);
		jsonOut = gson.toJson(mdlAPIResult);
		LogAdapter.InsertAPILog(apiSource, "SUCCESS", url, functionName, jsonIn, jsonOut);
		logger.info("SUCCESS. API : " + url + ", method : " + apiMethod + ", jsonIn :" + jsonIn + ", jsonOut : " + jsonOut);
	    }
	} catch (Exception ex) {
	    mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_99_999);
	    response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
	    jsonOut = gson.toJson(mdlAPIResult);
	    LogAdapter.InsertAPILog(apiSource, "FAILED", url, functionName, jsonIn, jsonOut, ex.toString());
	    logger.error("FAILED. API : " + url + ", method : " + apiMethod + ", jsonIn : " + jsonIn + ", jsonOut : " + jsonOut + ",Exception:" + ex.toString(), ex);
	}
	return gson.toJson(mdlAPIResult);
    }

    @RequestMapping(value = "/get-product-category-list", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody String GetProductCategory(@RequestHeader String Authorization, @RequestHeader String Key, @RequestHeader String Timestamp, 
	    @RequestHeader String Signature, @RequestParam(value = "id", required = false) String id, @RequestParam(value = "pageNumber", defaultValue = "1") String pageNumber, 
	    @RequestParam(value = "pageSize", defaultValue = "10") String pageSize, @RequestParam(value = "draw", defaultValue = "0") String draw, 
	    @RequestParam(value = "search", required = false) String search, HttpServletResponse response) {
	String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();
	model.mdlAPIArrayResult mdlAPIResult = new model.mdlAPIArrayResult();
	String jsonIn = "";
	String jsonOut = "";
	String apiMethod = "GET";
	String url = baseURL + "/get-product-category-list";

	try {
	    // check token, key and signature
	    model.mdlErrorSchema mdlAPIStatus = AuthorizationAdapter.CheckAuthorizationArray(Authorization, Key, Timestamp, Signature, response, jsonIn, apiMethod, url, mdlAPIResult);
	    if (!mdlAPIStatus.error_code.equals("ERR-00-000")) {
		mdlAPIResult.error_schema = mdlAPIStatus;
		return gson.toJson(mdlAPIResult);
	    }

	    List<model.mdlProductCategory> productCategoryList = ProductAdapter.GetProductCategoryList(id, pageNumber, pageSize, search);
	    int productCategoryListTotal = ProductAdapter.GetProductCategoryTotalList(id, search);
	    
	    if (productCategoryList == null || productCategoryList.size() == 0) {
		mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_46_001);
		response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		jsonOut = gson.toJson(mdlAPIResult);
		LogAdapter.InsertAPILog(apiSource, "FAILED", url, functionName, jsonIn, jsonOut);
		logger.info("FAILED = API : " + url + ", method : " + apiMethod + ", jsonIn :" + jsonIn + ", jsonOut : " + jsonOut);
		return gson.toJson(mdlAPIResult);
	    } else {
		mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_00_000);
		model.mdlArrayResult arrayResult = new model.mdlArrayResult();
		arrayResult.items = productCategoryList;
		arrayResult.recordsTotal = productCategoryListTotal;
		arrayResult.recordsFiltered = productCategoryListTotal;
		arrayResult.draw = Integer.parseInt(draw) + 1;
		mdlAPIResult.output_schema = arrayResult;
		response.setStatus(HttpServletResponse.SC_OK);
		jsonOut = gson.toJson(mdlAPIResult);
		LogAdapter.InsertAPILog(apiSource, "SUCCESS", url, functionName, jsonIn, jsonOut);
		logger.info("SUCCESS. API : " + url + ", method : " + apiMethod + ", jsonIn :" + jsonIn + ", jsonOut : " + jsonOut);
	    }
	} catch (Exception ex) {
	    mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_99_999);
	    response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
	    jsonOut = gson.toJson(mdlAPIResult);
	    LogAdapter.InsertAPILog(apiSource, "FAILED", url, functionName, jsonIn, jsonOut, ex.toString());
	    logger.error("FAILED. API : " + url + ", method : " + apiMethod + ", jsonIn : " + jsonIn + ", jsonOut : " + jsonOut + ",Exception:" + ex.toString());
	}
	return gson.toJson(mdlAPIResult);
    }

    @RequestMapping(value = "/insert-update-product-category", method = RequestMethod.POST, consumes = "multipart/form-data")
    public @ResponseBody String InsertUpdateProductCategory(@RequestHeader String Authorization, @RequestHeader String Key, @RequestHeader String Timestamp, @RequestHeader String Signature, @RequestParam(name = "file", required = false) final MultipartFile image, @RequestParam("json") final String json, HttpServletResponse response) {
	String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();
	model.mdlProductCategory mdlProductCategory = new Gson().fromJson(json, model.mdlProductCategory.class);
	String jsonIn = gson.toJson(mdlProductCategory);
	String jsonOut = "";
	String apiMethod = "POST";
	String url = baseURL + "/insert-update-product-category";
	model.mdlAPIObjectResult mdlAPIResult = new model.mdlAPIObjectResult();

	try {
	    // check token, key and signature
	    model.mdlErrorSchema mdlAPIStatus = AuthorizationAdapter.CheckAuthorization(Authorization, Key, Timestamp, Signature, response, jsonIn, apiMethod, url, mdlAPIResult);
	    if (!mdlAPIStatus.error_code.equals("ERR-00-000")) {
		mdlAPIResult.error_schema = mdlAPIStatus;
		return gson.toJson(mdlAPIResult);
	    }

	    boolean isValid = true;
	    if (mdlProductCategory.product_category_name == null || mdlProductCategory.product_category_name.equals("")) {
		mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_47_002); // product category name tidak valid
		isValid = false;
	    } else if (mdlProductCategory.product_category_name_english == null || mdlProductCategory.product_category_name_english.equals("")) {
		mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_47_002); // product category name tidak valid
		isValid = false;
	    }

	    if (image != null) {
			String fileName = image.getOriginalFilename();
			String ext = FilenameUtils.getExtension(fileName);
			if (!ext.isEmpty() && (ext.equals("jpg") || ext.equals("jpeg") || ext.equals("png"))) {
			    try {
				// If extension is JPEG or JPG, make it default to JPG
				if (ext.equals("jpg") || ext.equals("jpeg")) {
				    ext = "jpg";
				}
	
				StringBuilder sb = new StringBuilder();
				String paramFilePath = "C://Program Files/Apache Software Foundation/Tomcat 8.0/webapps/Images/bnilife";
				String directory = sb.append(paramFilePath).append("/product/").toString();
				String news_time_string = "PRODUCTCATEGORY_" + DateHelper.GetDateTimeNowCustomFormat("yyyyMMddHHmmssSSS");
				String uploadedFileLocation = sb.append("/").append(news_time_string).append(".").append(ext).toString();
	
				helper.FileHelper.writeToFile(image.getInputStream(), directory, uploadedFileLocation);
				Context context = (Context) new InitialContext().lookup("java:comp/env");
				String urlWebServer = (String) context.lookup("url_web_server_for_upload");
	
				mdlProductCategory.product_category_icon = new StringBuilder().append(urlWebServer).append("bnilife/product/").append(news_time_string).append(".").append(ext).toString();
	
			    } catch (Exception e) {
				LogAdapter.InsertAPILog(apiSource, "FAILED Upload Image", url, functionName, jsonIn, jsonOut);
				logger.error("FAILED Upload Image = API : " + url + ", method : " + apiMethod + ", jsonIn :" + jsonIn + ", jsonOut : " + jsonOut, e);
				isValid = false;
			    }
			} else {
			    isValid = false;
			}
	    }

	    if (!isValid) {
		response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		jsonOut = gson.toJson(mdlAPIResult);
		LogAdapter.InsertAPILog(apiSource, "FAILED", url, functionName, jsonIn, jsonOut);
		logger.error("FAILED = API : " + url + ", method : " + apiMethod + ", jsonIn :" + jsonIn + ", jsonOut : " + jsonOut);
		return gson.toJson(mdlAPIResult);
	    }

	    boolean isSuccess = ProductAdapter.InsertUpdateProductCategory(mdlProductCategory);
	    if (isSuccess) {
		mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_00_000);
		response.setStatus(HttpServletResponse.SC_OK);
		jsonOut = gson.toJson(mdlAPIResult);
		LogAdapter.InsertAPILog(apiSource, "SUCCESS", url, functionName, jsonIn, jsonOut);
		logger.info("SUCCESS. API : " + url + ", method : " + apiMethod + ", jsonIn :" + jsonIn + ", jsonOut : " + jsonOut);
	    } else {
		mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_47_004); // Register gagal.
		response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		jsonOut = gson.toJson(mdlAPIResult);
		LogAdapter.InsertAPILog(apiSource, "FAILED", url, functionName, jsonIn, jsonOut);
		logger.error("FAILED = API : " + url + ", method : " + apiMethod + ", jsonIn :" + jsonIn + ", jsonOut : " + jsonOut);
		return gson.toJson(mdlAPIResult);
	    }

	} catch (Exception ex) {
	    mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_99_999);
	    response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
	    jsonOut = gson.toJson(mdlAPIResult);
	    LogAdapter.InsertAPILog(apiSource, "FAILED", url, functionName, jsonIn, jsonOut, ex.toString());
	    logger.error("FAILED. API : " + url + ", method : " + apiMethod + ", jsonIn : " + jsonIn + ", jsonOut : " + jsonOut + ",Exception:" + ex.toString(), ex);
	}
	return gson.toJson(mdlAPIResult);
    }

    @RequestMapping(value = "/delete-product-category", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody String DeleteProductCategory(@RequestHeader String Authorization, @RequestHeader String Key, @RequestHeader String Timestamp, @RequestHeader String Signature, @RequestParam(value = "id", defaultValue = "") String id, HttpServletResponse response) {
	String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();
	String jsonIn = id;
	String jsonOut = "";
	String apiMethod = "GET";
	String url = baseURL + "/delete-product-category";
	model.mdlAPIObjectResult mdlAPIResult = new model.mdlAPIObjectResult();

	try {
	    // check token, key and signature
	    model.mdlErrorSchema mdlAPIStatus = AuthorizationAdapter.CheckAuthorization(Authorization, Key, Timestamp, Signature, response, jsonIn, apiMethod, url, mdlAPIResult);
	    if (!mdlAPIStatus.error_code.equals("ERR-00-000")) {
		mdlAPIResult.error_schema = mdlAPIStatus;
		return gson.toJson(mdlAPIResult);
	    }

	    if (id == null || id.equals("")) {
		mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_48_001);
		response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		jsonOut = gson.toJson(mdlAPIResult);
		LogAdapter.InsertAPILog(apiSource, "FAILED", url, functionName, jsonIn, jsonOut);
		logger.error("FAILED = API : " + url + ", method : " + apiMethod + ", jsonIn :" + jsonIn + ", jsonOut : " + jsonOut);
		return gson.toJson(mdlAPIResult);
	    }

	    boolean success = ProductAdapter.DeleteProductCategory(id);
	    if (!success) {
		mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_48_002);
		response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		jsonOut = gson.toJson(mdlAPIResult);
		LogAdapter.InsertAPILog(apiSource, "FAILED", url, functionName, jsonIn, jsonOut);
		logger.error("FAILED = API : " + url + ", method : " + apiMethod + ", jsonIn :" + jsonIn + ", jsonOut : " + jsonOut);
		return gson.toJson(mdlAPIResult);
	    } else {
		mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_00_000);
		response.setStatus(HttpServletResponse.SC_OK);
		jsonOut = gson.toJson(mdlAPIResult);
		LogAdapter.InsertAPILog(apiSource, "SUCCESS", url, functionName, jsonIn, jsonOut);
		logger.info("SUCCESS. API : " + url + ", method : " + apiMethod + ", jsonIn :" + jsonIn + ", jsonOut : " + jsonOut);
	    }
	} catch (Exception ex) {
	    mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_99_999);
	    response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
	    jsonOut = gson.toJson(mdlAPIResult);
	    LogAdapter.InsertAPILog(apiSource, "FAILED", url, functionName, jsonIn, jsonOut, ex.toString());
	    logger.error("FAILED. API : " + url + ", method : " + apiMethod + ", jsonIn : " + jsonIn + ", jsonOut : " + jsonOut + ",Exception:" + ex.toString(), ex);
	}
	return gson.toJson(mdlAPIResult);
    }
}
