package controller;

import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;

import adapter.Base64Adapter;
import adapter.ClientIdAdapter;
import adapter.EncryptAdapter;
import adapter.ErrorAdapter;
import model.ErrorStatus;

@RestController
@RequestMapping("/key")
public class EncryptController {
    final static Logger logger = Logger.getLogger(controller.class);
    static Gson gson = new Gson();
    
    @RequestMapping(value = "/encrypt-aes", method = RequestMethod.POST, consumes = "application/json", headers = "Accept=application/json")
    public @ResponseBody model.mdlAPIObjectResult EncryptAES(@RequestBody model.mdlAES mdlAES, HttpServletResponse response) {
	model.mdlAPIObjectResult mdlAPIResult = new model.mdlAPIObjectResult();
	String encrytedText = "";
	try {
	    encrytedText = EncryptAdapter.encrypt(mdlAES.text, mdlAES.key);
	} catch (Exception ex) {

	}
	mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_00_000);
	mdlAPIResult.output_schema = encrytedText;
	response.setStatus(HttpServletResponse.SC_OK);
	return mdlAPIResult;
    }

    @RequestMapping(value = "/decrypt-aes", method = RequestMethod.POST, consumes = "application/json", headers = "Accept=application/json")
    public @ResponseBody model.mdlAPIObjectResult DecryptAES(@RequestBody model.mdlAES mdlAES, HttpServletResponse response) {
	model.mdlAPIObjectResult mdlAPIResult = new model.mdlAPIObjectResult();
	String decrytedText = "";
	try {
	    decrytedText = EncryptAdapter.decrypt(mdlAES.text, mdlAES.key);
	} catch (Exception ex) {

	}
	mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_00_000);
	mdlAPIResult.output_schema = decrytedText;
	response.setStatus(HttpServletResponse.SC_OK);
	return mdlAPIResult;
    }

    // function to encrypt client id and client secret to token string
    @RequestMapping(value = "/encrypt-token-keys", method = RequestMethod.POST, consumes = "application/json", headers = "Accept=application/json")
    public @ResponseBody model.mdlAPIObjectResult EncryptTokenKeys(@RequestBody model.mdlKey mdlKey, HttpServletResponse response) {
	model.mdlAPIObjectResult mdlAPIResult = new model.mdlAPIObjectResult();
	String tokenKey = mdlKey.ClientID + ";" + mdlKey.ClientSecret;
	String encryptedClientKeys = Base64Adapter.EncryptBase64(tokenKey);
	mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_00_000);
	mdlAPIResult.output_schema = encryptedClientKeys;
	response.setStatus(HttpServletResponse.SC_OK);
	return mdlAPIResult;
    }

    // function to decrypt client id, client secret and api key
    @RequestMapping(value = "/decrypt-keys", method = RequestMethod.POST, consumes = "application/json", headers = "Accept=application/json")
    public @ResponseBody model.mdlAPIObjectResult DecryptKeys(@RequestBody model.mdlKey mdlKey, HttpServletResponse response) {
	model.mdlAPIObjectResult mdlAPIResult = new model.mdlAPIObjectResult();
	model.mdlKey mdlKeyDecrypted = new model.mdlKey();
	String jsonIn = gson.toJson(mdlKey);

	if (mdlKey.ClientID == null || mdlKey.ClientID.trim().equals("")) {
	    mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_99_001);
	    response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
	    return mdlAPIResult;
	}

	try {
	    mdlKeyDecrypted = ClientIdAdapter.DecryptClientKeys(mdlKey);
	    mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_00_000);
	    mdlAPIResult.output_schema = mdlKeyDecrypted;
	    response.setStatus(HttpServletResponse.SC_OK);
	    String jsonOut = gson.toJson(mdlAPIResult);
	    logger.info("SUCCESS. API : create-keys, method : GET, jsonIn :" + jsonIn + ", jsonOut : " + jsonOut);
	} catch (Exception ex) {
	    mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_99_999);
	    response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
	    logger.error("FAILED. API : create-keys, method : GET, jsonIn : " + jsonIn + ", Exception:" + ex.toString());
	}

	return mdlAPIResult;
    }

    // create new client id, client secret and api key fom appName
    @RequestMapping(value = "/create-keys/{appName}", method = RequestMethod.GET)
    public @ResponseBody model.mdlAPIObjectResult CreateKeys(@PathVariable("appName") String appName, HttpServletResponse response) {
	model.mdlAPIObjectResult mdlAPIResult = new model.mdlAPIObjectResult();
	model.mdlKey mdlKey = new model.mdlKey();

	if (appName.trim().equals("")) {
	    mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_99_001);
	    response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
	    return mdlAPIResult;
	}

	try {
	    mdlKey = ClientIdAdapter.CreateClientKeys(appName);
	    mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_00_000);
	    mdlAPIResult.output_schema = mdlKey;
	    response.setStatus(HttpServletResponse.SC_OK);
	    String jsonOut = gson.toJson(mdlAPIResult);
	    logger.info("SUCCESS. API : create-keys, method : GET, AppName :" + appName + ", jsonOut : " + jsonOut);
	} catch (Exception ex) {
	    mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_99_999);
	    response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
	    logger.error("FAILED. API : create-keys, method : GET, AppName : " + appName + ", Exception:" + ex.toString());
	}

	return mdlAPIResult;
    }
}
