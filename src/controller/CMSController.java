package controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;

import adapter.AuthorizationAdapter;
import adapter.CMSAdapter;
import adapter.ClaimAdapter;
import adapter.ErrorAdapter;
import adapter.UtilityAdapter;
import model.ErrorStatus;

@RestController
@RequestMapping("/cms")
public class CMSController {
    final static Logger logger = Logger.getLogger(ClaimController.class);
    static Gson gson = new Gson();
    static String baseURL = "/cms";
    static String apiSource = "Web Server";
    
    @RequestMapping(value = "/get-cms-user-list", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody String GetCMSUserList(@RequestHeader String Authorization, @RequestHeader String Key, @RequestHeader String Timestamp, @RequestHeader String Signature, @RequestParam(value = "pageNumber", defaultValue = "1") String pageNumber, @RequestParam(value = "pageSize", defaultValue = "10") String pageSize, @RequestParam(value = "draw", defaultValue = "0") String draw, @RequestParam(value = "search", required = false) String search, HttpServletResponse response) {
	model.mdlAPIArrayResult mdlAPIResult = new model.mdlAPIArrayResult();
	String jsonIn = "";
	String jsonOut = "";
	String apiMethod = "GET";
	String url = baseURL + "/get-cms-user-list";

	try {
	    // check token, key and signature	    
	    model.mdlErrorSchema mdlAPIStatus = AuthorizationAdapter.CheckAuthorizationArray(Authorization, Key, Timestamp, Signature, response, jsonIn, apiMethod, url, mdlAPIResult);
	    if (!mdlAPIStatus.error_code.equals("ERR-00-000")){
		mdlAPIResult.error_schema = mdlAPIStatus;
		return gson.toJson(mdlAPIResult);
	    }

	    List<model.mdlCMSUser> cmsUserList = CMSAdapter.GetCMSUserList(pageNumber, pageSize, search);
	    int cmsUserListTotal = CMSAdapter.GetCMSUserTotalList(search);
	    
	    if (cmsUserList == null || cmsUserList.size() == 0) {
			mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_99_999);
			
			model.mdlArrayResult arrayResult = new model.mdlArrayResult();
			arrayResult.recordsTotal = 0;
			arrayResult.recordsFiltered = 0;
			arrayResult.draw = Integer.parseInt(draw) + 1;
			arrayResult.data = new ArrayList<model.mdlClaim>() ;
			mdlAPIResult.output_schema = arrayResult;
			
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			jsonOut = gson.toJson(mdlAPIResult);
			logger.info("FAILED = API : " + url + ", method : " + apiMethod + ", jsonIn :" + jsonIn + ", jsonOut : " + jsonOut);
			return gson.toJson(mdlAPIResult);
	    } else {
			mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_00_000);
			model.mdlArrayResult arrayResult = new model.mdlArrayResult();
			arrayResult.recordsTotal = cmsUserListTotal;
			arrayResult.recordsFiltered = cmsUserListTotal;
			arrayResult.draw = Integer.parseInt(draw) + 1;
			arrayResult.data = cmsUserList;
			mdlAPIResult.output_schema = arrayResult;
			response.setStatus(HttpServletResponse.SC_OK);
			jsonOut = gson.toJson(mdlAPIResult);
			logger.info("SUCCESS. API : " + url + ", method : " + apiMethod + ", jsonIn :" + jsonIn + ", jsonOut : " + jsonOut);
	    }
	} catch (Exception ex) {
	    mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_99_999);
	    response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
	    jsonOut = gson.toJson(mdlAPIResult);
	    logger.error("FAILED. API : " + url + ", method : " + apiMethod + ", jsonIn : " + jsonIn + ", jsonOut : " + jsonOut + ",Exception:" + ex.toString());
	}
	return gson.toJson(mdlAPIResult);
    }
    
    @RequestMapping(value = "/insert-update-cms-user", method = RequestMethod.POST, consumes = "application/json", headers = "Accept=application/json")
    public @ResponseBody String InsertUpdateCMSUser(@RequestHeader String Authorization, @RequestHeader String Key, @RequestHeader String Timestamp, @RequestHeader String Signature, @RequestBody model.mdlCMSUser mdlCMSUser, HttpServletResponse response) {
	String jsonIn = gson.toJson(mdlCMSUser);
	String jsonOut = "";
	String apiMethod = "POST";
	String url = baseURL + "/insert-update-cms-user";
	model.mdlAPIObjectResult mdlAPIResult = new model.mdlAPIObjectResult();

	try {
	    // check token, key and signature	    
	    model.mdlErrorSchema mdlAPIStatus = AuthorizationAdapter.CheckAuthorization(Authorization, Key, Timestamp, Signature, response, jsonIn, apiMethod, url, mdlAPIResult);
	    if (!mdlAPIStatus.error_code.equals("ERR-00-000")){
		mdlAPIResult.error_schema = mdlAPIStatus;
		return gson.toJson(mdlAPIResult);
	    }

	    boolean isValid = true;
	    if (mdlCMSUser.username == null || mdlCMSUser.username.equals("")) {
		mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_99_999);
		isValid = false;
	    }

	    if (!isValid) {
		response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		jsonOut = gson.toJson(mdlAPIResult);
		logger.error("FAILED = API : " + url + ", method : " + apiMethod + ", jsonIn :" + jsonIn + ", jsonOut : " + jsonOut);
		return gson.toJson(mdlAPIResult);
	    }

	    boolean isSuccess = CMSAdapter.InsertUpdateCMSUser(mdlCMSUser);
	    if (isSuccess) {
		mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_00_000);
		response.setStatus(HttpServletResponse.SC_OK);
		jsonOut = gson.toJson(mdlAPIResult);
		logger.info("SUCCESS. API : " + url + ", method : " + apiMethod + ", jsonIn :" + jsonIn + ", jsonOut : " + jsonOut);
	    } else {
		mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_99_999);
		response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		jsonOut = gson.toJson(mdlAPIResult);
		logger.error("FAILED = API : " + url + ", method : " + apiMethod + ", jsonIn :" + jsonIn + ", jsonOut : " + jsonOut);
		return gson.toJson(mdlAPIResult);
	    }

	} catch (Exception ex) {
	    mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_99_999);
	    response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
	    jsonOut = gson.toJson(mdlAPIResult);
	    logger.error("FAILED. API : " + url + ", method : " + apiMethod + ", jsonIn : " + jsonIn + ", jsonOut : " + jsonOut + ",Exception:" + ex.toString());
	}
	return gson.toJson(mdlAPIResult);
    }

    @RequestMapping(value = "/delete-cms-user", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody String DeleteCMSUser(@RequestHeader String Authorization, @RequestHeader String Key, @RequestHeader String Timestamp, @RequestHeader String Signature, @RequestParam(value = "username", defaultValue = "") String username, HttpServletResponse response) {
	String jsonIn = username;
	String jsonOut = "";
	String apiMethod = "GET";
	String url = baseURL + "/delete-cms-user";
	model.mdlAPIObjectResult mdlAPIResult = new model.mdlAPIObjectResult();

	try {
	    // check token, key and signature	    
	    model.mdlErrorSchema mdlAPIStatus = AuthorizationAdapter.CheckAuthorization(Authorization, Key, Timestamp, Signature, response, jsonIn, apiMethod, url, mdlAPIResult);
	    if (!mdlAPIStatus.error_code.equals("ERR-00-000")){
		mdlAPIResult.error_schema = mdlAPIStatus;
		return gson.toJson(mdlAPIResult);
	    }

	    if (username == null || username.equals("")) {
		mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_99_999);
		response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		jsonOut = gson.toJson(mdlAPIResult);
		logger.error("FAILED = API : " + url + ", method : " + apiMethod + ", jsonIn :" + jsonIn + ", jsonOut : " + jsonOut);
		return gson.toJson(mdlAPIResult);
	    }

	    boolean success = CMSAdapter.DeleteCMSUser(username);
	    if (!success) {
		mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_99_999);
		response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		jsonOut = gson.toJson(mdlAPIResult);
		logger.error("FAILED = API : " + url + ", method : " + apiMethod + ", jsonIn :" + jsonIn + ", jsonOut : " + jsonOut);
		return gson.toJson(mdlAPIResult);
	    } else {
		mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_00_000);
		response.setStatus(HttpServletResponse.SC_OK);
		jsonOut = gson.toJson(mdlAPIResult);
		logger.info("SUCCESS. API : " + url + ", method : " + apiMethod + ", jsonIn :" + jsonIn + ", jsonOut : " + jsonOut);
	    }
	} catch (Exception ex) {
	    mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_99_999);
	    response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
	    jsonOut = gson.toJson(mdlAPIResult);
	    logger.error("FAILED. API : " + url + ", method : " + apiMethod + ", jsonIn : " + jsonIn + ", jsonOut : " + jsonOut + ",Exception:" + ex.toString());
	}
	return gson.toJson(mdlAPIResult);
    }
    
    @RequestMapping(value = "/get-api-log-list", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody String GetAPILogList(@RequestHeader String Authorization, @RequestHeader String Key, @RequestHeader String Timestamp, @RequestHeader String Signature, @RequestParam(value = "pageNumber", defaultValue = "1") String pageNumber, @RequestParam(value = "pageSize", defaultValue = "10") String pageSize, @RequestParam(value = "draw", defaultValue = "0") String draw, @RequestParam(value = "search", required = false) String search, HttpServletResponse response) {
	model.mdlAPIArrayResult mdlAPIResult = new model.mdlAPIArrayResult();
	String jsonIn = "";
	String jsonOut = "";
	String apiMethod = "GET";
	String url = baseURL + "/get-api-log-list";

	try {
	    // check token, key and signature
	    model.mdlErrorSchema mdlAPIStatus = AuthorizationAdapter.CheckAuthorization(Authorization, Key, url, Timestamp, Signature, apiMethod);
	    if (mdlAPIStatus == null) {
		mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_99_999);
		response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		jsonOut = gson.toJson(mdlAPIResult);
		logger.info("FAILED = API : " + url + ", method : " + apiMethod + ", jsonIn :" + jsonIn + ", jsonOut : " + jsonOut);
		return gson.toJson(mdlAPIResult);
	    } else if (!mdlAPIStatus.error_code.equalsIgnoreCase("ERR-00-000")) {
		mdlAPIResult.error_schema = mdlAPIStatus;
		if (mdlAPIStatus.error_code.equalsIgnoreCase("ERR-99-005") || mdlAPIStatus.error_code.equalsIgnoreCase("ERR-99-006")) {
		    // if token invalid or token expired
		    response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
		} else {
		    response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		}
		jsonOut = gson.toJson(mdlAPIStatus);
		logger.info("FAILED = API : " + url + ", method : " + apiMethod + ", jsonIn :" + jsonIn + ", jsonOut : " + jsonOut);
		return gson.toJson(mdlAPIResult);
	    }

	    List<model.mdlAPILog> apiLogList = CMSAdapter.GetAPILogList(pageNumber, pageSize, search);
	    int apiLogListTotal = CMSAdapter.GetAPILogTotalList(search);
	    
	    if (apiLogList == null || apiLogList.size() == 0) {
			mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_99_999);
			
			model.mdlArrayResult arrayResult = new model.mdlArrayResult();
			arrayResult.recordsTotal = 0;
			arrayResult.recordsFiltered = 0;
			arrayResult.draw = Integer.parseInt(draw) + 1;
			arrayResult.data = new ArrayList<model.mdlClaim>() ;
			mdlAPIResult.output_schema = arrayResult;
			
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			jsonOut = gson.toJson(mdlAPIResult);
			logger.info("FAILED = API : " + url + ", method : " + apiMethod + ", jsonIn :" + jsonIn + ", jsonOut : " + jsonOut);
			return gson.toJson(mdlAPIResult);
	    } else {
			mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_00_000);
			model.mdlArrayResult arrayResult = new model.mdlArrayResult();
			arrayResult.recordsTotal = apiLogListTotal;
			arrayResult.recordsFiltered = apiLogListTotal;
			arrayResult.draw = Integer.parseInt(draw) + 1;
			arrayResult.data = apiLogList;
			mdlAPIResult.output_schema = arrayResult;
			response.setStatus(HttpServletResponse.SC_OK);
			jsonOut = gson.toJson(mdlAPIResult);
			logger.info("SUCCESS. API : " + url + ", method : " + apiMethod + ", jsonIn :" + jsonIn + ", jsonOut : " + jsonOut);
	    }
	} catch (Exception ex) {
	    mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_99_999);
	    response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
	    jsonOut = gson.toJson(mdlAPIResult);
	    logger.error("FAILED. API : " + url + ", method : " + apiMethod + ", jsonIn : " + jsonIn + ", jsonOut : " + jsonOut + ",Exception:" + ex.toString());
	}
	return gson.toJson(mdlAPIResult);
    }
}
